//
//  AdjustmentItem.h
//  CNF App
//
//  Created by Anik on 2017-05-01.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "AdjustmentProduct.h"

@protocol AdjustmentItem @end

@interface AdjustmentItem : JSONModel

@property (strong, nonatomic) AdjustmentProduct * Product;
@property (strong, nonatomic) NSNumber * Quantity;
@property (strong, nonatomic) NSNumber * ValueListPrice;
@property (strong, nonatomic) NSNumber * ValueActivePrice;
@property (strong, nonatomic) NSNumber * ValueCost;
@property (strong, nonatomic) NSNumber * ValueBottleDeposit;
@property (strong, nonatomic) NSNumber * ValueEnvironmentalFee;
@property (strong, nonatomic) NSNumber * ValueActivePriceTax;
@property (strong, nonatomic) NSNumber * ValueActiveCostTax;

-(void)validate;

@end
