//
//  AdjustmentItem.m
//  CNF App
//
//  Created by Anik on 2017-05-01.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "AdjustmentItem.h"

@implementation AdjustmentItem

@synthesize Quantity=_Quantity,  ValueCost=_ValueCost, ValueListPrice=_ValueListPrice, ValueActivePrice=_ValueActivePrice, ValueBottleDeposit=_ValueBottleDeposit, ValueEnvironmentalFee=_ValueEnvironmentalFee, Product=_Product;

-(void)validate{
    if(self.Product==NULL
       || [self.ValueListPrice doubleValue]<[self.ValueCost doubleValue]
       || [self.ValueActivePrice doubleValue]<[self.ValueCost doubleValue]
       || [self.Quantity doubleValue]<=0
       || [self.ValueCost doubleValue]<=0
       )
        @throw [NSException exceptionWithName:@"Exception Occured"
                                       reason:@"Adjustment Product Item Validation Failed"
                                     userInfo:nil];
}


@end
