//
//  AdjustmentProduct.h
//  CNF App
//
//  Created by Anik on 2017-05-01.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol AdjustmentProduct @end

@interface AdjustmentProduct : JSONModel

@property (strong, nonatomic) NSString * ProductId;
@property (strong, nonatomic) NSString * DepartmentId;
@property (strong, nonatomic) NSString * SubDepartmentId;
@property (strong, nonatomic) NSString * CategoryId;
@property (strong, nonatomic) NSNumber * ListPrice;
@property (strong, nonatomic) NSNumber * ActivePrice;
@property (strong, nonatomic) NSNumber * Cost;
@property (strong, nonatomic) NSNumber * BottleDeposit;
@property (strong, nonatomic) NSNumber * EnvironmentalFee;
@property (strong, nonatomic) NSNumber * Taxable;
@property (strong, nonatomic) NSNumber * TaxRate;

-(void)validate;

@end
