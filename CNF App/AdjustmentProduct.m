//
//  AdjustmentProduct.m
//  CNF App
//
//  Created by Anik on 2017-05-01.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "AdjustmentProduct.h"

@implementation AdjustmentProduct


@synthesize ProductId=_ProductId, DepartmentId=_DepartmentId,CategoryId=_CategoryId, ListPrice=_ListPrice, ActivePrice=_ActivePrice, Cost=_Cost, BottleDeposit=_BottleDeposit, EnvironmentalFee=_EnvironmentalFee, Taxable=_Taxable, TaxRate=_TaxRate;

-(void)validate{
    if(self.ProductId==NULL
       || [self.ListPrice doubleValue]<[self.Cost doubleValue]
       || [self.ActivePrice doubleValue]<[self.Cost doubleValue]
       )
        @throw [NSException exceptionWithName:@"Exception Occured"
                                       reason:@"Adjustment Product Validation Failed"
                                     userInfo:nil];
}



@end
