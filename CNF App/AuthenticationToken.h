//
//  AuthenticationToken.h
//  CNF App
//
//  Created by Anik on 2015-03-31.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface AuthenticationToken : JSONModel

@property (nonatomic, retain) NSString * TokenValue;
@property (nonatomic, retain) NSDate * Expiration;


@end
