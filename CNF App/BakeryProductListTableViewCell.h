//
//  BakeryProductListTableViewCell.h
//  CNF App
//
//  Created by Anik on 2018-02-22.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BakeryProductListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *productUpcLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *s1Label;
@property (weak, nonatomic) IBOutlet UILabel *s2Label;
@property (weak, nonatomic) IBOutlet UILabel *s5Label;

@end
