//
//  BakeryProductListTableViewCell.m
//  CNF App
//
//  Created by Anik on 2018-02-22.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import "BakeryProductListTableViewCell.h"

@implementation BakeryProductListTableViewCell

@synthesize productUpcLabel=_productUpcLabel, productNameLabel=_productNameLabel, s1Label=_s1Label, s2Label=_s2Label, s5Label=_s5Label, totalQuantityLabel=_totalQuantityLabel, sectionLabel=_sectionLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
