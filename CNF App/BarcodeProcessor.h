//
//  BarcodeProcessor.h
//  CNF App
//
//  Created by Anik on 2016-08-10.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Products.h"

@interface BarcodeProcessor : NSObject

-(NSMutableDictionary*)processBarcode:(NSString*)barcode;

-(NSMutableDictionary*)processBarcodeWithVendor:(NSString*)barcode vendor:(NSString*)vendor;

-(Products *)processBarcodeForProductId:(NSString*)barcode;
-(NSMutableDictionary *)processBarcodeFor4DigitPLU:(NSString*)barcode;
@end
