//
//  BarcodeProcessor.m
//  CNF App
//
//  Created by Anik on 2016-08-10.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "BarcodeProcessor.h"
#import "Products.h"


@implementation BarcodeProcessor

-(NSMutableDictionary*)processBarcode:(NSString*)barcode{
    
    NSMutableDictionary * returnDic = [[NSMutableDictionary alloc] init];
    [returnDic setValue:NULL forKey:@"message"];
    
    NSMutableArray * productArray = [[NSMutableArray alloc]init];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    //start looking for productId
    
    NSPredicate *productIdPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", [barcode stringByAppendingString:@";"]];
    NSArray *fullResultBasedonProductId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:productIdPred];
    
    if(fullResultBasedonProductId.count>0){
        
        //product found based on productID
        
        for(NSObject * product in fullResultBasedonProductId){
            
            [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];
            
            databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];
            
            NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
            
            NSError * error;
            Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];
            
            if(error){
                [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                [returnDic setValue:NULL forKey:@"productArray"];
            }else{
                [productArray addObject:productFromJson];
            }
            
//            [productArray addObject:productFromJson];
            
        }
        [file closeFile];

    }else{
        
        //check for valid barcode
        
        if([self validateBarcode:barcode]){
            
            //barcode Valid..
            
            NSPredicate *productUpcPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", [barcode substringToIndex:[barcode length]-1]];
            NSArray *fullResultBasedonProductUpc = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:productUpcPred];
            
            if(fullResultBasedonProductUpc.count>0){
                
                //product found with UPC
                
                for(NSObject * product in fullResultBasedonProductUpc){
                    
                    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];
                    
                    databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];
                    
                    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
                    
                    NSError * error;
                    Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];
                    
                    if(error){
                        [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                        [returnDic setValue:NULL forKey:@"productArray"];
                    }else{
                        [productArray addObject:productFromJson];
                    }
                }
                [file closeFile];
                
            }else{
                
                //product not found with either ID or UPC, try n check for repack product
                
                NSString *theCharacter = [NSString stringWithFormat:@"%c", [barcode characterAtIndex:0]];
                if([theCharacter isEqualToString:@"2"]){
                    
                    //repack product
                    
                    NSString * modifiedBardcode= [barcode substringToIndex:[barcode length]-6];
                    
                    NSPredicate *repackProdPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", modifiedBardcode];
                    NSArray *fullResultBasedonRepackPred = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:repackProdPred];
                    
                    if(fullResultBasedonRepackPred.count>0){
                        
                        for(NSObject * product in fullResultBasedonRepackPred){
                            
                            [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];
                            
                            databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];
                            
                            NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
                            
                            NSError * error;
                            Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];
                            
                            if(error){
                                [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                                [returnDic setValue:NULL forKey:@"productArray"];
                            }else{
                                [productArray addObject:productFromJson];
                            }
                        }
                        [file closeFile];
                    }else {
                        //product not found from ID, UPC, repack
                        
                        NSLog(@"barcode not found..");
                        productArray=NULL;
                        [returnDic setValue:@"Product Not Found" forKey:@"errorMsg"];
                    }
                }else{
                    
                    //is not repack product, no question of looking
                    
                    NSLog(@"barcode not found..");
                    productArray=NULL;
                    
                    [returnDic setValue:@"Product Not Found" forKey:@"errorMsg"];
                    [returnDic setValue:productArray forKey:@"productArray"];
                    
                }
            }
        }else{
            //invalid barcode, send error...
            
            [returnDic setValue:@"Invalid Barcode" forKey:@"errorMsg"];
            [returnDic setValue:NULL forKey:@"productArray"];
        }
    }
    
    [returnDic setValue:productArray forKey:@"productArray"];
    return returnDic;

}


-(NSMutableDictionary*)processBarcodeWithVendor:(NSString*)barcode vendor:(NSString*)vendor{
    
    NSMutableDictionary * returnDic = [[NSMutableDictionary alloc] init];
    [returnDic setValue:NULL forKey:@"message"];
    
    NSMutableArray * productArray = [[NSMutableArray alloc]init];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    //start looking for productId
    
    NSPredicate *productIdPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", [barcode stringByAppendingString:@";"]];
    NSArray *fullResultBasedonProductId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:productIdPred];
    
    if(fullResultBasedonProductId.count>0){
        
        //product found based on productID
        
        for(NSObject * product in fullResultBasedonProductId){
            
            [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];
            
            databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];
            
            NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
            
            NSError * error;
            Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];
            
            if(error){
                [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                [returnDic setValue:NULL forKey:@"productArray"];
            }else{
                [productArray addObject:productFromJson];
            }
            
//            [productArray addObject:productFromJson];
            
        }
        [file closeFile];
        
    }else{
        
        //check if valid barcode
        
        if([self validateBarcode:barcode]){
            
            //product not found based on productID, check for productUPC
            
            NSPredicate *productUpcPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", [barcode substringToIndex:[barcode length]-1]];
            NSArray *fullResultBasedonProductUpc = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:productUpcPred];
            
            if(fullResultBasedonProductUpc.count>0){
                
                //product found with UPC
                
                for(NSObject * product in fullResultBasedonProductUpc){
                    
                    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];
                    
                    databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];
                    
                    
                    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
                    
                    NSError * error;
                    Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];
                    
                    if(error){
                        [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                        [returnDic setValue:NULL forKey:@"productArray"];
                    }else{
                        [productArray addObject:productFromJson];
                    }
                    
//                    [productArray addObject:productFromJson];
                    
                }
                [file closeFile];
                
            }else{
                
                //product not found with either ID or UPC, try n check for repack product
                
                NSString *theCharacter = [NSString stringWithFormat:@"%c", [barcode characterAtIndex:0]];
                if([theCharacter isEqualToString:@"2"]){
                    
                    //repack product
                    
                    NSString * modifiedBardcode= [barcode substringToIndex:[barcode length]-6];
                    
                    NSPredicate *repackProdPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", modifiedBardcode];
                    NSArray *fullResultBasedonRepackPred = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:repackProdPred];
                    
                    if(fullResultBasedonRepackPred.count>0){
                        
                        for(NSObject * product in fullResultBasedonRepackPred){
                            
                            [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];
                            
                            databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];
                            
                            NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
                            
                            NSError * error;
                            Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];
                            
                            
                            if(error){
                                [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                                [returnDic setValue:NULL forKey:@"productArray"];
                            }else{
                                [productArray addObject:productFromJson];
                            }
                            
//                            [productArray addObject:productFromJson];
                            
                        }
                        [file closeFile];
                        
                    }else {
                        //product not found from ID, UPC, repack
                        NSLog(@"barcode not found..");
                        productArray=NULL;
                    }
                }else{
                    //is not repack product, no point of looking
                    NSLog(@"barcode not found..");
                    productArray=NULL;
                }
            }
        }else{
            //invalid barcode, send error...
            
            [returnDic setValue:@"Invalid Barcode" forKey:@"errorMsg"];
            [returnDic setValue:NULL forKey:@"productArray"];
        }
    }
    
    if(!productArray){
        [returnDic setValue:productArray forKey:@"productArray"];
        return returnDic;
    }else{
        //need to filter products based on vendor
        
        NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"ANY %K.%K = %@", @"Vendors",@"VendorId",vendor];
        
         NSArray * filteredArray = [productArray filteredArrayUsingPredicate:vendorPredicate];
        
        [returnDic setValue:filteredArray forKey:@"productArray"];
        return returnDic;
    }
   
}


-(Products *)processBarcodeForProductId:(NSString*)barcode{

    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];

    //start looking for productId

    NSPredicate *productIdPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", [barcode stringByAppendingString:@";"]];
    NSArray *fullResultBasedonProductId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:productIdPred];

    if(fullResultBasedonProductId.count==1){
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[fullResultBasedonProductId.firstObject valueForKey:@"O"] intValue]];

        databuffer = [file readDataOfLength: [[fullResultBasedonProductId.firstObject valueForKey:@"L"] intValue]];

        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

        NSError * error;
        Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];

        if(error){
            return NULL;
        }else{
            return productFromJson;
        }
    }else return NULL;


}


-(NSMutableDictionary*)processBarcodeFor4DigitPLU:(NSString*)barcode{

    NSMutableDictionary * returnDic = [[NSMutableDictionary alloc] init];
    [returnDic setValue:NULL forKey:@"message"];

    NSMutableArray * productArray = [[NSMutableArray alloc]init];

    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];

    //if 3/4/5 digit barcode came up, only look for PLU..Else go for regular barcode processor

    if(barcode.length==3 || barcode.length==4 || barcode.length==5){
        NSPredicate *pluPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", barcode];
        NSArray *fullResultBasedonPlu = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:pluPred];
        for(NSDictionary * allCodes in fullResultBasedonPlu){
            if([[allCodes valueForKey:@"K"]componentsSeparatedByString:@";"].count>0){
                for(NSString * PluCode in [[allCodes valueForKey:@"K"]componentsSeparatedByString:@";"]){
                    if([PluCode isEqualToString:barcode]){
                        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[allCodes valueForKey:@"O"] intValue]];

                        databuffer = [file readDataOfLength: [[allCodes valueForKey:@"L"] intValue]];

                        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

                        NSError * error;
                        Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];

                        if(error){
                            [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                            [returnDic setValue:NULL forKey:@"productArray"];
                        }else{
                            [productArray addObject:productFromJson];
                        }
                    }
                }
            }
        }
    }else{
        //start looking for productId
        NSPredicate *productIdPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", [barcode stringByAppendingString:@";"]];
        NSArray *fullResultBasedonProductId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:productIdPred];

        if(fullResultBasedonProductId.count>0){

            //product found based on productID

            for(NSObject * product in fullResultBasedonProductId){

                [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];

                databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];

                NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

                NSError * error;
                Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];

                if(error){
                    [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                    [returnDic setValue:NULL forKey:@"productArray"];
                }else{
                    [productArray addObject:productFromJson];
                }

                //            [productArray addObject:productFromJson];

            }
            [file closeFile];

        }else{

            //check for valid barcode

            if([self validateBarcode:barcode]){

                //barcode Valid..

                NSPredicate *productUpcPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", [barcode substringToIndex:[barcode length]-1]];
                NSArray *fullResultBasedonProductUpc = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:productUpcPred];

                if(fullResultBasedonProductUpc.count>0){

                    //product found with UPC

                    for(NSObject * product in fullResultBasedonProductUpc){

                        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];

                        databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];

                        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

                        NSError * error;
                        Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];

                        if(error){
                            [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                            [returnDic setValue:NULL forKey:@"productArray"];
                        }else{
                            [productArray addObject:productFromJson];
                        }
                    }
                    [file closeFile];

                }else{

                    //product not found with either ID or UPC, try n check for repack product

                    NSString *theCharacter = [NSString stringWithFormat:@"%c", [barcode characterAtIndex:0]];
                    if([theCharacter isEqualToString:@"2"]){

                        //repack product

                        NSString * modifiedBardcode= [barcode substringToIndex:[barcode length]-6];

                        NSPredicate *repackProdPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", modifiedBardcode];
                        NSArray *fullResultBasedonRepackPred = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:repackProdPred];

                        if(fullResultBasedonRepackPred.count>0){

                            for(NSObject * product in fullResultBasedonRepackPred){

                                [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[product valueForKey:@"O"] intValue]];

                                databuffer = [file readDataOfLength: [[product valueForKey:@"L"] intValue]];

                                NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

                                NSError * error;
                                Products * productFromJson = [[Products alloc]initWithString:jsonStr error:&error];

                                if(error){
                                    [returnDic setValue:@"JSON serialize error" forKey:@"errorMsg"];
                                    [returnDic setValue:NULL forKey:@"productArray"];
                                }else{
                                    [productArray addObject:productFromJson];
                                }
                            }
                            [file closeFile];
                        }else {
                            //product not found from ID, UPC, repack

                            NSLog(@"barcode not found..");
                            productArray=NULL;
                            [returnDic setValue:@"Product Not Found" forKey:@"errorMsg"];
                        }
                    }else{

                        //is not repack product, no question of looking

                        NSLog(@"barcode not found..");
                        productArray=NULL;

                        [returnDic setValue:@"Product Not Found" forKey:@"errorMsg"];
                        [returnDic setValue:productArray forKey:@"productArray"];

                    }
                }
            }else{
                //invalid barcode, send error...

                [returnDic setValue:@"Invalid Barcode" forKey:@"errorMsg"];
                [returnDic setValue:NULL forKey:@"productArray"];
            }
        }
    }

    [returnDic setValue:productArray forKey:@"productArray"];
    return returnDic;

}


-(BOOL)validateBarcode:(NSString*) barcode{

    //check if barcode is only numbers
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([barcode rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        //barcode consists of only numbers
        
        switch (barcode.length) {
            case 8:
                barcode=[@"000000"stringByAppendingString:barcode];
                break;
                
            case 12:
                barcode=[@"00"stringByAppendingString:barcode];
                break;
                
            case 13:
                barcode=[@"0"stringByAppendingString:barcode];
                break;
                
            case 14:
                //
                break;
                
            default:
                return FALSE;
        }
        
        // calculate check digit
        
        NSInteger a[13];
        a[0]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:0]] intValue] *3;
        a[1]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:1]] intValue];
        a[2]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:2]] intValue]*3;
        a[3]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:3]] intValue];
        a[4]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:4]] intValue]*3;
        a[5]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:5]] intValue];
        a[6]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:6]] intValue]*3;
        a[7]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:7]] intValue];
        a[8]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:8]] intValue]*3;
        a[9]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:9]] intValue];
        a[10]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:10]] intValue]*3;
        a[11]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:11]] intValue];
        a[12]=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:12]] intValue]*3;

        int sum=(int)a[0]+(int)a[1]+(int)a[2]+(int)a[3]+(int)a[4]+(int)a[5]+(int)a[6]+(int)a[7]+(int)a[8]+(int)a[9]+(int)a[10]+(int)a[11]+(int)a[12];
        int check=(10-(sum%10))%10;
        
        int last=[[NSString stringWithFormat:@"%c",[barcode characterAtIndex:13]] intValue];
        return check==last;
        
    }else{
        //barcode not valid number
        return FALSE;
    }
    
}

@end
