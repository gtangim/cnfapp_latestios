//
//  BaseAdjustmentRequest.h
//  CNF App
//
//  Created by Anik on 2017-05-01.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "AdjustmentItem.h"

@protocol BaseAdjustmentRequest @end

@interface BaseAdjustmentRequest : JSONModel

@property (strong, nonatomic) NSString * UserId;
@property (strong, nonatomic) NSString * HoldingLocationId;
@property (strong, nonatomic) NSString * ConsumingLocationId;
@property (strong, nonatomic) NSDate * CreatedDateTime;
@property (strong, nonatomic) NSString * Comment;
@property (strong, nonatomic) NSNumber * ReversedEntry;
@property (strong, nonatomic) NSNumber * IsProductionPurpose;
@property (strong, nonatomic) NSArray<AdjustmentItem> * Items;

@end
