//
//  CNFAdjustmentLocationTableTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-04-12.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFAdjustmentLocationTableTableViewController : UITableViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *consumingLocationPicker;
- (IBAction)goToProducts:(id)sender;

@end
