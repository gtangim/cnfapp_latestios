//
//  CNFAdjustmentLocationTableTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-04-12.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFAdjustmentLocationTableTableViewController.h"

@interface CNFAdjustmentLocationTableTableViewController ()
@property (nonatomic, strong) NSMutableArray * locationArray;
@end

@implementation CNFAdjustmentLocationTableTableViewController

@synthesize consumingLocationPicker=_consumingLocationPicker, locationArray=_locationArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    
    self.consumingLocationPicker.dataSource=self;
    self.consumingLocationPicker.delegate=self;
    
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"REVERSE_ADJUSTMENT"]){
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor redColor]};
    }
    
    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"ConsumerLocationList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    NSError * serializerError;
    
    NSArray * locations = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];
    
    if(serializerError){
        NSLog(@"Serializer error, data file not downloaded properly..");
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }else{
        self.locationArray =[locations mutableCopy];
        
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]length]==0){
            [self.consumingLocationPicker selectRow:0 inComponent:0 animated:YES];
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
        }else{
            [self.consumingLocationPicker selectRow:[[self.locationArray valueForKey:@"StoreId"] indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]] inComponent:0 animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - pickerview functions
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component{
    return[self.locationArray count];
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *retval = (id)view;
    if (!retval) {
        retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
    }
    retval.text = [[self.locationArray objectAtIndex:row] valueForKey:@"StoreName"];
    retval.font = [UIFont systemFontOfSize:17];
    retval.textAlignment=NSTextAlignmentCenter;
    return retval;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
    
    if(pickerView==self.consumingLocationPicker){
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
}

#pragma mark - alert notification functions

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

- (IBAction)goToProducts:(id)sender {
    NSMutableDictionary * adjustmentObject = [[NSMutableDictionary alloc]init];
    [adjustmentObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"adjustmentObject"] valueForKey:@"reasonType"] forKey:@"reasonType"];
    [adjustmentObject setValue:[[self.locationArray objectAtIndex:[self.consumingLocationPicker selectedRowInComponent:0]] valueForKey:@"StoreId"] forKey:@"consumingLocationId"];

    [[NSUserDefaults standardUserDefaults] setObject:adjustmentObject forKey:@"adjustmentObject"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSegueWithIdentifier:@"locationToAdjustmentProductSegue" sender:self];
    
}
@end
