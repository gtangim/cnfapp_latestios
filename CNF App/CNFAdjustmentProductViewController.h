//
//  CNFAdjustmentProductViewController.h
//  CNF App
//
//  Created by Anik on 2017-04-10.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFAdjustmentProductViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, NSFetchedResultsControllerDelegate, ZBarReaderDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UITableView *productListTable;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *enterUpcText;

@property (weak, nonatomic) IBOutlet UIButton *scanButton;

- (IBAction)submitAudit:(id)sender;
- (IBAction)goBack:(id)sender;
- (IBAction)scanProduct:(id)sender;


@end
