//
//  CNFAdjustmentProductViewController.m
//  CNF App
//
//  Created by Anik on 2017-04-10.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFAdjustmentProductViewController.h"
#import "CNFAppEnums.h"
#import "Products.h"
#import "CNFGetDocumentId.h"
#import "ImageCompression.h"
#import "BarcodeProcessor.h"
#import "AdjustmentProduct.h"
#import "AdjustmentItem.h"
#import "BaseAdjustmentRequest.h"

@interface CNFAdjustmentProductViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic) BOOL shouldScanProduct;
@property (nonatomic, strong) NSMutableArray * auditedProducts;
@property (nonatomic, strong) Products * scannedProduct;
@property (nonatomic, strong) NSString * selectedProductId;
@property (nonatomic, strong) NSString * documentId;
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSDate * endTime;
@property (nonatomic, strong) NSString * notes;
@end

@implementation CNFAdjustmentProductViewController{
    NSManagedObjectContext * context;
    UIAlertController * statusAlert;
}

@synthesize userNameLabel=_userNameLabel,locationLabel=_locationLabel, productListTable=_productListTable,scanner=_scanner, scannerAvailable=_scannerAvailable, foundBarcode=_foundBarcode, shouldScanProduct=_shouldScanProduct, scanButton=_scanButton, context=_context, auditedProducts=_auditedProducts, scannedProduct=_scannedProduct, selectedProductId=_selectedProductId, documentId=_documentId, startTime=_startTime, endTime=_endTime, notes=_notes;

-(NSMutableArray *)auditedProducts{
    if(!_auditedProducts)_auditedProducts=[[NSMutableArray alloc]init];
    return _auditedProducts;
}

- (void)viewDidLoad {
//    [self startSpinner:@"Please Wait.."];
    

    
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    self.shouldScanProduct = YES;
    self.userNameLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
    self.locationLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
    
    self.enterUpcText.delegate=self;
    self.productListTable.delegate=self;
    self.productListTable.dataSource=self;
    
    self.scanner=[DTDevices sharedDevice];
    [self.scanner addDelegate:self];
    [self.scanner connect];
    
    //check if coming from previously saved adjustment
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"]){
        
        NSMutableDictionary * object = [[NSMutableDictionary alloc]init];
        [object setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"CONSUMING_LOCATION"] forKey:@"consumingLocationId"];
        [object setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"DOCUMENT_TYPE"] forKey:@"reasonType"];
        
        [[NSUserDefaults standardUserDefaults] setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"APP_CODE"] forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults] setValue:object forKey:@"adjustmentObject"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.startTime=[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"CREATED_DATE"];
        self.documentId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"DOCUMENT_ID"];
        
        [self populatePreScannedProducts];

    }else{
        self.startTime=[NSDate date];
        [self saveDocToDatabase];
    }
    
//    [self stopSpinner];
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailabilityRP) userInfo:nil repeats:NO];

}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    //    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.scanner addDelegate:self];
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    self.enterUpcText.text=@"";
    
    //check if there is a multiple UPC product selected.
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]){
        [self processBarcode:[[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]];
    }
}

#pragma mark - save document start to DB

-(void)saveDocToDatabase{
    self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];
    
    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];
    
    [newManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"adjustmentObject"] valueForKey:@"reasonType"] forKey:@"documentType"];
    [newManagedObject setValue:@"Created" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:[NSDate date] forKey:@"date"];
    [context save:&error];
    
    //save to temp tables
    
    NSManagedObject *saveManagedObject = [NSEntityDescription
                                          insertNewObjectForEntityForName:@"TempDocStatus"
                                          inManagedObjectContext:context];
    
    [saveManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"adjustmentObject"] valueForKey:@"reasonType"] forKey:@"documentType"];
    [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] forKey:@"holdingLocation"];
    [saveManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"adjustmentObject"]valueForKey:@"consumingLocationId"] forKey:@"toLocation"];
    [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [saveManagedObject setValue:self.documentId forKey:@"documentId"];
    [saveManagedObject setValue:[NSDate date] forKey:@"createdDate"];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"REVERSE_ADJUSTMENT"]){
        [saveManagedObject setValue:@"reverseAdjustment" forKey:@"documentNo"];
    }
    
    [context save:&error];

    [self stopSpinner];
    
}


-(void)populatePreScannedProducts{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
    NSPredicate * prodPredicate = [NSPredicate predicateWithFormat:@"documentId==%@",self.documentId];
    [productRequest setEntity:[NSEntityDescription entityForName:@"AdjustmentTempProdList" inManagedObjectContext:context]];
    [productRequest setPredicate:prodPredicate];
    
    NSError *error;
    NSArray *productResults= [context executeFetchRequest:productRequest error:&error];
    
    if(productResults.count==0){
        NSLog(@"no previously scanned products..start from new..");
    }else{
        for(NSManagedObject * product in productResults){
            
            NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
            [newProduct setValue:[product valueForKey:@"name"] forKey:@"Name"];
            [newProduct setValue:[product valueForKey:@"productId"] forKey:@"ProductId"];
            [newProduct setValue:[product valueForKey:@"sku"] forKey:@"Sku"];
            [newProduct setValue:[product valueForKey:@"quantity"] forKey:@"Quantity"];
            [newProduct setValue:[product valueForKey:@"uom"] forKey:@"Uom"];
            [newProduct setValue:[product valueForKey:@"size"] forKey:@"size"];
            [newProduct setValue:[product valueForKey:@"quantityText"] forKey:@"QuantityText"];
            
            [newProduct setValue:[product valueForKey:@"departmentId"] forKey:@"DepartmentId"];
            [newProduct setValue:[product valueForKey:@"subDepartmentId"] forKey:@"SubDepartmentId"];
            [newProduct setValue:[product valueForKey:@"categoryId"] forKey:@"CategoryId"];

            [newProduct setValue:[product valueForKey:@"listPrice"] forKey:@"listPrice"];
            [newProduct setValue:[product valueForKey:@"activePrice"] forKey:@"activePrice"];
            [newProduct setValue:[product valueForKey:@"cost"] forKey:@"cost"];

            [newProduct setValue:[product valueForKey:@"bottleDeposit"] forKey:@"bottleDeposit"];
            [newProduct setValue:[product valueForKey:@"environmentalFee"] forKey:@"environmentalFee"];
            [newProduct setValue:[product valueForKey:@"taxRate"] forKey:@"taxRate"];
            [newProduct setValue:[NSNumber numberWithBool:[[product valueForKey:@"taxable"]boolValue]] forKey:@"taxable"];

            [self.auditedProducts insertObject:newProduct atIndex:0];

        }
    }
    
    [self.productListTable reloadData];
    
}


#pragma mark - text field delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if([self.enterUpcText hasText]){
        if([[self.enterUpcText text]length]<3)
            [self showErrorAlert:@"Please enter a 3 digit code"];
        else
            
            [self processBarcode:self.enterUpcText.text];
    }else{
        [self showErrorAlert:@"Please Enter Valid product ID or UPC"];
    }
    return NO;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}


#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailabilityRP{
    if([self.scanner connstate]==1){
        [self.scanButton setTitle:@"" forState:UIControlStateNormal];
        [self.scanButton setBackgroundColor:nil];
        [self.scanButton setImage:[UIImage imageNamed:@"iPhoneCamera.png"] forState:UIControlStateNormal];
        self.scannerAvailable=FALSE;
//        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
}

-(void)stopScanner
{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}

#pragma mark - alertview and indicator spin handler

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *customVC     = [[UIViewController alloc] init];
        
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [statusAlert setValue:customVC forKey:@"contentViewController"];
        
        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:^{
            statusAlert = nil;
        }];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.auditedProducts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"adjustmentProductListCell" forIndexPath:indexPath];
    
    if([[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"size"]){
        cell.textLabel.text = [[[[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Name"]stringByAppendingString:@" - "]stringByAppendingString:[[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"size"]]; ;
    }else{
        cell.textLabel.text = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Name"];
    }

    if([[[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"Uom"]isEqualToString:@"ea"] || [[[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"Uom"]isEqualToString:@"kg"]){
        cell.detailTextLabel.text = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"QuantityText"];
    }else{
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.03f kg (%@)",[[[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Quantity"]doubleValue] , [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"QuantityText"]];
    }

    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"REVERSE_ADJSUTMENT"]){
        cell.textLabel.textColor=[UIColor redColor];
        cell.detailTextLabel.textColor = [UIColor redColor];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedProductId = [[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"ProductId"];
    [self reAuditAlert];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self.auditedProducts removeObjectAtIndex:indexPath.row];
        [self.productListTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    return @[deleteAction];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.auditedProducts removeObjectAtIndex:indexPath.row];
        [self.productListTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        
    }
}


#pragma mark - alert creation funcitons

-(void)reAuditAlert{
    self.shouldScanProduct=NO;
    
    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
    NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];

    UIAlertController *reAuditAlertController = [UIAlertController
                                              alertControllerWithTitle:[selectedProductResult.firstObject valueForKey:@"Name"]
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleAlert];

    if([[selectedProductResult.firstObject valueForKey:@"Uom"]isEqualToString:@"ea"]){
        [reAuditAlertController setMessage:[[@"This Product already has a given quantity of " stringByAppendingString:[selectedProductResult.firstObject valueForKey:@"QuantityText"]] stringByAppendingString:@",change quantity?"]];
    }else{
        [reAuditAlertController setMessage:[[@"This Product already has a given quantity of " stringByAppendingString:[NSString stringWithFormat:@"%@ kg", [selectedProductResult.firstObject valueForKey:@"Quantity"]]] stringByAppendingString:@",change quantity?"]];
    }


    UIAlertAction *yesaction = [UIAlertAction
                               actionWithTitle:@"Yes"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                       [self productQuantityAlert];
                               }];
    
    UIAlertAction *noaction = [UIAlertAction
                                actionWithTitle:@"No"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    [reAuditAlertController dismissViewControllerAnimated:YES completion:nil];
                                    self.shouldScanProduct=TRUE;
                                }];
    
    [reAuditAlertController addAction:yesaction];
    [reAuditAlertController addAction:noaction];
    [self presentViewController:reAuditAlertController animated:YES completion:nil];
    
}

-(void)productQuantityAlert{
    self.shouldScanProduct = NO;
    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
    NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];
    
    UIAlertController * quantityAlertController = [[UIAlertController alloc]init];
    
    if(selectedProductResult.count>0){
        quantityAlertController = [UIAlertController
                                  alertControllerWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
        
    }else{
        quantityAlertController = [UIAlertController
                                   alertControllerWithTitle:self.scannedProduct.ProductName
                                   message:nil
                                   preferredStyle:UIAlertControllerStyleAlert];
    }

    //set proper messaging for
    if(selectedProductResult.count>0){
        if([[[selectedProductResult objectAtIndex:0] valueForKey:@"Uom"] isEqualToString:@"ea"]){
            [quantityAlertController setMessage:@"Please enter quantity in EA"];
        }else{
            [quantityAlertController setMessage:@"Please enter quantity in KG"];
        }
    }else{
        if([self.scannedProduct.Uom isEqualToString:@"ea"]){
            [quantityAlertController setMessage:@"Please enter quantity in EA"];
        }else{
            [quantityAlertController setMessage:@"Please enter quantity in KG"];
        }
    }

    [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField* quantityTextField){
        
        if(selectedProductResult.count>0){
            if([[[selectedProductResult objectAtIndex:0] valueForKey:@"Uom"] isEqualToString:@"ea"]){
                [quantityTextField setKeyboardType:UIKeyboardTypeNumberPad];
            }else{
                [quantityTextField setKeyboardType:UIKeyboardTypeDecimalPad];
            }
            quantityTextField.text = [[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue];
            [quantityTextField becomeFirstResponder];
            
        }else{
            quantityTextField.placeholder = @"Product Quantity";
            if([self.scannedProduct.Uom isEqualToString:@"ea"]){
                [quantityTextField setKeyboardType:UIKeyboardTypeNumberPad];
                quantityTextField.placeholder = @"Enter product quantity in EA";
            }else{
                [quantityTextField setKeyboardType:UIKeyboardTypeDecimalPad];
                quantityTextField.placeholder = @"Enter product quantity in KG";
            }
            [quantityTextField becomeFirstResponder];
        }
    }];

    
    UIAlertAction *okaction = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                        self.shouldScanProduct=TRUE;
                                        UITextField * quantityTextField = quantityAlertController.textFields.firstObject;
                                        if([quantityTextField hasText]){
                                            
                                            //quantity entered
                                            
                                            NSLog(@"%@ quantity entered for product: %@",quantityTextField.text, self.selectedProductId);
                                            
                                            self.enterUpcText.text = @"";
                                            
                                            NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
                                            NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];
                                            
                                            if(alreadyProduct.count>0){
                                                
                                                //product already exist
                                                
                                                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[quantityTextField text] doubleValue]] forKey:@"Quantity"];

                                                if([[alreadyProduct.firstObject valueForKey:@"Uom"]isEqualToString:@"ea"]){
                                                    [[alreadyProduct objectAtIndex:0] setValue:[[[quantityTextField text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                                }else{
                                                    if([[alreadyProduct.firstObject valueForKey:@"Uom"]isEqualToString:@"100g"]){
                                                        [alreadyProduct.firstObject setValue:[NSString stringWithFormat:@"%.02f %@",[[quantityTextField text] doubleValue]*10 ,[alreadyProduct.firstObject valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                                    }else if([[alreadyProduct.firstObject valueForKey:@"Uom"]isEqualToString:@"250g"]){
                                                        [alreadyProduct.firstObject setValue:[NSString stringWithFormat:@"%.02f %@",[[quantityTextField text] doubleValue]*4 ,[alreadyProduct.firstObject valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                                    }else if([[alreadyProduct.firstObject valueForKey:@"Uom"]isEqualToString:@"500g"]){
                                                        [alreadyProduct.firstObject setValue:[NSString stringWithFormat:@"%.02f %@",[[quantityTextField text] doubleValue]*2 ,[alreadyProduct.firstObject valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                                    }else if([[alreadyProduct.firstObject valueForKey:@"Uom"]isEqualToString:@"kg"]){
                                                        [alreadyProduct.firstObject setValue:[NSString stringWithFormat:@"%.02f %@",[[quantityTextField text] doubleValue]*1 ,[alreadyProduct.firstObject valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                                    }else if([[alreadyProduct.firstObject valueForKey:@"Uom"]isEqualToString:@"lb"]){
                                                        [alreadyProduct.firstObject setValue:[NSString stringWithFormat:@"%.02f %@",[[quantityTextField text] doubleValue]*2.20462 ,[alreadyProduct.firstObject valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                                    }else if([[alreadyProduct.firstObject valueForKey:@"Uom"]isEqualToString:@"mg"]){
                                                        [alreadyProduct.firstObject setValue:[NSString stringWithFormat:@"%.02f %@",[[quantityTextField text] doubleValue]*1000000 ,[alreadyProduct.firstObject valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                                    }else if([[alreadyProduct.firstObject valueForKey:@"Uom"]isEqualToString:@"g"]){
                                                        [alreadyProduct.firstObject setValue:[NSString stringWithFormat:@"%.02f %@",[[quantityTextField text] doubleValue]*1000 ,[alreadyProduct.firstObject valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                                    }
                                                }



                                            }else{
                                                
                                                //add new product with quantity
                                                
                                                NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
                                                [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
                                                [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
                                                [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
                                                [newProduct setValue:[NSNumber numberWithDouble:[[quantityTextField text] doubleValue]] forKey:@"Quantity"];
                                                [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
                                                
                                                //new added fields
                                                
                                                [newProduct setValue:self.scannedProduct.DepartmentId forKey:@"DepartmentId"];
                                                [newProduct setValue:self.scannedProduct.SubDepartmentId forKey:@"SubDepartmentId"];
                                                [newProduct setValue:self.scannedProduct.CategoryId forKey:@"CategoryId"];
                                                
                                                NSPredicate * locationPredicate = [NSPredicate predicateWithFormat:@"Location.LocationId contains[cd] %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
                                                
                                                StoreSpecificInfo * storeInfo = [[self.scannedProduct.InfoByStore filteredArrayUsingPredicate:locationPredicate] objectAtIndex:0];
                                                
                                                [newProduct setValue:[NSDecimalNumber numberWithDouble:[storeInfo.ListPrice doubleValue]] forKey:@"listPrice"];
                                                [newProduct setValue:[NSDecimalNumber numberWithDouble:[storeInfo.ListPrice doubleValue]] forKey:@"activePrice"];
                                                [newProduct setValue:[NSDecimalNumber numberWithDouble:[self.scannedProduct.StandardCost doubleValue]] forKey:@"cost"];
                                                
                                                //                    [newProduct setValue:storeInfo.ListPrice forKey:@"listPrice"];
                                                //                    [newProduct setValue:storeInfo.ActivePrice forKey:@"activePrice"];
                                                //                    [newProduct setValue:self.scannedProduct.StandardCost forKey:@"cost"];
                                                
                                                
                                                [newProduct setValue:self.scannedProduct.BottleDeposit forKey:@"bottleDeposit"];
                                                [newProduct setValue:self.scannedProduct.EnvironmentalFree forKey:@"environmentalFee"];
                                                [newProduct setValue:self.scannedProduct.TaxRatePercent forKey:@"taxRate"];
                                                [newProduct setValue:[NSNumber numberWithBool:[self.scannedProduct.IsTaxable boolValue]] forKey:@"taxable"];
                                                
                                                
                                                if(self.scannedProduct.Size){
                                                    [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
                                                }


                                                if([self.scannedProduct.Uom isEqualToString:@"ea"]){
                                                    [newProduct setValue:[[[quantityTextField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                }else{
                                                    if([self.scannedProduct.Uom isEqualToString:@"100g"]){
                                                        [newProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*10 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"250g"]){
                                                        [newProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*4 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"500g"]){
                                                        [newProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*2 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"kg"]){
                                                        [newProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"lb"]){
                                                        [newProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*10 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"mg"]){
                                                        [newProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1000000 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"g"]){
                                                        [newProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1000 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }
                                                }


//                                                [newProduct setValue:[[[quantityTextField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];

                                                [self.auditedProducts insertObject:newProduct atIndex:0];
                                            }
                                            
                                            //save product to temp DB
                                            
                                            NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
                                            NSPredicate *productPredicateforDB   = [NSPredicate predicateWithFormat:@"documentId == %@  AND productId = %@",self.documentId, self.selectedProductId];
                                            
                                            [productRequest setPredicate:productPredicateforDB];
                                            [productRequest setEntity:[NSEntityDescription entityForName:@"AdjustmentTempProdList" inManagedObjectContext:context]];
                                            
                                            NSError *error;
                                            NSArray *results= [context executeFetchRequest:productRequest error:&error];
                                            
                                            if(results.count==0){
                                                NSManagedObject *saveProduct = [NSEntityDescription
                                                                                insertNewObjectForEntityForName:@"AdjustmentTempProdList"
                                                                                inManagedObjectContext:context];
                                                
                                                [saveProduct setValue:self.documentId forKey:@"documentId"];
                                                [saveProduct setValue:self.scannedProduct.ProductName forKey:@"name"];
                                                [saveProduct setValue:self.scannedProduct.ProductId forKey:@"productId"];
                                                [saveProduct setValue:self.scannedProduct.Upc forKey:@"sku"];
                                                [saveProduct setValue:[NSNumber numberWithDouble:[[quantityTextField text] doubleValue]] forKey:@"quantity"];
                                                [saveProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
                                                
                                                [saveProduct setValue:self.scannedProduct.DepartmentId forKey:@"DepartmentId"];
                                                [saveProduct setValue:self.scannedProduct.SubDepartmentId forKey:@"SubDepartmentId"];
                                                [saveProduct setValue:self.scannedProduct.CategoryId forKey:@"CategoryId"];
                                                
                                                NSPredicate * locationPredicate = [NSPredicate predicateWithFormat:@"Location.LocationId contains[cd] %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
                                                
                                                StoreSpecificInfo * storeInfo = [[self.scannedProduct.InfoByStore filteredArrayUsingPredicate:locationPredicate] objectAtIndex:0];
                                                
                                                [saveProduct setValue:[NSDecimalNumber numberWithDouble:[storeInfo.ListPrice doubleValue]] forKey:@"listPrice"];
                                                [saveProduct setValue:[NSDecimalNumber numberWithDouble:[storeInfo.ListPrice doubleValue]] forKey:@"activePrice"];
                                                [saveProduct setValue:[NSDecimalNumber numberWithDouble:[self.scannedProduct.StandardCost doubleValue]] forKey:@"cost"];
                                                [saveProduct setValue:self.scannedProduct.BottleDeposit forKey:@"bottleDeposit"];
                                                [saveProduct setValue:self.scannedProduct.EnvironmentalFree forKey:@"environmentalFee"];
                                                [saveProduct setValue:self.scannedProduct.TaxRatePercent forKey:@"taxRate"];
                                                [saveProduct setValue:[NSNumber numberWithBool:[self.scannedProduct.IsTaxable boolValue]] forKey:@"taxable"];
                                                
                                                if(self.scannedProduct.Size){
                                                    [saveProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
                                                }

                                                if([self.scannedProduct.Uom isEqualToString:@"ea"]){
                                                    [saveProduct setValue:[[[quantityTextField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                }else{
                                                    if([self.scannedProduct.Uom isEqualToString:@"100g"]){
                                                        [saveProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*10 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"250g"]){
                                                        [saveProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*4 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"500g"]){
                                                        [saveProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*2 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"kg"]){
                                                        [saveProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"lb"]){
                                                        [saveProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*10 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"mg"]){
                                                        [saveProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1000000 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else if([self.scannedProduct.Uom isEqualToString:@"g"]){
                                                        [saveProduct setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1000 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }
                                                }

//                                                [saveProduct setValue:[[[quantityTextField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];

                                                [context save:&error];
                                            }else{
                                                for (NSManagedObject * obj in results){
                                                    [obj setValue:[NSNumber numberWithDouble:[[quantityTextField text] doubleValue]] forKey:@"Quantity"];

                                                    if([[obj valueForKey:@"Uom"] isEqualToString:@"ea"]){
                                                        [obj setValue:[[[quantityTextField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                    }else{
                                                        if([[obj valueForKey:@"Uom"] isEqualToString:@"100g"]){
                                                            [obj setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*10 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                        }else if([[obj valueForKey:@"Uom"] isEqualToString:@"250g"]){
                                                            [obj setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*4 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                        }else if([[obj valueForKey:@"Uom"] isEqualToString:@"500g"]){
                                                            [obj setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*2 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                        }else if([[obj valueForKey:@"Uom"] isEqualToString:@"kg"]){
                                                            [obj setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                        }else if([[obj valueForKey:@"Uom"] isEqualToString:@"lb"]){
                                                            [obj setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*10 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                        }else if([[obj valueForKey:@"Uom"] isEqualToString:@"mg"]){
                                                            [obj setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1000000 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                        }else if([[obj valueForKey:@"Uom"] isEqualToString:@"g"]){
                                                            [obj setValue:[NSString stringWithFormat:@"%.03f %@",[[quantityTextField text] doubleValue]*1000 ,self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                        }
                                                    }

//                                                    [obj setValue:[[[quantityTextField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                                                }
                                                [context save:&error];
                                            }
                                            NSLog(@"product saved to temp DB...");
                                            
                                            [self.productListTable reloadData];
                                            
                                        }else{
                                            
                                            //no quantity entered, show alert again
                                            
                                            [self productQuantityAlert];
                                        }
                                }];
    
    UIAlertAction *cancelaction = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [quantityAlertController dismissViewControllerAnimated:YES completion:nil];
                                   self.shouldScanProduct=TRUE;
                               }];
    
    [quantityAlertController addAction:okaction];
    [quantityAlertController addAction:cancelaction];
    [self presentViewController:quantityAlertController animated:YES completion:nil];
}

-(void)documentNoteAlert{
    
    self.shouldScanProduct=NO;
    
    UIAlertController *documentNoteAlertController = [UIAlertController
                                                 alertControllerWithTitle:nil
                                                 message:@"Please Add a note to the document for the better understanding of the adjustment"
                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [documentNoteAlertController addTextFieldWithConfigurationHandler:^(UITextField* noteTextField){
        noteTextField.placeholder = @"Note for the adjustment";
        [noteTextField setKeyboardType:UIKeyboardTypeDefault];
        [noteTextField setAutocorrectionType:UITextAutocorrectionTypeYes];
        [noteTextField becomeFirstResponder];
    }];
    
    UIAlertAction *okaction = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                               {
                                   self.shouldScanProduct=YES;
                                   UITextField * noteText = documentNoteAlertController.textFields.firstObject;
                                    if([noteText hasText]){
                                        self.notes = [NSString stringWithString:[noteText text]];
                                        [self finishAuditAlert];
                                    }else{
                                        [self documentNoteAlert];
                                    }
                                }];
    [documentNoteAlertController addAction:okaction];
    [self presentViewController:documentNoteAlertController animated:YES completion:nil];
}

-(void)finishAuditAlert{
    self.shouldScanProduct=NO;
    UIAlertController *finishAlertController = [UIAlertController
                                                      alertControllerWithTitle:nil
                                                      message:@"Do you want to finish adjustment?"
                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesaction = [UIAlertAction
                               actionWithTitle:@"Yes"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   self.shouldScanProduct=NO;
                                   
                                   [self startSpinner:@"Saving Data..."];
                                   self.endTime = [NSDate date];
                                   [self saveAuditToFile];
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       
                                       [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"APP_CODE"];
                                       [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"SAVED_DOC"];
                                       [[NSUserDefaults standardUserDefaults]synchronize];
                                       
                                       self.auditedProducts=Nil;
                                       self.documentId=nil;
                                       self.selectedProductId=nil;
                                       self.scannedProduct=nil;
                                       [self.scanner disconnect];
                                       
                                       [statusAlert dismissViewControllerAnimated:YES completion:^{
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                       }];
                                   });
                               }];
    
    UIAlertAction *noaction = [UIAlertAction
                                actionWithTitle:@"No"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    self.shouldScanProduct=YES;
                                    [finishAlertController dismissViewControllerAnimated:YES completion:nil];
                                }];
    
    [finishAlertController addAction:yesaction];
    [finishAlertController addAction:noaction];
    [self presentViewController:finishAlertController animated:YES completion:nil];

}

-(void)showGoBackPromtAlert{
    self.shouldScanProduct=NO;
    
    UIAlertController *goBackAlertController = [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:@"Are you sure to quit adjustment?"
                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    self.shouldScanProduct=NO;
                                    
                                    NSLog(@"going back...");
                                    [self startSpinner:@"Deleting Records.."];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                        NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                        
                                        NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];
                                        
                                        [request setPredicate:predicate];
                                        [request setEntity:entityDesc];
                                        
                                        NSError *error;
                                        NSArray *results= [context executeFetchRequest:request error:&error];
                                        
                                        for (NSManagedObject * obj in results){
                                            [context deleteObject:obj];
                                        }
                                        [context save:&error];
                                        
                                        
                                        //delete from DocStatusInfo table
                                        
                                        NSFetchRequest *allDocs = [[NSFetchRequest alloc] init];
                                        [allDocs setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
                                        NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
                                        [allDocs setPredicate:docPredicate];
                                        NSArray *docs = [context executeFetchRequest:allDocs error:&error];
                                        for (NSManagedObject *doc in docs) {
                                            [context deleteObject:doc];  }
                                        NSError *saveError = nil;
                                        [context save:&saveError];
                                        
                                        //delete from TempProdList table
                                        
                                        NSFetchRequest *allProds = [[NSFetchRequest alloc] init];
                                        [allProds setEntity:[NSEntityDescription entityForName:@"AdjustmentTempProdList" inManagedObjectContext:context]];
                                        NSPredicate *prodPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
                                        [allProds setPredicate:prodPredicate];
                                        NSArray *prods = [context executeFetchRequest:allProds error:&error];
                                        for (NSManagedObject *prod in prods) {
                                            [context deleteObject:prod];  }
                                        [context save:&saveError];
                                        
                                        [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"APP_CODE"];
                                        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"SAVED_DOC"];
                                        [[NSUserDefaults standardUserDefaults]synchronize];
                                        
                                        self.auditedProducts=Nil;
                                        [self.scanner disconnect];
                                        
                                        [statusAlert dismissViewControllerAnimated:YES completion:^{
                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                        }];
                                    });
                                }];
    
    UIAlertAction *Cancelaction = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   self.shouldScanProduct=YES;
                                   [goBackAlertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [goBackAlertController addAction:okaction];
    [goBackAlertController addAction:Cancelaction];
    [self presentViewController:goBackAlertController animated:YES completion:nil];

}

-(void)showSaveAndBackAlert{
    self.shouldScanProduct=NO;
//    UIAlertView * saveAndgoBackAlert = [[UIAlertView alloc] initWithTitle:nil
//                                                                  message:nil
//                                                                 delegate:self
//                                                        cancelButtonTitle:nil
//                                                        otherButtonTitles:@"Save and Exit",@"Discard and Exit", Nil];
//    
//    saveAndgoBackAlert.tag=saveAndGoBackPrompt;
//    [saveAndgoBackAlert show];
    
    
    UIAlertController *saveAndgoBackAlertController = [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:@"Do you want to finish adjustment?"
                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *saveAndExitaction = [UIAlertAction
                                actionWithTitle:@"Save and Exit"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    [self startSpinner:@"Saving Data..."];
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"APP_CODE"];
                                        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"SAVED_DOC"];
                                        [[NSUserDefaults standardUserDefaults]synchronize];
                                        
                                        self.auditedProducts=Nil;
                                        self.documentId=nil;
                                        self.selectedProductId=nil;
                                        self.scannedProduct=nil;
                                        [self.scanner disconnect];
                                        
                                        [statusAlert dismissViewControllerAnimated:YES completion:^{
                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                        }];
                                    });
                                }];
    
    UIAlertAction *discardAndExitaction = [UIAlertAction
                               actionWithTitle:@"Delete and Exit"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self showGoBackPromtAlert];
                               }];
    
    [saveAndgoBackAlertController addAction:saveAndExitaction];
    [saveAndgoBackAlertController addAction:discardAndExitaction];
    [self presentViewController:saveAndgoBackAlertController animated:YES completion:nil];
}

-(void)showErrorAlert:(NSString *)message{
    self.shouldScanProduct=FALSE;
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.shouldScanProduct=TRUE;
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    self.shouldScanProduct=FALSE;
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.shouldScanProduct=TRUE;
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}



#pragma mark - iphone file system handler

- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

-(void)saveAuditToFile{
    NSLog(@"saving file..");
    
    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];
    
    [newManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"adjustmentObject"] valueForKey:@"reasonType"] forKey:@"documentType"];
    [newManagedObject setValue:@"Finished" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.endTime forKey:@"date"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
    [context save:&error];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *stringFromDate = [formatter stringFromDate:self.endTime];
    
    NSMutableArray<AdjustmentItem> * productItems = [[NSMutableArray<AdjustmentItem> alloc]init];
    
    for(NSObject * product in self.auditedProducts){
        
        AdjustmentItem * ProductItem = [[AdjustmentItem alloc]init];
        
        AdjustmentProduct * adjProduct = [[AdjustmentProduct alloc]init];
        
        adjProduct.ProductId = [product valueForKey:@"ProductId"];
        adjProduct.DepartmentId = [product valueForKey:@"DepartmentId"];
        adjProduct.SubDepartmentId = [product valueForKey:@"SubDepartmentId"];
        adjProduct.CategoryId = [product valueForKey:@"CategoryId"];
        adjProduct.ListPrice =  [NSNumber numberWithDouble:[[product valueForKey:@"listPrice"] doubleValue]];
        adjProduct.ActivePrice = [NSNumber numberWithDouble:[[product valueForKey:@"activePrice"] doubleValue]];
        adjProduct.Cost = [NSNumber numberWithDouble:[[product valueForKey:@"cost"] doubleValue]];
        adjProduct.BottleDeposit = [product valueForKey:@"bottleDeposit"];
        adjProduct.EnvironmentalFee = [product valueForKey:@"environmentalFee"];
        adjProduct.Taxable = [product valueForKey:@"taxable"];
        adjProduct.TaxRate = [product valueForKey:@"taxRate"];
        
//        [adjProduct validate];
        
        ProductItem.Product = adjProduct;
        NSNumber * quantity = [NSNumber numberWithDouble:[[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][0] doubleValue]];
//        ProductItem.Quantity = [product valueForKey:@"Quantity"];
        ProductItem.Quantity = quantity;
        ProductItem.ValueListPrice = [self multiplyNumbers:[product valueForKey:@"listPrice"] second:quantity];
        ProductItem.ValueActivePrice = [self multiplyNumbers:[product valueForKey:@"activePrice"] second:quantity];
        ProductItem.ValueCost = [self multiplyNumbers:[product valueForKey:@"cost"] second:quantity];
        ProductItem.ValueBottleDeposit = [self multiplyNumbers:[product valueForKey:@"bottleDeposit"] second:quantity];
        ProductItem.ValueEnvironmentalFee = [self multiplyNumbers:[product valueForKey:@"environmentalFee"] second:quantity];
        if([[product valueForKey:@"taxable"]boolValue]){
            ProductItem.ValueActivePriceTax = [NSNumber numberWithDouble:[ProductItem.ValueActivePrice doubleValue] * [adjProduct.TaxRate doubleValue]/100];
            ProductItem.ValueActiveCostTax = [NSNumber numberWithDouble:[ProductItem.ValueCost doubleValue] * [adjProduct.TaxRate doubleValue]/100];
        }
        else{
            ProductItem.ValueActivePriceTax = [NSNumber numberWithDouble:0.00];
            ProductItem.ValueActiveCostTax = [NSNumber numberWithDouble:0.00];
        }

//        [ProductItem validate];

        [productItems addObject:ProductItem];
    }
    
    BaseAdjustmentRequest * adjustmentRequest = [[BaseAdjustmentRequest alloc]init];
    adjustmentRequest.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
    adjustmentRequest.HoldingLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
    adjustmentRequest.ConsumingLocationId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"adjustmentObject"]valueForKey:@"consumingLocationId"];
    adjustmentRequest.CreatedDateTime = self.startTime;
    adjustmentRequest.Comment = self.notes;
    adjustmentRequest.Items = productItems;
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"REVERSE_ADJUSTMENT"]){
        adjustmentRequest.ReversedEntry = [NSNumber numberWithBool:TRUE];
    }else{
        adjustmentRequest.ReversedEntry = [NSNumber numberWithBool:FALSE];
    }

    adjustmentRequest.IsProductionPurpose = [NSNumber numberWithBool:NO];

    [self writeStringToFile:[adjustmentRequest toJSONString] fileName:stringFromDate];
    
    NSLog(@"File Saved for adjustment from %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"]);
    
    //delete from DocStatusInfo table
    
    NSFetchRequest *allDocs = [[NSFetchRequest alloc] init];
    [allDocs setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
    NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
    [allDocs setPredicate:docPredicate];
    NSArray *docs = [context executeFetchRequest:allDocs error:&error];
    for (NSManagedObject *doc in docs) {
        [context deleteObject:doc];  }
    NSError *saveError = nil;
    [context save:&saveError];
    
    //delete from TempProdList table
    
    NSFetchRequest *allProds = [[NSFetchRequest alloc] init];
    [allProds setEntity:[NSEntityDescription entityForName:@"AdjustmentTempProdList" inManagedObjectContext:context]];
    NSPredicate *prodPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
    [allProds setPredicate:prodPredicate];
    NSArray *prods = [context executeFetchRequest:allProds error:&error];
    for (NSManagedObject *prod in prods) {
        [context deleteObject:prod];
    }
    [context save:&saveError];
    NSLog(@"temp DB entries deleted..");
}


-(NSNumber*)multiplyNumbers :(NSNumber*)first second:(NSNumber*)second{
    double multiply = [first doubleValue]*[second doubleValue];
    return [NSNumber numberWithDouble:multiply];
}


#pragma mark - barcode read function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    if(self.shouldScanProduct){
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
        [self processBarcode:barcode];
    }else{
        int sound[]={3500,200,0,200,3500,200};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    [reader dismissViewControllerAnimated:YES completion:Nil];
    NSString * barcode=symbol.data;
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    [self processBarcode:barcode];
}


-(void)processBarcode:(NSString *)barcode{
    
    [self.enterUpcText endEditing:YES];
    
    self.enterUpcText.text=@"";
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc] init] processBarcodeFor4DigitPLU:barcode];
    
    if([resultDic valueForKey:@"errorMsg"]!=NULL){
        NSLog(@"Result returned error..");
        [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
    }else{
        NSArray * products = [resultDic valueForKey:@"productArray"];
        if(products.count==0){
            //no product found
            
            NSLog(@"product not found..");
            [self showErrorAlert:@"Product Not Found..."];
            
        }else if(products.count>1){
            //multiple products from other vendors
            
            NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
            
            for(NSObject * product in products){
                NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                
                [listedProductArray addObject:listedProduct];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"multipleUpcFromAdjustmentSegue" sender:self];
            
        }else if(products.count==1){
            Products * product = [[Products alloc]init];
            product = [products objectAtIndex:0];
            
            NSPredicate * existingProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", product.ProductId];
            if([self.auditedProducts filteredArrayUsingPredicate:existingProdPred].count==0){
                self.scannedProduct = product;
                self.selectedProductId = product.ProductId;
                [self productQuantityAlert];
            }else{
                self.selectedProductId = product.ProductId;
                [self reAuditAlert];
            }
        }
    }
}

- (IBAction)submitAudit:(id)sender {
    if(self.auditedProducts.count==0)
        [self showErrorAlert:@"No Product enlisted, Please add products to the list"];
    else
        [self documentNoteAlert];
}

- (IBAction)goBack:(id)sender {
    [self showSaveAndBackAlert];
}

- (IBAction)scanProduct:(id)sender {
    if(self.scannerAvailable){
        if(self.shouldScanProduct){
            [self.scanner barcodeStartScan:nil];
            self.foundBarcode = NO;
            
            [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
        }
    }else{
        if(self.shouldScanProduct){
            ZBarReaderViewController *reader = [ZBarReaderViewController new];
            reader.readerDelegate = self;
            [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
            reader.readerView.zoom = 1.0;
            [self presentViewController:reader animated:YES completion:Nil];
        }
    }
}
@end
