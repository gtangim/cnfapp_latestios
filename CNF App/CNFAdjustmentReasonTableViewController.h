//
//  CNFAdjustmentReasonTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-04-10.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFAdjustmentReasonTableViewController : UITableViewController<UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

- (IBAction)quitAdjustment:(id)sender;
- (IBAction)goToProducts:(id)sender;

@end
