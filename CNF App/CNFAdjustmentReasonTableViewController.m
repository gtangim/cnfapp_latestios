//
//  CNFAdjustmentReasonTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-04-10.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFAdjustmentReasonTableViewController.h"

@interface CNFAdjustmentReasonTableViewController ()
@property (nonatomic, strong) NSMutableArray * reasonArray;
@property (nonatomic, strong) NSMutableArray * headerArray;
@end

@implementation CNFAdjustmentReasonTableViewController{
//    UIAlertView * statusAlert;
}

@synthesize pickerView=_pickerView, reasonArray=_reasonArray, headerArray=_headerArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem.enabled=YES;
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"REVERSE_ADJUSTMENT"]){
        [self.navigationItem setTitle:@"Reverse Adjustment"];
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor redColor]};
    }else{
        [self.navigationItem setTitle:@"Adjustment"];
    }

    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;

    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"adjustmentObject"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    //added all this portion in the VIEW_DID_LOAD function other than view appear funciton

    NSMutableArray * reasonArray = [[NSMutableArray alloc]initWithObjects:@"Product Transferred to Café Requested by Café", @"Customer Giveaway/Charitable Donation/Prize",@"Not for Profit Discards",@"Expired/Spoiled Products",@"Staff Room Supplies/Meeting Supplies/Staff Training Product",@"Damaged Product/Receiver Error",@"",@"Damaged Product/Mislabeled/Wrong Packaging",nil];

    NSPredicate *adjustmentPredicate = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"Crs2AdjustmentList"];

    NSArray * adjustmentArray = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:adjustmentPredicate];
    if(adjustmentArray.count==0){
        //        [self stopSpinner];
        [self showErrorAlert:@"Older Data File, Please restart the app"];
    }else{
        NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:adjustmentPredicate] objectAtIndex:0];

        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];

        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];

        databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
        [file closeFile];

        NSError * parsingError;

        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        NSMutableArray* parseArray = [[NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&parsingError] mutableCopy];

        if(parsingError){
            NSLog(@"Data file can not be parsed");
            //            [self stopSpinner];
            [self showErrorAlert:@"Serializing Error, Incomplete Downloaded Data file"];
        }else{

            NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"AdjustmentTypeId == %@ OR AdjustmentTypeId == %@ OR AdjustmentTypeId == %@", @"C2_PRD_INV_DECR", @"C2_PRD_INV_INCR", @"C2_RET_DISCARD"];

            NSArray * filteredArray = [parseArray filteredArrayUsingPredicate:typePredicate];

            for(NSObject * adjustmentType in filteredArray){
                [parseArray removeObject:adjustmentType];
            }

            NSMutableArray * allAdjustmentReasons = [[NSMutableArray alloc]init];

            for(int i =0;i<parseArray.count;i++){
                NSObject * adjustmentObject = [[parseArray objectAtIndex:i] mutableCopy];
                [adjustmentObject setValue:[reasonArray objectAtIndex:i] forKey:@"Description"];
                [allAdjustmentReasons addObject:adjustmentObject];
            }

            //read from file for AD group permission

            NSPredicate *adjustmentPredicate = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"ADUserPermissionList"];

            NSArray * adjustmentArray = [[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:adjustmentPredicate];
            if(adjustmentArray.count==0){
                //                [self stopSpinner];
                [self showErrorAlert:@"Older Data File, Please restart the app"];
            }else{
                NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:adjustmentPredicate] objectAtIndex:0];

                NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
                NSFileHandle *file;
                NSData *databuffer;
                file = [NSFileHandle fileHandleForReadingAtPath:filepath];

                [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];

                databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
                [file closeFile];

                NSError * parsingError;

                NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
                NSMutableArray* permissionArray = [[NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&parsingError] mutableCopy];

                if(parsingError){
                    NSLog(@"Data file can not be parsed");
                    //                    [self stopSpinner];
                    [self showErrorAlert:@"Serializing Error, Incomplete Downloaded Data file"];
                }else{

                    NSPredicate * adjustmentGroupBelongingPred = [NSPredicate predicateWithFormat:@"self contains[cd] %@",@"adjustment"];
                    NSArray * adjustmentGroups=[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] filteredArrayUsingPredicate:adjustmentGroupBelongingPred];
                    if(adjustmentGroups.count==0){
                        NSLog(@"User is not a group member of any adjustment");
                        //                        [self stopSpinner];
                        [self showErrorAlert:@"User Does not have adjustment group access, please contact IT.."];
                        //                        [self.navigationController popViewControllerAnimated:YES];
                    }else{

                        NSMutableArray * filteredResonList = [[NSMutableArray alloc]init];

                        for(NSString * groupName in adjustmentGroups){
                            NSPredicate * filterPredicate = [NSPredicate predicateWithFormat:@"AdUserGroupName = %@", groupName];
                            NSArray * groupPermission = [permissionArray filteredArrayUsingPredicate:filterPredicate];

                            if(groupPermission.count==0){
                                continue;
                            }else{
                                for(NSObject * object in groupPermission){
                                    if(![filteredResonList containsObject:[object valueForKey:@"AdjustmentTypeId"]]){
                                        [filteredResonList addObject:[object valueForKey:@"AdjustmentTypeId"]];
                                    }
                                }
                            }
                        }

                        self.headerArray = [allAdjustmentReasons mutableCopy];

                        for(NSObject * adjustmentObject in allAdjustmentReasons){
                            if(![filteredResonList containsObject:[adjustmentObject valueForKey:@"AdjustmentTypeId"]]){
                                [self.headerArray removeObject:adjustmentObject];
                            }
                        }
                    }
                }
            }
        }
    }
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"adjustmentObject"] valueForKey:@"reasonType"]){
        [self.pickerView selectRow:[[self.headerArray valueForKey:@"AdjustmentTypeId"] indexOfObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"adjustmentObject"] valueForKey:@"reasonType"]] inComponent:0 animated:YES];
    }



}


#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50;
}


#pragma mark - pickerview functions

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component{
    return self.headerArray.count;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 70;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [[self.headerArray objectAtIndex:row] valueForKey:@"Name"];
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
        NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc]initWithString:[[self.headerArray objectAtIndex:row] valueForKey:@"Name"] attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:16]}];
        
//        if([self.reasonArray objectAtIndex:row]!=nil){
            NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:[@"\n" stringByAppendingString:[[self.headerArray objectAtIndex:row] valueForKey:@"Description"]] attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16]}];
            [muAtrStr appendAttributedString:atrStr];
//        }else{
//            NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:[@"\n" stringByAppendingString:@""] attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:16]}];
//            [muAtrStr appendAttributedString:atrStr];
//        }
        UILabel *labelView = [[UILabel alloc] init];
        labelView.attributedText = muAtrStr ;
        labelView.textAlignment=NSTextAlignmentCenter;
        labelView.numberOfLines=0;
        return labelView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
    self.navigationItem.rightBarButtonItem.enabled=TRUE;
}

#pragma mark - alert notification functions

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [errorAlertController dismissViewControllerAnimated:YES completion:nil];
                                       [self.navigationController popViewControllerAnimated:YES];
                                   });
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}


#pragma mark - animation UIView recogniser

-(bool) anySubViewScrolling:(UIView*)view
{
    if( [ view isKindOfClass:[ UIScrollView class ] ] )
    {
        UIScrollView* scroll_view = (UIScrollView*) view;
        if( scroll_view.dragging || scroll_view.decelerating )
        {
            return true;
        }
    }

    for( UIView *sub_view in [ view subviews ] )
    {
        if( [ self anySubViewScrolling:sub_view ] )
        {
            return true;
        }
    }

    return false;
}


- (IBAction)quitAdjustment:(id)sender {
    if(![self anySubViewScrolling:self.pickerView]){
        self.headerArray=nil;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self showInfoAlert:nil body:@"Please let the animation finish"];
    }
}

- (IBAction)goToProducts:(id)sender {
    if(![self anySubViewScrolling:self.pickerView]){
        NSMutableDictionary * adjustmentObject = [[NSMutableDictionary alloc]init];
        [adjustmentObject setValue:[[self.headerArray objectAtIndex:[self.pickerView selectedRowInComponent:0]] valueForKey:@"AdjustmentTypeId"] forKey:@"reasonType"];
        [[NSUserDefaults standardUserDefaults] setObject:adjustmentObject forKey:@"adjustmentObject"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [self performSegueWithIdentifier:@"holdingConsumingLocationSegue" sender:self];
    }else{
        [self showInfoAlert:nil body:@"Please let the animation finish"];
    }
}
@end

