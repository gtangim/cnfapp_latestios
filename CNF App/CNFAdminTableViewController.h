//
//  CNFAdminTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-08-18.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFAdminTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextView *logView;

@end
