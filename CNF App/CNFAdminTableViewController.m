//
//  CNFAdminTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-08-18.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFAdminTableViewController.h"
#import "CNFAppDelegate.h"

@interface CNFAdminTableViewController ()

@end

@implementation CNFAdminTableViewController

@synthesize logView=_logView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [@"/cnf_logs/" stringByAppendingString:[[NSUserDefaults standardUserDefaults]valueForKey:@"SELECTED_LOG_FILE"]];
    NSString *logFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    NSFileHandle *file;
    file = [NSFileHandle fileHandleForReadingAtPath:logFilePath];
    
    self.logView.text = [NSString stringWithContentsOfFile:logFilePath encoding:NSUTF8StringEncoding error:nil];
    
    
//    if([file seekToEndOfFile]<=20000){
//        self.logView.text = [NSString stringWithContentsOfFile:logFilePath encoding:NSUTF8StringEncoding error:nil];
//    }else{
//        [file seekToFileOffset:[file seekToEndOfFile]-20000];
//        
//        databuffer = [file readDataOfLength: 20000];
//        
//        self.logView.text = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
//    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 80;
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(nonnull UIView *)view forSection:(NSInteger)section{
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
    [footer.textLabel adjustsFontSizeToFitWidth];
    footer.textLabel.textAlignment=NSTextAlignmentCenter;
    
    NSString * servertext = [@"Conencted to: "stringByAppendingString:appDelegate.serverUrl];
    NSString * dataFIleText = [@"Data File Version: " stringByAppendingString:[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]];
    NSString * idText = [@" Device registered shortId:"stringByAppendingString:[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_SHORT_ID"]];
    
    footer.textLabel.text = [[[[[[NSString stringWithFormat:@"App Version Number: %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]] stringByAppendingString:@" "] stringByAppendingString:dataFIleText] stringByAppendingString:@" "] stringByAppendingString:servertext] stringByAppendingString:idText];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

@end
