//
//  CNFAppDelegate.h
//  CNF App
//
//  Created by Anik on 2015-03-03.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CNFAppDelegate : UIResponder <UIApplicationDelegate>{
    NSString* serverUrl;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSString* serverUrl;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
