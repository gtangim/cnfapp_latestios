//
//  CNFAppDelegate.m
//  CNF App
//
//  Created by Anik on 2015-03-03.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "CNFAppDelegate.h"
#import "BRLineReader.h"
#import "GZIP.h"
#import "Reachability.h"
#import "CNFLoadMemory.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import <Fabric/Fabric.h>
#import "HeartBeatModel.h"

#import "CNFGetDocStatus.h"

static long prevMemUsage = 0;
static long curMemUsage = 0;
static long memUsageDiff = 0;
static long curFreeMem = 0;

@implementation CNFAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize serverUrl;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

//    NSLog(@"%@", NSHomeDirectory());

    serverUrl=[[NSUserDefaults standardUserDefaults] valueForKey:@"CONNECTION"];

    if(serverUrl==NULL)
        serverUrl = @"https://webapp.cnfltd.com";
    
    [[NSUserDefaults standardUserDefaults] setValue:@"Community Kitchen" forKey:@"NFP_DISCARD_CHARITY_NAME"];

    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_INTERVAL"]){
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:5] forKey:@"ONLINE_TROUBLE_INTERVAL"];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    [NSTimer scheduledTimerWithTimeInterval:30.0 target:self
                                   selector:@selector(captureMemUsage) userInfo:nil repeats:YES];
    
    [self prepareHeartBeat];
    [self prepareDocStatus];

//    //start logging to the log file
//    [self checkLogFileAndUpdate];
//
//    //check to create seperate log file every hour
//    [NSTimer scheduledTimerWithTimeInterval:3600.0f target:self selector:@selector(checkLogFileAndUpdate) userInfo:nil repeats:YES];
//
//    //check everyday to delete log files back 10 days
//    [NSTimer scheduledTimerWithTimeInterval:24*3600.0f target:self selector:@selector(deleteOlderLogFiles) userInfo:nil repeats:YES];


//    send crash logs to CrashLytics
    [Fabric with:@[[Crashlytics class]]];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"app entered background...");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application{
    [self saveContext];
}

- (void)checkLogFileAndUpdate{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSError * error;
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/cnf_logs"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    if(!error){
        NSDateFormatter * dateformatter = [[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"yyyy-MM-dd"];
        NSString * dateString = [dateformatter stringFromDate:[NSDate date]];
        NSString * fileName = [dateString stringByAppendingString:@".log"];
        NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        freopen([fileAtPath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
        
    }else{
        NSLog(@"error occurred while creating directory for log: %@", error);
    }
}


-(void)deleteOlderLogFiles{
    NSArray * fileListing = [self listFileAtPath:@"Documents/cnf_logs"];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    for(NSString * name in fileListing){
            NSString * dateString = [name componentsSeparatedByString:@"."][0];
            NSDate * date = [dateFormatter dateFromString:dateString];
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [gregorianCalendar components: (NSCalendarUnitDay | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear )
                                                                fromDate:date
                                                                  toDate:[NSDate date]
                                                                 options:0];
            if([components day]>10){
                NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/cnf_logs"];
                NSString *filePath = [dataPath stringByAppendingPathComponent:name];
                NSError *error;
                BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                if (success) {
                    NSLog(@"file deleted");
                }
                else
                {
                    NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                }
            }
    }
}

-(NSArray *)listFileAtPath:(NSString*)filePath
{
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:filePath];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    return directoryContent;
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {

            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CNF_App" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CNF_App.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - memory tracker

-(void) get_free_memory {
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS) {
        NSLog(@"Failed to fetch vm statistics");
        return ;
    }
    
    /* Stats in bytes */
    NSLog(@"Free Memory: %lul", vm_stat.free_count * pagesize);
}


-(vm_size_t) freeMemory {
    mach_port_t host_port = mach_host_self();
    mach_msg_type_number_t host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    vm_size_t pagesize;
    vm_statistics_data_t vm_stat;
    
    host_page_size(host_port, &pagesize);
    (void) host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size);
    return vm_stat.free_count * pagesize;
}

-(vm_size_t) usedMemory {
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&info, &size);
    return (kerr == KERN_SUCCESS) ? info.resident_size : 0; // size in bytes
}

-(void) captureMemUsage {
    prevMemUsage = curMemUsage;
    curMemUsage = [self usedMemory];
    memUsageDiff = curMemUsage - prevMemUsage;
    curFreeMem = [self freeMemory];
    NSLog(@"**** Current memory used: %ldMB, free memory: %ldMB ****", curMemUsage/1024/1000, curFreeMem/1024/1000);
}


#pragma mark - heartbeat webservice Call


- (void) prepareHeartBeat{
    [NSTimer scheduledTimerWithTimeInterval:120.0f
                                     target:self selector:@selector(heartBeatStarter) userInfo:nil repeats:YES];
}


-(void)heartBeatStarter{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self sendHeartBeat];
    });
}


-(void)sendHeartBeat{
    
    NSUInteger docsInQueue=0;
    
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"status ==%@ AND docUploaded==NO", @"Finished"];
    
    [request setPredicate:predicate];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *results= [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if(error){
        NSLog(@"Unsubmitted doc fetch error during heartbeat: %@", error.localizedDescription);
    }else{
        if(results.count==0)
            docsInQueue=0;
        else
            docsInQueue=results.count;
    }
    
    HeartBeatModel * heartBeat = [[HeartBeatModel alloc] init];
    heartBeat.deviceLongId = [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_ID"];
    heartBeat.numDocsInQueue = [[NSNumber alloc]initWithUnsignedInt:(unsigned int)docsInQueue];
    heartBeat.dataFileVersion = [[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"];
    heartBeat.deviceName = [[UIDevice currentDevice] name];
    heartBeat.appName = [[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]? [[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]:@"";
    heartBeat.loggedInUser = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] ? [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"]:@"";

    NSData *postData = [[heartBeat toJSONString] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *len = [NSString stringWithFormat:@"%i", (int)[[heartBeat toJSONString] length]];
    
    NSMutableURLRequest *webrequest = [[NSMutableURLRequest alloc] init];
    
    [webrequest setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/SubmitDeviceHeartbit", serverUrl]]];
    [webrequest setHTTPMethod:@"POST"];
    [webrequest setValue:len forHTTPHeaderField:@"Content-Length"];
    [webrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [webrequest setHTTPBody:postData];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:webrequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(error)
                                              NSLog(@"Heartbeat send error: %@", error.localizedDescription);
                                          else{
                                              id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                              if(error){
                                                  NSLog(@"Heartbeat send error: %@", error.localizedDescription);
                                              }else{
                                                  if([[dictionary valueForKey:@"d"]isEqualToString:@"Success"])
                                                      NSLog(@"Device Heartbeat successfully logged to server");
                                                  else
                                                      NSLog(@"Heartbeat send error: %@", dictionary);
                                              }
                                          }
                                      }];
    [dataTask resume];

}


#pragma mark - Get document status web service

- (void) prepareDocStatus{
    [NSTimer scheduledTimerWithTimeInterval:30.0f
                                     target:self selector:@selector(docStatusStarter) userInfo:nil repeats:YES];
}

-(void)docStatusStarter{
    [self logAndResetQueueFlag];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [NSThread detachNewThreadSelector:@selector(getDocStatus) toTarget:self withObject:nil];
    });
}

-(void)getDocStatus{
    Reachability* reachability = [Reachability reachabilityWithHostName: serverUrl];
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    if(netStatus==1){
        CNFGetDocStatus * getDocStatus = [[CNFGetDocStatus alloc]init];
        getDocStatus.serverAddress = serverUrl;
        getDocStatus.context = self.managedObjectContext;
        [getDocStatus startgettingDocStatus];
    }else{
        NSLog(@"No connection for sync...");
    }
}

#pragma mark - Logs and resets Queue flag

-(void)logAndResetQueueFlag{
    NSLog(@"Is Queue running : %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"BACK_PROCESS"]);
    NSLog(@"Queue start time: %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"QUEUE_START_TIME"]);

    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    if([[NSUserDefaults standardUserDefaults] valueForKey:@"QUEUE_START_TIME"]){
        NSDate * date = [dateFormatter dateFromString:[[NSUserDefaults standardUserDefaults] valueForKey:@"QUEUE_START_TIME"]];

        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components: (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear )
                                                            fromDate:date
                                                              toDate:[NSDate date]
                                                             options:0];
        if([components minute]>10){
            NSLog(@"Queue is stuck for more than 10 minutes, resetting Queue flag...");
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BACK_PROCESS"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
}

@end
