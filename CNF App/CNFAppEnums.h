//
//  CNFAppEnums.h
//  CNF App
//
//  Created by Anik on 2015-04-06.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//


// Used to specify the application used in accessing the Keychain.
#define APP_NAME [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]

// Typedefs just to make it a little easier to read in code.
typedef enum {
    errorAlerts = 0,
    delistedPrompt,
    pulledPrompt,
    vendorOOSPrompt,
    onOrderPrompt,
    warehouseOOSPrompt,
    otherLocationPrompt,
    finishAuditPrompt,
    logoutPrompt,
    zeroCapacityPrompt,
    confirmationPrompt,
    quantityPrompt,
    reAuditPrompt,
    selectionErrorAlert,
    grandTotalPrompt,
    reasonPrompt,
    goBackPrompt,
    uomSelectPrompt,
    vendorPrompt,
    cMPushPrompt,
    productNotePrompt,
    documentNotePrompt,
    vosTransferSetectPrompt,
    vosReceiptSelectPrompt,
    produtsForSelectedVendorPrompt,
    noVosFoundPrompt,
    noTransferFoundPrompt,
    wrongVendorPrompt,
    reSubmitAuditPrompt,
    restartAlerts,
    productNotInVosAlerts,
    wrongCaseQuantityAlerts,
    discardAcknowledgementAlerts,
    charityNameAlerts,
    settingsAlerts,
    saveAndGoBackPrompt,
    transferNotePrompt
} AlertTypes;


typedef enum {
    startTakingPhoto = 0,
    takePhotoOfInvoice,
    takeAnotherPhotoOfInvoice,
    takeAnotherPhotoOther,
    finishInvoicePhoto,
    takeOptionalPhoto,
    finishOptionalPhoto
} photoAlertType;

typedef enum {
    supplyAuditSelect = 0,
    logoutSelect,
    receivingAppSelect,
    continueDocSelect,
    pendingNotification,
    failedNotification,
    pickingAppSelect
} SelectAppAlertTypes;

typedef enum {
    receiptWithoutOrderSheet = 0,
    receiptWithOrderSheet,
    createTransfer,
    receiveTransfer,
    createOrderSheet
} ReceivingHeaderDataAlertType;

typedef enum{
    barcodeReader = 0,
    imageCapture
}imagePickerType;

typedef enum{
    typePicker = 0,
    reasonPicker
}pickerViewType;


typedef enum {
    scanHoles = 0,
    scanReceiveing
} FinishButtonTypes;

typedef enum {
    outOfStock = 0,
    receiving,
    inventory,
    picking
} AppTypes;

typedef enum {
    addProductWithScan=0,
    addProductWithOutScan,
    finishPromo,
    costChangeAlert,
    saveProductAlert,
    vendorSelectAlert,
    saveProductAndProceedAlert
}promoAlertViewTypes;


typedef enum {
    velocityTextField=0,
    capacityTextField
}
textFieldTyepsForPromoDetails;

typedef enum {
    defaultAlerts=0,
    discrepencyQuantityAlertPrompt,
    newProductQuantityAlertPrompt,
    discrepencyErrorAlerts
}
discrepencyAlertList;


typedef enum {
    pickingErrorAlerts=0,
    pickingInfoAlerts,
    noProductToPickAlerts,
    pickProductQuantityAlerts,
    pickProductConfirmationAlerts,
    pickQuantityTooHighAlerts,
    pickQuantityHigherThanDemandAlets,
    returnToDepartmentAlerts,
    initializingErrorAlerts,
    finishPickingAlerts,
    noPickProductAlerts,
    validationFailureAlerts,
    printTransferNumberAlerts
}pickingAlertTypeList;


