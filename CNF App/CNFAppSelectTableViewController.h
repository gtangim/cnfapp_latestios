//
//  CNFAppSelectTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-08-12.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "Reachability.h"
#import "BRRequestUpload.h"
#import "BRRequest+_UserData.h"
#import "BRRequest.h"


@interface CNFAppSelectTableViewController : UITableViewController<UIActionSheetDelegate, BRRequestDelegate, NSURLSessionDelegate>

@property (weak, nonatomic) IBOutlet UITableViewCell *productLookUpCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *outOfStockCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *receivingCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *adminCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *adjustmentCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *printDiscardCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *reverseAdjustmentCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *pickingCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *bakeryCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *productionCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *produceCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *shelfTagCell;


@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)logIn_logOut:(id)sender;

@end
