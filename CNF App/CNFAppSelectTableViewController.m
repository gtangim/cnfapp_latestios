//
//  CNFAppSelectTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-08-12.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFAppSelectTableViewController.h"
#import "UserElement.h"
#import "JSONModel.h"
#import "ValidUserItems.h"
#import "CNFAppSyncSystem.h"
#import "SSKeychain.h"
#import "GZIP.h"
#import "BRLineReader.h"
#import "CNFGetDocStatus.h"
#import "CNFSubmitAttachment.h"
#import "CNFLoadMemory.h"
#import "CNFGetDocumentId.h"
#import "CNFAppEnums.h"
#import "CNFPendingFailedAlertGenerator.h"
#import <Crashlytics/Crashlytics.h>

@interface CNFAppSelectTableViewController ()
@property (nonatomic, strong) NSString * interVersion;
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic) int downloadCounter;
@property (nonatomic, strong) UIAlertController * statusAlert;
@end

typedef void (^Completion)(BOOL success, NSError * error, NSString * version);


@implementation CNFAppSelectTableViewController{
    NSManagedObjectContext * context;
    NSMutableData *downloadData;
    NSData *uploadData;
    BRRequestUpload *uploadFile;
    UIProgressView * progressView;
    UIAlertController * progressAlert;
}

@synthesize tableView=_tableView, productLookUpCell=_productLookUpCell, receivingCell=_receivingCell, outOfStockCell=_outOfStockCell, context=_context, adminCell=_adminCell, adjustmentCell=_adjustmentCell, printDiscardCell=_printDiscardCell, reverseAdjustmentCell=_reverseAdjustmentCell, pickingCell=_pickingCell, downloadCounter=_downloadCounter, statusAlert=_statusAlert, bakeryCell=_bakeryCell,productionCell=_productionCell, produceCell=_produceCell, shelfTagCell=_shelfTagCell;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];

    NSLog(@"-------------------------------------");
    NSLog(@"<<<<<<<<<<<<<<<<CRS_App>>>>>>>>>>>>>>");
    NSLog(@"-------------------------------------");
    
    NSLog(@"App Version Number: %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]);

    [[NSUserDefaults standardUserDefaults] setValue:[self getUniqueDeviceIdentifierAsString] forKey:@"DEVICE_ID"];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"PENDING_ALERT_SHOWN"];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BACK_PROCESS"];
    [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"QUEUE_START_TIME"];
    [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"USER_NAME"];
    [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"OFFICE"];
    [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"USERGROUPS"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    //    [self logAndResetQueueFlag];
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [self startBackGroundAttachmentSubmit];

    //start loggin queue running flag
//    [NSTimer scheduledTimerWithTimeInterval:30.0f target:self selector:@selector(logAndResetQueueFlag) userInfo:nil repeats:YES];

    //check pending documents and send alert
    [NSTimer scheduledTimerWithTimeInterval:500.0f target:self selector:@selector(getPendingSubmissionAlert) userInfo:nil repeats:YES];

    //check failed documents and send alert
    [NSTimer scheduledTimerWithTimeInterval:3600.0f target:self selector:@selector(getFailedDocumentAlert) userInfo:nil repeats:YES];

    //check to look for failed due to same VOS
    [NSTimer scheduledTimerWithTimeInterval:300.0f target:self selector:@selector(checkLastFailedVosAlert) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)checkAndGetLatestDatafile{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"DATA_FILE_DOWNLOAD_DATE"]!=nil){
        if([self daysBetween:[[NSUserDefaults standardUserDefaults] objectForKey:@"DATA_FILE_DOWNLOAD_DATE"]])
            [self testConnectionAndGetData];
    }else{
        [self testConnectionAndGetData];
    }
}

-(void)logAndResetQueueFlag{
    NSLog(@"Is Queue running : %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"BACK_PROCESS"]);
    NSLog(@"Queue start time: %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"QUEUE_START_TIME"]);
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"QUEUE_START_TIME"]){
        NSDate * date = [dateFormatter dateFromString:[[NSUserDefaults standardUserDefaults] valueForKey:@"QUEUE_START_TIME"]];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components: (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear )
                                                            fromDate:date
                                                              toDate:[NSDate date]
                                                             options:0];
        if([components minute]>10){
            NSLog(@"Queue is stuck for more than 10 minutes, resetting Queue flag...");
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BACK_PROCESS"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
}


-(void)checkLastFailedVosAlert{
    CNFPendingFailedAlertGenerator * new = [[CNFPendingFailedAlertGenerator alloc]init];
    NSArray* returnArray =[new getAlertDataforVosFailedDocuments];
    
    if(returnArray){
        NSMutableString *alertMsg = [[NSMutableString alloc] init];
        [alertMsg appendString:@"These documents failed to process for having the same PO number: \n"];
        
        for(NSObject * savedObject in returnArray){
            NSString * saveMsg = [NSString stringWithFormat:@"%@ by user:%@", [savedObject valueForKey:@"documentNo"], [savedObject valueForKey:@"userName"] ];
            [alertMsg appendString:@"\n"];
            [alertMsg appendString:saveMsg];
        }

        UIAlertController *pendingAlertController = [UIAlertController
                                                      alertControllerWithTitle:@"Please Goto Status and Resubmit"
                                                      message:[NSString stringWithFormat:@"%@", alertMsg]
                                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okaction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [pendingAlertController addAction:okaction];
        [self presentViewController:pendingAlertController animated:YES completion:nil];

    }else
        NSLog(@"No duplicate VOS failed from this handheld..");
}


-(void)getFailedDocumentAlert{
    
    CNFPendingFailedAlertGenerator * new = [[CNFPendingFailedAlertGenerator alloc]init];
    NSArray* returnArray =[new getAlertDataforFailedDocuments];
    
    if(returnArray){
        NSMutableString *alertMsg = [[NSMutableString alloc] init];
        [alertMsg appendString:@"These documents failed to process: \n"];
        
        for(NSObject * savedObject in returnArray){
            NSString * saveMsg = [NSString stringWithFormat:@"%@ by user:%@", [savedObject valueForKey:@"documentNo"], [savedObject valueForKey:@"userName"] ];
            [alertMsg appendString:@"\n"];
            [alertMsg appendString:saveMsg];
        }
        
        UIAlertController *pendingAlertController = [UIAlertController
                                                     alertControllerWithTitle:@"Please contact IT"
                                                     message:[NSString stringWithFormat:@"%@", alertMsg]
                                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okaction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [pendingAlertController addAction:okaction];
        [self presentViewController:pendingAlertController animated:YES completion:nil];
        
    }else
        NSLog(@"No new failed documents..");
}

-(void)getPendingSubmissionAlert{
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"PENDING_ALERT_SHOWN"] isEqualToString:@"NO"]){
        CNFPendingFailedAlertGenerator * new = [[CNFPendingFailedAlertGenerator alloc]init];
        NSArray* returnArray =[new getAlertDataforPendingDocument];
        
        if(returnArray){
            NSMutableString *alertMsg = [[NSMutableString alloc] init];
            [alertMsg appendString:@"These documents are not uploded yet: \n"];
            
            for(NSObject * savedObject in returnArray){
                NSString * saveMsg = [NSString stringWithFormat:@"%@ by user:%@", [savedObject valueForKey:@"documentNo"]?[savedObject valueForKey:@"documentNo"]:[savedObject valueForKey:@"documentType"], [savedObject valueForKey:@"userName"] ];
                [alertMsg appendString:@"\n"];
                [alertMsg appendString:saveMsg];
            }
            
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"PENDING_ALERT_SHOWN"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            UIAlertController *pendingAlertController = [UIAlertController
                                                         alertControllerWithTitle:@"Please contact IT"
                                                         message:[NSString stringWithFormat:@"%@", alertMsg]
                                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okaction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [self dismissViewControllerAnimated:YES completion:^{
                                               [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"PENDING_ALERT_SHOWN"];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                           }];
                                       }];
            
            [pendingAlertController addAction:okaction];
            [self presentViewController:pendingAlertController animated:YES completion:nil];

        }else
            NSLog(@"No currently saved and not uploaded document..");
    }else{
        NSLog(@"Already has a pending alert");
    }
}


-(void)viewWillAppear:(BOOL)animated{

    //everytime the screen loads, will check if the data file is updated today..
    [self checkAndGetLatestDatafile];

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"adjustmentObject"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SELECTED_LOG_FILE"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"VENDOR"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TO_LOCATION"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PICKING"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FILE_VERSION_V2"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DOCUMENT_NO"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MULTIPLE_UPC_PRODUCTS"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"VENDOR_PRODUCT"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"VOS_RECEIPT"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RECEIVING_OBJECT"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"VOS_ID"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SAVED_DOC"];
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"APP_CODE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    if([appDelegate.serverUrl isEqualToString:@"http://webapp.cnfltd.com"]){
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blueColor]};
        
        UIImageView * imageView = [[UIImageView alloc]init];
        [imageView setFrame:CGRectMake(0, 0, 40, 40)];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [imageView setImage:[UIImage imageNamed:@"demo_app.png"]];

        self.navigationItem.titleView = imageView;

    }else{
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
        [self.navigationController.navigationBar setBackgroundImage:NULL forBarMetrics:UIBarMetricsDefault];
    }

    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [self.navigationController setNavigationBarHidden:NO];

    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.downloadCounter=1;
    
    if([[NSUserDefaults standardUserDefaults]valueForKey:@"USER_NAME"]){
        self.navigationItem.title=[[NSUserDefaults standardUserDefaults]valueForKey:@"USER_NAME"];
        NSLog(@"Logged In user: %@", [[NSUserDefaults standardUserDefaults]valueForKey:@"USER_NAME"]);
        self.navigationItem.leftBarButtonItem.title=@"Log Out";
        self.navigationItem.rightBarButtonItem.enabled=YES;
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu2.png"] style:UIBarButtonItemStylePlain target:self action:@selector(settingsAlert)];

    }else{
        self.navigationItem.title=@"CNF App";
        self.navigationItem.backBarButtonItem.title=@"CNF App";
        self.navigationItem.leftBarButtonItem.title=@"Log In";
        self.navigationItem.rightBarButtonItem.enabled=NO;
        self.navigationItem.rightBarButtonItem=nil;
        self.navigationItem.rightBarButtonItem.title=@"";
        self.navigationItem.rightBarButtonItem.action = NULL;
    }
    
    BOOL outOfStock = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-OutOfStock-Auditor"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"Outofstockauditor"];
    
    if(!outOfStock) self.outOfStockCell.hidden=TRUE;
    else self.outOfStockCell.hidden=FALSE;
    
    BOOL receiving = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-Receivers"];
    
    if(!receiving){
        self.receivingCell.hidden=TRUE;
        self.printDiscardCell.hidden=TRUE;
    }else{
        self.receivingCell.hidden=FALSE;
        self.printDiscardCell.hidden=FALSE;
    }
    
    BOOL adjustment = FALSE;
    for(NSString * groupName in [[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"]){
        if([groupName rangeOfString:@"adjustment"].location == NSNotFound)
            continue;
        else
            adjustment=TRUE;
    }
    
    if(!adjustment) self.adjustmentCell.hidden=TRUE;
    else self.adjustmentCell.hidden=FALSE;
    
    BOOL reverseAdjustment =[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-adjustment-Reversal"];
    
    if(!reverseAdjustment) self.reverseAdjustmentCell.hidden=TRUE;
    else self.reverseAdjustmentCell.hidden=FALSE;
    
    BOOL picking =[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-Picking"];
    
    if(!picking) self.pickingCell.hidden=TRUE;
    else self.pickingCell.hidden=FALSE;

    BOOL bakery =[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-BakeryFulfillment"];

    if(!bakery) self.bakeryCell.hidden=TRUE;
    else self.bakeryCell.hidden=FALSE;

    BOOL production =[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-ProductionInventory"];

    if(!production) self.productionCell.hidden=TRUE;
    else self.productionCell.hidden=FALSE;

    BOOL produce = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-ProducePurchaser"];
    if(!produce) self.produceCell.hidden=TRUE;
    else self.produceCell.hidden=FALSE;

    BOOL shelfTag = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-ShelfTagPrinting"];
    if(!shelfTag) self.shelfTagCell.hidden=TRUE;
    else self.shelfTagCell.hidden=FALSE;

    BOOL admin = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-Admins"];
    
    if(!admin){
        self.adminCell.hidden=TRUE;
    }else{
        self.adminCell.hidden=FALSE;
        self.outOfStockCell.hidden=FALSE;
        self.receivingCell.hidden=FALSE;
        self.adjustmentCell.hidden=FALSE;
        self.printDiscardCell.hidden=FALSE;
        self.reverseAdjustmentCell.hidden=FALSE;
        self.pickingCell.hidden=FALSE;
        self.bakeryCell.hidden=FALSE;
        self.productionCell.hidden=FALSE;
        self.produceCell.hidden=FALSE;
        self.shelfTagCell.hidden=false;
    }
    [self.tableView reloadData];
}


#pragma mark - Put DeviceID in keychain

-(NSString *)getUniqueDeviceIdentifierAsString
{
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];

    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:@"incoding"];
    if (strApplicationUUID == nil)
    {
        strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SSKeychain setPassword:strApplicationUUID forService:appName account:@"incoding"];
    }
    return strApplicationUUID;
}



#pragma mark - background submit attachment funcitons

- (void) startBackGroundAttachmentSubmit{
    [NSTimer scheduledTimerWithTimeInterval:60.0f
                                     target:self selector:@selector(attachmentTheradSelector) userInfo:nil repeats:YES];
}
-(void) attachmentTheradSelector{
    if([[UIApplication sharedApplication]applicationState]==UIApplicationStateBackground){
        NSLog(@"in background, ATTACHMENT SUBMISSION shouldn't run");
    }else{
        
        if([[[NSUserDefaults standardUserDefaults]valueForKey:@"BACK_PROCESS"]isEqualToString:@"YES"]){
            NSLog(@"already a background process running..");
        }else{
            CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
            Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];
            NetworkStatus netStatus = [reachability currentReachabilityStatus];
            if(netStatus==1){
                [NSThread detachNewThreadSelector:@selector(getBackgroundAttachment) toTarget:self withObject:nil];
            }else{
                NSLog(@"No connection for sync...");
            }
        }
    }
}
- (void) getBackgroundAttachment{
//    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
//    Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];
//
//    NetworkStatus netStatus = [reachability currentReachabilityStatus];
//
//    if(netStatus==1){
        CNFSubmitAttachment * submitAttachement = [[CNFSubmitAttachment alloc]init];
        [submitAttachement startSubmittingAttchment];
//    }else{
//        NSLog(@"No connection for sync...");
//    }
}

#pragma mark - Sync Process methods
- (void)performSyncInBackground:(void (^)(void))block {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        block();
    });
}
- (void) startSync{
    [NSTimer scheduledTimerWithTimeInterval:60.0f
                                     target:self selector:@selector(thredSelector) userInfo:nil repeats:YES];
}
-(void) thredSelector{
    if([[UIApplication sharedApplication]applicationState]==UIApplicationStateBackground){
        NSLog(@"in background, SYNC shouldn't run");
    }else{
        CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
        Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];

        NetworkStatus netStatus = [reachability currentReachabilityStatus];

        if(netStatus==1){
            [NSThread detachNewThreadSelector:@selector(syncTimer:) toTarget:self withObject:nil];
        }else{
            NSLog(@"No connection for sync...");
        }
    }
}
- (void) syncTimer:(NSTimer *)timer{
    
//    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
//    Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];
//
//    NetworkStatus netStatus = [reachability currentReachabilityStatus];
//
//    if(netStatus==1){
        CNFAppSyncSystem *sync=[[CNFAppSyncSystem alloc]init];
        [sync startSync];
//    }else{
//        NSLog(@"No connection for sync...");
//    }
}

#pragma mark - connection tester

-(int)connectionTester{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];

    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
    if(netStatus==1){
        NSLog(@"connection OK...");
    }else{
        NSLog(@"No connection for validation...");
    }
    
    return netStatus;
}

#pragma mark - webservice functions

-(void)testConnectionAndGetData{
    if([self connectionTester]!=0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getDataFileVersion];
        });
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.downloadCounter++;
            
            if(self.downloadCounter>3)
                [self showErrorAlert:@"No Connection for Data Fetch..Please Try Again.."];
            else
                [self testConnectionAndGetData];

        });
    }
}

-(void)startGetData{

    NSLog(@"start get data......");
    
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentDir stringByAppendingPathComponent:@"db.json.gz"];
    
    NSString *post =   @"{\"userName\":\"CNFMobileSystem\",\"token\":\"YJVLNEVFZA\", \"applicationName\":\"cnfmobile\",\"dataFileVersion\":\"1\",\"deviceId\":\"TestDevice\"}";

    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getDataV1", appDelegate.serverUrl]]];

    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              NSLog(@"Download Error:%@",error.description);
                                              //re try downloading data file
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  if(self.downloadCounter>3){
                                                      [self.statusAlert dismissViewControllerAnimated:YES completion:^{
                                                          [self showErrorAlert:@"No Connection, please restart"];
                                                      }];
                                                  }else
                                                      [self testConnectionAndGetData];
                                              });

                                          }
                                          if (data) {
                                              NSDictionary *strings = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                              if(error){
                                                  NSLog(@"web service error, can't serialize data..");
//                                                  [self stopSpinner];
                                                  //re try downloading data file

                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self.statusAlert dismissViewControllerAnimated:YES completion:^{
                                                          self.downloadCounter++;
                                                          
                                                          if(self.downloadCounter>3){
                                                              [self.statusAlert dismissViewControllerAnimated:YES completion:^{
                                                                  [self showErrorAlert:@"Web service error, Restart app.."];
                                                              }];
                                                          }else
                                                              [self testConnectionAndGetData];
                                                      }];
                                                  });

                                              }else{
                                                  if([strings valueForKey:@"Message"]==Nil){
                                                      NSData *nsdataFromBase64String = [[NSData alloc] initWithBase64EncodedString:[strings objectForKey:@"d"] options:0];
                                                      
                                                      [nsdataFromBase64String writeToFile:filePath atomically:YES];
                                                      NSLog(@"Downloaded data File is saved to %@",filePath);
                                                      [self unzipFile];
                                                  }else{
                                                      NSLog(@"error msg: %@", [strings valueForKey:@"Message"]);
                                                      //re try downloading data file
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          self.downloadCounter++;
                                                          if(self.downloadCounter>3){
                                                              [self dismissViewControllerAnimated:YES completion:^{
                                                                  [self showErrorAlert:[strings valueForKey:@"Message"]];
                                                              }];
                                                          }
                                                          else{
                                                              [self dismissViewControllerAnimated:YES completion:^{
                                                                  [self testConnectionAndGetData];
                                                              }];
                                                          }
                                                      });
                                                  }
                                              }
                                          }
                                      }];
    [dataTask resume];
}
-(void)getDataFileVersion{

    if(self.statusAlert){
        self.statusAlert=nil;
        [self startSpinner:[NSString stringWithFormat:@"Getting Data, Attempt: %d",self.downloadCounter]];
    }else
        [self startSpinner:[NSString stringWithFormat:@"Getting Data, Attempt: %d",self.downloadCounter]];

    NSString *post = @"{\"userName\":\"CNFMobileSystem\",\"token\":\"YJVLNEVFZA\", \"deviceId\":\"TestDevice\", \"appId\":\"CNFMobileV1\"}";
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getDataFileVersionV1", appDelegate.serverUrl]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              NSLog(@"Download Error:%@",error.description);
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [self.statusAlert dismissViewControllerAnimated:YES completion:^{
                                                      NSLog(@"connection didFailWithError: %@ %@", error.localizedDescription,
                                                            [error.userInfo objectForKey:NSURLErrorFailingURLStringErrorKey]);
                                                      [self showErrorAlert:@"Connection error with server"];
                                                  }];
                                              });
                                          }else{
                                              if (data) {
                                                  id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                  
                                                  if(error){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
//                                                          [self stopSpinner];
                                                          
                                                          [self.statusAlert dismissViewControllerAnimated:YES completion:^{
                                                              NSLog(@"Data file version serialization error..");
                                                              [self showErrorAlert:@"Error serializing data from webservice, please restart"];
                                                          }];
                                                      });
                                                  }else{
                                                      if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              
                                                              [self.statusAlert dismissViewControllerAnimated:YES completion:^{
                                                                  NSLog(@"Data file version check Msg sent..");
                                                                  [self showErrorAlert:[dictionary valueForKey:@"Message"]];
                                                              }];
                                                          });
                                                          
                                                      }else{
                                                          NSLog(@"Current data file version on phone:  %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]);
                                                          
                                                          if([[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]isEqualToString:[dictionary valueForKey:@"d"]]){
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  [self.statusAlert dismissViewControllerAnimated:YES completion:^{
                                                                      NSLog(@"Data file up to date");
                                                                      [self showErrorAlert:@"Data file is up to date"];
                                                                  }];
                                                              });
                                                          }else{
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  self.interVersion = [dictionary valueForKey:@"d"];
                                                                  [self startGetData];
                                                              });
                                                          }
                                                      }
                                                  }
                                              }
                                          }
                                      }];
    [dataTask resume];
    
}



#pragma mark - nsdate conversion functions

- (NSDate*) getDateFromJSON:(NSString *)dateString{
    // Expect date in this format "/Date(1268123281843)/"
    int startPos = (int)[dateString rangeOfString:@"("].location+1;
    int endPos = (int)[dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    //    NSLog(@"%llu",milliseconds);
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}


#pragma mark - unzip function

-(void)unzipFile{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json.gz"];
    NSString *destinationPath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir,@"db.json"];
    
    NSData *gzData = [NSData dataWithContentsOfFile:filepath];
    NSData *ungzippedData = [gzData gunzippedData];
    
    [ungzippedData writeToFile:destinationPath atomically:YES];
    
    NSLog(@"done unzipping file..");
    
    if([self validateFileSizeEnd])
         [self readFileHeader];
    else{
        [self stopSpinner];
        //re try downloading data file
        
        self.downloadCounter++;
        
        if(self.downloadCounter>3){
            NSLog(@"sending corrupted file to FTP..");
            [self uploadCorruptedFile];
        }
//            [self showErrorAlert:@"Error reading file, corrupt data file"];
        else
            [self testConnectionAndGetData];
    }
}

#pragma mark - read file header

-(void)readFileHeader{
    NSLog(@"start loading to memory..");
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    
    BRLineReader * line =[[BRLineReader alloc]initWithFile:filepath encoding:NSUTF8StringEncoding];
    NSString * firstLine= [line readLine];
    
    NSError * serializerError;
    
    NSData *data = [firstLine dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary * jsonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&serializerError];
    
    if(serializerError){
        NSLog(@"Could not serialize..");
        [self stopSpinner];
        
        //re try downloading data file
        self.downloadCounter++;
        
        if(self.downloadCounter>3)
            [self showErrorAlert:@"Serialization error, data file not downloaded properly"];
        else
            [self testConnectionAndGetData];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithLong:firstLine.length+6] forKey:@"START_OFFSET"];
        [[NSUserDefaults standardUserDefaults] setObject:[jsonDic objectForKey:@"DataList"] forKey:@"DATA_LIST"];
        [[NSUserDefaults standardUserDefaults] setValue:self.interVersion forKey:@"FILE_VERSION"];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"DATA_FILE_DOWNLOAD_DATE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
//        [self stopSpinner];
        NSLog(@"Initial procedures completed...");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.statusAlert dismissViewControllerAnimated:YES completion:nil];
        });
        
    }
}


#pragma mark - upload corrupted file to FTP funcitons

- (void) uploadCorruptedFile
{
    //----- get the file to upload as an NSData object
    
    NSLog(@"start time for file upload: %@", [NSDate date]);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* finalPath = [documentsDirectory stringByAppendingPathComponent:@"db.json.gz"];
    
    uploadData = [NSData dataWithContentsOfFile: finalPath];
    uploadFile = [[BRRequestUpload alloc] initWithDelegate:self];
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString * uploadFileName = [NSString stringWithFormat:@"%@_%@.gz", [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_SHORT_ID"],[dateFormatter stringFromDate:[NSDate date]]];
    uploadFile.path = [NSString stringWithFormat:@"/Corrupted_files_CNFApp/%@",uploadFileName];
    
    NSLog(@"started uploading to: %@", uploadFile.path);
    
    //    uploadFile.path=@"/CNFApp_Logs/testingCnf.txt";
    
    uploadFile.hostname = @"webapp.cnfltd.com";
    uploadFile.username = @"testusers5";
    uploadFile.password = @"P@ssw0rd";
    
    [uploadFile start];
    
    progressAlert = [UIAlertController alertControllerWithTitle: nil
                                                      message: @"Uploading Corrupted File"
                                               preferredStyle: UIAlertControllerStyleAlert];
    
    UIViewController *customVC     = [[UIViewController alloc] init];
    
    progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    progressView.frame = CGRectMake(20, 20, 200, 15);
    progressView.bounds = CGRectMake(0, 0, 200, 100);
    progressView.backgroundColor = [UIColor whiteColor];
    
    [customVC.view addSubview:progressView];
    
    [customVC.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem: progressView
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:customVC.view
                                  attribute:NSLayoutAttributeCenterX
                                  multiplier:1.0f
                                  constant:0.0f]];
    
    
    [customVC.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem: progressView
                                  attribute:NSLayoutAttributeCenterY
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:customVC.view
                                  attribute:NSLayoutAttributeCenterY
                                  multiplier:1.0f
                                  constant:0.0f]];
    
    [progressAlert setValue:customVC forKey:@"contentViewController"];
    
    [self presentViewController: progressAlert
                       animated: true
                     completion: nil];
}

- (void) cancelAction
{
    if (uploadFile)
    {
        uploadFile.cancelDoesNotCallDelegate = TRUE;
        [uploadFile cancelRequestWithFlag];
    }
}

//- (void) requestDataAvailable: (BRRequestDownload *) request;
//{
//    [downloadData appendData: request.receivedData];
//}

-(BOOL) shouldOverwriteFileWithRequest: (BRRequest *) request
{
    //----- set this as appropriate if you want the file to be overwritten
    if (request == uploadFile)
    {
        //----- if uploading a file, we set it to YES
        return YES;
    }
    
    //----- anything else (directories, etc) we set to NO
    return NO;
}


- (void) percentCompleted: (BRRequest *) request
{
    progressView.progress = request.percentCompleted;
    
//    NSLog(@"%f completed...", request.percentCompleted);
//    NSLog(@"%ld bytes this iteration", request.bytesSent);
//    NSLog(@"%ld total bytes", request.totalBytesSent);
}

- (long) requestDataSendSize: (BRRequestUpload *) request
{
    //----- user returns the total size of data to send. Used ONLY for percentComplete
    return [uploadData length];
}

- (NSData *) requestDataToSend: (BRRequestUpload *) request
{
    //----- returns data object or nil when complete
    //----- basically, first time we return the pointer to the NSData.
    //----- and BR will upload the data.
    //----- Second time we return nil which means no more data to send
    NSData *temp = uploadData;                                                  // this is a shallow copy of the pointer, not a deep copy
    
    uploadData = nil;                                                           // next time around, return nil...
    
    return temp;
}

-(void) requestCompleted: (BRRequest *) request
{
    if (request == uploadFile)
    {
        [progressAlert dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"%@ completed!", request);
        uploadFile = nil;
        
        NSLog(@"Upload file completing time: %@", [NSDate date]);
    }
}


-(void) requestFailed:(BRRequest *) request
{
    if (request == uploadFile)
    {
        NSLog(@"%@", request.error.message);
        uploadFile = nil;

        [progressAlert dismissViewControllerAnimated:YES completion:nil];
        uploadFile = nil;

        NSLog(@"Upload file failed");
    }
}


#pragma mark - check date period

- (BOOL)daysBetween:(NSDate *)dt1{
    NSDateComponents *componentsFromPrevious = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:dt1];
    NSDateComponents *componentsCurrent = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:[NSDate date]];
    if(componentsCurrent.day==componentsFromPrevious.day)
        return FALSE;
    else
        return TRUE;
}

-(BOOL)validateFileSizeEnd{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    [file seekToFileOffset:[file seekToEndOfFile]-15];
    NSData *databuffer = [file readDataOfLength:15];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    if([jsonStr rangeOfString:@"####"].location != NSNotFound){
        NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        long long fileSize= [[[jsonStr componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""] longLongValue];
        if(fileSize == [file seekToEndOfFile]){
           [file closeFile];
           return TRUE;
        }else{
            [file closeFile];
            return FALSE;
        }
    }else{
        [file closeFile];
        return FALSE;
    }
}


#pragma mark - alertview and indicator spin handler

- (void)startSpinner:(NSString *)message {
    if (!self.statusAlert){
        self.statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *customVC     = [[UIViewController alloc] init];
        
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [self.statusAlert setValue:customVC forKey:@"contentViewController"];
    }
    [self presentViewController: self.statusAlert
                           animated: true
                         completion: nil];

}

- (void)stopSpinner {
    if (self.statusAlert) {
        [self.statusAlert dismissViewControllerAnimated:YES completion:nil];
        self.statusAlert = nil;
    }
}


#pragma mark - tableview functions

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
    footer.textLabel.textAlignment = NSTextAlignmentCenter;
}

- (NSString *)tableView:(UITableView *)table titleForFooterInSection:(NSInteger)section{
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if([appDelegate.serverUrl isEqualToString:@"http://webapp.cnfltd.com"]){
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 30)];
        [footerView setBackgroundColor:[UIColor redColor]];
        return [NSString stringWithFormat:@"App Version Number: %@, Device Short ID: %@, DEMO APP, NOT IN PRODUCTION", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"], [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_SHORT_ID"]];
    }else{
        return [NSString stringWithFormat:@"App Version Number: %@, Device Short ID: %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"], [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_SHORT_ID"]];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    BOOL outOfStock = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-OutOfStock-Auditor"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"Outofstockauditor"];
    BOOL receiving = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-Receivers"];
    BOOL admin = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-Admins"];
    BOOL adjustment = FALSE;
    for(NSString * groupName in [[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"]){
        if([groupName rangeOfString:@"adjustment"].location == NSNotFound)
            continue;
        else
            adjustment=TRUE;
    }
    BOOL reverseAdjustment =[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-adjustment-Reversal"];
    BOOL picking =[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-Picking"];
    BOOL bakery =[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-BakeryFulfillment"];
    BOOL production =[[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-ProductionInventory"];
    BOOL produce = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-ProducePurchaser"];
    BOOL shelfTag = [[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-ShelfTagPrinting"];


    if(indexPath.section==0 && indexPath.row==0)
        return 90;
    else if(indexPath.section==0 && indexPath.row==1){
        if(admin){
            return 90;
        }else{
            if(outOfStock)
                return 90;
            else
                return 0;
        }

    }else if(indexPath.section==0 && indexPath.row==2){
        
        if(admin){
            return 90;
        }else{
            if(receiving)
                return 90;
            else
                return 0;
        }

    }else if(indexPath.section==0 && indexPath.row==3){
        if(admin){
            return 90;
        }else{
            if(adjustment)
                return 90;
            else
                return 0;
        }
        
    }else if(indexPath.section==0 && indexPath.row==4){
        if(admin){
            return 90;
        }else{
            if(receiving)
                return 90;
            else
                return 0;
        }
    }else if(indexPath.section==0 && indexPath.row==5){
        if(admin){
            return 90;
        }else{
            if(reverseAdjustment)
                return 90;
            else
                return 0;
        }
    }else if(indexPath.section==0 && indexPath.row==6){
        if(admin){
            return 90;
        }else{
            if(picking)
                return 90;
            else
                return 0;
        }
    }else if(indexPath.section==0 && indexPath.row==7){
        if(admin){
            return 90;
        }else{
            if(bakery)
                return 90;
            else
                return 0;
        }
    }else if(indexPath.section==0 && indexPath.row==8){
        if(admin){
            return 90;
        }else{
            if(production)
                return 90;
            else
                return 0;
        }
    }else if(indexPath.section==0 && indexPath.row==9){
        if(admin){
            return 90;
        }else{
            if(produce)
                return 90;
            else
                return 0;
        }
    }else if(indexPath.section==0 && indexPath.row==10){
        if(admin){
            return 90;
        }else{
            if(shelfTag)
                return 90;
            else
                return 0;
        }
    }else if(indexPath.section==0 && indexPath.row==11){
        if(admin)
            return 90;
        else
            return 0;
    }

    return 90;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==0 && indexPath.row==0){
        [[NSUserDefaults standardUserDefaults]setObject:@"PRODUCT_LOOKUP" forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self performSegueWithIdentifier:@"productLookUpSegue" sender:self];
    }
    
    if(indexPath.section==0 && indexPath.row==1){

        //check if the data file is upto date, if not download the latest data file.
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            [self getDataFileVersionWithError:^(BOOL success, NSError *error, NSString *version) {
                if(success){
                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]isEqualToString:version]){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                            [[NSUserDefaults standardUserDefaults]setObject:@"OUT_OF_STOCK_AUDIT_SUBMISSION" forKey:@"APP_CODE"];
                            [[NSUserDefaults standardUserDefaults]synchronize];

                            if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]isEqualToString:@"A1"]){
                                [self performSegueWithIdentifier:@"outOfStockSegue" sender:self];
                            }else{
                                UIAlertController *finishedAlertController = [UIAlertController
                                                                              alertControllerWithTitle:@"Request Error"
                                                                              message:@"User can not perform Out-of-Stock audit from Localtion A1, Please login again and select a suitable location"
                                                                              preferredStyle:UIAlertControllerStyleAlert];

                                UIAlertAction *okaction = [UIAlertAction
                                                           actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action)
                                                           {
                                                               NSLog(@"User not permitted to do Out-of-stock");
                                                               [self dismissViewControllerAnimated:YES completion:nil];
                                                           }];

                                [finishedAlertController addAction:okaction];
                                [self presentViewController:finishedAlertController animated:YES completion:nil];
                            }
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                            [self showErrorAlert:@"Data file is outdated, please get latest data file to continue"];
                        });
                    }
                }else{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    [self showErrorAlert:[NSString stringWithFormat:@"Error Occured: %@", error.localizedDescription]];
                }
            }];
    }
    
    if(indexPath.section==0 && indexPath.row==2){
        if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]isEqualToString:@"A1"]){
            
            
            UIAlertController *receivingAlertController = [UIAlertController
                                                          alertControllerWithTitle:nil
                                                          message:nil
                                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelaction = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction *action)
                                       {
                                          [self dismissViewControllerAnimated:YES completion:nil];
                                       }];
            
            UIAlertAction *vosAction = [UIAlertAction
                                           actionWithTitle:@"Receive with Order Sheet"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
//                                               [self dismissViewControllerAnimated:YES completion:^{
                                                   [[NSUserDefaults standardUserDefaults]setValue:NULL forKey:@"SAVED_DOC"];
                                                   [[NSUserDefaults standardUserDefaults]setObject:@"VOS_RECEIVE" forKey:@"APP_CODE"];
                                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                                   [self performSegueWithIdentifier:@"vosTransferSelectSague" sender:self];
//                                               }];
                                           }];
            
            UIAlertAction *receiptAction = [UIAlertAction
                                           actionWithTitle:@"Receive without Order Sheet"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
//                                               [self dismissViewControllerAnimated:YES completion:^{
                                                   [[NSUserDefaults standardUserDefaults]setObject:@"RECEIPT_RECEIVE" forKey:@"APP_CODE"];
                                                   [[NSUserDefaults standardUserDefaults]setValue:NULL forKey:@"SAVED_DOC"];
                                                   [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"VENDOR"];
                                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                                   [self performSegueWithIdentifier:@"selectVendorForReceiptCreationSague" sender:self];
//                                               }];
                                           }];
            
            UIAlertAction *txcreateAction = [UIAlertAction
                                           actionWithTitle:@"Create Transfer"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
//                                               [self dismissViewControllerAnimated:YES completion:^{
                                                   [[NSUserDefaults standardUserDefaults]setObject:@"TRANSFER_CREATION" forKey:@"APP_CODE"];
                                                   [[NSUserDefaults standardUserDefaults]setValue:NULL forKey:@"SAVED_DOC"];
                                                   [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"TO_LOCATION"];
                                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                                   [self performSegueWithIdentifier:@"selectLocationForTransferCreationSague" sender:self];
//                                               }];
                                           }];
            
            UIAlertAction *txReceiveAction = [UIAlertAction
                                             actionWithTitle:@"Receive Transfer"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction *action)
                                             {
//                                                 [self dismissViewControllerAnimated:YES completion:^{
                                                     [[NSUserDefaults standardUserDefaults]setObject:@"TRANSFER_RECEIVE" forKey:@"APP_CODE"];
                                                     [[NSUserDefaults standardUserDefaults]setValue:NULL forKey:@"SAVED_DOC"];
                                                     [[NSUserDefaults standardUserDefaults]synchronize];
                                                     [self performSegueWithIdentifier:@"vosTransferSelectSague" sender:self];
//                                                 }];
                                             }];
            
            UIAlertAction *produceReceiveAction = [UIAlertAction
                                              actionWithTitle:@"Produce Receiving"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction *action)
                                              {
                                                      [[NSUserDefaults standardUserDefaults]setObject:@"PRODUCE_RECEIVE" forKey:@"APP_CODE"];
                                                      [[NSUserDefaults standardUserDefaults]setValue:NULL forKey:@"SAVED_DOC"];
                                                      [[NSUserDefaults standardUserDefaults]synchronize];
                                                      [self performSegueWithIdentifier:@"produceReceiveSegue" sender:self];

                                              }];

            UIAlertAction *invoiceLookup = [UIAlertAction
                                                  actionWithTitle:@"Invoice Lookup"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action)
                                                  {
                                                      [[NSUserDefaults standardUserDefaults]setObject:@"INVOICE_LOOKUP" forKey:@"APP_CODE"];
                                                      [[NSUserDefaults standardUserDefaults]setValue:NULL forKey:@"SAVED_DOC"];
                                                      [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"VENDOR"];
                                                      [[NSUserDefaults standardUserDefaults]synchronize];
                                                      [self performSegueWithIdentifier:@"invoiceLookUpSegue" sender:self];
                                                      
                                                  }];

            [receivingAlertController addAction:vosAction];
            [receivingAlertController addAction:receiptAction];
            [receivingAlertController addAction:txcreateAction];
            [receivingAlertController addAction:txReceiveAction];
            //manual receiving option changed to produce receiving this version
            [receivingAlertController addAction:produceReceiveAction];
            [receivingAlertController addAction:invoiceLookup];
            [receivingAlertController addAction:cancelaction];
            
            [self presentViewController:receivingAlertController animated:YES completion:nil];
            
        }else{
            UIAlertController *noPermitAlertController = [UIAlertController
                                                          alertControllerWithTitle:@"Request Error"
                                                          message:@"User can not perform Out-of-Stock audit from Localtion A1, Please login again and select a suitable location"
                                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okaction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"User not permitted to do receiving");
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }];
            
            [noPermitAlertController addAction:okaction];
            [self presentViewController:noPermitAlertController animated:YES completion:nil];

        }
    }
    
    //adjustment selection action
    
    if(indexPath.section==0 && indexPath.row==3){
        [[NSUserDefaults standardUserDefaults]setObject:@"ADJUSTMENT" forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults]synchronize];
            [self performSegueWithIdentifier:@"adjustmentSegue" sender:self];
    }
    
    if(indexPath.section==0 && indexPath.row==4){
        [[NSUserDefaults standardUserDefaults]setObject:@"ADJ_NFP_DISCARD_SHIP" forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self performSegueWithIdentifier:@"printDiscardSegue" sender:self];
    }
    
    if(indexPath.section==0 && indexPath.row==5){
        [[NSUserDefaults standardUserDefaults]setObject:@"REVERSE_ADJUSTMENT" forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self performSegueWithIdentifier:@"adjustmentSegue" sender:self];
    }

    if(indexPath.section==0 && indexPath.row==6){
        [[NSUserDefaults standardUserDefaults]setValue:NULL forKey:@"PICKING"];
        [[NSUserDefaults standardUserDefaults]setObject:@"PICKING" forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self performSegueWithIdentifier:@"pickingSegue" sender:self];
    }

    if(indexPath.section==0 && indexPath.row==7){
        //check if the data file is updated for doing bakery order processing..If not tell the user to go do that
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        [self getDataFileVersionWithError:^(BOOL success, NSError *error, NSString *version) {
            if(success){
                if([[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]isEqualToString:version]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                        [[NSUserDefaults standardUserDefaults]setObject:@"BAKERY_ORDERING" forKey:@"APP_CODE"];
                        [[NSUserDefaults standardUserDefaults]synchronize];

                        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]isEqualToString:@"B1"]){
                            [self performSegueWithIdentifier:@"bakerySegue" sender:self];
                        }else{
                            UIAlertController *finishedAlertController = [UIAlertController
                                                                          alertControllerWithTitle:@"Request Error"
                                                                          message:@"User can not perform bakery ordering from any other location, Please login again and select a suitable location"
                                                                          preferredStyle:UIAlertControllerStyleAlert];

                            UIAlertAction *okaction = [UIAlertAction
                                                       actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action)
                                                       {
                                                           NSLog(@"User not permitted to do Bakery Order Management");
                                                           [self dismissViewControllerAnimated:YES completion:nil];
                                                       }];

                            [finishedAlertController addAction:okaction];
                            [self presentViewController:finishedAlertController animated:YES completion:nil];
                        }
                    });
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                        [self showErrorAlert:@"Data file is outdated, please get latest data file to continue"];
                    });
                }
            }else{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                [self showErrorAlert:[NSString stringWithFormat:@"Error Occured: %@", error.localizedDescription]];
            }
        }];
    }

    if(indexPath.section==0 && indexPath.row==8){
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]isEqualToString:@"S8"]){
            [[NSUserDefaults standardUserDefaults]setObject:@"PRODUCTION_INVENTORY" forKey:@"APP_CODE"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self performSegueWithIdentifier:@"productionInventorySegue" sender:self];
        }else{
            UIAlertController *finishedAlertController = [UIAlertController
                                                          alertControllerWithTitle:@"Request Error"
                                                          message:@"User can not perform Production Inventory from any other location, Please login again and select a suitable location"                                                                                                                        preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *okaction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"User not permitted to do bakery order management");
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }];

            [finishedAlertController addAction:okaction];
            [self presentViewController:finishedAlertController animated:YES completion:nil];
        }
    }

    if(indexPath.section==0 && indexPath.row==9){
        [[NSUserDefaults standardUserDefaults]setObject:@"PRODUCE_TRACKING" forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self performSegueWithIdentifier:@"producePurchaseSegue" sender:self];
    }

    if(indexPath.section==0 && indexPath.row==10){
        [[NSUserDefaults standardUserDefaults]setObject:@"SHELF_TAG_BATCH_CREATE" forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self performSegueWithIdentifier:@"shelfTagPrintSegue" sender:self];
    }

    if(indexPath.section==0 && indexPath.row==11){
        [self performSegueWithIdentifier:@"adminSegue" sender:self];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

#pragma mark - alertview delegate

-(void)showErrorAlert:(NSString *)message{
    
    UIAlertController *errorAlertController = [UIAlertController
                                                 alertControllerWithTitle:nil
                                                 message:[NSString stringWithFormat:@"%@", message]
                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)logOutAlert{
   
    UIAlertController *logoutAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", @"Are you sure you want to Log Out?"]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesaction = [UIAlertAction
                               actionWithTitle:@"YES"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                       NSLog(@"%@ user logout...", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"]);
                                       [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"USER_NAME"];
                                       [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"OFFICE"];
                                       [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"APP_CODE"];
                                       [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"USERGROUPS"];
                                       
                                       [[NSUserDefaults standardUserDefaults]synchronize];
                                       
                                       [self viewWillAppear:YES];
                               }];
    
    UIAlertAction *noaction = [UIAlertAction
                                actionWithTitle:@"NO"
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction *action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }];
    
    [logoutAlertController addAction:yesaction];
    [logoutAlertController addAction:noaction];
    [self presentViewController:logoutAlertController animated:YES completion:nil];
    
}


-(void)settingsAlert{
    
    UIAlertController *settingsAlertController = [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:nil
                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *showStatusaction = [UIAlertAction
                                actionWithTitle:@"Show Status"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                        [self performSegueWithIdentifier:@"statusViewSague" sender:self];
                                   
                                }];
    
    UIAlertAction *getLatestDataaction = [UIAlertAction
                                       actionWithTitle:@"Get Latest Data"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                               [self testConnectionAndGetData];
                                       }];
    
    UIAlertAction *incompleteDocumentaction = [UIAlertAction
                                       actionWithTitle:@"Incomplete Documents"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                               [self performSegueWithIdentifier:@"pendingDocumentSegue" sender:self];

                                       }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [settingsAlertController addAction:showStatusaction];
    [settingsAlertController addAction:getLatestDataaction];
    [settingsAlertController addAction:incompleteDocumentaction];
    [settingsAlertController addAction:cancelAction];
    [self presentViewController:settingsAlertController animated:YES completion:nil];
    
}

- (IBAction)logIn_logOut:(id)sender {
    if(![[NSUserDefaults standardUserDefaults]valueForKey:@"USER_NAME"])
       [self performSegueWithIdentifier:@"userLoginSegue" sender:self];
    else{
        [self logOutAlert];
    }
}

-(void)getDataFileVersionWithError:(Completion)block{
    NSString *post = @"{\"userName\":\"CNFMobileSystem\",\"token\":\"YJVLNEVFZA\", \"deviceId\":\"TestDevice\", \"appId\":\"CNFMobileV1\"}";
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getDataFileVersionV1", appDelegate.serverUrl]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              if(block){
                                                  block(FALSE, error, NULL);
                                              }
                                          }else{
                                              if (data) {
                                                  id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

                                                  if(error){
                                                      if(block){
                                                          block(FALSE, error, NULL);
                                                      }
                                                  }else{
                                                      if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                          NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
                                                          [errorDetail setValue:@"Failed to get message from server" forKey:NSLocalizedDescriptionKey];
                                                          if(block){
                                                              block(FALSE, [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail], NULL);
                                                          }

                                                      }else{
                                                          if(block){
                                                              block(TRUE, NULL, [dictionary valueForKey:@"d"]);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
                                      }];
    [dataTask resume];
}



@end
