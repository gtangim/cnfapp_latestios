//
//  CNFAppSpecTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-11-25.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"

@interface CNFAppSpecTableViewController : UITableViewController<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *versionNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataFileVersionLabel;
@property (weak, nonatomic) IBOutlet UILabel *deviceShortIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *deviceMacLabel;

@property (weak, nonatomic) IBOutlet UILabel *onlineIntervalLabel;

@property (weak, nonatomic) IBOutlet UISwitch *productionSwitch;
- (IBAction)stepperValueChanged:(id)sender;

- (IBAction)switchChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UIStepper *intervalStepper;

@end
