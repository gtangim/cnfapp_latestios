//
//  CNFAppSpecTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-11-25.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFAppSpecTableViewController.h"
#import "CNFAppEnums.h"

@interface CNFAppSpecTableViewController ()

@end

@implementation CNFAppSpecTableViewController

@synthesize versionNoLabel=_versionNoLabel, dataFileVersionLabel=_dataFileVersionLabel, deviceMacLabel=_deviceMacLabel, deviceShortIdLabel=_deviceShortIdLabel, productionSwitch=_productionSwitch, onlineIntervalLabel=_onlineIntervalLabel, intervalStepper=_intervalStepper;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.versionNoLabel.text=[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    self.dataFileVersionLabel.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"];
    self.deviceShortIdLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_SHORT_ID"];
    self.deviceMacLabel.text = [[[UIDevice currentDevice] identifierForVendor] UUIDString];

    self.onlineIntervalLabel.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_INTERVAL"] stringValue];
    
    NSLog(@"%@", [[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_INTERVAL"] stringValue]);
    
    self.intervalStepper.value = [[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_INTERVAL"] doubleValue];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if([appDelegate.serverUrl isEqualToString:@"http://webapp.cnfltd.com"]){
        [self.productionSwitch setOn:NO];
    }else{
        [self.productionSwitch setOn:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)stepperValueChanged:(id)sender {
    self.onlineIntervalLabel.text = [NSString stringWithFormat:@"%d", (int)[self.intervalStepper value]];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:(int)[self.intervalStepper value]] forKey:@"ONLINE_TROUBLE_INTERVAL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)switchChanged:(id)sender {
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    if([appDelegate.serverUrl isEqualToString:@"http://webapp.cnfltd.com"]){
        appDelegate.serverUrl=@"http://opentapsapp.cnfltd.com";
    }else{
        appDelegate.serverUrl=@"http://webapp.cnfltd.com";
    }
    
    [[NSUserDefaults standardUserDefaults]setValue:appDelegate.serverUrl forKey:@"CONNECTION"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIAlertController *restartAlertController = [UIAlertController
                                               alertControllerWithTitle:@"Restart App"
                                               message:@"Pressing OK will close the app and take effect from next start"
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   exit(0);
                               }];
    
    [restartAlertController addAction:okaction];
    [self presentViewController:restartAlertController animated:YES completion:nil];

}

//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(alertView.tag==restartAlerts){
//        if(buttonIndex==0){
//            exit(0);
//        }
//    }
//}




@end
