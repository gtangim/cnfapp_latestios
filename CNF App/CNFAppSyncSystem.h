//
//  CNFAppSyncSystem.h
//  CNF App
//
//  Created by Anik on 2015-04-09.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CNFAppSyncSystem : NSObject

-(void)startSync;

@end
