//
//  CNFAppSyncSystem.m
//  CNF App
//
//  Created by Anik on 2015-04-09.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "CNFAppSyncSystem.h"
#import "CNFSubmitData.h"


@implementation CNFAppSyncSystem


-(void)startSync{
    [self uploadData];
}


#pragma mark - data file acquire methods

-(void)uploadData{
    CNFSubmitData * submitData = [[CNFSubmitData alloc]init];
    [submitData startSubmittingData];
}


@end
