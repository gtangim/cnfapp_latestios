//
//  CNFBakeryOrderPrintViewController.h
//  CNF App
//
//  Created by Anik on 2017-10-26.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CNFBakeryOrderPrintViewController : UIViewController<WKNavigationDelegate, WKUIDelegate, UIPageViewControllerDelegate,UIPrintInteractionControllerDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSMutableArray * calculatedOrderProductArray;
@property (nonatomic, strong) NSMutableArray * displayProductArray;
@property (nonatomic, strong) NSString * toLocationId;
@property (nonatomic, strong) NSObject * currentOrder;
@end
