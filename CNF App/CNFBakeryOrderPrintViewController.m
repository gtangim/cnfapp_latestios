//
//  CNFBakeryOrderPrintViewController.m
//  CNF App
//
//  Created by Anik on 2017-10-26.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFBakeryOrderPrintViewController.h"
#import "CNFAppDelegate.h"

@interface CNFBakeryOrderPrintViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * productList;
@property (nonatomic, strong) NSMutableArray * sectionList;
@end

@implementation CNFBakeryOrderPrintViewController{
    NSManagedObjectContext * context;
}

@synthesize context=_context;
@synthesize webView=_webView;
@synthesize displayProductArray=_displayProductArray, calculatedOrderProductArray=_calculatedOrderProductArray, toLocationId=_toLocationId, currentOrder=_currentOrder;
@synthesize productList=_productList, sectionList=_sectionList;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];

    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Print" style:UIBarButtonItemStylePlain target:self action:@selector(printOrder)];

    [self.navigationItem.rightBarButtonItem setEnabled:FALSE];
    self.navigationController.navigationItem.backBarButtonItem.enabled=NO;

    if([self setProductSection]){
        if([self sortSectionsFromFile]){
            if([self createCSVFile]) [self showCsvFile];
            else {
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                self.navigationController.navigationItem.backBarButtonItem.enabled=YES;
                [self showErrorAlert:@"file error"];
            }
        }else{
            [self showErrorAlert:@"Could not load product sections, please try again"];
        }
    }else{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        self.navigationController.navigationItem.backBarButtonItem.enabled=YES;
        [self showErrorAlert:@"Could not load product-section file"];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - product section set funcitons

-(BOOL)sortSectionsFromFile{
    NSError * error;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* finalPath = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery.csv"];

    NSString *lines = [NSString stringWithContentsOfFile:finalPath encoding:NSASCIIStringEncoding error:&error];

    if(error) return FALSE;
    else{
        NSArray * sectionLines = [lines componentsSeparatedByString:@"\n"];
        NSArray* sections =[self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];
        self.sectionList = [[NSMutableArray alloc]init];

        @try{
            for(NSString * section in sections){
                NSPredicate * sectiondPred = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", section];
                NSArray * filteredArray = [sectionLines filteredArrayUsingPredicate:sectiondPred];

                NSMutableDictionary * sectionDict = [[NSMutableDictionary alloc]init];

                if(filteredArray.count==0){
                    [sectionDict setValue:@"Unassigned"  forKey:@"section"];
                    [sectionDict setValue:[NSNumber numberWithInteger:0] forKey:@"sectionSeq"];
                }else{
                    NSString * section = [[filteredArray.firstObject componentsSeparatedByString:@","][3] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                    [sectionDict setValue:section  forKey:@"section"];
                    [sectionDict setValue:[NSNumber numberWithInteger:[sectionLines indexOfObject:filteredArray.firstObject]] forKey:@"sectionSeq"];
                }

                [self.sectionList addObject:sectionDict];
            }

            NSSortDescriptor * sectionSorter = [NSSortDescriptor sortDescriptorWithKey:@"sectionSeq" ascending:YES];
            [self.sectionList sortUsingDescriptors:[[NSArray alloc]initWithObjects:sectionSorter, nil]];

            return TRUE;
        }@catch(NSException * ex){
            return FALSE;
        }
    }
}


-(BOOL)setProductSection{
    NSError * error;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* finalPath = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery.csv"];

    NSString *lines = [NSString stringWithContentsOfFile:finalPath encoding:NSASCIIStringEncoding error:&error];

    if(error) return FALSE;
    else{
        NSArray * subSectionArray = [lines componentsSeparatedByString:@"\n"];
        if(!self.productList) self.productList= [[NSMutableArray alloc]init];
        else [self.productList removeAllObjects];

        for(id product in self.displayProductArray){
            NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [product valueForKey:@"productId"]];
            NSArray * filteredArray = [subSectionArray filteredArrayUsingPredicate:prodPred];

            if(filteredArray.count>0){

                NSString * section = [[filteredArray.firstObject componentsSeparatedByString:@","][3] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString * sub_section = [[filteredArray.firstObject componentsSeparatedByString:@","][4] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                if([product isMemberOfClass:[NSDictionary class]]){
                    NSMutableDictionary * dict = product;
                    [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                    [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                    [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                    [self.productList addObject:dict];

                }else if([product isMemberOfClass:[NSManagedObject class]]){
                    NSArray *keys = [[[product entity] attributesByName] allKeys];
                    NSMutableDictionary * dict = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                    [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                    [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                    [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                    [self.productList addObject:dict];
                }else{
                    NSMutableDictionary * dict = product;
                    [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                    [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                    [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                    [self.productList addObject:dict];
                }
            }else{
                if([product isMemberOfClass:[NSDictionary class]]){
                    NSMutableDictionary * dict = product;
                    [dict setValue:@"Unassigned" forKey:@"sub_section"];
                    [dict setValue:@"Unassigned" forKey:@"section"];
                    [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                    [self.productList addObject:dict];

                }else if([product isMemberOfClass:[NSManagedObject class]]){
                    NSArray *keys = [[[product entity] attributesByName] allKeys];
                    NSMutableDictionary * dict = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                    //                    [dict setValue:@"Others" forKey:@"section"];
                    [dict setValue:@"Unassigned" forKey:@"sub_section"];
                    [dict setValue:@"Unassigned" forKey:@"section"];
                    [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                    [self.productList addObject:dict];
                }else{
                    NSMutableDictionary * dict = product;
                    //                    [dict setValue:@"Others" forKey:@"section"];
                    [dict setValue:@"Unassigned" forKey:@"sub_section"];
                    [dict setValue:@"Unassigned" forKey:@"section"];
                    [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                    [self.productList addObject:dict];
                }
            }
        }
        return  TRUE;
    }
}


#pragma mark - CSV file creation and display funciton

-(BOOL)createCSVFile{

    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/BakeryOrderFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];

    //delete all files in directory
//    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:&error]) {
//        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", dataPath, file] error:&error];
//        }

    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", [self.currentOrder valueForKey:@"orderId"]]];

    if(error) return FALSE;

    if(self.productList.count==0){
        return FALSE;
    }

    else{
        NSMutableString *writeString = [NSMutableString stringWithCapacity:0];
        [writeString appendString:@",,Name: Community Natural Foods \n"];
        [writeString appendString:[NSString stringWithFormat:@",,Order Created Date: %@ \n",[self getDateString:[self.currentOrder valueForKey:@"orderCreatedDate"]]]];
        [writeString appendString:[NSString stringWithFormat:@",,Order Print Date: %@ \n",[self getDateString:[NSDate date]]]];
//        [writeString appendString:[NSString stringWithFormat:@",,Order Number: %@ \n",[[self.currentOrder valueForKey:@"orderId"]stringValue]]];

        if(self.calculatedOrderProductArray){
            [writeString appendString:@"Serial No., Product ID, Product Name, Product Upc,Sub-Section ,Quantity S1, Quantity S2, Quantity S5, Total Order \n"];

//            NSArray *sections = [self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];


            for(NSDictionary * section in self.sectionList){
                NSPredicate * sectionpred = [NSPredicate predicateWithFormat:@"section == %@", [section valueForKey:@"section"]];
                NSArray * filteredSectionArray = [self.productList filteredArrayUsingPredicate:sectionpred];

//                NSSortDescriptor * subSectionSort = [NSSortDescriptor sortDescriptorWithKey:@"sub_section" ascending:YES];
                NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:YES];
                filteredSectionArray = [filteredSectionArray sortedArrayUsingDescriptors:[[NSArray alloc]initWithObjects: seqSort,nil]];

                [writeString appendString:[NSString stringWithFormat:@", Item Section: ,%@ , , , \n", [[section valueForKey:@"section"] stringByReplacingOccurrencesOfString:@"\r" withString:@""]]];
//                [writeString appendString:[NSString stringWithFormat:@", Item Section: ,%@", section]];

                for(int i=0; i< filteredSectionArray.count; i++){
                    NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"productId == %@", [[filteredSectionArray objectAtIndex:i] valueForKey:@"productId"]];
                    NSArray * filteredArray = [self.calculatedOrderProductArray filteredArrayUsingPredicate:prodPred];

                    [writeString appendString:[NSString stringWithFormat:@"%d, %@, %@, %@, %@, ",i+1, [[filteredSectionArray objectAtIndex:i]valueForKey:@"productId"],[[filteredSectionArray objectAtIndex:i]valueForKey:@"productName"], [[filteredSectionArray objectAtIndex:i]valueForKey:@"productUpc"], [[filteredSectionArray objectAtIndex:i]valueForKey:@"sub_section"]]];

                    NSString * s1_quantity = @",";
                    NSString * s2_quantity = @",";
                    NSString * s5_quantity = @",";

                    for(NSObject * product in filteredArray){
                        if([[product valueForKey:@"toLocationId"]isEqualToString:@"S1"]){
                            s1_quantity= [NSString stringWithFormat:@"%d,", [[product valueForKey:@"requestedQuantity"]intValue]] ;
                        }else if([[product valueForKey:@"toLocationId"]isEqualToString:@"S2"]){
                            s2_quantity= [NSString stringWithFormat:@"%d,", [[product valueForKey:@"requestedQuantity"]intValue]] ;
                        }else if([[product valueForKey:@"toLocationId"]isEqualToString:@"S5"]){
                            s5_quantity= [NSString stringWithFormat:@"%d,", [[product valueForKey:@"requestedQuantity"]intValue]] ;
                        }else{

                        }
                    }
                    [writeString appendString:s1_quantity];
                    [writeString appendString:s2_quantity];
                    [writeString appendString:s5_quantity];
                    [writeString appendString:[NSString stringWithFormat:@"%d \n",[[[filteredSectionArray objectAtIndex:i]valueForKey:@"requestedQuantity"]intValue]]];
                }
            }
        }else{
            [writeString appendString:[NSString stringWithFormat:@",,Order for Location: %@ \n",[[self.productList objectAtIndex:0] valueForKey:@"toLocationId"]]];

            [writeString appendString:@"Serial No., Product ID, Product Name, Product Upc, Sub-Section, Quantity \n"];

//            NSArray *sections = [self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];

            for(NSDictionary * section in self.sectionList){
                NSPredicate * sectionpred = [NSPredicate predicateWithFormat:@"section == %@", [section valueForKey:@"section"]];
                NSArray * filteredSectionArray = [self.productList filteredArrayUsingPredicate:sectionpred];

//                NSSortDescriptor * subSectionSort = [NSSortDescriptor sortDescriptorWithKey:@"sub_section" ascending:YES];
                NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:YES];
                filteredSectionArray = [filteredSectionArray sortedArrayUsingDescriptors:[[NSArray alloc]initWithObjects:seqSort, nil]];

                [writeString appendString:[NSString stringWithFormat:@", Item Section: ,%@ , , \n", [[section valueForKey:@"section"] stringByReplacingOccurrencesOfString:@"\r" withString:@""]]];

                for (int i=0; i<filteredSectionArray.count; i++) {
                    [writeString appendString:[NSString stringWithFormat:@"%d, %@, %@, %@, %@, %d \n",i+1, [[filteredSectionArray objectAtIndex:i]valueForKey:@"productId"],[[filteredSectionArray objectAtIndex:i]valueForKey:@"productName"], [[filteredSectionArray objectAtIndex:i]valueForKey:@"productUpc"],[[filteredSectionArray objectAtIndex:i]valueForKey:@"sub_section"], [[[filteredSectionArray objectAtIndex:i]valueForKey:@"requestedQuantity"]intValue]]];
                }
            }
        }

        [writeString writeToFile:fileAtPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        if(error) return FALSE;
        else return TRUE;
    }
    return FALSE;
}


-(void)showCsvFile{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/BakeryOrderFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", [self.currentOrder valueForKey:@"orderId"]]];

    NSURL *url = [NSURL fileURLWithPath:fileAtPath];
    [self.webView loadFileURL:url allowingReadAccessToURL:url];
    [self.view addSubview:self.webView];
}

#pragma mark - webview delegate funcitons

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.navigationController.navigationItem.backBarButtonItem.enabled=YES;
    [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
}

-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.navigationController.navigationItem.backBarButtonItem.enabled=YES;
    NSLog(@"error occureed: %@", error.localizedDescription);
    [self showErrorAlert:@"Error occured shile loading CSV file. Please try again"];
}

#pragma mark - button funciton

-(void)printOrder{

    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];

    pic.delegate = self;

    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"nothing";
    printInfo.duplex = UIPrintInfoDuplexLongEdge;
    pic.printInfo = printInfo;
    pic.printFormatter = self.webView.viewPrintFormatter;

    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            if(!completed && error){
                NSLog(@"Print Error: %@", error);
                [self showErrorAlert:@"Error occured while printing, Please try again"];
            }else if(completed){
                [self showPrintedAlert];
            }else{

            }
    };
    [pic presentAnimated:YES completionHandler:completionHandler];
}

#pragma mark - Alertview funciton

-(void)showPrintedAlert{
    UIAlertController *printAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:@"Did the order print?"
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesaction = [UIAlertAction
                               actionWithTitle:@"YES"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   //save isprinted flag in the DB for the order

                                   NSFetchRequest *request = [[NSFetchRequest alloc] init];
                                   NSEntityDescription *entity = [NSEntityDescription entityForName:@"BakeryOrderList"
                                                                             inManagedObjectContext:context];
                                   [request setEntity:entity];

                                   request.predicate = [NSPredicate predicateWithFormat:@"orderId == %ld", [[self.currentOrder valueForKey:@"orderId"]longValue]];

                                   NSError *error = nil;
                                   NSArray *array = [context executeFetchRequest:request error:&error];

                                   if(array.count>0){
                                       [array.firstObject setValue:[NSNumber numberWithBool:YES] forKey:@"orderPrinted"];
                                       [context save:&error];
                                   }
                               }];

    UIAlertAction *noaction = [UIAlertAction
                                actionWithTitle:@"NO"
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction *action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }];

    [printAlertController addAction:yesaction];
    [printAlertController addAction:noaction];
    [self presentViewController:printAlertController animated:YES completion:nil];
}

-(void)showErrorAlert:(NSString *)message{

    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}




-(NSString *)getDateString:(NSDate *)date{
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    return [formatter stringFromDate:date];
}


@end
