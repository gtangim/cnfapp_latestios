//
//  CNFBakeryOrderProductsTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-10-24.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BRRequestDownload.h"
#import "BRRequest.h"

@interface CNFBakeryOrderProductsTableViewController : UITableViewController<NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSObject * currentOrder;
@property (nonatomic, strong) NSObject * previousOrder;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;

- (IBAction)segmentChanged:(id)sender;

@end
