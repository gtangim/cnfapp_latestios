//
//  CNFBakeryOrderProductsTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-10-24.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFBakeryOrderProductsTableViewController.h"
#import "CNFAppDelegate.h"
#import "CNFBakeryOrderPrintViewController.h"
#import "CNFBakeryPickProductTableViewController.h"
#import "BakeryProductListTableViewCell.h"

#import "DLSFTPConnection.h"
#import "DLSFTPUploadRequest.h"
#import "DLSFTPFile.h"
#import "DLSFTPDownloadRequest.h"

@interface CNFBakeryOrderProductsTableViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * calculatedOrderProductArray;
@property (nonatomic, strong) NSMutableArray * displayProductArray;
@property (nonatomic, strong) NSMutableArray * productList;
@property (nonatomic, strong) NSMutableArray * sectionList;
@property (strong, nonatomic) DLSFTPConnection *connection;
@end

typedef void (^Completion)(BOOL success, NSString *msg);

@implementation CNFBakeryOrderProductsTableViewController{
    NSManagedObjectContext * context;
    BRRequestDownload *downloadFile;
    NSMutableData * downloadData;
}

@synthesize context=_context;
@synthesize currentOrder=_currentOrder, previousOrder=_previousOrder;
@synthesize calculatedOrderProductArray=_calculatedOrderProductArray, displayProductArray=_displayProductArray, productList=_productList, sectionList=_sectionList;
@synthesize segment=_segment;


- (void)viewDidLoad {
    [super viewDidLoad];

    [self.segment.subviews[0] setTintColor:[UIColor redColor]];
    [self.segment.subviews[1] setTintColor:[UIColor orangeColor]];
    [self.segment.subviews[2] setTintColor:[UIColor greenColor]];
    [self.segment.subviews[3] setTintColor:[UIColor blueColor]];

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];

    //new funciton for FTP file download
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//    [self downloadLatestSectioFile];
    [self FTPConnectionTester];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initializeOrderProduct{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    self.displayProductArray = [[NSMutableArray alloc]init];
    if(![self loadProductTable]) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self showErrorAlert:@"Error loading products for order, Please try again"];
        [self.segment setEnabled:FALSE];
    }
    else{
        [self.segment setEnabled:TRUE];
        for(NSManagedObject * product in self.calculatedOrderProductArray){
            NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"productId == %@", [product valueForKey:@"productId"]];
            NSArray * filteredArray = [self.calculatedOrderProductArray filteredArrayUsingPredicate:prodPred];
            if(filteredArray.count>1){
                if([self.displayProductArray filteredArrayUsingPredicate:prodPred].count==0){
                    double quantity = 0.0;
                    for(NSObject * eachLocationProd in filteredArray){
                        quantity += [[eachLocationProd valueForKey:@"requestedQuantity"] doubleValue];
                    }
                    NSArray *keys = [[[product entity] attributesByName] allKeys];
                    NSMutableDictionary *modifiedProd = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                    [modifiedProd setValue:[NSNumber numberWithDouble:quantity] forKey:@"requestedQuantity"];
                    [self.displayProductArray addObject:modifiedProd];
                }
            }else{
                [self.displayProductArray addObject:product];
            }
        }

        if([self setProductSection]){
            if([self sortSectionsFromFile]){
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(printOrder)];
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                [self.tableView reloadData];
            }else{
                [self showErrorAlert:@"Could not load product sections, please try again"];
            }
        }else{
            [self showErrorAlert:@"Could not load product sections, please try again"];
        }
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.productList && self.sectionList) return self.sectionList.count;
    else return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(self.productList && self.sectionList){
        return [[self.sectionList objectAtIndex:section] valueForKey:@"section"];
    }
    else return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.productList && self.sectionList){
        NSPredicate * sectionPred = [NSPredicate predicateWithFormat:@"SELF.section == %@", [[self.sectionList objectAtIndex:section] valueForKey:@"section"]];
        return [[self.productList filteredArrayUsingPredicate:sectionPred] count];
    } else return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 115;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    BakeryProductListTableViewCell *cell = (BakeryProductListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bakeryOrderProductCells"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"bakeryOrderProductCells" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

//    NSArray* sectionArray =[self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];

    NSPredicate * sectionPred = [NSPredicate predicateWithFormat:@"SELF.section == %@", [[self.sectionList objectAtIndex:indexPath.section]valueForKey:@"section"]];

    NSArray * productBySectionArray = [self.productList filteredArrayUsingPredicate:sectionPred];

    if(productBySectionArray.count>0){

        //need to sort product array by sequence number

        NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:YES];
//        NSSortDescriptor * subSectionSort = [NSSortDescriptor sortDescriptorWithKey:@"sub_section" ascending:YES];
        productBySectionArray=[productBySectionArray sortedArrayUsingDescriptors:[[NSArray alloc]initWithObjects:seqSort,nil]];

        cell.productNameLabel.text = productBySectionArray.firstObject?[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"productName"]:@"";
        cell.productUpcLabel.text = [NSString stringWithFormat:@"Product UPC: %@", [[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"productUpc"]];
        cell.sectionLabel.text = [[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"sub_section"];

        if(self.productList){
            NSString * uom =  [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"uomId"] componentsSeparatedByString:@"_"][1];

            if([self.segment selectedSegmentIndex]==0){

                cell.totalQuantityLabel.text = [NSString stringWithFormat:@"Total Demand: %d %@", [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]intValue], uom];

                NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"productId == %@", [[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"productId"]];
                NSArray * filteredArray = [self.calculatedOrderProductArray filteredArrayUsingPredicate:prodPred];

                NSPredicate * s1locaitonPred = [NSPredicate predicateWithFormat:@"toLocationId == %@", @"S1"];
                NSPredicate * s2locaitonPred = [NSPredicate predicateWithFormat:@"toLocationId == %@", @"S2"];
                NSPredicate * s5locaitonPred = [NSPredicate predicateWithFormat:@"toLocationId == %@", @"S5"];

                if([filteredArray filteredArrayUsingPredicate:s1locaitonPred].count>0) {
                    cell.s1Label.text = [NSString stringWithFormat:@"S1 \n%d %@",[[[filteredArray filteredArrayUsingPredicate:s1locaitonPred].firstObject valueForKey:@"requestedQuantity"]intValue],uom];
                }else{
                    cell.s1Label.text = @"";
                }
                if([filteredArray filteredArrayUsingPredicate:s2locaitonPred].count>0) {
                    cell.s2Label.text = [NSString stringWithFormat:@"S2 \n%d %@",[[[filteredArray filteredArrayUsingPredicate:s2locaitonPred].firstObject valueForKey:@"requestedQuantity"]intValue],uom];
                }else{
                    cell.s2Label.text = @"";
                }
                if([filteredArray filteredArrayUsingPredicate:s5locaitonPred].count>0) {
                    cell.s5Label.text = [NSString stringWithFormat:@"S5 \n%d %@",[[[filteredArray filteredArrayUsingPredicate:s5locaitonPred].firstObject valueForKey:@"requestedQuantity"]intValue],uom];
                }else{
                    cell.s5Label.text = @"";
                }

            }else if([self.segment selectedSegmentIndex]==1){
                cell.s1Label.text = [NSString stringWithFormat:@"%d %@",[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]intValue], uom];
                cell.s2Label.text=cell.s5Label.text =@"";
                cell.totalQuantityLabel.text=@"";
            }else if([self.segment selectedSegmentIndex]==2){
                cell.s2Label.text = [NSString stringWithFormat:@"%d %@",[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]intValue], uom];
                cell.s1Label.text=cell.s5Label.text =@"";
                cell.totalQuantityLabel.text=@"";
            }else if([self.segment selectedSegmentIndex]==3){
                cell.s5Label.text = [NSString stringWithFormat:@"%d %@",[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]intValue], uom];
                cell.s2Label.text=cell.s1Label.text =@"";
                cell.totalQuantityLabel.text=@"";
            }else{
                cell.detailTextLabel.text = [NSString stringWithFormat:@"Product UPC: %@ \nRequested Quantity: %d %@", [[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"productUpc"],[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]intValue], uom];
            }
        }
    }
    return cell;
}

#pragma mark - product table load function

-(BOOL)sortSectionsFromFile{
    NSError * error;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* finalPath = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery.csv"];

    NSString *lines = [NSString stringWithContentsOfFile:finalPath encoding:NSASCIIStringEncoding error:&error];

    if(error) return FALSE;
    else{
        NSArray * sectionLines = [lines componentsSeparatedByString:@"\n"];
        NSArray* sections =[self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];
        self.sectionList = [[NSMutableArray alloc]init];

        @try{
            for(NSString * section in sections){
                NSPredicate * sectiondPred = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", section];
                NSArray * filteredArray = [sectionLines filteredArrayUsingPredicate:sectiondPred];

                NSMutableDictionary * sectionDict = [[NSMutableDictionary alloc]init];

                if(filteredArray.count==0){
                    [sectionDict setValue:@"Unassigned"  forKey:@"section"];
                    [sectionDict setValue:[NSNumber numberWithInteger:0] forKey:@"sectionSeq"];
                }else{
                    NSString * section = [[filteredArray.firstObject componentsSeparatedByString:@","][3] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                    [sectionDict setValue:section  forKey:@"section"];
                    [sectionDict setValue:[NSNumber numberWithInteger:[sectionLines indexOfObject:filteredArray.firstObject]] forKey:@"sectionSeq"];
                }

                [self.sectionList addObject:sectionDict];
            }

            NSSortDescriptor * sectionSorter = [NSSortDescriptor sortDescriptorWithKey:@"sectionSeq" ascending:YES];
            [self.sectionList sortUsingDescriptors:[[NSArray alloc]initWithObjects:sectionSorter, nil]];

            return TRUE;
        }@catch(NSException * ex){
            return FALSE;
        }
    }
}


-(BOOL)setProductSection{
    NSError * error;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* finalPath = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery.csv"];

    NSString *lines = [NSString stringWithContentsOfFile:finalPath encoding:NSASCIIStringEncoding error:&error];

    if(error) return FALSE;
    else{
        NSArray * subSectionArray = [lines componentsSeparatedByString:@"\n"];
        if(!self.productList) self.productList= [[NSMutableArray alloc]init];
        else [self.productList removeAllObjects];

        for(id product in self.displayProductArray){
            NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [product valueForKey:@"productId"]];
            NSArray * filteredArray = [subSectionArray filteredArrayUsingPredicate:prodPred];

            if(filteredArray.count>0){

                NSString * section = [[filteredArray.firstObject componentsSeparatedByString:@","][3] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString * sub_section = [[filteredArray.firstObject componentsSeparatedByString:@","][4] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                if([product isMemberOfClass:[NSDictionary class]]){
                    NSMutableDictionary * dict = product;
                    [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                    [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                    [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                    [self.productList addObject:dict];

                }else if([product isMemberOfClass:[NSManagedObject class]]){
                    NSArray *keys = [[[product entity] attributesByName] allKeys];
                    NSMutableDictionary * dict = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                    [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                    [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                    [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                    [self.productList addObject:dict];
                }else{
                    NSMutableDictionary * dict = product;
                    [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                    [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                    [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                    [self.productList addObject:dict];
                }
            }else{
                if([product isMemberOfClass:[NSDictionary class]]){
                    NSMutableDictionary * dict = product;
                    [dict setValue:@"Unassigned" forKey:@"sub_section"];
                    [dict setValue:@"Unassigned" forKey:@"section"];
                    [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                    [self.productList addObject:dict];

                }else if([product isMemberOfClass:[NSManagedObject class]]){
                    NSArray *keys = [[[product entity] attributesByName] allKeys];
                    NSMutableDictionary * dict = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
//                    [dict setValue:@"Others" forKey:@"section"];
                    [dict setValue:@"Unassigned" forKey:@"sub_section"];
                    [dict setValue:@"Unassigned" forKey:@"section"];
                    [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                    [self.productList addObject:dict];
                }else{
                    NSMutableDictionary * dict = product;
//                    [dict setValue:@"Others" forKey:@"section"];
                    [dict setValue:@"Unassigned" forKey:@"sub_section"];
                    [dict setValue:@"Unassigned" forKey:@"section"];
                    [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                    [self.productList addObject:dict];
                }
            }
        }
        return  TRUE;
    }
}


-(BOOL)loadProductTable{
    NSMutableArray * currentOrderProductArray = [[NSMutableArray alloc]initWithArray:[self getCurrentProductArray]];

    if(currentOrderProductArray){
        if(self.previousOrder){
            NSMutableArray * previousOrderProductArray = [[NSMutableArray alloc]initWithArray:[self getPreviousProductArray]];
            if(previousOrderProductArray){
                if([self calculateOrderProductQuantity:currentOrderProductArray previous:previousOrderProductArray]) return TRUE;
                else return FALSE;
            }else{
                return FALSE;
            }
        }else{
            self.calculatedOrderProductArray = [[NSMutableArray alloc]initWithArray:currentOrderProductArray];
            return TRUE;
        }
    }else{
        return FALSE;
    }
}

-(NSArray*)getCurrentProductArray{
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"BakeryOrderProductList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];

    NSPredicate *orderPredicate = [NSPredicate predicateWithFormat:@"orderId == %ld",[[self.currentOrder valueForKey:@"orderId"]longValue]];
    [request setPredicate:orderPredicate];

    NSSortDescriptor *eventSorter=[[NSSortDescriptor alloc]initWithKey:@"toLocationId" ascending:YES];
    [request setSortDescriptors:[[NSArray alloc] initWithObjects:eventSorter, nil]];
    [request setEntity:entityDesc];

    NSError *error;
    NSArray *results= [context executeFetchRequest:request error:&error];

    if(error) {
        return NULL;
    }
    else
        return results;
}

-(NSArray*)getPreviousProductArray{
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"BakeryOrderProductList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];

    NSPredicate *orderPredicate = [NSPredicate predicateWithFormat:@"orderId == %ld",[[self.previousOrder valueForKey:@"orderId"]longValue]];
    [request setPredicate:orderPredicate];

    NSSortDescriptor *eventSorter=[[NSSortDescriptor alloc]initWithKey:@"toLocationId" ascending:YES];
    [request setSortDescriptors:[[NSArray alloc] initWithObjects:eventSorter, nil]];
    [request setEntity:entityDesc];

    NSError *error;
    NSArray *results= [context executeFetchRequest:request error:&error];

    if(error) {
        return NULL;
    }
    else
        return results;
}

-(BOOL)calculateOrderProductQuantity:(NSMutableArray*)currentOrderArray previous:(NSMutableArray*)previousOrderArray{

    self.calculatedOrderProductArray = [[NSMutableArray alloc]init];

    for(NSObject * product in currentOrderArray){
        NSPredicate * productLocationPred = [NSPredicate predicateWithFormat:@"productId == %@ AND toLocationId == %@", [product valueForKey:@"productId"],[product valueForKey:@"toLocationId"]];
        NSArray * filteredAry = [previousOrderArray filteredArrayUsingPredicate:productLocationPred];
        if(filteredAry.count==0){
            [self.calculatedOrderProductArray addObject:product];
        }else{
            NSObject * modifiedProduct = product;
            if([[product valueForKey:@"requestedQuantity"] doubleValue]-[[filteredAry.firstObject valueForKey:@"requestedQuantity"]doubleValue]==0){
            }else if([[product valueForKey:@"requestedQuantity"] doubleValue]-[[filteredAry.firstObject valueForKey:@"requestedQuantity"]doubleValue]<=0){
                [modifiedProduct setValue:[NSNumber numberWithDouble:[[product valueForKey:@"requestedQuantity"] doubleValue]] forKey:@"requestedQuantity"];
                [self.calculatedOrderProductArray addObject:modifiedProduct];
            }else{
                [modifiedProduct setValue:[NSNumber numberWithDouble:[[product valueForKey:@"requestedQuantity"] doubleValue]-[[filteredAry.firstObject valueForKey:@"requestedQuantity"]doubleValue]] forKey:@"requestedQuantity"];
                [self.calculatedOrderProductArray addObject:modifiedProduct];
            }
        }
    }
    return TRUE;
}


#pragma mark - FTP file download

-(void)FTPConnectionTester{

    NSString *username = @"BakeryFTP";
    NSString *password = @"BakeryP@ssw0rd";
    NSInteger port = 22;
    NSString *host = @"apps.cnfltd.com";

    // make a connection object and attempt to connect
    DLSFTPConnection *connection = [[DLSFTPConnection alloc] initWithHostname:host
                                                                         port:port
                                                                     username:username
                                                                     password:password];
    self.connection=connection;
    DLSFTPClientSuccessBlock successBlock = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            // login successful
            NSLog(@"connection valid, start downloading files");
            [self downloadFile];
        });
    };

    DLSFTPClientFailureBlock failureBlock = ^(NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.connection=nil;
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            NSLog(@"connection failure, file download cancelled");
            [self showErrorAlert:[NSString stringWithFormat:@"File download error, %@, Will continue with the older file", error.localizedDescription]];
            [self initializeOrderProduct];
        });
    };
    [connection connectWithSuccessBlock:successBlock failureBlock:failureBlock];
}

-(void)downloadFile{

    NSString * remotePath = @"/Section_Product_Relationship_bakery.csv";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* localPath = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery.csv"];
    NSString* localPathTemp = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery_temp.csv"];

    DLSFTPClientProgressBlock progressBlock = ^void(unsigned long long bytesReceived, unsigned long long bytesTotal) {
    };

    DLSFTPClientFileTransferSuccessBlock successBlock = ^(DLSFTPFile *file, NSDate *startTime, NSDate *finishTime) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"successfully downloaded file");
            NSError * error;
            if([[NSFileManager defaultManager] fileExistsAtPath:localPath])
                [[NSFileManager defaultManager] removeItemAtPath:localPath error:&error];
            [[NSFileManager defaultManager] moveItemAtPath:localPathTemp toPath:localPath error:&error];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self initializeOrderProduct];
        });
    };

    DLSFTPClientFailureBlock failureBlock = ^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"file download failed..");
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self showErrorAlert:[NSString stringWithFormat:@"File download error, %@, Will continue with the older file", error.localizedDescription]];
            [self initializeOrderProduct];
        });
    };

    DLSFTPDownloadRequest * request = [[DLSFTPDownloadRequest alloc]initWithRemotePath:remotePath localPath:localPathTemp resume:YES successBlock:successBlock failureBlock:failureBlock progressBlock:progressBlock];

    [self.connection submitRequest:request];
}



//-(void)downloadLatestSectioFile{
//    downloadData = [NSMutableData dataWithCapacity: 1];
//    downloadFile = [[BRRequestDownload alloc] initWithDelegate: self];
//
//    downloadFile.path = @"/Bakery_Product/Section_Product_Relationship_bakery.csv";
//    downloadFile.hostname = @"webapp.cnfltd.com";
//    downloadFile.username = @"testusers5";
//    downloadFile.password = @"P@ssw0rd";
//
//    [downloadFile start];
//}
//
//- (void) cancelAction
//    {
//        if (downloadFile)
//        {
//            downloadFile.cancelDoesNotCallDelegate = TRUE;
//            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//        }
//    }
//
//- (BOOL)shouldOverwriteFileWithRequest:(BRRequest *)request{
//    if (request == downloadFile)
//    {
//        return YES;
//    }
//    return NO;
//}
//
//-(void) requestCompleted: (BRRequest *) request
//    {
//        if (request == downloadFile)
//        {
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
//            NSString *documentsDirectory = [paths objectAtIndex:0];
//            NSString* finalPath = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery.csv"];
//
////            NSData *data = downloadFile.receivedData;
//
//            //----- save the NSData as a file object
//            NSError *error;
//            [downloadData writeToFile: finalPath options: NSDataWritingFileProtectionNone error: &error];
//
//            downloadFile = nil;
//            NSLog(@"%@ completed!, file saved to %@", request, finalPath);
//            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//            [self initializeOrderProduct];
//        }
//    }
//
//-(void) requestFailed:(BRRequest *) request
//    {
//        if (request == downloadFile)
//        {
//            NSLog(@"%@", request.error.message);
//            downloadFile = nil;
//            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//            [self showErrorAlert:[NSString stringWithFormat:@"File download error, %@, Will continue with the older file", request.error.message]];
//            [self initializeOrderProduct];
//        }
//    }
//
//- (void) requestDataAvailable: (BRRequestDownload *) request;
//{
//    [downloadData appendData: request.receivedData];
//}

#pragma mark - alert controllers funciton

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


#pragma mark - segment click funciton

- (IBAction)segmentChanged:(id)sender {

    switch (self.segment.selectedSegmentIndex)
    {
    case 0:
        if(self.displayProductArray && self.calculatedOrderProductArray){
            [self.displayProductArray removeAllObjects];

            for(NSManagedObject * product in self.calculatedOrderProductArray){
                NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"productId == %@", [product valueForKey:@"productId"]];
                NSArray * filteredArray = [self.calculatedOrderProductArray filteredArrayUsingPredicate:prodPred];
                if(filteredArray.count>1){
                    if([self.displayProductArray filteredArrayUsingPredicate:prodPred].count==0){
                        double quantity = 0.0;
                        for(NSObject * eachLocationProd in filteredArray){
                            quantity += [[eachLocationProd valueForKey:@"requestedQuantity"] doubleValue];
                        }
                        NSArray *keys = [[[product entity] attributesByName] allKeys];
                        NSMutableDictionary *modifiedProd = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                        [modifiedProd setValue:[NSNumber numberWithDouble:quantity] forKey:@"requestedQuantity"];
                        [self.displayProductArray addObject:modifiedProd];
                    }
                }else{
                    [self.displayProductArray addObject:product];
                }
            }

            if(![self setProductSection]) [self showErrorAlert:@"Could not load product sections, please try again"];
            [self.tableView reloadData];
        }
        break;
    case 1:
        if(self.displayProductArray && self.calculatedOrderProductArray){
            [self.displayProductArray removeAllObjects];
            NSPredicate * locationPred = [NSPredicate predicateWithFormat:@"toLocationId == %@",@"S1"];
            self.displayProductArray = [[NSMutableArray alloc]initWithArray:[self.calculatedOrderProductArray filteredArrayUsingPredicate:locationPred]];
            if(![self setProductSection]) [self showErrorAlert:@"Could not load product sections, please try again"];
            [self.tableView reloadData];
        }
        break;
    case 2:
        if(self.displayProductArray && self.calculatedOrderProductArray){
            [self.displayProductArray removeAllObjects];
            NSPredicate * locationPred = [NSPredicate predicateWithFormat:@"toLocationId == %@",@"S2"];
            self.displayProductArray = [[NSMutableArray alloc]initWithArray:[self.calculatedOrderProductArray filteredArrayUsingPredicate:locationPred]];
            if(![self setProductSection]) [self showErrorAlert:@"Could not load product sections, please try again"];
            [self.tableView reloadData];
        }
        break;
    case 3:
        if(self.displayProductArray && self.calculatedOrderProductArray){
            [self.displayProductArray removeAllObjects];
            NSPredicate * locationPred = [NSPredicate predicateWithFormat:@"toLocationId == %@",@"S5"];
            self.displayProductArray = [[NSMutableArray alloc]initWithArray:[self.calculatedOrderProductArray filteredArrayUsingPredicate:locationPred]];
            if(![self setProductSection]) [self showErrorAlert:@"Could not load product sections, please try again"];
            [self.tableView reloadData];
        }
        break;
    default:
                break;
    }
}

#pragma mark - storyboard segue funciton

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"bakeryOrderPrintSegue"]){
        CNFBakeryOrderPrintViewController * bakeryPrint = segue.destinationViewController;
        bakeryPrint.displayProductArray = self.displayProductArray;
        bakeryPrint.currentOrder = self.currentOrder;

        if(self.segment.selectedSegmentIndex == 0){
            bakeryPrint.calculatedOrderProductArray = self.calculatedOrderProductArray;
            bakeryPrint.toLocationId =NULL;
        }else{
            bakeryPrint.calculatedOrderProductArray=NULL;
            if(self.segment.selectedSegmentIndex == 1) bakeryPrint.toLocationId=@"S1";
            else if (self.segment.selectedSegmentIndex == 2) bakeryPrint.toLocationId=@"S2";
            else bakeryPrint.toLocationId=@"S5";
        }
    }
    if([segue.identifier isEqualToString:@"bakeryOrderPickSegue"]){
        CNFBakeryPickProductTableViewController * bakeryPick = segue.destinationViewController;
        bakeryPick.displayProductArray = self.displayProductArray;
        bakeryPick.currentOrder = self.currentOrder;

        if(self.segment.selectedSegmentIndex == 0){
            bakeryPick.calculatedOrderProductArray = self.calculatedOrderProductArray;
            bakeryPick.toLocationId =NULL;
        }else{
            bakeryPick.calculatedOrderProductArray=NULL;
            if(self.segment.selectedSegmentIndex == 1) bakeryPick.toLocationId=@"S1";
            else if (self.segment.selectedSegmentIndex == 2) bakeryPick.toLocationId=@"S2";
            else bakeryPick.toLocationId=@"S5";
        }
    }
}

#pragma mark - button click funciton

-(void)printOrder{
    if(self.calculatedOrderProductArray.count!=0 && self.displayProductArray.count!=0){
        UIAlertController *doneAlertController = [UIAlertController
                                                   alertControllerWithTitle:nil
                                                   message:nil
                                                   preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *printaction = [UIAlertAction
                                   actionWithTitle:@"Print Order"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       [self performSegueWithIdentifier:@"bakeryOrderPrintSegue" sender:self];
                                   }];

        UIAlertAction *pickaction = [UIAlertAction
                                      actionWithTitle:@"Ship Transfer"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction *action)
                                      {
                                          [self dismissViewControllerAnimated:YES completion:nil];
                                          [self performSegueWithIdentifier:@"bakeryOrderPickSegue" sender:self];
                                      }];

        UIAlertAction *cancelAction = [UIAlertAction
                                      actionWithTitle:@"Cancel"
                                      style:UIAlertActionStyleDestructive
                                      handler:^(UIAlertAction *action)
                                      {
                                          [self dismissViewControllerAnimated:YES completion:nil];
                                      }];

        [doneAlertController addAction:printaction];

        if([self.segment selectedSegmentIndex]==0){
            if(![[self.currentOrder valueForKey:@"orderFulfilled"] boolValue] && ![[self.currentOrder valueForKey:@"orderFulfilledS1"] boolValue] && ![[self.currentOrder valueForKey:@"orderFulfilledS2"] boolValue] && ![[self.currentOrder valueForKey:@"orderFulfilledS5"] boolValue]){
                [doneAlertController addAction:pickaction];
            }
        }else if([self.segment selectedSegmentIndex]==1){
            if(![[self.currentOrder valueForKey:@"orderFulfilledS1"] boolValue]){
                [doneAlertController addAction:pickaction];
            }
        }else if([self.segment selectedSegmentIndex]==2){
            if(![[self.currentOrder valueForKey:@"orderFulfilledS2"] boolValue]){
                [doneAlertController addAction:pickaction];
            }
        }else if([self.segment selectedSegmentIndex]==3){
            if(![[self.currentOrder valueForKey:@"orderFulfilledS5"] boolValue]){
                [doneAlertController addAction:pickaction];
            }
        }else{

        }

        [doneAlertController addAction:cancelAction];
        [self presentViewController:doneAlertController animated:YES completion:nil];
    }

    else
        [self showErrorAlert:@"No product to printing or transfer"];
}

@end
