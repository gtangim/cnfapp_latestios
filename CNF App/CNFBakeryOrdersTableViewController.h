//
//  CNFBakeryOrdersTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-10-23.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFBakeryOrdersTableViewController : UITableViewController<NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UISegmentedControl *orderSegment;
- (IBAction)orderSegmentChanged:(id)sender;
@end
