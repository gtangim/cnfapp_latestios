//
//  CNFBakeryOrdersTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-10-23.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFBakeryOrdersTableViewController.h"
#import "CNFAppDelegate.h"
#import "CNFBakeryOrderProductsTableViewController.h"

@interface CNFBakeryOrdersTableViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic) long maxOrderNumber;
@property (nonatomic, strong) NSMutableArray * orderTableData;
@property (nonatomic, strong) NSMutableArray * displayOrderArray;
@end

@implementation CNFBakeryOrdersTableViewController{
    NSManagedObjectContext * context;
}

@synthesize context=_context;
@synthesize maxOrderNumber=_maxOrderNumber;
@synthesize orderTableData=_orderTableData;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.maxOrderNumber=0;

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];

    //check if the table has data for the day

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BakeryOrderList"
                                              inManagedObjectContext:context];
    [request setEntity:entity];

    request.predicate = [NSPredicate predicateWithFormat:@"orderCreatedDate==max(orderCreatedDate)"];

    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];

    if(array.count==0){
        [self createNewBakeryOrder];
    }else if ([self daysBetween:[array.firstObject valueForKey:@"orderCreatedDate"]]){
        self.maxOrderNumber = [[array.firstObject valueForKey:@"orderId"] longValue];
        [self createNewBakeryOrder];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadOrderTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.displayOrderArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bakeryOrderCells" forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"Order for %@", [self getDateString:[[self.displayOrderArray objectAtIndex:indexPath.row] valueForKey:@"orderCreatedDate"]]];

    NSString * printed = [NSString stringWithFormat:@"%@", [[[self.displayOrderArray objectAtIndex:indexPath.row]valueForKey:@"orderPrinted"]boolValue]?@"Order Printed for Production \n":@""];

    NSString * fulfilled = [NSString stringWithFormat:@"%@", [[[self.displayOrderArray objectAtIndex:indexPath.row]valueForKey:@"orderFulfilled"]boolValue]?[NSString stringWithFormat:@"Order Fulfilled by %@ on %@", [[self.displayOrderArray objectAtIndex:indexPath.row]valueForKey:@"orderFulfilledBy"], [self getDateString:[[self.displayOrderArray objectAtIndex:indexPath.row]valueForKey:@"orderFulfilledDate"]]]:@""];

    if([[[self.displayOrderArray objectAtIndex:indexPath.row]valueForKey:@"orderFulfilledS1"]boolValue])
       fulfilled= [fulfilled stringByAppendingString:@"\nOrder fulfilled for S1"];
    if([[[self.displayOrderArray objectAtIndex:indexPath.row]valueForKey:@"orderFulfilledS2"]boolValue])
        fulfilled=[fulfilled stringByAppendingString:@"\nOrder fulfilled for S2"];
    if([[[self.displayOrderArray objectAtIndex:indexPath.row]valueForKey:@"orderFulfilledS5"]boolValue])
        fulfilled=[fulfilled stringByAppendingString:@"\nOrder fulfilled for S5"];

    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@",printed, fulfilled];

    cell.textLabel.numberOfLines =0;
    cell.detailTextLabel.numberOfLines=0;

    if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor whiteColor];
    else
        cell.backgroundColor = [UIColor lightGrayColor];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"bakeryOrderProductSegue" sender:self];
}



#pragma mark - new order creator funciton

-(void)createNewBakeryOrder{
    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"PickingOrderList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];

    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];

    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];

    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    [file closeFile];

    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

    NSError * serializerError;
    NSArray * pickingOrders = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];

    if(serializerError)
        [self showErrorAlert:@"Could not save new orders to DB"];
    else{

#warning - change the location to something else for more data

        NSPredicate * locationPredicate = [NSPredicate predicateWithFormat:@"FromLocationId == %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"OFFICE"]];
        NSArray * bakeryPick = [pickingOrders filteredArrayUsingPredicate:locationPredicate];

        if(bakeryPick.count==0){
            [self loadOrderTable];
        }else{
            //create a DB entry for a new order
            NSError *error;
            NSManagedObject *newManagedObject = [NSEntityDescription
                                                 insertNewObjectForEntityForName:@"BakeryOrderList"
                                                 inManagedObjectContext:context];

            [newManagedObject setValue:[NSDate date] forKey:@"orderCreatedDate"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"orderCreatedBy"];
            [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"orderFulfilled"];
            [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"orderInProduction"];
            [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"orderPrinted"];
            [newManagedObject setValue:[NSNumber numberWithLong:self.maxOrderNumber+1] forKey:@"orderId"];
            [context save:&error];

            //create DB entries for all the products and quantities

            for(NSObject * product in bakeryPick){
                NSManagedObject *newManagedObject = [NSEntityDescription
                                                     insertNewObjectForEntityForName:@"BakeryOrderProductList"
                                                     inManagedObjectContext:context];

                [newManagedObject setValue:[NSNumber numberWithLong:self.maxOrderNumber+1] forKey:@"orderId"];
                if([product valueForKey:@"Brand"]!=[NSNull null]){
                    [newManagedObject setValue:[product valueForKey:@"Brand"] forKey:@"productBrand"];
                }
                [newManagedObject setValue:[product valueForKey:@"ProductId"] forKey:@"productId"];
                [newManagedObject setValue:[product valueForKey:@"ProductName"] forKey:@"productName"];
                [newManagedObject setValue:[[product valueForKey:@"Size"]stringValue] forKey:@"productSize"];
                [newManagedObject setValue:[product valueForKey:@"ToLocationId"] forKey:@"toLocationId"];
                [newManagedObject setValue:[product valueForKey:@"UomId"] forKey:@"uomId"];
                [newManagedObject setValue:[product valueForKey:@"Upc"] forKey:@"productUpc"];
                [newManagedObject setValue:[product valueForKey:@"DepartmentId"] forKey:@"departmentId"];
                [newManagedObject setValue:[NSNumber numberWithDouble:[[product valueForKey:@"QuantityRequested"] doubleValue]] forKey:@"requestedQuantity"];
                [context save:&error];
            }
            if(error) [self showErrorAlert:@"Could not save new orders to DB"];
            else [self loadOrderTable];
        }
    }
}


#pragma mark - load order table

-(void)loadOrderTable{
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"BakeryOrderList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];

    NSDate* startDate=[[NSDate date] dateByAddingTimeInterval:-14*24*60*60];
    NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"orderCreatedDate >= %@",startDate];
    [request setPredicate:datePredicate];

    NSSortDescriptor *eventSorter=[[NSSortDescriptor alloc]initWithKey:@"orderCreatedDate" ascending:NO];
    [request setSortDescriptors:[[NSArray alloc] initWithObjects:eventSorter, nil]];
    [request setEntity:entityDesc];

    NSError *error;
    NSArray *results= [context executeFetchRequest:request error:&error];

    if(error) {
        [self showErrorAlert:@"Could not load Order List, Please try again"];
        self.orderTableData = [[NSMutableArray alloc]init];
    }
    else{
        self.orderTableData=[[NSMutableArray alloc]initWithArray:results];
        self.displayOrderArray = [[NSMutableArray alloc]initWithArray:self.orderTableData];
    }

}



#pragma mark - random functions
- (BOOL)daysBetween:(NSDate *)dt1{
    NSDateComponents *componentsFromPrevious = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:dt1];
    NSDateComponents *componentsCurrent = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:[NSDate date]];
    if(componentsCurrent.day==componentsFromPrevious.day)
        return FALSE;
    else
        return TRUE;
}

-(NSString *)getDateString:(NSDate *)date{
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    return [formatter stringFromDate:date];
}

#pragma mark - alert controllers funciton

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


 #pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if([segue.identifier isEqualToString:@"bakeryOrderProductSegue"]){
         CNFBakeryOrderProductsTableViewController * bakeryOrderProductsTable = segue.destinationViewController;
         bakeryOrderProductsTable.currentOrder = [self.displayOrderArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];

         NSUInteger indexForSelectedObject = [self.orderTableData indexOfObject:[self.displayOrderArray objectAtIndex:[self.tableView indexPathForSelectedRow].row]];

         if(self.orderTableData.count == indexForSelectedObject +1){
             bakeryOrderProductsTable.previousOrder = NULL;
         }else{
             bakeryOrderProductsTable.previousOrder = [self.orderTableData objectAtIndex:indexForSelectedObject+1];
         }
     }
 }

#pragma mark - order segment changed function

- (IBAction)orderSegmentChanged:(id)sender {

    switch (self.orderSegment.selectedSegmentIndex)
    {
        case 0:
            self.displayOrderArray = [[NSMutableArray alloc]initWithArray:self.orderTableData];
            [self.tableView reloadData];
            break;
        case 1:
            [self.displayOrderArray removeAllObjects];
            NSPredicate * fulfilledPred = [NSPredicate predicateWithFormat:@"orderFulfilled == %@ OR (orderFulfilledS1 == %@ AND orderFulfilledS2 == %@ AND orderFulfilledS5 == %@)", [NSNumber numberWithBool: YES], [NSNumber numberWithBool: YES], [NSNumber numberWithBool: YES], [NSNumber numberWithBool: YES]];
            self.displayOrderArray = [[NSMutableArray alloc]initWithArray:[self.orderTableData filteredArrayUsingPredicate:fulfilledPred]];
            [self.tableView reloadData];
            break;
    }
}
@end
