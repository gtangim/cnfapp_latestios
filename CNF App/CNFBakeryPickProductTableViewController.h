//
//  CNFBakeryPickProductTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-10-31.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFBakeryPickProductTableViewController : UITableViewController<NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSMutableArray * calculatedOrderProductArray;
@property (nonatomic, strong) NSMutableArray * displayProductArray;
@property (nonatomic, strong) NSString * toLocationId;
@property (nonatomic, strong) NSObject * currentOrder;


@end
