//
//  CNFBakeryPickProductTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-10-31.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFBakeryPickProductTableViewController.h"
#import "CNFAppDelegate.h"
#import "CNFGetDocumentId.h"
#import "BakeryPickProductTableViewCell.h"

#import "WarehousePicking.h"
#import "QuantityDetails.h"
#import "WarehousePickingItem.h"

#import "CNFPrintQRCodeViewController.h"

@interface CNFBakeryPickProductTableViewController ()

@property (nonatomic, strong) NSMutableArray * productList;
@property (nonatomic, strong) NSMutableArray * sectionList;
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * transferNumbers;

@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSDate * endTime;
@property (nonatomic, strong) NSString * documentId;
@end

@implementation CNFBakeryPickProductTableViewController{
    NSManagedObjectContext * context;
    UIAlertController * statusAlert;
}

@synthesize displayProductArray=_displayProductArray, calculatedOrderProductArray=_calculatedOrderProductArray, toLocationId=_toLocationId, currentOrder=_currentOrder;
@synthesize context=_context, transferNumbers=_transferNumbers;
@synthesize startTime=_startTime, endTime=_endTime, documentId=_documentId;
@synthesize productList=_productList, sectionList=_sectionList;

- (void)viewDidLoad {
    [super viewDidLoad];
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];

    if([self loadProductList]){
        if([self sortSectionsFromFile]){
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submitPick)];
        }else{
            [self showErrorAlert:@"Could not load product sections, please try again"];
        }
    }else{
        [self showErrorAlert:@"Error loading product list, please try again"];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)initializePicking{

    self.startTime = [NSDate date];
    self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];

    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:@"PICKING" forKey:@"documentType"];
    [newManagedObject setValue:@"Created" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.startTime forKey:@"date"];
    [context save:&error];

    if(error)
        return FALSE;
    else
        return TRUE;
}


#pragma mark - product list load funciton

-(BOOL)loadProductList{
    @try{
        NSError * error;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* finalPath = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery.csv"];

        NSString *lines = [NSString stringWithContentsOfFile:finalPath encoding:NSASCIIStringEncoding error:&error];

        if(error) return false;
        else{
            NSArray * subSectionArray = [lines componentsSeparatedByString:@"\n"];
            if(!self.productList) self.productList= [[NSMutableArray alloc]init];
            else [self.productList removeAllObjects];

            if(self.calculatedOrderProductArray==nil && self.toLocationId){
                for(id product in self.displayProductArray){
                    NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [product valueForKey:@"productId"]];
                    NSArray * filteredArray = [subSectionArray filteredArrayUsingPredicate:prodPred];

                    if(filteredArray.count>0){

                        NSString * section = [[filteredArray.firstObject componentsSeparatedByString:@","][3] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        NSString * sub_section = [[filteredArray.firstObject componentsSeparatedByString:@","][4] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];


                        if([product isMemberOfClass:[NSDictionary class]]){
                            NSMutableDictionary * dict = product;
                            [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                            [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                            [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                            [self.productList addObject:dict];

                        }else if([product isMemberOfClass:[NSManagedObject class]]){
                            NSArray *keys = [[[product entity] attributesByName] allKeys];
                            NSMutableDictionary * dict = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                            [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                            [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                            [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }else{
                            NSMutableDictionary * dict = product;
                            [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                            [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                            [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredArray.firstObject]] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }
                    }else{
                        if([product isMemberOfClass:[NSDictionary class]]){
                            NSMutableDictionary * dict = product;
                            [dict setValue:@"Unassigned" forKey:@"sub_section"];
                            [dict setValue:@"Unassigned" forKey:@"section"];
                            [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                            [self.productList addObject:dict];

                        }else if([product isMemberOfClass:[NSManagedObject class]]){
                            NSArray *keys = [[[product entity] attributesByName] allKeys];
                            NSMutableDictionary * dict = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                            //                    [dict setValue:@"Others" forKey:@"section"];
                            [dict setValue:@"Unassigned" forKey:@"sub_section"];
                            [dict setValue:@"Unassigned" forKey:@"section"];
                            [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }else{
                            NSMutableDictionary * dict = product;
                            //                    [dict setValue:@"Others" forKey:@"section"];
                            [dict setValue:@"Unassigned" forKey:@"sub_section"];
                            [dict setValue:@"Unassigned" forKey:@"section"];
                            [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }
                    }
                }
            }else{
                for(id product in self.displayProductArray){
                    NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"productId == %@", [product valueForKey:@"productId"]];
                    NSArray * filteredArray = [self.calculatedOrderProductArray filteredArrayUsingPredicate:prodPred];

                    NSMutableArray * detailedQuantityArray = [[NSMutableArray alloc]init];

                    for (NSObject * filteredProduct in filteredArray){
                        NSMutableDictionary * detailedQuantity = [[NSMutableDictionary alloc]init];
                        [detailedQuantity setValue:[NSNumber numberWithDouble:[[filteredProduct valueForKey:@"requestedQuantity"]doubleValue]] forKey:@"quantity"];
                        [detailedQuantity setValue:[filteredProduct valueForKey:@"toLocationId"] forKey:@"location"];
                        [detailedQuantityArray addObject:detailedQuantity];
                    }

                    NSPredicate * produpcPred = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [product valueForKey:@"productId"]];
                    NSArray * filteredsubArray = [subSectionArray filteredArrayUsingPredicate:produpcPred];

                    if(filteredsubArray.count>0){

                        NSString * section = [[filteredsubArray.firstObject componentsSeparatedByString:@","][3] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        NSString * sub_section = [[filteredsubArray.firstObject componentsSeparatedByString:@","][4] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];


                        if([product isMemberOfClass:[NSDictionary class]]){
                            NSMutableDictionary * dict = product;
                            [dict setValue:detailedQuantityArray forKey:@"detailedQuantity"];
                            [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                            [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                            [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredsubArray.firstObject]] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }else if([product isMemberOfClass:[NSManagedObject class]]){
                            NSArray *keys = [[[product entity] attributesByName] allKeys];
                            NSMutableDictionary * dict = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                            [dict setValue:detailedQuantityArray forKey:@"detailedQuantity"];
                            [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                            [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                            [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredsubArray.firstObject]] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }else{
                            NSMutableDictionary * dict = product;
                            [dict setValue:detailedQuantityArray forKey:@"detailedQuantity"];
                            [dict setValue:sub_section.length <=0? @"Unassigned": sub_section forKey:@"sub_section"];
                            [dict setValue:section.length <=0? @"Unassigned": section forKey:@"section"];
                            [dict setValue:[NSNumber numberWithInteger:[subSectionArray indexOfObject:filteredsubArray.firstObject]] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }
                    }else{
                        if([product isMemberOfClass:[NSDictionary class]]){
                            NSMutableDictionary * dict = product;
                            [dict setValue:detailedQuantityArray forKey:@"detailedQuantity"];
                            [dict setValue:@"Unassigned" forKey:@"sub_section"];
                            [dict setValue:@"Unassigned" forKey:@"section"];
                            [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }else if([product isMemberOfClass:[NSManagedObject class]]){
                            NSArray *keys = [[[product entity] attributesByName] allKeys];
                            NSMutableDictionary * dict = [[product dictionaryWithValuesForKeys:keys] mutableCopy];
                            [dict setValue:detailedQuantityArray forKey:@"detailedQuantity"];
                            [dict setValue:@"Unassigned" forKey:@"sub_section"];
                            [dict setValue:@"Unassigned" forKey:@"section"];
                            [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }else{
                            NSMutableDictionary * dict = product;
                            [dict setValue:detailedQuantityArray forKey:@"detailedQuantity"];
                            [dict setValue:@"Unassigned" forKey:@"sub_section"];
                            [dict setValue:@"Unassigned" forKey:@"section"];
                            [dict setObject:[NSNumber numberWithInteger:0] forKey:@"seq"];
                            [self.productList addObject:dict];
                        }
                    }
                }
            }
            return true;
        }
    }@catch(NSException * ex){
        return false;
    }
}

-(BOOL)sortSectionsFromFile{
    NSError * error;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* finalPath = [documentsDirectory stringByAppendingPathComponent:@"Section_Product_Relationship_bakery.csv"];

    NSString *lines = [NSString stringWithContentsOfFile:finalPath encoding:NSASCIIStringEncoding error:&error];

    if(error) return FALSE;
    else{
        NSArray * sectionLines = [lines componentsSeparatedByString:@"\n"];
        NSArray* sections =[self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];
        self.sectionList = [[NSMutableArray alloc]init];

        @try{
            for(NSString * section in sections){
                NSPredicate * sectiondPred = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", section];
                NSArray * filteredArray = [sectionLines filteredArrayUsingPredicate:sectiondPred];

                NSMutableDictionary * sectionDict = [[NSMutableDictionary alloc]init];

                if(filteredArray.count==0){
                    [sectionDict setValue:@"Unassigned"  forKey:@"section"];
                    [sectionDict setValue:[NSNumber numberWithInteger:0] forKey:@"sectionSeq"];
                }else{
                    NSString * section = [[filteredArray.firstObject componentsSeparatedByString:@","][3] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    [sectionDict setValue:section  forKey:@"section"];
                    [sectionDict setValue:[NSNumber numberWithInteger:[sectionLines indexOfObject:filteredArray.firstObject]] forKey:@"sectionSeq"];
                }

                [self.sectionList addObject:sectionDict];
            }

            NSSortDescriptor * sectionSorter = [NSSortDescriptor sortDescriptorWithKey:@"sectionSeq" ascending:YES];
            [self.sectionList sortUsingDescriptors:[[NSArray alloc]initWithObjects:sectionSorter, nil]];

            return TRUE;
        }@catch(NSException * ex){
            return FALSE;
        }
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.productList && self.sectionList) return self.sectionList.count;
    else return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(self.productList && self.sectionList){
        return [[self.sectionList objectAtIndex:section] valueForKey:@"section"];
    }
    else return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.productList){
        NSPredicate * sectionPred = [NSPredicate predicateWithFormat:@"section == %@", [[self.sectionList objectAtIndex:section]valueForKey:@"section"]];
        return [[self.productList filteredArrayUsingPredicate:sectionPred] count];
    } else return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 115;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    BakeryPickProductTableViewCell *cell = (BakeryPickProductTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bakeryPickProductCells"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"bakeryPickProductCells" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

//    NSArray* sectionArray =[self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];
    NSPredicate * sectionPred = [NSPredicate predicateWithFormat:@"section == %@", [[self.sectionList objectAtIndex:indexPath.section]valueForKey:@"section"]];

    NSArray * productBySectionArray = [self.productList filteredArrayUsingPredicate:sectionPred];

    if(productBySectionArray.count>0){

        //need to sort product array by section by sub_section by seq

//        NSSortDescriptor * subSectionSort = [NSSortDescriptor sortDescriptorWithKey:@"sub_section" ascending:YES];
        NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:YES];
        productBySectionArray=[productBySectionArray sortedArrayUsingDescriptors:[[NSArray alloc]initWithObjects:seqSort,nil]];

        cell.productNameLabel.text = productBySectionArray.firstObject?[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"productName"]:@"";
        cell.productUpcLabel.text = [NSString stringWithFormat:@"Product UPC: %@", [[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"productUpc"]];
        cell.sectionLabel.text = [[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"sub_section"];

        if(self.calculatedOrderProductArray==nil && self.toLocationId){
            cell.totalQuantityLabel.text = nil;
            cell.reasonLabel.text = nil;
            if([self.toLocationId isEqualToString:@"S1"]){
                cell.s1Label.text = [NSString stringWithFormat:@"%@ %@", [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]stringValue], [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"uomId"]componentsSeparatedByString:@"_" ][1]];
                cell.s2Label.text = cell.s5Label.text = @"";
            }else if([self.toLocationId isEqualToString:@"S2"]){
                cell.s2Label.text = [NSString stringWithFormat:@"%@ %@", [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]stringValue], [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"uomId"]componentsSeparatedByString:@"_" ][1]];
                cell.s1Label.text= cell.s5Label.text = @"";
            }else if([self.toLocationId isEqualToString:@"S5"]){
                cell.s5Label.text = [NSString stringWithFormat:@"%@ %@", [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]stringValue], [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"uomId"]componentsSeparatedByString:@"_" ][1]];
                cell.s1Label.text=cell.s2Label.text= @"";
            }else{
                cell.s1Label.text=cell.s2Label.text = cell.s5Label.text = @"";
            }
        }else{
            cell.totalQuantityLabel.text = [NSString stringWithFormat:@"Shipping Quantity: %@ %@", [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"requestedQuantity"]stringValue], [[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"uomId"]componentsSeparatedByString:@"_" ][1]];;

            NSPredicate * s1locaitonPred = [NSPredicate predicateWithFormat:@"location == %@", @"S1"];
            NSPredicate * s2locaitonPred = [NSPredicate predicateWithFormat:@"location == %@", @"S2"];
            NSPredicate * s5locaitonPred = [NSPredicate predicateWithFormat:@"location == %@", @"S5"];

            if([[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:s1locaitonPred].count>0){
                cell.s1Label.text = [NSString stringWithFormat:@"%@ %@", [[[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:s1locaitonPred].firstObject valueForKey:@"quantity"]stringValue],[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"uomId"]componentsSeparatedByString:@"_" ][1]];
            }else{
                cell.s1Label.text = @"";
            }

            if([[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:s2locaitonPred].count>0){
                cell.s2Label.text = [NSString stringWithFormat:@"%@ %@", [[[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:s2locaitonPred].firstObject valueForKey:@"quantity"]stringValue],[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"uomId"]componentsSeparatedByString:@"_" ][1]];
            }else{
                cell.s2Label.text = @"";
            }

            if([[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:s5locaitonPred].count>0){
                cell.s5Label.text = [NSString stringWithFormat:@"%@ %@", [[[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:s5locaitonPred].firstObject valueForKey:@"quantity"]stringValue],[[[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"uomId"]componentsSeparatedByString:@"_" ][1]];
            }else{
                cell.s5Label.text = @"";
            }

            NSMutableString * reason = [[NSMutableString alloc]init];
            for(NSObject * locationObjects in [[productBySectionArray objectAtIndex:indexPath.row] valueForKey:@"detailedQuantity"]){
                if([locationObjects valueForKey:@"reason"]){
                    [reason appendString:[NSString stringWithFormat:@" %@;",[locationObjects valueForKey:@"reason"]]];
                }
            }
            cell.reasonLabel.text = reason;

        }
    }
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self.productList removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];

    return @[deleteAction];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self quantityAlert];
}


#pragma mark - Alert funciton

-(void)quantityAlert{

    UIAlertController * quantityAlertController;

//    NSArray* sectionArray =[self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];
    NSPredicate * sectionPred = [NSPredicate predicateWithFormat:@"section == %@", [[self.sectionList objectAtIndex:self.tableView.indexPathForSelectedRow.section]valueForKey:@"section"]];

    NSArray * productBySectionArray = [self.productList filteredArrayUsingPredicate:sectionPred];

    if(productBySectionArray.count>0){
//        NSSortDescriptor * subSectionSort = [NSSortDescriptor sortDescriptorWithKey:@"sub_section" ascending:YES];
        NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:YES];
        productBySectionArray=[productBySectionArray sortedArrayUsingDescriptors:[[NSArray alloc]initWithObjects:seqSort,nil]];

        NSObject * product = [productBySectionArray objectAtIndex:self.tableView.indexPathForSelectedRow.row];

        if(!self.calculatedOrderProductArray && self.toLocationId){

            quantityAlertController = [UIAlertController alertControllerWithTitle:[product valueForKey:@"productName"] message:[NSString stringWithFormat:@"Please enter corrected quantity in %@ for location %@", @"ea", self.toLocationId] preferredStyle:UIAlertControllerStyleAlert];

            [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                [textField setKeyboardType:UIKeyboardTypeNumberPad];
                textField.text = [product valueForKey:@"requestedQuantity"]?
                [[product valueForKey:@"requestedQuantity"] stringValue]:
                [[product valueForKey:@"requestedQuantity"] stringValue];
                [textField becomeFirstResponder];
            }];

            UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                UITextField * quantityTextField = quantityAlertController.textFields.firstObject;
                if([quantityTextField hasText]){
                    NSCharacterSet* nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                    NSRange r = [quantityTextField.text rangeOfCharacterFromSet: nonNumbers];
                    if( r.location == NSNotFound && quantityTextField.text.length > 0) {
                        [product setValue:[NSNumber numberWithDouble:[quantityTextField.text doubleValue]] forKey:@"requestedQuantity"];
                        [self.tableView reloadData];
                    }else{
                        [self showErrorAlert:@"Please enter only numbers in quantity"];
                    }
                }else [self showErrorAlert:@"Please enter quantity"];
            }];

            [quantityAlertController addAction:okAction];

        }else{
            quantityAlertController = [UIAlertController alertControllerWithTitle:[product valueForKey:@"productName"] message:[NSString stringWithFormat:@"Please select location to correct quantity"] preferredStyle:UIAlertControllerStyleAlert];

            NSArray * locationArray = [NSArray arrayWithObjects:@"S1",@"S2", @"S5", nil];

            for(NSString * location in locationArray){
                UIAlertAction * modifiedAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Location: %@", location] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [self multiLocationQuantityAlert:location index:[self.tableView indexPathForSelectedRow]];
                }];
                [quantityAlertController addAction:modifiedAction];
            }
        }

        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            //cancel action
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [quantityAlertController addAction:cancelAction];
        [self presentViewController:quantityAlertController animated:YES completion:nil];
    }
}

-(void)multiLocationQuantityAlert:(NSString*)location index:(NSIndexPath*)index{

//    NSArray* sectionArray =[self.productList valueForKeyPath:@"@distinctUnionOfObjects.section"];
    NSPredicate * sectionPred = [NSPredicate predicateWithFormat:@"section == %@", [[self.sectionList objectAtIndex:index.section]valueForKey:@"section"]];

    NSArray * productBySectionArray = [self.productList filteredArrayUsingPredicate:sectionPred];

//    NSSortDescriptor * subSectionSort = [NSSortDescriptor sortDescriptorWithKey:@"sub_section" ascending:YES];
    NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"seq" ascending:YES];
    productBySectionArray=[productBySectionArray sortedArrayUsingDescriptors:[[NSArray alloc]initWithObjects:seqSort,nil]];

    NSObject * product = [productBySectionArray objectAtIndex:index.row];

    NSString * uom =  [[product valueForKey:@"uomId"] componentsSeparatedByString:@"_"][1];
    UIAlertController * quantityAlertController = [UIAlertController alertControllerWithTitle:[product valueForKey:@"productName"] message:[NSString stringWithFormat:@"Please enter corrected quantity in %@ for location %@", uom, location] preferredStyle:UIAlertControllerStyleAlert];

    [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        [textField setKeyboardType:UIKeyboardTypeNumberPad];

        NSPredicate * locationPred = [NSPredicate predicateWithFormat:@"location == %@", location];
        NSObject * filteredlocaion = [[product valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:locationPred].firstObject;

        if([[filteredlocaion valueForKey:@"quantity"]intValue]==0){
            textField.placeholder = @"Enter Quantity";
        }else{
            textField.text = [NSString stringWithFormat:@"%d", [[filteredlocaion valueForKey:@"quantity"]intValue]];
        }
        [textField becomeFirstResponder];
    }];

    [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        [textField setKeyboardType:UIKeyboardTypeDefault];

        NSPredicate * locationPred = [NSPredicate predicateWithFormat:@"location == %@", location];
        NSObject * filteredlocaion = [[product valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:locationPred].firstObject;

        if(![filteredlocaion valueForKey:@"reason"]){
            textField.placeholder = @"Enter Reason";
        }else{
            textField.text = [NSString stringWithFormat:@"%@", [filteredlocaion valueForKey:@"reason"]?[filteredlocaion valueForKey:@"reason"]:@""];
        }
    }];

    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField * quantityTextField = quantityAlertController.textFields.firstObject;
        UITextField * reasonTextField = [quantityAlertController.textFields objectAtIndex:1];

        if([quantityTextField hasText]){
            NSCharacterSet* nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            NSRange r = [quantityTextField.text rangeOfCharacterFromSet: nonNumbers];
            if( r.location == NSNotFound && quantityTextField.text.length > 0) {

                id product_new = product;

                NSPredicate * locationPred = [NSPredicate predicateWithFormat:@"location == %@", location];
                NSArray * filteredLocationArray = [[product valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:locationPred];

                if(filteredLocationArray.count==1){
                    NSObject * filteredlocaion = filteredLocationArray.firstObject;

                    if([[quantityTextField text] doubleValue] == [[filteredlocaion valueForKey:@"quantity"] doubleValue]){
                        //no reason needed for this
                        [filteredlocaion setValue:[NSNumber numberWithDouble:[[quantityTextField text] doubleValue]] forKey:@"quantity"];
                        [[product_new valueForKey:@"detailedQuantity"]replaceObjectAtIndex:[[product_new valueForKey:@"detailedQuantity"]indexOfObject:filteredlocaion] withObject:filteredlocaion];

                        double quantity = 0.0;
                        for(NSObject * locQuan in [product_new valueForKey:@"detailedQuantity"]){
                            quantity = quantity + [[locQuan valueForKey:@"quantity"] doubleValue];
                        }
                        [product_new setValue:[NSNumber numberWithDouble:quantity] forKey:@"requestedQuantity"];
//                        [self.productList replaceObjectAtIndex:[self.tableView indexPathForSelectedRow].row withObject:product];
                        [self.productList replaceObjectAtIndex:[self.productList indexOfObject:product] withObject:product_new];
                    }else{
                        [filteredlocaion setValue:[NSNumber numberWithDouble:[filteredlocaion valueForKey:@"orderedQuantity"]?[[filteredlocaion valueForKey:@"orderedQuantity"]doubleValue]:[[filteredlocaion valueForKey:@"quantity"] doubleValue]] forKey:@"orderedQuantity"];
                        [filteredlocaion setValue:[NSNumber numberWithDouble:[[quantityTextField text] doubleValue]] forKey:@"quantity"];
                        [filteredlocaion setValue: [reasonTextField hasText]?reasonTextField.text:@"N/A" forKey:@"reason"];
                        [[product_new valueForKey:@"detailedQuantity"]replaceObjectAtIndex:[[product_new valueForKey:@"detailedQuantity"]indexOfObject:filteredlocaion] withObject:filteredlocaion];

                        double quantity = 0.0;
                        for(NSObject * locQuan in [product_new valueForKey:@"detailedQuantity"]){
                            quantity = quantity + [[locQuan valueForKey:@"quantity"] doubleValue];
                        }
                        [product setValue:[NSNumber numberWithDouble:quantity] forKey:@"requestedQuantity"];
//                        [self.productList replaceObjectAtIndex:[self.tableView indexPathForSelectedRow].row withObject:product];
                        [self.productList replaceObjectAtIndex:[self.productList indexOfObject:product] withObject:product_new];
                    }

                }else{
                    if([reasonTextField hasText]){
                        //location not found, change object with added location
                        NSMutableDictionary * newLocation = [[NSMutableDictionary alloc]init];
                        [newLocation setValue:location forKey:@"location"];
                        [newLocation setValue:[NSNumber numberWithDouble:0.0] forKey:@"orderedQuantity"];
                        [newLocation setValue:[NSNumber numberWithDouble:[[quantityTextField text] doubleValue]] forKey:@"quantity"];
                        [newLocation setValue:reasonTextField.text forKey:@"reason"];
                        NSMutableArray * newDetailedQuantity = [product valueForKey:@"detailedQuantity"];
                        [newDetailedQuantity addObject:newLocation];
                        [product setValue:newDetailedQuantity forKey:@"detailedQuantity"];

                        double quantity = 0.0;
                        for(NSObject * locQuan in newDetailedQuantity){
                            quantity = quantity + [[locQuan valueForKey:@"quantity"] doubleValue];
                        }
                        [product setValue:[NSNumber numberWithDouble:quantity] forKey:@"requestedQuantity"];
                    }else{
                        [self showErrorAlert:@"Cannot change the order quantity without providing reason"];
                    }
                }

            }else{
                [self showErrorAlert:@"Please enter only numbers in quantity"];
            }
        }else [self showErrorAlert:@"Please enter quantity"];

        [self.tableView reloadData];
    }];

    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        //cancel action
        [self dismissViewControllerAnimated:YES completion:nil];
    }];

    [quantityAlertController addAction:okAction];
    [quantityAlertController addAction:cancelAction];
    [self presentViewController:quantityAlertController animated:YES completion:nil];

}



-(void)submitPick{

        UIAlertController *finishAlertController = [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"Do you want to submit transfer?"
                                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yesaction = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        [self startSpinner:@"Saving Data..."];
                                        [self savePickToFile];

                                        dispatch_async(dispatch_get_main_queue(), ^{

                                            self.productList=Nil;
                                            self.currentOrder=nil;
                                            self.calculatedOrderProductArray=nil;
                                            self.displayProductArray=nil;

                                            [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                [self performSegueWithIdentifier:@"printBarcodeFromBakerySegue" sender:self];
                                            }];
                                        });
                                    }];

        UIAlertAction *noaction = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [finishAlertController dismissViewControllerAnimated:YES completion:nil];
                                   }];

        [finishAlertController addAction:yesaction];
        [finishAlertController addAction:noaction];
        [self presentViewController:finishAlertController animated:YES completion:nil];
}

-(void)showErrorAlert:(NSString *)message{

    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

#pragma mark - alertview and indicator spin handler

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];

        UIViewController *customVC     = [[UIViewController alloc] init];

        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];


        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];


        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];

        [statusAlert setValue:customVC forKey:@"contentViewController"];

        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:nil];
        statusAlert = nil;
    }
}

#pragma mark - save file method
-(void)savePickToFile{
    if(self.calculatedOrderProductArray==nil && self.toLocationId){

        [self initializePicking];
        NSMutableArray<WarehousePickingItem> * pickedItems = [[NSMutableArray<WarehousePickingItem> alloc]init];

        for(NSObject * product in self.productList){
            WarehousePickingItem * item = [[WarehousePickingItem alloc]init];
            item.ProductId = [product valueForKey:@"productId"];
            item.DepartmentId = [product valueForKey:@"departmentId"];
            QuantityDetails * itemQuantity = [[QuantityDetails alloc]init];
            itemQuantity.Quantity = [NSNumber numberWithDouble:[[product valueForKey:@"requestedQuantity"] doubleValue]];
            itemQuantity.UomId = [product valueForKey:@"uomId"];
            item.Amount=itemQuantity;
            [pickedItems addObject:item];
        }

        self.endTime = [NSDate date];
        WarehousePicking * pickingObject = [[WarehousePicking alloc]init];

        pickingObject.TransferNumber = [NSString stringWithFormat:@"T%@%@%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"], self.toLocationId, [self.documentId componentsSeparatedByString:@"_"][1]];
        pickingObject.WarehouseLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        pickingObject.ReceivingLocationId = self.toLocationId;

        NSDate *pickForDate = [self.currentOrder valueForKey:@"orderCreatedDate"];

        NSCalendar *cal = [NSCalendar currentCalendar];
        [cal setTimeZone:[NSTimeZone localTimeZone]];
        [cal setLocale:[NSLocale currentLocale]];
        NSDateComponents *toDateComponents = [cal components:( NSCalendarUnitYear   |
                                                              NSCalendarUnitMonth  |
                                                              NSCalendarUnitDay      ) fromDate:pickForDate];


        pickingObject.PickForDate= [cal dateFromComponents:toDateComponents];
        pickingObject.SubmissionDateTime = self.endTime;
        pickingObject.SubmittedByUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
        pickingObject.Items = pickedItems;

        NSLog(@"object creation successful..., save file to directory..");

        NSError *error;
        NSManagedObject *newManagedObject = [NSEntityDescription
                                             insertNewObjectForEntityForName:@"DocStatusInfo"
                                             inManagedObjectContext:context];

        [newManagedObject setValue:@"PICKING" forKey:@"documentType"];
        [newManagedObject setValue:@"Finished" forKey:@"status"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [newManagedObject setValue:self.documentId forKey:@"documentId"];
        [newManagedObject setValue:self.endTime forKey:@"date"];
        [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
        [context save:&error];

        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"BakeryOrderList"
                                                  inManagedObjectContext:context];
        [request setEntity:entity];

        request.predicate = [NSPredicate predicateWithFormat:@"orderId == %ld", [[self.currentOrder valueForKey:@"orderId"]longValue]];

        NSArray *array = [context executeFetchRequest:request error:&error];

        if(array.count>0){
            if([self.toLocationId isEqualToString:@"S1"]){
                [array.firstObject setValue:[NSNumber numberWithBool:YES] forKey:@"orderFulfilledS1"];
                [context save:&error];
            }else if([self.toLocationId isEqualToString:@"S2"]){
                [array.firstObject setValue:[NSNumber numberWithBool:YES] forKey:@"orderFulfilledS2"];
                [context save:&error];
            }else if([self.toLocationId isEqualToString:@"S5"]){
                [array.firstObject setValue:[NSNumber numberWithBool:YES] forKey:@"orderFulfilledS5"];
                [context save:&error];
            }else{
                [array.firstObject setValue:[NSNumber numberWithBool:YES] forKey:@"orderFulfilled"];
                [context save:&error];
            }
        }

        if(error){
            [self stopSpinner];
            [self showErrorAlert:@"Error saving records to DB"];
            return;
        }else{
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

            NSString *stringFromDate = [formatter stringFromDate:self.endTime];
            [self writeStringToFile:[pickingObject toJSONString] fileName:stringFromDate];
            NSLog(@"File Saved!!!");
        }
    }else{

        self.transferNumbers = [[NSMutableArray alloc]init];

        NSMutableArray * allLocations = [[NSMutableArray alloc]init];

        for(NSObject * product in self.productList){
            for(NSString * locations in [product valueForKey:@"detailedQuantity"]){
                [allLocations addObject:locations];
            }
        }

        NSArray *locationgroups = [allLocations valueForKeyPath:@"@distinctUnionOfObjects.location"];


        //change the mili second portion for the endtime variation
        int i = 1;

        for(NSString * location in locationgroups){

            //get new documentId everytime
            [self initializePicking];

            NSMutableArray<WarehousePickingItem> * pickedItems = [[NSMutableArray<WarehousePickingItem> alloc]init];

            for(NSObject * product in self.productList){
                NSPredicate * filterUpdatedArrayPred = [NSPredicate predicateWithFormat:@"location == %@", location];
                NSArray * filteredUpdatedArray = [[product valueForKey:@"detailedQuantity"] filteredArrayUsingPredicate:filterUpdatedArrayPred];
                if(filteredUpdatedArray.count>0){
                    WarehousePickingItem * item = [[WarehousePickingItem alloc]init];
                    item.ProductId = [product valueForKey:@"productId"];
                    item.DepartmentId = [product valueForKey:@"departmentId"];
                    QuantityDetails * itemQuantity = [[QuantityDetails alloc]init];
                    itemQuantity.Quantity = [NSNumber numberWithDouble:[[filteredUpdatedArray.firstObject valueForKey:@"quantity"] doubleValue]];
                    itemQuantity.UomId = [product valueForKey:@"uomId"];
                    item.Amount=itemQuantity;
                    [pickedItems addObject:item];
                }
            }

            self.endTime = [[NSDate date] dateByAddingTimeInterval:i];

            WarehousePicking * pickingObject = [[WarehousePicking alloc]init];
            pickingObject.TransferNumber = [NSString stringWithFormat:@"T%@%@%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"], location, [self.documentId componentsSeparatedByString:@"_"][1]];
            pickingObject.WarehouseLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
            pickingObject.ReceivingLocationId = location;

            NSDate *pickForDate = [self.currentOrder valueForKey:@"orderCreatedDate"];

            NSCalendar *cal = [NSCalendar currentCalendar];
            [cal setTimeZone:[NSTimeZone localTimeZone]];
            [cal setLocale:[NSLocale currentLocale]];
            NSDateComponents *toDateComponents = [cal components:( NSCalendarUnitYear   |
                                                                  NSCalendarUnitMonth  |
                                                                  NSCalendarUnitDay      ) fromDate:pickForDate];

            pickingObject.PickForDate= [cal dateFromComponents:toDateComponents];
            pickingObject.SubmissionDateTime = self.endTime;
            pickingObject.SubmittedByUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
            pickingObject.Items = pickedItems;

            [self.transferNumbers addObject:pickingObject.TransferNumber];

            NSLog(@"object creation successful..., save file to directory..");

            NSError *error;
            NSManagedObject *newManagedObject = [NSEntityDescription
                                                 insertNewObjectForEntityForName:@"DocStatusInfo"
                                                 inManagedObjectContext:context];

            [newManagedObject setValue:@"PICKING" forKey:@"documentType"];
            [newManagedObject setValue:@"Finished" forKey:@"status"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [newManagedObject setValue:self.documentId forKey:@"documentId"];
            [newManagedObject setValue:self.endTime forKey:@"date"];
            [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
            [context save:&error];

            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"BakeryOrderList"
                                                      inManagedObjectContext:context];
            [request setEntity:entity];

            request.predicate = [NSPredicate predicateWithFormat:@"orderId == %ld", [[self.currentOrder valueForKey:@"orderId"]longValue]];

            NSArray *array = [context executeFetchRequest:request error:&error];

            if(array.count>0){
                [array.firstObject setValue:[NSNumber numberWithBool:YES] forKey:@"orderFulfilled"];
                [array.firstObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"orderFulfilledBy"];
                [array.firstObject setValue:[NSDate date] forKey:@"orderFulfilledDate"];
                [context save:&error];
            }

            //change i to one second to make sure the end time is different
            i++;

            if(error){
                [self stopSpinner];
                [self showErrorAlert:@"Error saving records to DB"];
                return;
            }else{
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

                NSString *stringFromDate = [formatter stringFromDate:self.endTime];
                [self writeStringToFile:[pickingObject toJSONString] fileName:stringFromDate];
                NSLog(@"File Saved!!!");
            }
        }
    }
}

#pragma mark - iphone file system handler

- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];

    if(error){
        [self stopSpinner];
        [self showErrorAlert:@"Error saving file to the directory, Please try again.."];
        return;
    }

    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }

    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"printBarcodeFromBakerySegue"]){
        CNFPrintQRCodeViewController * qrCodeViewContr = segue.destinationViewController;
        if(self.calculatedOrderProductArray==nil && self.toLocationId){
            qrCodeViewContr.transferNumber = [NSString stringWithFormat:@"T%@%@%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"], self.toLocationId, [self.documentId componentsSeparatedByString:@"_"][1]];
        }else{
            qrCodeViewContr.transferNumbers = self.transferNumbers;
        }
    }
}


@end
