//
//  CNFBakeryProductSectionRelationTableViewController.m
//  CNF App
//
//  Created by Anik on 2018-02-26.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import "CNFBakeryProductsTableViewController.h"

@interface CNFBakeryProductsTableViewController ()
@property (nonatomic, strong) NSMutableArray * productList;
@property (nonatomic, strong) NSMutableArray * sectionList;
@end

@implementation CNFBakeryProductSectionRelationTableViewController
@synthesize productList=_productList;

- (void)viewDidLoad {
    [super viewDidLoad];

    //read file and load Array
    if(![self readFileAndLoadArray]){
        self.productList = nil;
        [self showErrorAlert:@"Could not load file array, Please try again"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.productList) return self.productList.count;
    else return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bakerySectioProductCell" forIndexPath:indexPath];

    if(self.productList){

    }

    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


#pragma mark - load section file

-(BOOL)readFileAndLoadArray{
    NSError * error;
    NSString *lines = [NSString stringWithContentsOfFile:[[NSBundle mainBundle]
                                                          pathForResource: @"Section_Product_Relationship_bakery" ofType: @"csv"] encoding:NSASCIIStringEncoding error:&error];
    if(error) return FALSE;
    else{
        self.productList= [[NSMutableArray alloc]initWithArray:[lines componentsSeparatedByString:@"\n"]];
        return true;
    }
}

#pragma mark - alert controllers funciton

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

@end
