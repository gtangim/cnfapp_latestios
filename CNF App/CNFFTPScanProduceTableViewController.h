//
//  CNFFTPScanProduceTableViewController.h
//  CNF App
//
//  Created by Anik on 2018-06-20.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BoolPassDelegate<NSObject>
-(void)returnPhotoSelected:(BOOL)selected;
@end

@interface CNFFTPScanProduceTableViewController : UITableViewController<UIWebViewDelegate>
@property(weak,nonatomic)id<BoolPassDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
