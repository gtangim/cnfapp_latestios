//
//  CNFFTPScanProduceTableViewController.m
//  CNF App
//
//  Created by Anik on 2018-06-20.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import "CNFFTPScanProduceTableViewController.h"
#import "DLSFTP.h"
#import "DLSFTPConnection.h"
#import "DLSFTPListFilesRequest.h"
#import "DLSFTPDownloadRequest.h"
#import "DLSFTPFile.h"

@interface CNFFTPScanProduceTableViewController ()
@property (strong, nonatomic) DLSFTPConnection *connection;
@property (strong, nonatomic) NSMutableArray * ftpFiles;
@end

typedef void (^Completion)(BOOL success, NSString *msg);

@implementation CNFFTPScanProduceTableViewController

@synthesize connection=_connection, ftpFiles=_ftpFiles;
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ftpFiles=nil;
}

-(void)viewDidAppear:(BOOL)animated{
    UIAlertController *startAlertController = [UIAlertController
                                               alertControllerWithTitle:@"Scan Receipt"
                                               message:@"Please scan your document and press ok after you're done scanning"
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   [self startFTPScanning];
                               }];

    [startAlertController addAction:okAction];
    [self presentViewController:startAlertController animated:YES completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.connection=nil;
    self.ftpFiles=nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - FTP functions
-(void)startFTPScanning{
    [self.navigationItem setTitle:[[NSUserDefaults standardUserDefaults]valueForKey:@"DOCUMENT_NO"]];
    self.navigationItem.hidesBackButton = TRUE;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem=nil;
    if([self removeFilesFormManualDirectory])
        [self FTPConnectionTester];
    else{
        [self errorAlert:@"Could not clear directory, please try again"];
        [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Retry" style:UIBarButtonItemStylePlain target:self action:@selector(startFTPScanning)];
    }
}

-(void)FTPConnectionTester{

    @try{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

        NSString *username = @"CRSMobileFTP";
        NSString *password = @"CRSP@ssw0rd2017!";
        NSInteger port = 22;
        NSString *host = @"ftp.cnfltd.com";

        // make a connection object and attempt to connect
        DLSFTPConnection *connection = [[DLSFTPConnection alloc] initWithHostname:host
                                                                             port:port
                                                                         username:username
                                                                         password:password];
        self.connection=connection;

        DLSFTPClientSuccessBlock successBlock = ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Loading already existing files..");
                [self getFilesForLocation];
            });
        };

        DLSFTPClientFailureBlock failureBlock = ^(NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{

                self.connection=nil;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    [self showFailureAlert:[NSString stringWithFormat:@"Ftp connection error: %@, use Camera to take photo.", error.localizedDescription]];
                });
            });
        };
        [connection connectWithSuccessBlock:successBlock failureBlock:failureBlock];

    }@catch(NSException * ex){
        [self errorAlert:ex.description];
    }
}


-(void)getFilesForLocation{

    @try{
        DLSFTPClientArraySuccessBlock successBlock = ^(NSArray *files) {
            dispatch_async(dispatch_get_main_queue(), ^{


                self.ftpFiles = [[NSMutableArray alloc]initWithArray:files];
                NSMutableArray * otherFilesThanPdf = [[NSMutableArray alloc]init];

                for(DLSFTPFile * file in self.ftpFiles){
                    if(![file.filename containsString:@".pdf"]){
                        [otherFilesThanPdf addObject:file];
                    }
                }
                if(otherFilesThanPdf.count>0)
                    [self.ftpFiles removeObjectsInArray:otherFilesThanPdf];

                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"attributes.NSFileModificationDate"
                                                                               ascending:NO];
                NSArray *sortedArray = [self.ftpFiles sortedArrayUsingDescriptors:@[sortDescriptor]];

                if(sortedArray.count < 1){
                    [self showFailureAlert:@"No document found, Please take photo"];
                }else{
                    [self downloadNewfiles:sortedArray.firstObject completion:^(BOOL success, NSString *msg) {
                        if (success) {
                            NSLog(@"%@", msg);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self showPdfInWebView];
                            });
                            [self.connection disconnect];
                        } else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                [self showFailureAlert:@"Download Error, retry?"];
                            });
                        }
                    }];
                }
            });
        };

        DLSFTPClientFailureBlock failureBlock = ^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    [self showFailureAlert:@"No document found, Please take photo"];
//                    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Retry" style:UIBarButtonItemStylePlain target:self action:@selector(startFTPScanning)];
                });
                NSLog(@"Connecion problem..%@", error.localizedDescription);
            });
        };

        // begin loading files
        DLSFTPRequest *request = [[DLSFTPListFilesRequest alloc] initWithDirectoryPath:[NSString
                                                                                        stringWithFormat:@"/%@/", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]]
                                                                          successBlock:successBlock
                                                                          failureBlock:failureBlock];
        [self.connection submitRequest:request];
    }@catch(NSException * ex){
        [self errorAlert:ex.description];
    }

}

-(void)downloadNewfiles:(DLSFTPFile*)file completion :(Completion)block{

    @try{
        NSString * remotePath = file.path;

        NSError * error;
        NSString *localPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:localPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:localPath withIntermediateDirectories:NO attributes:nil error:&error];

        if(error)
            if (block) block(FALSE, @"Directory could not be created..");


        DLSFTPClientProgressBlock progressBlock = ^void(unsigned long long bytesReceived, unsigned long long bytesTotal) {

        };

        DLSFTPClientFileTransferSuccessBlock successBlock = ^(DLSFTPFile *file, NSDate *startTime, NSDate *finishTime) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *alertMessage = [NSString stringWithFormat:@"Downloaded %@", file.filename];
                if (block) block(TRUE, alertMessage);

            });
        };

        DLSFTPClientFailureBlock failureBlock = ^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *errorString = [NSString stringWithFormat:@"Error %ld", (long)error.code];
                if (block) block(FALSE, errorString);
            });
        };

        DLSFTPRequest *request = [[DLSFTPDownloadRequest alloc] initWithRemotePath:remotePath
                                                                         localPath:[localPath stringByAppendingPathComponent:file.filename]
                                                                            resume:NO
                                                                      successBlock:successBlock
                                                                      failureBlock:failureBlock
                                                                     progressBlock:progressBlock];
        [self.connection submitRequest:request];
    }@catch(NSException * ex){
        [self errorAlert:ex.description];
    }
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  [UIScreen mainScreen].bounds.size.height;
}

#pragma mark - scrollview of photos

-(void)showPdfInWebView{

    @try{
        self.webView.delegate=self;
        NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
        NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
        self.webView.backgroundColor = [UIColor clearColor];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@", [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/manual_images/%@",directoryContent.firstObject]]] isDirectory:NO]];
        [self.webView setScalesPageToFit:YES];
        [self.webView loadRequest:request];
    }@catch(NSException * ex){
        NSLog(@"Exception occured: %@", ex.description);
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self.navigationItem.leftBarButtonItem setEnabled:FALSE];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelScan)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneScanning)];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
    [self showFailureAlert:error.localizedDescription];
}

#pragma mark - Scanning completion fucntions

-(void)doneScanning{
    [delegate returnPhotoSelected:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)cancelScan{
    if([self removeFilesFormManualDirectory]){
        [delegate returnPhotoSelected:NO];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - alert functions

-(void)showFailureAlert:(NSString*) alertMsg{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    UIAlertController *failureAlertController = [UIAlertController
                                                 alertControllerWithTitle:@"Error Alert"
                                                 message:[NSString stringWithFormat:@"%@", alertMsg]
                                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *retryAction = [UIAlertAction
                                  actionWithTitle:@"Retry"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      NSLog(@"Trying to reconnect to the FTP server..");
                                      [self startFTPScanning];
                                  }];

    UIAlertAction *proceedAction = [UIAlertAction
                                    actionWithTitle:@"Proceed to Photo"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        NSLog(@"Will switch to photo mode in next view controller..");
                                        [self.navigationController popViewControllerAnimated:YES];

                                    }];

    [failureAlertController addAction:retryAction];
    [failureAlertController addAction:proceedAction];

    [self presentViewController:failureAlertController animated:YES completion:nil];

}

-(void)errorAlert:(NSString*)alertMsg{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:@"Error Alert"
                                               message:[NSString stringWithFormat:@"%@", alertMsg]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {

                               }];

    [errorAlertController addAction:okAction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

#pragma mark - directory functions

-(BOOL)removeFilesFormManualDirectory{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
    NSError *error = nil;

    if (![[NSFileManager defaultManager] fileExistsAtPath:directory]){
        return TRUE;
    }else{
        for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
            BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, file] error:&error];
            if (!success || error)
                return FALSE;
        }
    }
    return TRUE;
}


@end
