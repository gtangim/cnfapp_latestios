//
//  CNFFTPScannerNewTableViewController.h
//  CNF App
//
//  Created by Anik on 2018-05-04.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFFTPScannerTableViewController : UITableViewController<UIAlertViewDelegate, NSFetchedResultsControllerDelegate, UIWebViewDelegate>

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
