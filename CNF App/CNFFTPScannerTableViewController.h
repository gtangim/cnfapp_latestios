//
//  CNFFTPScannerTableViewController.h
//  CNF App Test
//
//  Created by Anik on 2017-08-08.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFFTPScannerTableViewController : UITableViewController<UIAlertViewDelegate, NSFetchedResultsControllerDelegate, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
