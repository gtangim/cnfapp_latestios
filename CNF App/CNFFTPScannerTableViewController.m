//
//  CNFFTPScannerTableViewController.m
//  CNF App Test
//
//  Created by Anik on 2017-08-08.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFFTPScannerTableViewController.h"
#import "DLSFTP.h"
#import "DLSFTPConnection.h"
#import "DLSFTPListFilesRequest.h"
#import "DLSFTPDownloadRequest.h"
#import "DLSFTPFile.h"
#import "CNFAppDelegate.h"
#import "CNFGetDocumentId.h"

@interface CNFFTPScannerTableViewController ()
@property (strong, nonatomic) DLSFTPConnection *connection;
@property (strong, nonatomic) NSMutableArray * existingFiles;
@property (strong, nonatomic) NSMutableArray * newerFiles;
@property (nonatomic, strong) NSManagedObjectContext * context;
@end

typedef void (^Completion)(BOOL success, NSString *msg);

@implementation CNFFTPScannerTableViewController{
    UIAlertController * statusAlert;
    NSManagedObjectContext * context;
}

@synthesize connection=_connection, existingFiles=_existingFiles, newerFiles=_newerFiles,context=_context, webView=_webView;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.existingFiles=nil;
    self.newerFiles=nil;
    [self startFTPScanning];
}


-(void)viewWillDisappear:(BOOL)animated{
    self.connection=nil;
    self.existingFiles=nil;
    self.newerFiles=nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)startFTPScanning{
    [self.navigationItem setTitle:[[NSUserDefaults standardUserDefaults]valueForKey:@"DOCUMENT_NO"]];
    [self.navigationItem.leftBarButtonItem setEnabled:FALSE];
    self.navigationItem.rightBarButtonItem=nil;
    if([self removeFilesFormDownloadDirectory])
        [self FTPConnectionTester];
    else{
        [self errorAlert:@"Could not clear directory, please try again"];
        [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Retry" style:UIBarButtonItemStylePlain target:self action:@selector(startFTPScanning)];
    }
}

-(void)FTPConnectionTester{

    @try{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

        NSString *username = @"CRSMobileFTP";
        NSString *password = @"CRSP@ssw0rd2017!";
        NSInteger port = 22;
        NSString *host = @"ftp.cnfltd.com";

        // make a connection object and attempt to connect
        DLSFTPConnection *connection = [[DLSFTPConnection alloc] initWithHostname:host
                                                                             port:port
                                                                         username:username
                                                                         password:password];
        self.connection=connection;

        DLSFTPClientSuccessBlock successBlock = ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Loading already existing files..");
                [self getFilesForLocation];
            });
        };

        DLSFTPClientFailureBlock failureBlock = ^(NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{

                self.connection=nil;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    [self showFailureAlert:[NSString stringWithFormat:@"Ftp connection error: %@, use Camera to take photo.", error.localizedDescription]];
                });
            });
        };
        [connection connectWithSuccessBlock:successBlock failureBlock:failureBlock];

    }@catch(NSException * ex){
        [self errorAlert:ex.description];
    }


}

-(void)getFilesForLocation{


    @try{
        DLSFTPClientArraySuccessBlock successBlock = ^(NSArray *files) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(self.existingFiles==nil && self.newerFiles==nil){
                    self.existingFiles = [[NSMutableArray alloc]initWithArray:files];

                    UIAlertController *failureAlertController = [UIAlertController
                                                                 alertControllerWithTitle:nil
                                                                 message:@"Please scan the receipts and press OK once its done scanning"
                                                                 preferredStyle:UIAlertControllerStyleAlert];

                    UIAlertAction *doneScanningAction = [UIAlertAction
                                                         actionWithTitle:@"OK"
                                                         style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
                                                         {
                                                             NSLog(@"Getting Latest files from FTP..");
                                                             [self FTPConnectionTester];
                                                         }];

                    [failureAlertController addAction:doneScanningAction];

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                        [self presentViewController:failureAlertController animated:YES completion:nil];
                    });

                }else{
                    self.newerFiles = [[NSMutableArray alloc]initWithArray:files];
                    //array compare
                    [self.newerFiles removeObjectsInArray:self.existingFiles];

                    //check if the newer files has other files in it, if do, delete them

                    NSMutableArray * otherFilesThanPdf = [[NSMutableArray alloc]init];

                    for(DLSFTPFile * file in self.newerFiles){
                        if(![file.filename containsString:@".pdf"]){
                            [otherFilesThanPdf addObject:file];
                        }
                    }
                    if(otherFilesThanPdf.count>0)
                        [self.newerFiles removeObjectsInArray:otherFilesThanPdf];

                    otherFilesThanPdf=nil;

                    if(self.newerFiles.count==0){
                        //no files found, let the user try again
                        [self showFailureAlert:@"No new document found, would you like to retry?"];
                    }else{
                        //new files found, now download them and save to temp folder

                        //                    __block int fileCount= (int)self.newerFiles.count;

                        //                    for(DLSFTPFile * file in self.newerFiles){
                        //now downlaod each file one at a time..

                        [self downloadNewfiles:self.newerFiles.firstObject completion:^(BOOL success, NSString *msg) {
                            if (success) {
                                //do nothing
                                NSLog(@"%@", msg);
                                //                                fileCount--;

                                //                                if(fileCount==0){
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self showPdfInWebView];
                                });
                                [self.connection disconnect];
                                //                                }

                            } else {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                    [self showFailureAlert:@"Download Error, retry?"];
                                });
                            }
                        }];
                        //                    }
                    }
                }
            });
        };

        DLSFTPClientFailureBlock failureBlock = ^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Retry" style:UIBarButtonItemStylePlain target:self action:@selector(startFTPScanning)];
                });
                NSLog(@"Connecion problem..%@", error.localizedDescription);
            });
        };

        // begin loading files
        DLSFTPRequest *request = [[DLSFTPListFilesRequest alloc] initWithDirectoryPath:[NSString
                                                                                        stringWithFormat:@"/%@/", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]]
                                                                          successBlock:successBlock
                                                                          failureBlock:failureBlock];
        [self.connection submitRequest:request];
    }@catch(NSException * ex){
        [self errorAlert:ex.description];
    }

}

-(void)downloadNewfiles:(DLSFTPFile*)file completion :(Completion)block{
    
    @try{
        NSString * remotePath = file.path;

        NSError * error;
        NSString *localPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/downloaded_images"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:localPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:localPath withIntermediateDirectories:NO attributes:nil error:&error];

        if(error)
            if (block) block(FALSE, @"Directory could not be created..");


        DLSFTPClientProgressBlock progressBlock = ^void(unsigned long long bytesReceived, unsigned long long bytesTotal) {

        };

        DLSFTPClientFileTransferSuccessBlock successBlock = ^(DLSFTPFile *file, NSDate *startTime, NSDate *finishTime) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *alertMessage = [NSString stringWithFormat:@"Downloaded %@", file.filename];
                if (block) block(TRUE, alertMessage);

            });
        };

        DLSFTPClientFailureBlock failureBlock = ^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *errorString = [NSString stringWithFormat:@"Error %ld", (long)error.code];
                if (block) block(FALSE, errorString);
            });
        };

        DLSFTPRequest *request = [[DLSFTPDownloadRequest alloc] initWithRemotePath:remotePath
                                                                         localPath:[localPath stringByAppendingPathComponent:file.filename]
                                                                            resume:NO
                                                                      successBlock:successBlock
                                                                      failureBlock:failureBlock
                                                                     progressBlock:progressBlock];
        [self.connection submitRequest:request];
    }@catch(NSException * ex){
        [self errorAlert:ex.description];
    }
    
}

#pragma mark - table view function

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  [UIScreen mainScreen].bounds.size.height;
}

#pragma mark - alertview and indicator spin handler

//- (void)startSpinner:(NSString *)message {
//    if (!statusAlert){
//        statusAlert = [UIAlertController alertControllerWithTitle: nil
//                                                          message: message
//                                                   preferredStyle: UIAlertControllerStyleAlert];
//
//        UIViewController *customVC     = [[UIViewController alloc] init];
//
//        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [spinner startAnimating];
//        [customVC.view addSubview:spinner];
//
//
//        [customVC.view addConstraint:[NSLayoutConstraint
//                                      constraintWithItem: spinner
//                                      attribute:NSLayoutAttributeCenterX
//                                      relatedBy:NSLayoutRelationEqual
//                                      toItem:customVC.view
//                                      attribute:NSLayoutAttributeCenterX
//                                      multiplier:1.0f
//                                      constant:0.0f]];
//
//
//        [customVC.view addConstraint:[NSLayoutConstraint
//                                      constraintWithItem: spinner
//                                      attribute:NSLayoutAttributeCenterY
//                                      relatedBy:NSLayoutRelationEqual
//                                      toItem:customVC.view
//                                      attribute:NSLayoutAttributeCenterY
//                                      multiplier:1.0f
//                                      constant:0.0f]];
//
//        [statusAlert setValue:customVC forKey:@"contentViewController"];
//
//        [self presentViewController: statusAlert
//                           animated: true
//                         completion: nil];
//    }
//}
//
//- (void)stopSpinner {
//    if (statusAlert) {
//        [statusAlert dismissViewControllerAnimated:YES completion:nil];
//        statusAlert = nil;
//    }
//}

#pragma mark - failure alertview controllers

-(void)showFailureAlert:(NSString*) alertMsg{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    UIAlertController *failureAlertController = [UIAlertController
                                                 alertControllerWithTitle:@"Error Alert"
                                                 message:[NSString stringWithFormat:@"%@", alertMsg]
                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *retryAction = [UIAlertAction
                                  actionWithTitle:@"Retry"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      NSLog(@"Trying to reconnect to the FTP server..");
//                                      [self startFTPScanning];
                                      [self getFilesForLocation];
                                  }];
    
    UIAlertAction *proceedAction = [UIAlertAction
                                    actionWithTitle:@"Proceed to Photo"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        NSLog(@"Will switch to photo mode in next view controller..");
                                        //Take them to receiving Product Viewcontroller

                                        [self performSegueWithIdentifier:@"scannerToReceivingProductSegue" sender:self];

                                    }];
    
    [failureAlertController addAction:retryAction];
    [failureAlertController addAction:proceedAction];
    
    [self presentViewController:failureAlertController animated:YES completion:nil];
    
}


-(void)errorAlert:(NSString*)alertMsg{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    UIAlertController *errorAlertController = [UIAlertController
                                                 alertControllerWithTitle:@"Error Alert"
                                                 message:[NSString stringWithFormat:@"%@", alertMsg]
                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {

                                    }];
    
    [errorAlertController addAction:okAction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

#pragma mark - scrollview of photos

-(void)showPdfInWebView{

    @try{
        self.webView.delegate=self;
        NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/downloaded_images"];
        NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
        self.webView.backgroundColor = [UIColor clearColor];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@", [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/downloaded_images/%@",directoryContent.firstObject]]] isDirectory:NO]];
        [self.webView setScalesPageToFit:YES];
        [self.webView loadRequest:request];
    }@catch(NSException * ex){
        NSLog(@"Exception occured: %@", ex.description);
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self.navigationItem.leftBarButtonItem setEnabled:FALSE];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneScanning)];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
    [self showFailureAlert:error.localizedDescription];
    
}

#pragma mark - Scanning completion fucntions

-(void)doneScanning{

    @try{
        int imageCounter=0;
        NSDate * startDate = [NSDate date];
        NSString * documentId = [[CNFGetDocumentId alloc] getDocumentId];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *stringFromDate = [formatter stringFromDate:startDate];

        //save the images to temp folder one at a time track image counter

        NSString *oldDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/downloaded_images"];
        NSString *newDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"];

        NSFileManager *fm = [NSFileManager defaultManager];
        NSError * error;
        // Get all the files at ~/Documents/user
        NSArray *files = [fm contentsOfDirectoryAtPath:oldDirectory error:&error];

        if(error){
            [self errorAlert:[NSString stringWithFormat:@"Directory listen error: %@", error.localizedDescription]];
        }else{
            if (![[NSFileManager defaultManager] fileExistsAtPath:newDirectory])
                [[NSFileManager defaultManager] createDirectoryAtPath:newDirectory withIntermediateDirectories:NO attributes:nil error:&error];

            if(error){
                [self errorAlert:[NSString stringWithFormat:@"Directory listen error: %@", error.localizedDescription]];
            }else{
                //            for (NSString *file in files) {
                NSString * newFileName = [[[stringFromDate stringByAppendingString:@"."]stringByAppendingString:[NSString stringWithFormat:@"%d",imageCounter]] stringByAppendingString:@".pdf"];
                [fm moveItemAtPath:[oldDirectory stringByAppendingPathComponent:files.firstObject]
                            toPath:[newDirectory stringByAppendingPathComponent:newFileName]
                             error:&error];
                if(error){
                    [self errorAlert:[NSString stringWithFormat:@"File moving error: %@", error.localizedDescription]];
                    //                    break;
                }else{
                    imageCounter++;
                }
                //            }
            }
        }

        NSLog(@"all files moved to the temp folder..");

        //populate doc status DB and temp DB with image counter

        CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
        context =[appDelegate managedObjectContext];

        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"receiveWithOrderSheet"]){
            NSError *error;
            NSManagedObject *newManagedObject = [NSEntityDescription
                                                 insertNewObjectForEntityForName:@"DocStatusInfo"
                                                 inManagedObjectContext:context];

            [newManagedObject setValue:@"VOS_RECEIVE" forKey:@"documentType"];
            [newManagedObject setValue:@"Created" forKey:@"status"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [newManagedObject setValue:documentId forKey:@"documentId"];
            [newManagedObject setValue:startDate forKey:@"date"];
            [newManagedObject setValue:@"Saved" forKey:@"attachmentStatus"];
            [newManagedObject setValue:[NSNumber numberWithInt:imageCounter] forKey:@"attachmentNo"];
            [context save:&error];

            //save to temp tables

            NSManagedObject *saveManagedObject = [NSEntityDescription
                                                  insertNewObjectForEntityForName:@"TempDocStatus"
                                                  inManagedObjectContext:context];

            [saveManagedObject setValue:@"VOS_RECEIVE" forKey:@"documentType"];
            [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
            [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"VOS_RECEIPT"] forKey:@"receiptNo"];
            [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [saveManagedObject setValue:startDate forKey:@"createdDate"];
            [saveManagedObject setValue:documentId forKey:@"documentId"];
            [context save:&error];

        }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"receiveWithoutOrderSheet"]){
            NSError *error;
            NSManagedObject *newManagedObject = [NSEntityDescription
                                                 insertNewObjectForEntityForName:@"DocStatusInfo"
                                                 inManagedObjectContext:context];

            [newManagedObject setValue:@"RECEIPT_RECEIVE" forKey:@"documentType"];
            [newManagedObject setValue:@"Created" forKey:@"status"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [newManagedObject setValue:documentId forKey:@"documentId"];
            [newManagedObject setValue:startDate forKey:@"date"];
            [newManagedObject setValue:@"Saved" forKey:@"attachmentStatus"];
            [newManagedObject setValue:[NSNumber numberWithInt:imageCounter] forKey:@"attachmentNo"];
            [context save:&error];

            //save to temp tables

            NSManagedObject *saveManagedObject = [NSEntityDescription
                                                  insertNewObjectForEntityForName:@"TempDocStatus"
                                                  inManagedObjectContext:context];

            [saveManagedObject setValue:@"RECEIPT_RECEIVE" forKey:@"documentType"];
            [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
            [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [saveManagedObject setValue:documentId forKey:@"documentId"];
            [saveManagedObject setValue:startDate forKey:@"createdDate"];
            [saveManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"] forKey:@"vendorId"];
            [context save:&error];
        }


        NSMutableDictionary * saveDictionary = [[NSMutableDictionary alloc]init];

        [saveDictionary setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] forKey:@"APP_CODE"];
        [saveDictionary setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"DOCUMENT_NO"];
        [saveDictionary setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] forKey:@"VENDOR"];
        [saveDictionary setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"VOS_RECEIPT"] forKey:@"VOS_RECEIPT"];
        [saveDictionary setValue:documentId forKey:@"DOCUMENT_ID"];
        [saveDictionary setObject:startDate forKey:@"DATE"];
        [saveDictionary setObject:[NSNumber numberWithInt:imageCounter] forKey:@"IMAGE_NO"];

        [[NSUserDefaults standardUserDefaults] setValue:saveDictionary forKey:@"SAVED_DOC"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"scannerToReceivingProductSegue" sender:self];
        });
    }@catch(NSException * ex){
        [self errorAlert:ex.description];
    }

}


-(BOOL)removeFilesFormDownloadDirectory{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/downloaded_images"];
    NSError *error = nil;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:directory]){
        return TRUE;
    }else{
        for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
            BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, file] error:&error];
            if (!success || error)
                return FALSE;
        }
    }
    return TRUE;
}



@end
