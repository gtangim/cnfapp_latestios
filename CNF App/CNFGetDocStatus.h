//
//  CNFGetDocStatus.h
//  CNF App
//
//  Created by Anik on 2015-05-15.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNFAppDelegate.h"

@interface CNFGetDocStatus : NSObject<NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@property (strong, nonatomic) NSString * serverAddress;
@property (strong, nonatomic) NSManagedObjectContext * context;


-(void)startgettingDocStatus;

@end
