//
//  CNFGetDocStatus.m
//  CNF App
//
//  Created by Anik on 2015-05-15.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "CNFGetDocStatus.h"
#import "UserElement.h"
#import "ValidUserItems.h"
#import "SSKeychain.h"
#import "DocStatusElement.h"

@interface CNFGetDocStatus()
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSString * webServiceName;
@property (nonatomic, strong) NSMutableArray * docStatusList;

@end

@implementation CNFGetDocStatus{
//    NSManagedObjectContext *context;
}


@synthesize docStatusList=_docStatusList, serverAddress=_serverAddress, context=_context;


-(NSMutableArray *)docStatusList{
    if(!_docStatusList)_docStatusList= [[NSMutableArray alloc]init];
    return _docStatusList;
}


-(void)startgettingDocStatus{
    @try{
        [self getDocStatus];
    }@catch(NSException* ex){
        NSLog(@"Exception while getting doc status: %@", ex.description);
    }

    self.docStatusList = nil;
    NSLog(@"Getting doc status finished...");

//    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BACK_PROCESS"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(NSDate *) getQueryDate{
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"LAST_DATE_ELEMENT"]){
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [formatter dateFromString:[[NSUserDefaults standardUserDefaults] valueForKey:@"LAST_DATE_ELEMENT"]];
    }else{
        return [[NSDate date] dateByAddingTimeInterval:-7*24*60*60];
    }
}



#pragma mark - web service functions


-(void)getDocStatus{
    @try{
        DocStatusElement * docElement = [[DocStatusElement alloc]init];
        docElement.userName = @"CNFMobileSystem";
        docElement.token = @"YJVLNEVFZA";
        docElement.deviceId = @"TestDevice";
        docElement.startDate=[self getQueryDate];

        NSLog(@"start date for the web service call:%@", docElement.startDate);

        NSString *post = [docElement toJSONString];

        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];

        NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];

        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//        CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
//        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getDocumentStatus", appDelegate.serverUrl]]];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getDocumentStatus", self.serverAddress]]];

        [request setHTTPMethod:@"POST"];
        [request setValue:len forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];

        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if(error){
                                                  NSLog(@"error hitting doc status web service..");
                                              }else{
                                                  id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

                                                  if(error){
                                                      NSLog(@"Error serializing data from web service, getDocStatus failed..");
                                                  }else{
                                                      if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                          NSLog(@"web service error getting doc status: %@", [dictionary valueForKey:@"Message"]);
                                                      }else{

                                                          self.docStatusList = [NSJSONSerialization JSONObjectWithData:[[dictionary valueForKey:@"d"] dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];

                                                          if(error){
                                                              NSLog(@"Failed to serilize the getDocStatus result..");
                                                          }else{
//                                                              CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
//                                                              context =[appDelegate managedObjectContext];

                                                              if([self.docStatusList count]>0){

                                                                  NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
                                                                  [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

                                                                  NSDate * latestDate = [self getDateFromJSON:[[self.docStatusList objectAtIndex:self.docStatusList.count - 1] valueForKey:@"Date"]];

                                                                  [[NSUserDefaults standardUserDefaults] setValue:[formatter stringFromDate:[latestDate dateByAddingTimeInterval:1.0]] forKey:@"LAST_DATE_ELEMENT"];
                                                                  [[NSUserDefaults standardUserDefaults] synchronize];

                                                                  NSLog(@"Total %lu updates found..", (unsigned long)self.docStatusList.count);
                                                                  for (NSDictionary * doc in self.docStatusList){
                                                                      NSManagedObject *newManagedObject = [NSEntityDescription
                                                                                                           insertNewObjectForEntityForName:@"DocStatusInfo"
                                                                                                           inManagedObjectContext:self.context];

                                                                      [newManagedObject setValue:[doc valueForKey:@"DocumentType"] forKey:@"documentType"];
                                                                      [newManagedObject setValue:[doc valueForKey:@"Status"] forKey:@"status"];
                                                                      [newManagedObject setValue:[self getDateFromJSON:[doc valueForKey:@"Date"]] forKey:@"date"];
                                                                      [newManagedObject setValue:[doc valueForKey:@"version"] forKey:@"version"];
                                                                      [newManagedObject setValue:[doc valueForKey:@"DocumentId"] forKey:@"documentId"];
                                                                      [newManagedObject setValue:[doc valueForKey:@"UserName"] forKey:@"userName"];

                                                                      if([doc valueForKey:@"Description"]== [NSNull null]){
                                                                          [newManagedObject setValue:NULL forKey:@"des"];
                                                                      }else{
                                                                          [newManagedObject setValue:[doc valueForKey:@"Description"] forKey:@"des"];
                                                                      }

                                                                      [self.context save:&error];

                                                                      if(error)
                                                                          NSLog(@"Error occurred when trying to save new status in DB..");

                                                                  }
                                                              }else{
                                                                  NSLog(@"no new data..");
                                                              }
                                                          }
                                                      }
                                                  }
                                              }
                                          }];
        [dataTask resume];
    }@catch(NSException * ex){
        NSLog(@"Exception thrown in get doc status: %@", ex.description);
    }
}

#pragma mark - nsdate conversion functions

- (NSDate*) getDateFromJSON:(NSString *)dateString
{
    // Expect date in this format "/Date(1268123281843)/"
    int startPos = (int)[dateString rangeOfString:@"("].location+1;
    int endPos = (int)[dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    //    NSLog(@"%llu",milliseconds);
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}





@end
