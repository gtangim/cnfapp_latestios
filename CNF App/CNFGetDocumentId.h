//
//  CNFGetDocumentId.h
//  CNF App
//
//  Created by Anik on 2016-06-23.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CNFGetDocumentId : NSObject

-(NSString *)getDocumentId;

@end
