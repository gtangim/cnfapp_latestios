//
//  CNFGetDocumentId.m
//  CNF App
//
//  Created by Anik on 2016-06-23.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFGetDocumentId.h"

@implementation CNFGetDocumentId

-(NSString *)getDocumentId{
    
    if(![[NSUserDefaults standardUserDefaults] valueForKey:@"DOC_COUNTER"]){
        [[NSUserDefaults standardUserDefaults] setValue:@"000" forKey:@"DOC_COUNTER"];
    }
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"ddMMYY"];
    NSString * stringDate = [formatter stringFromDate:[NSDate date]];
    NSString *documentId = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_SHORT_ID"]stringByAppendingString:stringDate] stringByAppendingString:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOC_COUNTER"]];
    NSString * newValue = [NSString stringWithFormat:@"%03d", [[[NSUserDefaults standardUserDefaults] valueForKey:@"DOC_COUNTER"]intValue]+1];
    if([newValue isEqualToString:@"999"]){
        newValue = @"000";
    }
    [[NSUserDefaults standardUserDefaults] setValue:newValue forKey:@"DOC_COUNTER"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
    
    return [[[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] stringByAppendingString:@"_"]stringByAppendingString:documentId];
    
//    return documentId;
}


@end
