//
//  CNFGroupStatusTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-08-09.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DocStatusInfo.h"

@interface CNFGroupStatusTableViewController : UITableViewController<NSFetchedResultsControllerDelegate>

- (IBAction)goBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableVIew;

@end
