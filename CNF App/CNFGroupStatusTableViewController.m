//
//  CNFGroupStatusTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-08-09.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFGroupStatusTableViewController.h"
#import "OutOfStockAuditData.h"
#import "SupplyAuditData.h"
#import "transferCreationAuditData.h"
#import "transferReceiveAuditData.h"
#import "vosCreationAuditData.h"
#import "vosReceiveAuditData.h"
#import "receiptReceiveAuditData.h"

@interface CNFGroupStatusTableViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * tableData;
@property NSUInteger selectedIndex;
@end

@implementation CNFGroupStatusTableViewController{
    NSManagedObjectContext * context;
//    UIAlertView * statusAlert;
}

@synthesize context=_context, tableData=_tableData, selectedIndex=_selectedIndex, tableVIew=_tableVIew;

- (void)viewDidLoad {
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [super viewDidLoad];
    
//    self.tableView.contentInset = UIEdgeInsetsMake(65.0f, 0.0f, 0.0f, 0.0f);

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    [refreshControl addTarget:self action:@selector(updateTable) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;

    @try{
        [self getTableDataReady];
        [self deleteListedFiles];
    }@catch(NSException * ex){
        NSLog(@"Expection Occured: %@", ex.description);
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateTable{
    [self getTableDataReady];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}

-(void)getTableDataReady{
    
    self.tableData = [[NSMutableArray alloc]init];
    
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSDate* startDate=[[NSDate date] dateByAddingTimeInterval:-3*24*60*60];
    NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date >= %@",startDate];
    [request setPredicate:datePredicate];
    
    NSSortDescriptor *eventSorter=[[NSSortDescriptor alloc]initWithKey:@"date" ascending:NO];
    [request setSortDescriptors:[[NSArray alloc] initWithObjects:eventSorter, nil]];
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *results= [context executeFetchRequest:request error:&error];
    
    NSMutableArray *resultArray = [NSMutableArray new];
    NSArray *groups = [results valueForKeyPath:@"@distinctUnionOfObjects.documentId"];
    
    NSMutableArray * unsortedArray = [[NSMutableArray alloc]init];
    
    for(NSString * groupId in groups){
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"documentId == %@",groupId];
        NSArray * new = [results filteredArrayUsingPredicate:predicate];
        NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
        [newProduct setValue:groupId forKey:@"id"];
        [newProduct setValue:[[new objectAtIndex:0] valueForKey:@"date"]  forKey:@"time"];
        [unsortedArray addObject:newProduct];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey: @"time" ascending: NO];
    NSArray *sortedArray = [unsortedArray sortedArrayUsingDescriptors: [NSArray arrayWithObject:sortDescriptor]];
    
    NSArray * sortedGroups = [sortedArray valueForKeyPath:@"id"];
    
    for (NSString *documentId in sortedGroups)
    {
        NSMutableDictionary *entry = [NSMutableDictionary new];
        [entry setObject:documentId forKey:@"documentId"];
        NSArray *groupNames = [results filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"documentId = %@", documentId]];
        [entry setObject:[[groupNames objectAtIndex:0] valueForKey:@"documentType"] forKey:@"documentType"];
        [entry setObject:[[groupNames objectAtIndex:0] valueForKey:@"userName"] forKey:@"userName"];
        for (int i = 0; i < groupNames.count; i++)
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *date = [formatter stringFromDate:[[groupNames objectAtIndex:i] valueForKey:@"date"]];
            
            if([entry valueForKey:[[groupNames objectAtIndex:i] valueForKey:@"status"]]){
                [entry setObject:[[[entry valueForKey:[[groupNames objectAtIndex:i] valueForKey:@"status"]] stringByAppendingString:@", "] stringByAppendingString:date] forKey:[[groupNames objectAtIndex:i] valueForKey:@"status"]];
            }else{
                [entry setObject:date forKey:[[groupNames objectAtIndex:i] valueForKey:@"status"]];
            }
            
            if([[groupNames objectAtIndex:i] valueForKey:@"attachmentStatus"]!=nil){
                [entry setObject:[[groupNames objectAtIndex:i] valueForKey:@"attachmentStatus"] forKey:@"attachmentStatus"];
            }
            
            if([[groupNames objectAtIndex:i] valueForKey:@"documentNo"]!=nil){
                [entry setObject:[[groupNames objectAtIndex:i] valueForKey:@"documentNo"] forKey:@"documentNo"];
            }
            
            if(([[groupNames objectAtIndex:i] valueForKey:@"des"]!=nil) && (![[[groupNames objectAtIndex:i] valueForKey:@"des"] isEqualToString:@""])){
                [entry setObject:[[groupNames objectAtIndex:i] valueForKey:@"des"] forKey:@"des"];
            }
        }
        [resultArray addObject:entry];
    }
    self.tableData = resultArray;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    return self.tableData.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statusCells" forIndexPath:indexPath];
    
    if (self.tableData.count ==0){

    }else{
        
        NSManagedObject *obj=[self.tableData objectAtIndex:indexPath.row];
        
        if([obj valueForKey:@"documentNo"]){
            if([[obj valueForKey:@"documentType"]isEqualToString:@"TRANSFER_CREATION"]&&[obj valueForKey:@"Processed"]){
                cell.textLabel.text = [obj valueForKey:@"des"];
            }else{
                cell.textLabel.text = [obj valueForKey:@"documentNo"];
            }
        }else{
            cell.textLabel.text = [obj valueForKey:@"documentType"];
        }
        
        if([obj valueForKey:@"Failed"]){
            cell.detailTextLabel.text = [@" Failed at: " stringByAppendingString:[obj valueForKey:@"Failed"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"button-no.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"Processed"]){
            cell.detailTextLabel.text = [@" Processed successfully at: " stringByAppendingString:[obj valueForKey:@"Processed"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"modify.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"PreValidated"]){
            cell.detailTextLabel.text = [@" PreValidated at: " stringByAppendingString:[obj valueForKey:@"PreValidated"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"modify.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"Submitted"]){
            cell.detailTextLabel.text = [@" Submitted at: " stringByAppendingString:[obj valueForKey:@"Submitted"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"upload.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"Finished"]){
            cell.detailTextLabel.text = [@" Finished at: " stringByAppendingString:[obj valueForKey:@"Finished"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"sync.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"Created"]){
            cell.detailTextLabel.text = [@" Created at: " stringByAppendingString:[obj valueForKey:@"Created"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"sync.png"] scaledToSize:CGSizeMake(30,30)];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *obj =[self.tableData objectAtIndex:indexPath.row];
    
    NSString * failedText = ([obj valueForKey:@"Failed"]) ? ([@"\r Failed at: " stringByAppendingString:[obj valueForKey:@"Failed"]]) : (@"");
    NSString * processedText = ([obj valueForKey:@"Processed"]) ? ([@"\r Processed at: " stringByAppendingString:[obj valueForKey:@"Processed"]]) : (@"");
    NSString * prevalidatedText = ([obj valueForKey:@"PreValidated"]) ? ([@"\r PreValidated at: " stringByAppendingString:[obj valueForKey:@"PreValidated"]]) : (@"");
    NSString * submittedText = ([obj valueForKey:@"Submitted"]) ? ([@"\r Submitted at: " stringByAppendingString:[obj valueForKey:@"Submitted"]]) : (@"");
    
    NSString * finishedText = ([obj valueForKey:@"Finished"]) ? ([@"\r Finished at: " stringByAppendingString:[obj valueForKey:@"Finished"]]) : (@"");
    NSString * createdText = ([obj valueForKey:@"Created"]) ? ([@"\r Created at: " stringByAppendingString:[obj valueForKey:@"Created"]]) : (@"");
    NSString * attachmentText = ([obj valueForKey:@"attachmentStatus"]) ? ([@"\r Attachment: " stringByAppendingString:[obj valueForKey:@"attachmentStatus"]]) : (@"");
    NSString * docNoText = ([obj valueForKey:@"documentNo"]) ? ([@"\r Document No: " stringByAppendingString:[obj valueForKey:@"documentNo"]]) : (@"");
    NSString * desText = ([obj valueForKey:@"des"]) ? ([@"\r Details: " stringByAppendingString:[obj valueForKey:@"des"]]) : (@"");
    
    NSString * detailedMsg = [[[[[[[[docNoText stringByAppendingString:failedText]stringByAppendingString:processedText]stringByAppendingString:prevalidatedText]stringByAppendingString:attachmentText] stringByAppendingString:submittedText]stringByAppendingString:finishedText] stringByAppendingString:createdText]stringByAppendingString:desText];
    
    NSString * detailedMsgWithUser = [[NSString stringWithFormat:@"Performed by: %@", [[obj valueForKey:@"userName"] uppercaseString]] stringByAppendingString:detailedMsg];

//        UIAlertView * auditDetails = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Detailed Info",@"")
//                                                                message:detailedMsgWithUser
//                                                               delegate:self
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//        [auditDetails show];
    
    UIAlertController *auditDetailsController = [UIAlertController
                                                 alertControllerWithTitle:NSLocalizedString(@"Detailed Info",@"")
                                                 message:detailedMsgWithUser
                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [auditDetailsController addAction:okaction];
    [self presentViewController:auditDetailsController animated:YES completion:nil];
    
}


-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - alertview funciton
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    if(buttonIndex==1){
//        NSManagedObject *obj =[self.tableData objectAtIndex:self.selectedIndex];
//        NSString * fileContent = [[NSString alloc]initWithString:[self readStringFromFile:[obj valueForKey:@"Saved"]]];
//        //        NSLog(@"%@", fileContent);
//        
//        NSDate * newDate = [[NSDate alloc]init];
//        
//        NSError *error;
//        NSManagedObject *newManagedObject = [NSEntityDescription
//                                             insertNewObjectForEntityForName:@"DocStatusInfo"
//                                             inManagedObjectContext:context];
//        
//        [newManagedObject setValue:[obj valueForKey:@"documentType"] forKey:@"documentType"];
//        [newManagedObject setValue:@"Saved" forKey:@"status"];
//        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
//        [newManagedObject setValue:[NSNumber numberWithInt:-1] forKey:@"documentId"];
//        [newManagedObject setValue:newDate forKey:@"date"];
//        [newManagedObject setValue:[obj valueForKey:@"documentNo"] forKey:@"documentNo"];
//        if([obj valueForKey:@"attachmentStatus"]){
//            [newManagedObject setValue:@"Saved" forKey:@"attachmentStatus"];
//        }
//        [context save:&error];
//        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        NSString *stringFromDate = [formatter stringFromDate:newDate];
//        
//        [self writeStringToFile:fileContent fileName:stringFromDate];
//        
//        if([obj valueForKey:@"attachmentStatus"]){
//            NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
//            NSData * pdfData = [[NSData alloc] initWithData:[self readDataFromFile:[obj valueForKey:@"Saved"]]];
//            NSString *PDFPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",stringFromDate]];
//            [pdfData writeToFile:PDFPath atomically:YES];
//        }
//        
//        NSLog(@"File re-saved...");
//    }
//}




#pragma mark - Delete Files more than 3 months old

-(NSArray *)listFileAtPath
{
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    return directoryContent;
}

-(void)deleteListedFiles{
    NSArray * fileListing = [self listFileAtPath];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    for(NSString * name in fileListing){
        if(!([name rangeOfString:@"pdf" options:NSCaseInsensitiveSearch].length==0)){
        }else{
            NSDate * date = [dateFormatter dateFromString:name];
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [gregorianCalendar components: (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear )
                                                                fromDate:date
                                                                  toDate:[NSDate date]
                                                                 options:0];

            if([components month]>2){
                [self deleteAuditFile:name];
            }
        }
    }
    
    
    
    //delete processed image files
    for(NSDictionary* object in self.tableData){
        if([object valueForKey:@"Processed"]){
            if([object valueForKey:@"Created"]){
                [self deleteImageFiles:[object valueForKey:@"Created"]];
            }
        }
    }
    
    
}

- (void)deleteAuditFile:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    
    NSString *filePath = [dataPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"file deleted");
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

-(void)deleteImageFiles:(NSString* )fileName{
    
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    NSArray *filteredArray = [directoryContent filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains %@", fileName]];
    NSError *error;
    for(NSString* name in filteredArray){
        NSString* finalPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"] stringByAppendingPathComponent:name];
        [[NSFileManager defaultManager] removeItemAtPath:finalPath error:&error];
        NSLog(@"images removed..");
    }
    
}


#pragma mark - alertview and indicator spin handler

//- (void)startSpinner:(NSString *)message {
//    if (!statusAlert){
//        statusAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(message,@"") message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
//        
//        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [indicator startAnimating];
//        
//        [statusAlert setValue:indicator forKey:@"accessoryView"];
//        [statusAlert show];
//    }
//}
//
//- (void)stopSpinner {
//    if (statusAlert) {
//        [statusAlert dismissWithClickedButtonIndex:0 animated:YES];
//        statusAlert = nil;
//    }
//}


#pragma mark - resubmit audit funcitons


- (NSString*)readStringFromFile:(NSString*)file {
    
    // Build the path...
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    NSString* fileName = file;
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    // The main act...
    return [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
}


- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

- (NSData*)readDataFromFile:(NSString*)file {
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    NSString* fileName = [file stringByAppendingString:@".pdf"];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    NSData * pdfData = [NSData dataWithContentsOfFile:fileAtPath];
    return pdfData;
}


- (IBAction)goBack:(id)sender {
}
@end
