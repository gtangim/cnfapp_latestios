//
//  CNFInvoicePhotoViewController.h
//  CNF App
//
//  Created by Anik on 2017-09-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFInvoicePhotoViewController : UIViewController<UIWebViewDelegate>
@property (nonatomic, strong) NSString * photoHexString;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
