//
//  CNFInvoicePhotoViewController.m
//  CNF App
//
//  Created by Anik on 2017-09-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFInvoicePhotoViewController.h"

@interface CNFInvoicePhotoViewController ()

@end

@implementation CNFInvoicePhotoViewController

@synthesize photoHexString=_photoHexString, webView=_webView;

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.photoHexString!=NULL){
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYMMDD"];
        NSString* fileName = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] stringByAppendingString:[formatter stringFromDate:[NSDate date]]] stringByAppendingString:@".pdf"];
        
        NSData *nsdataFromBase64String = [[NSData alloc] initWithBase64EncodedString:self.photoHexString options:0];
        NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/downloaded_images"];
        NSString *filePath = [dataPath stringByAppendingPathComponent:fileName];
        
        NSError * error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
        
        if(error){
            [self errorAlert:@"Error loading Photo, Please try again"];
        }else{
            if( [self removeFilesFormDownloadDirectory]){
                [nsdataFromBase64String writeToFile:filePath atomically:YES];
                NSLog(@"PDF File is saved to %@",filePath);
                [self showPDFFile];
            }else{
                [self errorAlert:@"Error loading Photo, Please try again"];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showPDFFile{
    self.webView.delegate=self;

    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/downloaded_images"];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    self.webView.backgroundColor = [UIColor clearColor];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@", [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/downloaded_images/%@",directoryContent.firstObject]]] isDirectory:NO]];
    [self.webView setScalesPageToFit:YES];
    [self.webView loadRequest:request];
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self.navigationItem.leftBarButtonItem setEnabled:FALSE];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
    [self errorAlert:@"Error loading Photo, Please try again"];
}

-(BOOL)removeFilesFormDownloadDirectory{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/downloaded_images"];
    NSError *error = nil;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:directory]){
        return TRUE;
    }else{
        for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
            BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, file] error:&error];
            if (!success || error)
                return FALSE;
        }
    }
    return TRUE;
}

-(void)errorAlert:(NSString*)alertMsg{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:@"Error Alert"
                                               message:[NSString stringWithFormat:@"%@", alertMsg]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
    
    [errorAlertController addAction:okAction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

@end
