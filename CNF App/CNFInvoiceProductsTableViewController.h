//
//  CNFInvoiceProductsTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-09-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFInvoiceProductsTableViewController : UITableViewController

@property (nonatomic, strong) NSArray * productList;

@end
