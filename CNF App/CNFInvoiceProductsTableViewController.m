//
//  CNFInvoiceProductsTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-09-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFInvoiceProductsTableViewController.h"

@interface CNFInvoiceProductsTableViewController ()

@end

@implementation CNFInvoiceProductsTableViewController

@synthesize productList=_productList;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.productList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"invoiceProductCells" forIndexPath:indexPath];
    
    cell.textLabel.text = [[self.productList objectAtIndex:indexPath.row] valueForKey:@"ProductName"];
    cell.detailTextLabel.text = [@"Received Quantity: " stringByAppendingString:[[[[[self.productList objectAtIndex:indexPath.row] valueForKey:@"Quantity"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[[self.productList objectAtIndex:indexPath.row] valueForKey:@"Uom"]]];
    ;
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


@end
