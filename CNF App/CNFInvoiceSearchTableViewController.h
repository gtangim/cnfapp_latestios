//
//  CNFInvoiceSearchTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-09-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFInvoiceSearchTableViewController : UITableViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *invoiceNumber;
@property (weak, nonatomic) IBOutlet UILabel *vendorLabel;

@property (weak, nonatomic) IBOutlet UILabel *receivedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *receivedOn;
@property (weak, nonatomic) IBOutlet UILabel *totalQuantity;
@property (weak, nonatomic) IBOutlet UILabel *grandTotal;
@property (weak, nonatomic) IBOutlet UILabel *notes;

- (IBAction)doneClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *productDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *photoDetailsLabel;

@end
