//
//  CNFInvoiceSearchTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-09-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFInvoiceSearchTableViewController.h"
#import "InvoiceDetailsModel.h"
#import "CNFAppDelegate.h"
#import "CNFInvoiceProductsTableViewController.h"
#import "CNFInvoicePhotoViewController.h"

@interface CNFInvoiceSearchTableViewController ()

@property (nonatomic) BOOL hideDetailsSection;
@property (nonatomic, strong) NSArray * productList;
@property (nonatomic, strong) NSString * hexPhotoString;

@end

@implementation CNFInvoiceSearchTableViewController

@synthesize hideDetailsSection=_hideDetailsSection;

@synthesize vendorLabel=_vendorLabel, invoiceNumber=_invoiceNumber;

@synthesize receivedByLabel=_receivedByLabel, receivedOn=_receivedOn, totalQuantity=_totalQuantity, grandTotal=_grandTotal, notes=_notes;

@synthesize productDetailsLabel=_productDetailsLabel, photoDetailsLabel=_photoDetailsLabel;

@synthesize productList=_productList, hexPhotoString=_hexPhotoString;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hideDetailsSection=TRUE;
    self.invoiceNumber.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated{
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"]){
        self.vendorLabel.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorName"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(self.hideDetailsSection) return 1;
    else return 2;
}



- (IBAction)doneClicked:(id)sender {
    if([self.invoiceNumber hasText] && ![self.vendorLabel.text isEqualToString:@"Please Select"]){
        //do done funciton
        
        self.hideDetailsSection = TRUE;
        [self.tableView reloadData];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        [self.navigationItem.rightBarButtonItem setEnabled:FALSE];
        [self getInvoiceDetailsWeb];

    }else{
        [self showErrorAlert:@"Please type invoice number and select vendor"];
    }
}


#pragma  mark - alert views

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


#pragma mark - web method


-(void)getInvoiceDetailsWeb{
    NSLog(@"get web data..");
    
    InvoiceDetailsModel * invoiceInfo = [[InvoiceDetailsModel alloc]init];
    invoiceInfo.invoiceNumber = self.invoiceNumber.text;
    invoiceInfo.vendorId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"];
    invoiceInfo.locationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];

    NSData *postData = [[invoiceInfo toJSONString] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *len = [NSString stringWithFormat:@"%i", (int)[[invoiceInfo toJSONString] length]];
    
    NSMutableURLRequest *webrequest = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [webrequest setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getInvoiceDetails", appDelegate.serverUrl]]];
    [webrequest setHTTPMethod:@"POST"];
    [webrequest setValue:len forHTTPHeaderField:@"Content-Length"];
    [webrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [webrequest setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:webrequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(error){
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                              NSLog(@"Web service error for invoice details: %@", error.localizedDescription);
                                              self.hideDetailsSection = TRUE;
                                              [self.tableView reloadData];
                                              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                              [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                              [self showErrorAlert:error.localizedDescription];
                                              });
                                          }else{
                                              id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                              if(error){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                  NSLog(@"Serialization Error for Invoice details: %@", error.localizedDescription);
                                                  self.hideDetailsSection = TRUE;
                                                  [self.tableView reloadData];
                                                  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                  [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                                  [self showErrorAlert:error.localizedDescription];
                                                  });
                                              }else if([dictionary valueForKey:@"Message"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      NSLog(@"Error: %@", [dictionary valueForKey:@"Message"]);
                                                      self.hideDetailsSection = TRUE;
                                                      [self.tableView reloadData];
                                                      [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                      [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                                      [self showErrorAlert:error.localizedDescription];
                                                  });
                                              }else{
                                                  if([[dictionary valueForKey:@"d"] rangeOfString:@"Could not find any invoice numbered"].location == NSNotFound ){
                                                      //found invoice
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          self.hideDetailsSection = FALSE;
                                                          [self.tableView reloadData];
                                                          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                          [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                                          [self loadDetailsSectionData:dictionary];
                                                      });
                                                      
                                                  }else{
                                                      
                                                      //invoice not found
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          self.hideDetailsSection = TRUE;
                                                          [self.tableView reloadData];
                                                          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                          [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                                          [self showErrorAlert:[dictionary valueForKey:@"d"]];
                                                          
                                                      });
                                                  }
                                              }
                                          }
                                      }];
    [dataTask resume];
}

#pragma mark - load Table Data

-(void)loadDetailsSectionData:(id)dictionary{
    
    NSData *objectData = [[dictionary valueForKey:@"d"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *details = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:nil];

    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    self.receivedByLabel.text = [NSString stringWithFormat:@"Received by: %@", [details valueForKey:@"ReceivedByUser"]!=[NSNull null]?[details valueForKey:@"ReceivedByUser"]:@""];
    self.receivedOn.text = [NSString stringWithFormat:@"Received On: %@", [formatter stringFromDate:[self getDateFromJSON:[details valueForKey:@"ReceivedOn"]]]];
    self.totalQuantity.text = [NSString stringWithFormat:@"Total Quantity: %@", [[details valueForKey:@"TotalQuantity"] stringValue]?[[details valueForKey:@"TotalQuantity"] stringValue]:@""];
    self.grandTotal.text = [NSString stringWithFormat:@"Grand Total: %@", [[details valueForKey:@"Total"] stringValue]?[[details valueForKey:@"Total"] stringValue]:@""];
    
    NSMutableString *notes = [[NSMutableString alloc] init];
    [notes appendString:@"Notes with this invoice: \n"];
    if([details valueForKey:@"InvoiceNotes"]!=[NSNull null]){
        for(NSString * note in [details valueForKey:@"InvoiceNotes"]){
            [notes appendString:@"\n"];
            [notes appendString:[[note componentsSeparatedByString:@"="][1] componentsSeparatedByString:@"}"][0]];
        }
        self.notes.text = notes;
    }else self.notes.text =@"";
    
    self.productDetailsLabel.text = [NSString stringWithFormat:@"%lu total",[details valueForKey:@"Products"]!=[NSNull null]?[[details valueForKey:@"Products"] count]:0];
    self.photoDetailsLabel.text = [details valueForKey:@"PDFData"] != [NSNull null]?@"Available":@"N/A";
    self.productList = [details valueForKey:@"Products"]!=[NSNull null]?[details valueForKey:@"Products"]:NULL;
    self.hexPhotoString = [details valueForKey:@"PDFData"] != [NSNull null]?[details valueForKey:@"PDFData"]:NULL;
}


#pragma mark - nsdate conversion functions

- (NSDate*) getDateFromJSON:(NSString *)dateString{
    // Expect date in this format "/Date(1268123281843)/"
    int startPos = (int)[dateString rangeOfString:@"("].location+1;
    int endPos = (int)[dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    //    NSLog(@"%llu",milliseconds);
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}


#pragma mark - segue function

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"invoiceLookupProductSegue"]){
        
        CNFInvoiceProductsTableViewController * productController = segue.destinationViewController;
        productController.productList = self.productList;
        
    }else if([segue.identifier isEqualToString:@"invoiceLookupPhotoSegue"]){
        CNFInvoicePhotoViewController * photoController = segue.destinationViewController;
        photoController.photoHexString = self.hexPhotoString;
    }
}

@end
