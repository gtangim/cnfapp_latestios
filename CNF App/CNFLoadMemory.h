//
//  CNFLoadMemory.h
//  CNF App
//
//  Created by Anik on 2016-04-20.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNFAppDelegate.h"
@interface CNFLoadMemory : NSObject<NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

-(void)startMemoryLoad;

@end
