//
//  CNFLoadMemory.m
//  CNF App
//
//  Created by Anik on 2016-04-20.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFLoadMemory.h"
#import "GZIP.h"
#import "BRLineReader.h"


@implementation CNFLoadMemory

-(void)startMemoryLoad{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self unzipFile];
    });
}

#pragma mark - unzip function

-(void)unzipFile{
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json.gz"];
    NSString *destinationPath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir,@"db.json"];
    
    NSData *gzData = [NSData dataWithContentsOfFile:filepath];
    NSData *ungzippedData = [gzData gunzippedData];
    
    [ungzippedData writeToFile:destinationPath atomically:YES];
    NSLog(@"done unzipping file..");
    [self readFileHeader];
}

#pragma mark - read file header

-(void)readFileHeader{
    NSLog(@"start loading to memory..");
    
    NSError * serializerError;
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    
    BRLineReader * line =[[BRLineReader alloc]initWithFile:filepath encoding:NSUTF8StringEncoding];
    NSString * firstLine= [line readLine];
    
    NSData *data = [firstLine dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary * jsonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&serializerError];
    
    
    if(serializerError){
        NSLog(@"Error occured while serializing data file, data file not downloaded properly..");
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithLong:firstLine.length+6] forKey:@"START_OFFSET"];
        [[NSUserDefaults standardUserDefaults] setObject:[jsonDic objectForKey:@"DataList"] forKey:@"DATA_LIST"];
        [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION_V2"] forKey:@"FILE_VERSION"];
        [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"FILE_VERSION_V2"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSLog(@"Initial procedures completed...");
    }
}


@end
