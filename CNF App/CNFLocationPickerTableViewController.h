//
//  CNFLocationPickerTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-12-08.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFLocationPickerTableViewController : UITableViewController<UIPickerViewDelegate, UIPickerViewDataSource,DTDeviceDelegate, ZBarReaderDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *locationPicker;

@end
