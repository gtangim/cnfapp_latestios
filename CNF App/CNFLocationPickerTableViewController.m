//
//  CNFLocationPickerTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-12-08.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFLocationPickerTableViewController.h"

@interface CNFLocationPickerTableViewController ()
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic, strong) NSMutableArray * locationArray;
@property (nonatomic, strong) NSString * selectedLocation;
@end

@implementation CNFLocationPickerTableViewController{
    UIAlertController * statusAlert;
    DTDevices *scanner;
}

@synthesize scanner=_scanner, scannerAvailable=_scannerAvailable, foundBarcode=_foundBarcode, locationPicker=_locationPicker, locationArray=_locationArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    
    self.scanner=[DTDevices sharedDevice];
    [self.scanner addDelegate:self];
    
    self.locationPicker.delegate=self;
    self.locationPicker.dataSource=self;
    
    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"LocationList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    
    [file closeFile];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    NSError * serializerError;
    
    NSArray * locations = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];
    
    if(serializerError){
        NSLog(@"Serializer error, data file not downloaded properly..");
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }else{
        self.locationArray = [[NSMutableArray alloc]initWithArray:[locations valueForKey:@"LocationId"]];
        
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]length]==0 || [[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] isEqualToString:@"A1"]){
            [self.locationPicker selectRow:0 inComponent:0 animated:YES];
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
        }else{
            [self.locationPicker selectRow:[self.locationArray indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]] inComponent:0 animated:YES];
            self.selectedLocation=[self.locationArray objectAtIndex:[self.locationArray indexOfObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]]];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(locationPicked)];
        }
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    self.navigationItem.hidesBackButton = YES;
    [super viewWillAppear:YES];
    [self.scanner addDelegate:self];
    [self.scanner connect];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - scanner code
-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailability{

    if([self.scanner connstate]==1){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(scanShelfTag)];
        self.scannerAvailable=FALSE;
//        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
    
    
}

-(void)stopScanner
{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}

#pragma mark - pickerview functions

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component{
    return[self.locationArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [self.locationArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(locationPicked)];
    self.selectedLocation=[self.locationArray objectAtIndex:row];
    
}

#pragma mark - butotn functions
- (void)scanShelfTag{
    if(self.scannerAvailable){
        [self.scanner barcodeStartScan:nil];
        self.foundBarcode = NO;
        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
    }else{
        ZBarReaderViewController *reader = [ZBarReaderViewController new];
        reader.readerDelegate = self;
        [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
        reader.readerView.zoom = 1.0;
//        [self presentModalViewController: reader animated: YES];
        [self presentViewController:reader animated:YES completion:nil];
    }
}

-(void)locationPicked{
    
//    [self startSpinner:@"Please Wait.."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        if([self.selectedLocation isEqualToString:@"Default"]){
            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]length]==0){
                
//                [statusAlert dismissViewControllerAnimated:YES completion:^{
                    [self showErrorAlert:@"PLease select a location other than default"];
//                }];
                
            }else{
                dispatch_async (dispatch_get_main_queue(), ^{
                    
//                    [statusAlert dismissViewControllerAnimated:YES completion:^{
                        [self.navigationController popToRootViewControllerAnimated:YES];
//                    }];
                });
            }
        }else{
            [[NSUserDefaults standardUserDefaults]setValue:self.selectedLocation forKey:@"OFFICE"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            dispatch_async (dispatch_get_main_queue(), ^{
//                [statusAlert dismissViewControllerAnimated:YES completion:^{
                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }];
            });
        }
    });
}


#pragma mark - barcode read function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    int sound[]={2000,100};
    [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    [self processBarcode:barcode];
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    NSString * barcode=symbol.data;
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    [self processBarcode:barcode];
}

-(void)processBarcode:(NSString *)barcode{
    
    if ([barcode rangeOfString:@": "].location != NSNotFound){
        if([[barcode componentsSeparatedByString:@": "][0] isEqualToString:@"location"]){
            
//            [self startSpinner:@"Please Wait.."];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [[NSUserDefaults standardUserDefaults]setObject:[barcode componentsSeparatedByString:@": "][1] forKey:@"OFFICE"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                dispatch_async (dispatch_get_main_queue(), ^{
//                    [statusAlert dismissViewControllerAnimated:YES completion:^{
                        [self.navigationController popToRootViewControllerAnimated:YES];
//                    }];
                });
            });
        }else{
            [self showErrorAlert:@"Invalid Location Tag.."];
        }
    }else{
        [self showErrorAlert:@"Invalid Location Tag.."];
    }
}


#pragma mark - alert notification functions

-(void)showErrorAlert:(NSString *)message{
    
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
    
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}


#pragma mark - alertview and indicator spin handler

//- (void)startSpinner:(NSString *)message {
//    if (!statusAlert){
//        statusAlert = [UIAlertController alertControllerWithTitle: nil
//                                                          message: message
//                                                   preferredStyle: UIAlertControllerStyleAlert];
//        
//        UIViewController *customVC     = [[UIViewController alloc] init];
//        
//        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [spinner startAnimating];
//        [customVC.view addSubview:spinner];
//        
//        
//        [customVC.view addConstraint:[NSLayoutConstraint
//                                      constraintWithItem: spinner
//                                      attribute:NSLayoutAttributeCenterX
//                                      relatedBy:NSLayoutRelationEqual
//                                      toItem:customVC.view
//                                      attribute:NSLayoutAttributeCenterX
//                                      multiplier:1.0f
//                                      constant:0.0f]];
//        
//        
//        [customVC.view addConstraint:[NSLayoutConstraint
//                                      constraintWithItem: spinner
//                                      attribute:NSLayoutAttributeCenterY
//                                      relatedBy:NSLayoutRelationEqual
//                                      toItem:customVC.view
//                                      attribute:NSLayoutAttributeCenterY
//                                      multiplier:1.0f
//                                      constant:0.0f]];
//        
//        [statusAlert setValue:customVC forKey:@"contentViewController"];
//        
//        [self presentViewController: statusAlert
//                           animated: true
//                         completion: nil];
//    }
//}
//
//- (void)stopSpinner {
//    if (statusAlert) {
//        [statusAlert dismissViewControllerAnimated:YES completion:^{
//            statusAlert = nil;
//        }];
//    }
//}



@end
