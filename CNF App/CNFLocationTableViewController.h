//
//  CNFLocationTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-01-14.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"

@interface CNFLocationTableViewController : UITableViewController
- (IBAction)back:(id)sender;

@end
