//
//  CNFLocationTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-01-14.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFLocationTableViewController.h"

@interface CNFLocationTableViewController ()

@property (nonatomic, strong) NSArray * locationArray;

@end

@implementation CNFLocationTableViewController

@synthesize locationArray=_locationArray;

- (void)viewDidLoad {
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"LocationList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSError * serializerError;
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    self.locationArray = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];
    
    if(serializerError){
        NSLog(@"Serializer error, data file not downloaded properly..");
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.locationArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
    
    NSObject * locationObject = [self.locationArray objectAtIndex:indexPath.row];

    cell.textLabel.text = [[[locationObject valueForKey:@"LocationId"] stringByAppendingString:@"-"] stringByAppendingString:[locationObject valueForKey:@"Description"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[[self.locationArray objectAtIndex:indexPath.row] valueForKey:@"LocationId"]isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]]){
        [self showErrorAlert:@"Please select location other than the origin location"];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:[self.locationArray objectAtIndex:indexPath.row] forKey:@"TO_LOCATION"];
        [[NSUserDefaults standardUserDefaults] setObject:@"New Transfer" forKey:@"DOCUMENT_NO"];
        [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"VENDOR"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"user %@ started creating %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"],[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]);
        
        [self performSegueWithIdentifier:@"createTransferToProductSague" sender:self];
    }
}

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
