//
//  CNFLogListTableTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-11-15.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFLogListTableTableViewController.h"

@interface CNFLogListTableTableViewController ()

@property (nonatomic, strong) NSMutableArray * logListArray;

@end

@implementation CNFLogListTableTableViewController

@synthesize logListArray=_loglistArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getLogListAndPopulateTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getLogListAndPopulateTable{
    NSString * filePath = @"Documents/cnf_logs";
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:filePath];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    self.logListArray=[[NSMutableArray alloc]initWithArray:directoryContent];
    
    [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"SELECTED_LOG_FILE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.logListArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"logCell"];
    cell.textLabel.text= [self.logListArray objectAtIndex:indexPath.row];
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[NSUserDefaults standardUserDefaults] setValue:[self.logListArray objectAtIndex:indexPath.row] forKey:@"SELECTED_LOG_FILE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




@end
