//
//  CNFMultipleUPCProductTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-08-10.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"

@interface CNFMultipleUPCProductTableViewController : UITableViewController<UISearchResultsUpdating, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end
