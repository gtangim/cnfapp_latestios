//
//  CNFMultipleUPCProductTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-08-10.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFMultipleUPCProductTableViewController.h"

@interface CNFMultipleUPCProductTableViewController ()
@property (nonatomic, strong) NSArray * productArray;
@property (strong, nonatomic) UISearchController *searchController;

@end

@implementation CNFMultipleUPCProductTableViewController{
    NSArray *searchResults;
}

@synthesize productArray=_productArray, searchBar=_searchBar, searchController=_searchController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.productArray=[[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_PRODUCTS"];

    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    [self.searchController.searchBar sizeToFit];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (searchResults.count>0) {
        return [searchResults count];
        
    } else {
        return self.productArray.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"multipleUpcProductCell"];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"multipleUpcProductCell"];
    }
    if (searchResults.count>0) {
        cell.textLabel.text = [[searchResults objectAtIndex:indexPath.row]valueForKey:@"ProductName"];
        cell.detailTextLabel.text =[[searchResults objectAtIndex:indexPath.row]valueForKey:@"Size"];
        
    } else {
        cell.textLabel.text = [[self.productArray objectAtIndex:indexPath.row] valueForKey:@"ProductName"];
        cell.detailTextLabel.text =[[self.productArray objectAtIndex:indexPath.row]valueForKey:@"Size"];
    }
    
    cell.textLabel.numberOfLines=0;
    return cell;
    
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (searchResults.count>0) {
        [[NSUserDefaults standardUserDefaults] setObject:[[searchResults objectAtIndex:indexPath.row] valueForKey:@"ProductId"] forKey:@"MULTIPLE_UPC_ID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:[[self.productArray objectAtIndex:indexPath.row] valueForKey:@"ProductId"] forKey:@"MULTIPLE_UPC_ID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - search filter functions

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

-(void)searchForText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ProductName CONTAINS[cd] %@", searchText];
    searchResults = [self.productArray filteredArrayUsingPredicate:resultPredicate];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchController.searchBar.text=@"";
    [self.searchController.searchBar resignFirstResponder];
    
    searchResults=NULL;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView reloadData];
}


@end
