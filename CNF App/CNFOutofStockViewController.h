//
//  CNFOutofStockViewController.h
//  CNF App
//
//  Created by Anik on 2015-04-08.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFOutofStockViewController : UIViewController<NSFetchedResultsControllerDelegate, DTDeviceDelegate, UIAlertViewDelegate, ZBarReaderDelegate>

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *productQOHLabel;
@property (weak, nonatomic) IBOutlet UILabel *productBrandLabel;
@property (weak, nonatomic) IBOutlet UILabel *productVendorLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateShowLabel;
@property (weak, nonatomic) IBOutlet UIButton *scanShelfTagbutton;


- (IBAction)cancelLastScan:(id)sender;
- (IBAction)finishScanning:(id)sender;
- (IBAction)scanShelfTag:(id)sender;
- (IBAction)backToMain:(id)sender;

@end
