//
//  CNFOutofStockViewController.m
//  CNF App
//
//  Created by Anik on 2015-04-08.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "CNFOutofStockViewController.h"
#import "ReportElement.h"
#import "CNFAppEnums.h"
#import "OutOfStockAuditData.h"
#import "Products.h"
#import "CNFGetDocumentId.h"
#import "BarcodeProcessor.h"


@interface CNFOutofStockViewController ()
@property (nonatomic, strong ) NSManagedObjectContext * context;
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic, strong) NSMutableArray<ReportElement> * auditedProducts;
@property (nonatomic) BOOL activateCancelButton;
@property (nonatomic) BOOL scanProduct;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic, strong) NSString * documentId;

//second webservice population variables
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSDate * endTime;
@property (nonatomic) BOOL * shouldScanProduct;


@end

@implementation CNFOutofStockViewController{
    NSManagedObjectContext * context;
    DTDevices *scanner;
    UIAlertController * statusAlert;
}


@synthesize userNameLabel=_userNameLabel, locationLabel=_locationLabel, productNameLabel=_productNameLabel, productSizeLabel=_productSizeLabel, productQOHLabel=_productQOHLabel, scanner=_scanner, context=_context, productBrandLabel=_productBrandLabel, productVendorLabel=_productVendorLabel, dateShowLabel=_dateShowLabel, scanProduct=_scanProduct, foundBarcode=_foundBarcode, scanShelfTagbutton=_scanShelfTagbutton, scannerAvailable=_scannerAvailable, documentId=_documentId, shouldScanProduct=_shouldScanProduct;


-(NSMutableArray *)auditedProducts{
    if(!_auditedProducts)_auditedProducts=[[NSMutableArray<ReportElement> alloc]init];
    return _auditedProducts;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    [self startSpinner:@"Loading Data..."];
    
    self.userNameLabel.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"USER_NAME"];
    self.locationLabel.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"OFFICE"];
    
    self.dateShowLabel.text = [@"Data File Version: " stringByAppendingString:[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    self.activateCancelButton=FALSE;
    self.scanProduct=TRUE;
    self.scannerAvailable = TRUE;
    
    self.startTime = [NSDate date];
    self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];
    
    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];
    
    [newManagedObject setValue:@"OUT_OF_STOCK_AUDIT_SUBMISSION" forKey:@"documentType"];
    [newManagedObject setValue:@"Created" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.startTime forKey:@"date"];
    [context save:&error];
    
//    [statusAlert dismissViewControllerAnimated:YES completion:nil];
    
    self.scanner=[DTDevices sharedDevice];
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailability) userInfo:nil repeats:NO];
    [super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [super viewWillAppear:YES];
    [self.scanner connect];
    [self.scanner addDelegate:self];
    
    //check if there is a multiple UPC product selected.
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]){
        [self processBarcode:[[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]];
    }
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma  mark - barcode scanner function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    self.foundBarcode =YES;
    if(self.scanProduct){
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
        
        [self processBarcode:barcode];
    }else{
        int sound[]={3500,200,0,200,3500,200};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

#pragma mark - camera function

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    
    NSString * barcode=symbol.data;
    
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    
    [self processBarcode:barcode];
    
}

#pragma mark - barcode processing function


-(void)processBarcode:(NSString *)barcode{
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcode:barcode];
    
    if([resultDic valueForKey:@"errorMsg"]!=NULL){
        NSLog(@"Result returned error..");
        [self showErrorAlert:[resultDic valueForKey:@"errorMsg"] tag:errorAlerts];
    }else{
    
        NSArray * products= [resultDic valueForKey:@"productArray"];
        if(products.count==0){
            //no product found
            NSLog(@"product not found..");
            self.productNameLabel.text=self.productSizeLabel.text=self.productQOHLabel.text=self.productVendorLabel.text=self.productBrandLabel.text=@"";
            [self showErrorAlert:@"Product Not Found..." tag:errorAlerts];
//            self.scanProduct=TRUE;
            
            
        }else if(products.count>1){
            //multiple products found
            
            NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
            
            for(NSObject * product in products){
                NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                if([product valueForKey:@"Size"]!=NULL && [product valueForKey:@"UomQty"]!=NULL){
                    [listedProduct setValue:[[[[product valueForKey:@"Size"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[product valueForKey:@"UomQty"]] forKey:@"Size"];
                }
                [listedProductArray addObject:listedProduct];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"multipleUpcFromOutofStockSegue" sender:self];
        }else{
            //only one product found
            
            Products * product = [[Products alloc]init];
            product = [products objectAtIndex:0];
            
            NSPredicate * locationPredicate = [NSPredicate predicateWithFormat:@"Location.LocationId contains[cd] %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
            
            //            StoreSpecificInfo * storeInfo = [[StoreSpecificInfo alloc]init];
            
            StoreSpecificInfo * storeInfo = [[product.InfoByStore filteredArrayUsingPredicate:locationPredicate] objectAtIndex:0];
            
            ReportElement * foundProduct = [[ReportElement alloc]init];
            NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            
            //populate new found product
            foundProduct.ProductId = product.ProductId? product.ProductId:NULL;
            foundProduct.ProductName = product.ProductName? product.ProductName:NULL;
            foundProduct.Size = product.Size? product.Size:NULL;
            foundProduct.UomQty = product.UomQty? product.UomQty:NULL;
            foundProduct.ProductBrand = product.Brand? product.Brand:NULL;
            foundProduct.Vendor = product.Supplier.DefaultVendor.VendorName? product.Supplier.DefaultVendor.VendorName:NULL;
            
            if(product.Supplier.WarehouseId){
                foundProduct.Supplier=product.Supplier.WarehouseId;
            }else{
                foundProduct.Supplier = product.Supplier.DefaultVendor.VendorName==Nil? NULL:product.Supplier.DefaultVendor.VendorName;
            }
            
            foundProduct.Location = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
            foundProduct.Delisted = product.Delisted==Nil? NULL:product.Delisted;
            foundProduct.DelistedReason = product.DelistedReason==Nil? NULL:product.DelistedReason;
            foundProduct.Pulled = product.Pulled==Nil? NULL:product.Pulled;
            foundProduct.PulledReason = product.PulledReason==Nil? NULL:product.PulledReason;
            //            foundProduct.PulledDate= product.PulledDate==Nil? NULL:product.PulledDate;
            
            foundProduct.PulledDate = product.PulledDate==Nil? NULL:[formatter dateFromString:product.PulledDate.DateTime];
            
            foundProduct.VendorId = product.Supplier.DefaultVendor.VendorId==Nil? NULL:product.Supplier.DefaultVendor.VendorId;
            foundProduct.VendorOutOfStock = product.VendorOutOfStock==Nil? NULL:product.VendorOutOfStock;
            //            foundProduct.VendorOOSDate = product.VendorOutOfStockToDate==Nil? NULL:product.VendorOutOfStockToDate;
            
            foundProduct.VendorOOSDate = product.VendorOutOfStockToDate==Nil? NULL:[formatter dateFromString:product.VendorOutOfStockToDate.DateTime];
            
            foundProduct.VendorOOSReason = product.VendorOutOfStockReason==Nil? NULL:product.VendorOutOfStockReason;
            foundProduct.WarehouseOutOfStock = product.WarehouseOutOfStock==Nil? NULL:product.WarehouseOutOfStock;
            
            //populate location specific info
            foundProduct.CrsQOH = storeInfo.Qoh==Nil? NULL:storeInfo.Qoh;
            foundProduct.VendorShortShipped = storeInfo.VendorShortShipped==Nil? NULL:storeInfo.VendorShortShipped;
            foundProduct.VendorShortageCount = storeInfo.VendorShortageCount==Nil? NULL:storeInfo.VendorShortageCount;
            foundProduct.OnOrder = storeInfo.OnOrder==Nil? NULL:storeInfo.OnOrder;
            foundProduct.WarehouseShortShipped = storeInfo.WarehouseShortShipped==Nil? NULL:storeInfo.WarehouseShortShipped;
            foundProduct.ShelfCapacity = storeInfo.ShelfCapacity==Nil? NULL:storeInfo.ShelfCapacity;
            //            NSLog(@"%@, %@", product, foundProduct);
            
            
            //populate user interface
            
            self.productBrandLabel.text = foundProduct.ProductBrand?[@"  "stringByAppendingString:foundProduct.ProductBrand]:@"";
            self.productNameLabel.text = foundProduct.ProductName?[@"  "stringByAppendingString:foundProduct.ProductName]:@"";
            self.productVendorLabel.text = foundProduct.Vendor?[@"  "stringByAppendingString:foundProduct.Vendor]:@"";
            NSString * sizeUom = foundProduct.UomQty?[@" " stringByAppendingString:foundProduct.UomQty]:@"";
            self.productSizeLabel.text = foundProduct.Size?[[@"  "stringByAppendingString:[foundProduct.Size stringValue]] stringByAppendingString:sizeUom]:@"";
            self.productQOHLabel.text = foundProduct.CrsQOH?[@"  "stringByAppendingString:[foundProduct.CrsQOH stringValue]]:@"";
            
            
            //add product to the audited list...
            
            [self.auditedProducts addObject:foundProduct];
            //                NSLog(@"audited product list: %@", self.auditedProducts);
            
            //get cancel button working...
            self.activateCancelButton = TRUE;
            
            
            if(!([self.locationLabel.text isEqual:@"S8"]||[self.locationLabel.text isEqual:@"S7"]||[self.locationLabel.text isEqual:@"B1"])){
                
                if([foundProduct.Delisted boolValue]){
                    [self showPromtAlert:foundProduct.ProductName message:@"Is there a shelf-tag informing the customer that the product is delisted?" tag:delistedPrompt];
//                    self.scanProduct=FALSE;
                }
                
                if([foundProduct.Pulled boolValue]){
                    [self showPromtAlert:foundProduct.ProductName  message:@"Is there a shelf-tag informing the customer that the product is pulled?" tag:pulledPrompt];
//                    self.scanProduct=FALSE;
                }
                
                if([foundProduct.VendorOutOfStock boolValue]){
                    [self showPromtAlert:foundProduct.ProductName  message:@"Is there a shelf-tag informing how long the vendor is Out-of-stock?" tag:vendorOOSPrompt];
//                    self.scanProduct=FALSE;
                }
                
                if([foundProduct.OnOrder doubleValue]>0){
                    //                     [self showPromtAlert:foundProdDetails.ProductName  message:@"Is there a shelf-tag informing the customer that the product is on Order?" tag:onOrderPrompt];
                    //                    self.scanProduct=FALSE;
                }
                
                if([foundProduct.WarehouseOutOfStock boolValue]){
                    //                    [self showPromtAlert:foundProdDetails.ProductName  message:@"Is there a shelf-tag informing the customer that the product is Out-of-stock in warehouse?" tag:warehouseOOSPrompt];
                    //                    self.scanProduct=FALSE;
                }
                
            }else{
                if([foundProduct.Delisted boolValue]){
                    [self showErrorAlert:@"Please remove the shelf tag as the product is Delisted" tag:errorAlerts];
//                    self.scanProduct=FALSE;
                }
                
                if(![foundProduct.Supplier isEqualToString:@"S8"]){
                    [self showErrorAlert:@"Please remove shelf tag as the product is no longer a warehouse product" tag:errorAlerts];
//                    self.scanProduct=FALSE;
                }
            }
            
            if([foundProduct.CrsQOH doubleValue]>0){
                NSString * nystr = [NSString stringWithFormat:@"There should be %.2f of this product. ",[foundProduct.CrsQOH doubleValue]];
                [self showPromtAlert:foundProduct.ProductName  message:[nystr stringByAppendingString:@"Is the product anywhere else in the Store?"] tag:otherLocationPrompt ];
//                self.scanProduct=FALSE;
            }
            
            if(!([foundProduct.Delisted boolValue] || [foundProduct.Pulled boolValue])){
                if([storeInfo.ShelfCapacity doubleValue]==0){
                    [self showPromtAlert:foundProduct.ProductName  message:@"Is there a shelf-tag informing the product is no longer carried in this location?" tag:zeroCapacityPrompt];
//                    self.scanProduct=FALSE;
                }
            }
        }
    }
}

#pragma mark - alert notification functions


-(void)showLogoutAlert{
//    UIAlertController *infoAlertController = [UIAlertController
//                                              alertControllerWithTitle:NSLocalizedString(title,@"")
//                                              message:[NSString stringWithFormat:@"%@", body]
//                                              preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction *okaction = [UIAlertAction
//                               actionWithTitle:@"OK"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction *action)
//                               {
//                                   [self dismissViewControllerAnimated:YES completion:nil];
//                                   self.scanProduct=TRUE;
//                               }];
//    
//    [infoAlertController addAction:okaction];
//    [self presentViewController:infoAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    self.scanProduct=FALSE;
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

-(void)showErrorAlert:(NSString *)message tag:(AlertTypes)tag{
    self.scanProduct=FALSE;
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showPromtAlert:(NSString *)title message:(NSString *)message tag:(AlertTypes)tag{
    self.scanProduct=FALSE;
    
    
    UIAlertController *promptAlertController = [UIAlertController
                                                alertControllerWithTitle:NSLocalizedString(title,@"")
                                                message:[NSString stringWithFormat:@"%@", message]
                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction
                               actionWithTitle:@"Yes"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                                   
                                   ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
                                   
                                   switch (tag) {
                                       case delistedPrompt:{
                                           productInFocus.DelistedWithShelfTag=[NSNumber numberWithBool:YES];
                                       }
                                           break;
                                       
                                       case pulledPrompt:{                                                                                      productInFocus.PulledWithShelfTag=[NSNumber numberWithBool:YES];
                                       }
                                           break;
                                       case vendorOOSPrompt:{
                                           
                                           NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
                                           [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                                           
                                           NSString * oosDate = productInFocus.VendorOOSDate==NULL? @"" : [formatter stringFromDate:productInFocus.VendorOOSDate];
                                           
                                           NSString * vendorComment=[@"Please ensure the shelf-tag states Vendor will be Out-of-Stock till: " stringByAppendingString:productInFocus.VendorOOSDate==Nil? @"":[oosDate componentsSeparatedByString:@"T"][0]];
                                           
                                           productInFocus.VendorOOSWtShelfTag=[NSNumber numberWithBool:YES];
//                                           [promptAlertController dismissViewControllerAnimated:YES completion:^{
                                           [self showInfoAlert:@"Put Shelf Tag" body:vendorComment];
//                                           [self showErrorAlert:vendorComment tag:errorAlerts];
//                                           }];
                                       }
                                           break;
                                       case onOrderPrompt:{
                                           productInFocus.OnOrderWithShelfTag=[NSNumber numberWithBool:YES];
                                       }
                                           
                                           break;
                                       case warehouseOOSPrompt:{
                                           productInFocus.WareHouseOOSWtShelfTag=[NSNumber numberWithBool:YES];
                                       }
                                           
                                           break;
                                       case otherLocationPrompt:{
                                           productInFocus.InStore=[NSNumber numberWithBool:YES];
                                       }
                                           
                                           break;
                                       case finishAuditPrompt:{
                                           self.endTime = [NSDate date];
                                           [promptAlertController dismissViewControllerAnimated:YES completion:^{
                                               [self showLogoutAlert];
                                           }];
                                       }
                                           
                                           break;
                                       case zeroCapacityPrompt:{
                                           productInFocus.CapacityZeroWithShelfTag=[NSNumber numberWithBool:YES];
                                       }
                                           
                                           break;
                                       case goBackPrompt:{
                                           
                                       }
                                           break;
                                           
                                       default:
                                           [NSException raise:NSGenericException format:@"Unknown alert.."];
                                           break;
                                   }
                               }];
    
    UIAlertAction *noAction = [UIAlertAction
                                actionWithTitle:@"No"
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction *action)
                                {
                                    
                                    self.scanProduct=TRUE;
                                    
                                    ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
                                    
                                    switch (tag) {
                                        case delistedPrompt:{
                                            NSString * delistedComment=[@"Please post an appropriate shelf-tag. Catagory manager's comment: " stringByAppendingString:productInFocus.DelistedReason==Nil? @"":productInFocus.DelistedReason];
                                                productInFocus.DelistedWithShelfTag=[NSNumber numberWithBool:NO];
//                                            [promptAlertController dismissViewControllerAnimated:YES completion:^{
                                            [self showInfoAlert:@"Post Shelf Tag" body:delistedComment];
//                                                [self showErrorAlert:delistedComment tag:errorAlerts];
//                                            }];
                                        }
                                            break;
                                            
                                        case pulledPrompt:{
                                            
                                            NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
                                            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                                            
                                            NSString * pulledDate = productInFocus.PulledDate==NULL? @"" : [formatter stringFromDate:productInFocus.PulledDate];
                                            
                                            NSString * pulledDateComment = [@"Product Pulled on: "stringByAppendingString: productInFocus.PulledDate==NULL? @"":[pulledDate componentsSeparatedByString:@"T"][0]];
                                            NSString * pulledComment=[@"Please post an appropriate shelf-tag. Catagory manager's comment: " stringByAppendingString:productInFocus.PulledReason==Nil? @"":productInFocus.PulledReason];
                                            NSString * completePulledComment = [pulledComment stringByAppendingString:pulledDateComment];
                                            
                                            productInFocus.PulledWithShelfTag=[NSNumber numberWithBool:NO];
//                                            [promptAlertController dismissViewControllerAnimated:yesAction completion:^{
//                                                [self showErrorAlert:completePulledComment tag:errorAlerts];
                                            [self showInfoAlert:@"Post Shelf Tag" body:completePulledComment];
//                                            }];
                                        }
                                            break;
                                        case vendorOOSPrompt:{
                                            
                                            NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
                                            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                                            
                                            NSString * oosDate = productInFocus.VendorOOSDate==NULL? @"" : [formatter stringFromDate:productInFocus.VendorOOSDate];
                                            
                                            NSString * vendorComment=[@"Please ensure the shelf-tag states Vendor will be Out-of-Stock till: " stringByAppendingString:productInFocus.VendorOOSDate==Nil? @"":[oosDate componentsSeparatedByString:@"T"][0]];
                                            
                                            productInFocus.VendorOOSWtShelfTag=[NSNumber numberWithBool:NO];
//                                            [promptAlertController dismissViewControllerAnimated:YES completion:^{
//                                                [self showErrorAlert:vendorComment tag:errorAlerts];
                                            [self showInfoAlert:@"Check Shelf Tag" body:vendorComment];
//                                            }];
                                        }
                                            break;
                                        case onOrderPrompt:{
                                            productInFocus.OnOrderWithShelfTag=[NSNumber numberWithBool:NO];
//                                            [promptAlertController dismissViewControllerAnimated:YES completion:^{
//                                                [self showErrorAlert:@"Please put a shelf-tag informing the product is on order." tag:errorAlerts];
                                            [self showInfoAlert:nil body:@"Please put a shelf-tag informing the product is on order."];
//                                            }];
                                        }
                                            
                                            break;
                                        case warehouseOOSPrompt:{
                                            productInFocus.WareHouseOOSWtShelfTag=[NSNumber numberWithBool:NO];
//                                            [promptAlertController dismissViewControllerAnimated:YES completion:^{
                                            [self showInfoAlert:nil body:@"Please put a shelf-tag informing the warehouse is out-of-stock."];
//                                            }];
                                        }
                                            
                                            break;
                                        case otherLocationPrompt:{
                                            productInFocus.InStore=[NSNumber numberWithBool:NO];
                                        }
                                            
                                            break;
                                        case finishAuditPrompt:{
                                            [promptAlertController dismissViewControllerAnimated:YES completion:nil];
                                        }
                                            
                                            break;
                                        case zeroCapacityPrompt:{
                                            productInFocus.CapacityZeroWithShelfTag=[NSNumber numberWithBool:NO];
                                        }
                                            
                                            break;
                                        case goBackPrompt:{
                                            [promptAlertController dismissViewControllerAnimated:YES completion:nil];
                                        }
                                            break;
                                            
                                        default:
                                            [NSException raise:NSGenericException format:@"Unknown alert.."];
                                            break;
                                    }
                                }];
    
    
    [promptAlertController addAction:yesAction];
    [promptAlertController addAction:noAction];
    [self presentViewController:promptAlertController animated:YES completion:nil];
    
}

//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
//    self.scanProduct=TRUE;
//    
//    switch (alertView.tag) {
//        case errorAlerts:
//        {
////            self.scanProduct=TRUE;
//        }
//            break;
//            
//        case delistedPrompt:
//            {
//                NSLog(@"delisted prompt alert...");
//                ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
//                NSString * delistedComment=[@"Please post an appropriate shelf-tag. Catagory manager's comment: " stringByAppendingString:productInFocus.DelistedReason==Nil? @"":productInFocus.DelistedReason];
//                if(buttonIndex==0){
//                    NSLog(@"no shelf tag..");
//                    productInFocus.DelistedWithShelfTag=[NSNumber numberWithBool:NO];
//                    [self showErrorAlert:delistedComment tag:errorAlerts];
////                    self.scanProduct=TRUE;
//                }else{
//                    NSLog(@"yes shelf tag..");
//                    productInFocus.DelistedWithShelfTag=[NSNumber numberWithBool:YES];
////                    self.scanProduct=TRUE;
//                }
//            }
//            break;
//            
//        case pulledPrompt:
//            {
//                NSLog(@"pulled prompt alert...");
//                ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
//                
//                NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
//                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
//                
//                NSString * pulledDate = productInFocus.PulledDate==NULL? @"" : [formatter stringFromDate:productInFocus.PulledDate];
//                
//                NSString * pulledDateComment = [@"Product Pulled on: "stringByAppendingString: productInFocus.PulledDate==NULL? @"":[pulledDate componentsSeparatedByString:@"T"][0]];
//                NSString * pulledComment=[@"Please post an appropriate shelf-tag. Catagory manager's comment: " stringByAppendingString:productInFocus.PulledReason==Nil? @"":productInFocus.PulledReason];
//                NSString * completePulledComment = [pulledComment stringByAppendingString:pulledDateComment];
//                
//                if(buttonIndex==0){
//                    NSLog(@"no shelf tag..");
//                    productInFocus.PulledWithShelfTag=[NSNumber numberWithBool:NO];
//                    [self showErrorAlert:completePulledComment tag:errorAlerts];
////                    self.scanProduct=TRUE;
//                }else{
//                    NSLog(@"yes shelf tag..");
//                    productInFocus.PulledWithShelfTag=[NSNumber numberWithBool:YES];
////                    self.scanProduct=TRUE;
//                }
//            }
//            break;
//            
//        case zeroCapacityPrompt:
//        {
//            NSLog(@"zero capacity prompt alert...");
//            ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
//            
//            if(buttonIndex==0){
//                NSLog(@"no shelf tag..");
//                productInFocus.CapacityZeroWithShelfTag=[NSNumber numberWithBool:NO];
////                self.scanProduct=TRUE;
//            }else{
//                NSLog(@"yes shelf tag..");
//                productInFocus.CapacityZeroWithShelfTag=[NSNumber numberWithBool:YES];
////                self.scanProduct=TRUE;
//            }
//        }
//        break;
//            
//        case vendorOOSPrompt:
//            {
//                NSLog(@"vendorOOS prompt alert...");
//                ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
//                
//                NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
//                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
//                
//                NSString * oosDate = productInFocus.VendorOOSDate==NULL? @"" : [formatter stringFromDate:productInFocus.VendorOOSDate];
//                
//                NSString * vendorComment=[@"Please ensure the shelf-tag states Vendor will be Out-of-Stock till: " stringByAppendingString:productInFocus.VendorOOSDate==Nil? @"":[oosDate componentsSeparatedByString:@"T"][0]];
//                if(buttonIndex==0){
//                    NSLog(@"no shelf tag..");
//                    productInFocus.VendorOOSWtShelfTag=[NSNumber numberWithBool:NO];
//                    [self showErrorAlert:vendorComment tag:errorAlerts];
////                    self.scanProduct=TRUE;
//                }else{
//                    NSLog(@"yes shelf tag..");
//                    productInFocus.VendorOOSWtShelfTag=[NSNumber numberWithBool:YES];
//                    [self showErrorAlert:vendorComment tag:errorAlerts];
////                    self.scanProduct=TRUE;
//                }
//            }
//            break;
//        case onOrderPrompt:
//            {
//                NSLog(@"onorder prompt alert...");
//                ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
//                if(buttonIndex==0){
//                    NSLog(@"no shelf tag..");
//                    productInFocus.OnOrderWithShelfTag=[NSNumber numberWithBool:NO];
//                    [self showErrorAlert:@"Please put a shelf-tag informing the product is on order." tag:errorAlerts];
////                    self.scanProduct=TRUE;
//                }else{
//                    NSLog(@"yes shelf tag..");
//                    productInFocus.OnOrderWithShelfTag=[NSNumber numberWithBool:YES];
////                    self.scanProduct=TRUE;
//                }
//            }
//            break;
//        case warehouseOOSPrompt:
//            {
//                NSLog(@"warehouseOOS prompt alert...");
//                ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
//                if(buttonIndex==0){
//                    NSLog(@"no shelf tag..");
//                    productInFocus.WareHouseOOSWtShelfTag=[NSNumber numberWithBool:NO];
//                    [self showErrorAlert:@"Please put a shelf-tag informing the warehouse is out-of-stock." tag:errorAlerts];
////                    self.scanProduct=TRUE;
//                }else{
//                    NSLog(@"yes shelf tag..");
//                    productInFocus.WareHouseOOSWtShelfTag=[NSNumber numberWithBool:YES];
////                    self.scanProduct=TRUE;
//                }
//            }
//            break;
//        case otherLocationPrompt:
//            {
//                NSLog(@"otherLocation prompt alert...");
//                ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
//                if(buttonIndex==0){
//                    NSLog(@"no shelf tag..");
//                    productInFocus.InStore=[NSNumber numberWithBool:NO];
////                    self.scanProduct=TRUE;
//                }else{
//                    NSLog(@"yes shelf tag..");
//                    productInFocus.InStore=[NSNumber numberWithBool:YES];
////                    self.scanProduct=TRUE;
//                }
//            }
//            break;
//        case logoutPrompt:
//            {
//                [self startSpinner:@"Saving Audit..."];
//                [self saveAuditToFile];
//                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    [NSUserDefaults resetStandardUserDefaults];
//                    [[NSUserDefaults standardUserDefaults]synchronize];
//                    
//                    self.auditedProducts=Nil;
//                    
//                    [self stopSpinner];
//                    [self.navigationController popViewControllerAnimated:YES];
//                });
//            }
//            break;
//        case finishAuditPrompt:
//            {
//                NSLog(@"submit audit prompt alert...");
//                if(buttonIndex==0){
//                }else{
//                    NSLog(@"submitting audit...");
//                    self.endTime = [NSDate date];
//                    [self showErrorAlert:@"Audit Finished.." tag:logoutPrompt];
//                }
//            }
//            break;
//            
//        case goBackPrompt:
//        {
//            NSLog(@"go back prompt");
//            if(buttonIndex==0){
//            }else{
//                NSLog(@"going back...");
//                [self startSpinner:@"Deleting Audit.."];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
//                    NSFetchRequest *request = [[NSFetchRequest alloc]init];
//
//                    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];
//                    
//                    [request setPredicate:predicate];
//                    [request setEntity:entityDesc];
//                    
//                    NSError *error;
//                    NSArray *results= [context executeFetchRequest:request error:&error];
//                    
//                    for (NSManagedObject * obj in results){
//                        [context deleteObject:obj];
//                    }
//                    [context save:&error];
//                    
//                    [NSUserDefaults resetStandardUserDefaults];
//                    [[NSUserDefaults standardUserDefaults]synchronize];
//                    
//                    self.auditedProducts=Nil;
//                    
//                    [self stopSpinner];
//                    [self.navigationController popViewControllerAnimated:YES];
//                });
//            }
//        }
//            break;
//            
//        default:
//            [NSException raise:NSGenericException format:@"Unknown product specification.."];
//            break;
//    }
//}



#pragma mark - spinner functions

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *customVC     = [[UIViewController alloc] init];
        
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [statusAlert setValue:customVC forKey:@"contentViewController"];
        
        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:nil];
        statusAlert = nil;
    }
}



#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailability{
    if([self.scanner connstate]==1){
        [self.scanShelfTagbutton setTitle:@"" forState:UIControlStateNormal];
        [self.scanShelfTagbutton setBackgroundColor:nil];
        [self.scanShelfTagbutton setImage:[UIImage imageNamed:@"iPhoneCamera.png"] forState:UIControlStateNormal];
        self.scannerAvailable=FALSE;
//        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
}




#pragma mark - button functions

- (IBAction)cancelLastScan:(id)sender {
    if(self.activateCancelButton){
        ReportElement * productInFocus =[self.auditedProducts objectAtIndex:self.auditedProducts.count-1];
        [self showInfoAlert:productInFocus.ProductName body:@"This product has been deleted from the audit"];
        
        [self.auditedProducts removeObjectAtIndex:self.auditedProducts.count-1];
        NSLog(@"last scanned product deleted..");
        self.productNameLabel.text=self.productSizeLabel.text=self.productQOHLabel.text=self.productVendorLabel.text=self.productBrandLabel.text=@"";
        
        self.activateCancelButton=FALSE;
    }else{
        NSLog(@"cancel deactivated...");
        [self showInfoAlert:@"Delete Deactivated" body:@"Please scan new product to activate this feature"];
        
    }
}

- (IBAction)finishScanning:(id)sender {
    self.scanProduct=NO;
    if(self.auditedProducts.count==0){
        [self showInfoAlert:nil body:@"Please scan some products to submit"];
    }else{
        UIAlertController *finishAlertController = [UIAlertController
                                                    alertControllerWithTitle:nil
                                                    message:@"Sure To Finish Audit?"
                                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *yesAction = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                        self.endTime = [NSDate date];
                                        
                                        [self startSpinner:@"Saving Audit..."];
                                        [self saveAuditToFile];
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            [NSUserDefaults resetStandardUserDefaults];
                                            [[NSUserDefaults standardUserDefaults]synchronize];
                                            
                                            self.auditedProducts=Nil;
                                            
                                            [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                [self.navigationController popViewControllerAnimated:YES];
                                            }];
                                        });
                                    }];
        
        UIAlertAction *noAction = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       self.scanProduct=TRUE;
                                   }];
        
        [finishAlertController addAction:yesAction];
        [finishAlertController addAction:noAction];
        [self presentViewController:finishAlertController animated:YES completion:nil];
    }
}


- (IBAction)scanShelfTag:(id)sender {
    if(self.scannerAvailable){
    self.productNameLabel.text=self.productSizeLabel.text=self.productQOHLabel.text=self.productVendorLabel.text=self.productBrandLabel.text=@"";
        [self.scanner barcodeStartScan:nil];
        self.foundBarcode = NO;
        
        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
    }else{
        ZBarReaderViewController *reader = [ZBarReaderViewController new];
        reader.readerDelegate = self;
        [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
        reader.readerView.zoom = 1.0;
        [self presentViewController:reader animated:YES completion:nil];
    }
}

- (IBAction)backToMain:(id)sender {
//    [self showPromtAlert:@"Quit Audit" message:@"Sure to quit audit? Clicking YES will delete all work you've done on this audit" tag:goBackPrompt];
    
    self.scanProduct=NO;
    UIAlertController *finishAlertController = [UIAlertController
                                                alertControllerWithTitle:@"Quit Audit"
                                                message:@"Sure to quit audit? Clicking YES will delete all work you've done on this audit"
                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
//                                    [finishAlertController dismissViewControllerAnimated:YES completion:nil];
//                                    NSLog(@"going back...");
                                    
//                                    [finishAlertController dismissViewControllerAnimated:YES completion:^{
                                        [self startSpinner:@"Deleting Audit.."];
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                            NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                            
                                            NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];
                                            
                                            [request setPredicate:predicate];
                                            [request setEntity:entityDesc];
                                            
                                            NSError *error;
                                            NSArray *results= [context executeFetchRequest:request error:&error];
                                            
                                            for (NSManagedObject * obj in results){
                                                [context deleteObject:obj];
                                            }
                                            [context save:&error];
                                            
                                            [NSUserDefaults resetStandardUserDefaults];
                                            [[NSUserDefaults standardUserDefaults]synchronize];
                                            
                                            self.auditedProducts=Nil;
                                            
//                                            [self stopSpinner];
                                            
                                            [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                [self.navigationController popViewControllerAnimated:YES];
                                            }];
                                        });
//                                    }];
                                }];

    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [finishAlertController addAction:yesAction];
    [finishAlertController addAction:noAction];
    [self presentViewController:finishAlertController animated:YES completion:nil];

}

-(void)stopScanner
{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}


-(void)saveAuditToFile{
    
    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:@"OUT_OF_STOCK_AUDIT_SUBMISSION" forKey:@"documentType"];
    [newManagedObject setValue:@"Finished" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.endTime forKey:@"date"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
    [context save:&error];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *stringFromDate = [formatter stringFromDate:self.endTime];
    
    OutOfStockAuditData * auditData = [[OutOfStockAuditData alloc]init];
    auditData.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
    auditData.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
    auditData.StartTime = self.startTime;
    auditData.EndTime = self.endTime;
    auditData.AuditList = self.auditedProducts;
    
    [self writeStringToFile:[auditData toJSONString] fileName:stringFromDate];
    NSLog(@"File Saved!!!");
}



#pragma mark - nsdate conversion functions

- (NSDate*) getDateFromJSON:(NSString *)dateString
{
    // Expect date in this format "/Date(1268123281843)/"
    int startPos = (int)[dateString rangeOfString:@"("].location+1;
    int endPos = (int)[dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    //    NSLog(@"%llu",milliseconds);
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}

#pragma mark - iphone file system handler

- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}



@end
