//
//  CNFPendingDocumentListTableViewController.m
//  CNF App Test
//
//  Created by Anik on 2017-06-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFPendingDocumentListTableViewController.h"
#import "CNFAppEnums.h"

@interface CNFPendingDocumentListTableViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * pendingDocumentArray;
@property (nonatomic) NSUInteger selectedRow;
@end

@implementation CNFPendingDocumentListTableViewController{
    UIAlertController * statusAlert;
    NSManagedObjectContext * context;
}

@synthesize context=_context, pendingDocumentArray=_pendingDocumentArray, selectedRow=_selectedRow;

- (void)viewDidLoad {
    [super viewDidLoad];

//    [self startSpinner:@"Loading Data.."];
    
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"SAVED_DOC"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    NSFetchRequest *statusRequest = [[NSFetchRequest alloc]init];
    [statusRequest setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
    
    NSSortDescriptor *eventSorter=[[NSSortDescriptor alloc]initWithKey:@"createdDate" ascending:NO];
    [statusRequest setSortDescriptors:[[NSArray alloc] initWithObjects:eventSorter, nil]];
    
    NSError *error;
    NSArray *results= [context executeFetchRequest:statusRequest error:&error];
    self.pendingDocumentArray = [[NSMutableArray alloc]initWithArray:results];

    BOOL adjustment = FALSE;
    for(NSString * groupName in [[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"]){
        if([groupName rangeOfString:@"adjustment"].location == NSNotFound)
            continue;
        else
            adjustment=TRUE;
    }
    
    if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-Receivers"]){
        NSPredicate * receiverPredicate = [NSPredicate predicateWithFormat:@"documentType==%@ OR documentType==%@ OR documentType==%@ OR documentType==%@ OR documentType==%@", @"VOS_RECEIVE", @"TRANSFER_CREATION", @"RECEIPT_RECEIVE", @"VOS_RECEIVE_DESCR", @"PRODUCE_RECEIVE"];
        NSArray * truncateReceive = [self.pendingDocumentArray filteredArrayUsingPredicate:receiverPredicate];
        [self.pendingDocumentArray removeObjectsInArray:truncateReceive];
    }

    if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"USERGROUPS"] containsObject: @"CNFAPP-Picking"]){
        //check if a picker logged in..
        NSPredicate * pickingPredicate = [NSPredicate predicateWithFormat:@"documentType==%@", @"PICKING"];
        NSArray * truncatePick = [self.pendingDocumentArray filteredArrayUsingPredicate:pickingPredicate];
        [self.pendingDocumentArray removeObjectsInArray:truncatePick];
    }else{
        //picker logged in..check if created date is today..if not truncate

        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *comps = [cal components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear)
                                         fromDate:[NSDate date]];
        NSDate *today = [cal dateFromComponents:comps];
        NSPredicate * pickingPredicate = [NSPredicate predicateWithFormat:@"documentType==%@ AND createdDate <= %@", @"PICKING", today];
        NSArray * truncatePick = [self.pendingDocumentArray filteredArrayUsingPredicate:pickingPredicate];
        [self.pendingDocumentArray removeObjectsInArray:truncatePick];

    }
    
    if(!adjustment){
        NSPredicate * adjusterPredicate = [NSPredicate predicateWithFormat:@"documentType==%@ OR documentType==%@ OR documentType==%@ OR documentType==%@ OR documentType==%@ OR documentType==%@ OR documentType==%@ OR documentType==%@", @"C2_CAFE", @"C2_MARKETING", @"C2_NFP_DISCARD", @"C2_RET_DISCARD", @"C2_SPOILED", @"C2_STAFF_BENEFIT", @"C2_STAFF_ERROR", @"C2_VENDOR_ERROR"];
        NSArray * truncateAdjustment = [self.pendingDocumentArray filteredArrayUsingPredicate:adjusterPredicate];
        [self.pendingDocumentArray removeObjectsInArray:truncateAdjustment];
    }
    
//    [statusAlert dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - alertview and indicator spin handler

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *customVC     = [[UIViewController alloc] init];
        
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [statusAlert setValue:customVC forKey:@"contentViewController"];
        
        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:nil];
        statusAlert = nil;
    }
}



#pragma mark - alertview functions and delegates

-(void)continueDocAlert{

    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-yy HH:mm:ss"];
    
    if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"C2_CAFE"]||[[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"C2_MARKETING"]||[[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"C2_NFP_DISCARD"]||[[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"C2_RET_DISCARD"]||[[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"C2_SPOILED"]||[[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"C2_STAFF_BENEFIT"]||[[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"C2_STAFF_ERROR"]||[[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"C2_VENDOR_ERROR"]){
        
        
        UIAlertController * continueAdjustmentAlertController = [UIAlertController alertControllerWithTitle: [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]
                                                                                          message: [NSString stringWithFormat:@"Created by user: %@, from location: %@, for location: %@, on: %@, Press continue to complete", [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"userName"], [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"holdingLocation"], [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"toLocation"], [formatter stringFromDate:[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"createdDate"]]]
                                                                                   preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction * continueAction = [UIAlertAction
                                          actionWithTitle:@"Continue"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction *action)
                                          {
                                              [self dismissViewControllerAnimated:YES completion:nil];
                                              [self performSegueWithIdentifier:@"continueAdjustment" sender:self];
                                          }];
        
        UIAlertAction * cancelAction = [UIAlertAction
                                          actionWithTitle:@"Cancel"
                                          style:UIAlertActionStyleDestructive
                                          handler:^(UIAlertAction *action)
                                          {
                                              [self dismissViewControllerAnimated:YES completion:nil];
                                          }];
        
        [continueAdjustmentAlertController addAction:continueAction];
        [continueAdjustmentAlertController addAction:cancelAction];
        
        [self presentViewController:continueAdjustmentAlertController animated:YES completion:nil];


    }else if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"PICKING"]){
        //continue doing a pick document

        UIAlertController * continuePickAlertController = [UIAlertController alertControllerWithTitle: [NSString stringWithFormat:@"%@ for %@", [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"], [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"vosCmNote"]]
                                                                                                    message: [NSString stringWithFormat:@"Created by user: %@, from location: %@, for location: %@, on: %@, Press continue to complete", [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"userName"], [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"holdingLocation"], [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"toLocation"], [formatter stringFromDate:[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"createdDate"]]]
                                                                                             preferredStyle: UIAlertControllerStyleAlert];

        UIAlertAction * continueAction = [UIAlertAction
                                          actionWithTitle:@"Continue"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction *action)
                                          {
                                              [self dismissViewControllerAnimated:YES completion:nil];
                                              [self performSegueWithIdentifier:@"continuePicking" sender:self];
                                          }];

        UIAlertAction * cancelAction = [UIAlertAction
                                        actionWithTitle:@"Cancel"
                                        style:UIAlertActionStyleDestructive
                                        handler:^(UIAlertAction *action)
                                        {
                                            [self dismissViewControllerAnimated:YES completion:nil];
                                        }];

        [continuePickAlertController addAction:continueAction];
        [continuePickAlertController addAction:cancelAction];

        [self presentViewController:continuePickAlertController animated:YES completion:nil];




    }else{

            UIAlertController * continueReceivingAlertController = [[UIAlertController alloc]init];
            
            if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"TRANSFER_CREATION"]){
            
                continueReceivingAlertController = [UIAlertController alertControllerWithTitle: [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentNo"]
                                                                                       message: [NSString stringWithFormat:@"Created by user: %@, from location: %@, for location: %@, created on: %@", [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"userName"], [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"holdingLocation"], [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"toLocation"], [formatter stringFromDate:[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"createdDate"]]]
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                
            }else{
                
                continueReceivingAlertController = [UIAlertController alertControllerWithTitle: [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentNo"]
                                                                                       message: [NSString stringWithFormat:@"Created by user: %@ on: %@, receipt no: %@, from Vendor: %@", [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"userName"],[formatter stringFromDate:[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"createdDate"]] ,[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"receiptNo"]?[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"receiptNo"]:[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentNo"], [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"vendorId"]?[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"vendorId"]:[[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentNo"] componentsSeparatedByString:@"-"][0]]
                                                                                preferredStyle: UIAlertControllerStyleAlert];                
            }
        
            
            UIAlertAction * continueAction = [UIAlertAction
                                              actionWithTitle:@"Continue"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction *action)
                                              {
                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                  
                                                  if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"TRANSFER_CREATION"]|| [[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"VOS_RECEIVE"] || [[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"RECEIPT_RECEIVE"]){
                                                      
                                                      [self performSegueWithIdentifier:@"continueReceiving" sender:self];
                                                      
                                                  }else if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"VOS_RECEIVE_DESCR"]){
                                                      [self performSegueWithIdentifier:@"continueDiscrepency" sender:self];
                                                  }else if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"documentType"]isEqualToString:@"PRODUCE_RECEIVE"]){
                                                      [self performSegueWithIdentifier:@"continueProduceReceiving" sender:self];
                                                  }

                                              }];
            
            UIAlertAction * cancelAction = [UIAlertAction
                                            actionWithTitle:@"Cancel"
                                            style:UIAlertActionStyleDestructive
                                            handler:^(UIAlertAction *action)
                                            {
                                                [self dismissViewControllerAnimated:YES completion:nil];
                                            }];
            
            [continueReceivingAlertController addAction:continueAction];
            [continueReceivingAlertController addAction:cancelAction];
            
            [self presentViewController:continueReceivingAlertController animated:YES completion:nil];

    }
}

-(void)showErrorAlert:(NSString *)message{
    
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pendingDocumentArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pendingDocumentCell" forIndexPath:indexPath];
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-yy HH:mm:ss"];
    
    if([[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"C2_CAFE"]||[[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"C2_MARKETING"]||[[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"C2_NFP_DISCARD"]||[[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"C2_RET_DISCARD"]||[[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"C2_SPOILED"]||[[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"C2_STAFF_BENEFIT"]||[[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"C2_STAFF_ERROR"]||[[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"C2_VENDOR_ERROR"]){
    
        cell.textLabel.text = [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Created by user: %@, from location: %@, for location: %@, created on: %@", [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"userName"], [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"holdingLocation"], [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"toLocation"], [formatter stringFromDate:[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"createdDate"]]];
    }else if([[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"PICKING"]){
        cell.textLabel.text = [NSString stringWithFormat:@"%@ for department: %@",[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"], [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"vosCmNote"] ];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Created by user: %@, from location: %@, for location: %@, created on: %@", [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"userName"], [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"holdingLocation"], [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"toLocation"], [formatter stringFromDate:[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"createdDate"]]];
    }
    else{
        
        if([[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentType"]isEqualToString:@"TRANSFER_CREATION"]){
            cell.textLabel.text = [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentNo"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Created by user: %@ on: %@ for location: %@", [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"userName"],[formatter stringFromDate:[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"createdDate"]], [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"toLocation"]];
        }else{
            cell.textLabel.text = [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentNo"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Created by user: %@ on: %@, receipt no: %@, from Vendor: %@", [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"userName"],[formatter stringFromDate:[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"createdDate"]] ,[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"receiptNo"]?[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"receiptNo"]:[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentNo"], [[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"vendorId"]?[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"vendorId"]:[[[self.pendingDocumentArray objectAtIndex:indexPath.row] valueForKey:@"documentNo"] componentsSeparatedByString:@"-"][0]];
        }
    }
    
    cell.textLabel.numberOfLines=0;
    cell.detailTextLabel.numberOfLines=0;

    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedRow=indexPath.row;
    [self continueDocAlert];
}



#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    //global variables

    NSError * error;
    NSMutableDictionary * tempDic = [[NSMutableDictionary alloc]init];

    @try{
        if([segue.identifier isEqual:@"continueReceiving"]){

            NSLog(@"will continue with receiving");
            //        [self startSpinner:@"Please Wait"];


            if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentType"]isEqualToString:@"VOS_RECEIVE"]){

                NSPredicate *vendorPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"VendorList"];
                NSObject *vendorOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:vendorPred] objectAtIndex:0];

                NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
                NSFileHandle *file;
                NSData *databuffer;
                file = [NSFileHandle fileHandleForReadingAtPath:filepath];

                [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[vendorOffset valueForKey:@"O"] intValue]];

                databuffer = [file readDataOfLength: [[vendorOffset valueForKey:@"L"] intValue]];
                [file closeFile];

                NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
                NSArray* vendorArray = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]
                                                                       options:0 error:&error];

                if(error){
                    NSLog(@"Could not serialize..");
                    [self showErrorAlert:@"Serialization error, data file not downloaded properly"];

                }else{
                    NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"VendorId == %@", [[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentNo"]componentsSeparatedByString:@"-"][0]];

                    NSManagedObject * vendor = [[vendorArray filteredArrayUsingPredicate:vendorPredicate] objectAtIndex:0];

//                    [tempDic setObject:@"receiveWithOrderSheet" forKey:@"APP_CODE"];
                    [tempDic setObject:@"VOS_RECEIVE" forKey:@"APP_CODE"];
                    [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentNo"] forKey:@"DOCUMENT_NO"];
                    [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"] forKey:@"DOCUMENT_ID"];
                    [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"receiptNo"] forKey:@"VOS_RECEIPT"];
                    [tempDic setObject:vendor forKey:@"VENDOR"];
                }

            }else if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentType"]isEqualToString:@"RECEIPT_RECEIVE"]){

                NSPredicate *vendorPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"VendorList"];
                NSObject *vendorOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:vendorPred] objectAtIndex:0];

                NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
                NSFileHandle *file;
                NSData *databuffer;
                file = [NSFileHandle fileHandleForReadingAtPath:filepath];

                [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[vendorOffset valueForKey:@"O"] intValue]];

                databuffer = [file readDataOfLength: [[vendorOffset valueForKey:@"L"] intValue]];
                [file closeFile];

                NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
                NSArray* vendorArray = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]
                                                                       options:0 error:&error];

                if(error){
                    NSLog(@"Could not serialize..");
                    [self showErrorAlert:@"Serialization error, data file not downloaded properly"];
                }else{
                    NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"VendorId == %@", [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"vendorId"]];

                    NSManagedObject * vendor = [[vendorArray filteredArrayUsingPredicate:vendorPredicate] objectAtIndex:0];

//                    [tempDic setObject:@"receiveWithoutOrderSheet" forKey:@"APP_CODE"];
                    [tempDic setObject:@"RECEIPT_RECEIVE" forKey:@"APP_CODE"];
                    [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentNo"] forKey:@"DOCUMENT_NO"];
                    [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"] forKey:@"DOCUMENT_ID"];
                    [tempDic setObject:vendor forKey:@"VENDOR"];
                }

            }else if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentType"]isEqualToString:@"TRANSFER_RECEIVE"]){

                NSMutableDictionary * tempDic = [[NSMutableDictionary alloc]init];
//                [tempDic setObject:@"receiveTransfer" forKey:@"APP_CODE"];
                [tempDic setObject:@"TRANSFER_RECEIVE" forKey:@"APP_CODE"];
                [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentNo"] forKey:@"DOCUMENT_NO"];
                [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"] forKey:@"DOCUMENT_ID"];
                [[NSUserDefaults standardUserDefaults]setValue:tempDic forKey:@"SAVED_DOC"];
                [[NSUserDefaults standardUserDefaults] synchronize];

            }else if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentType"]isEqualToString:@"TRANSFER_CREATION"]){

                NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"LocationList"];
                NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];

                NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
                NSFileHandle *file;
                NSData *databuffer;
                file = [NSFileHandle fileHandleForReadingAtPath:filepath];

                [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];

                databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
                [file closeFile];

                NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
                NSArray* locationArray = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];

                if(error){
                    NSLog(@"Could not serialize..");
                    [self showErrorAlert:@"Serialization error, data file not downloaded properly"];
                }else{
                    NSPredicate * locationPredicate = [NSPredicate predicateWithFormat:@"LocationId == %@", [[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"toLocation"]];

                    NSManagedObject * location = [[locationArray filteredArrayUsingPredicate:locationPredicate] objectAtIndex:0];

//                    [tempDic setObject:@"createTransfer" forKey:@"APP_CODE"];
                    [tempDic setObject:@"TRANSFER_CREATION" forKey:@"APP_CODE"];
                    [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentNo"] forKey:@"DOCUMENT_NO"];
                    [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"] forKey:@"DOCUMENT_ID"];
                    [tempDic setObject:location forKey:@"TO_LOCATION"];
                }
            }else if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentType"]isEqualToString:@"VOS_RECEIVE_DESCR"]){

                NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context];
                NSFetchRequest *request = [[NSFetchRequest alloc]init];

                NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId==%@",[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"]];

                [request setPredicate:predicate];
                [request setEntity:entityDesc];

                NSArray *productResults= [context executeFetchRequest:request error:&error];

                NSMutableArray * allProducts = [[NSMutableArray alloc]init];
                NSMutableArray * notes = [[NSMutableArray alloc]init];

                for(NSDictionary * product in productResults){
                    NSMutableDictionary * finalProduct = [[NSMutableDictionary alloc]init];
                    [finalProduct setValue:[product valueForKey:@"productId"] forKey:@"ProductId"];
                    [finalProduct setValue:[product valueForKey:@"productName"] forKey:@"Name"];
                    [finalProduct setValue:[product valueForKey:@"quantityText"] forKey:@"QuantityText"];
                    [allProducts addObject:finalProduct];
                    if([product valueForKey:@"note"])
                        [notes addObject:[product valueForKey:@"note"]];
                }

                entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                request = [[NSFetchRequest alloc]init];

                predicate   = [NSPredicate predicateWithFormat:@"documentId==%@",[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"]];

                [request setPredicate:predicate];
                [request setEntity:entityDesc];

                NSArray *docResults= [context executeFetchRequest:request error:&error];
                int imageCounter = [[[docResults objectAtIndex:0]valueForKey:@"attachmentNo"] intValue];

                BOOL pushToCM = [[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"vosCmNote"]?YES:NO;

                if(pushToCM)
                    [notes addObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"vosCmNote"]];

                NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
                [receiving setValue:allProducts forKey:@"productList"];
                [receiving setValue:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"] forKey:@"documentId"];
                [receiving setValue:[NSNumber numberWithInt:imageCounter] forKey:@"imagecounter"];
                [receiving setValue:[NSNumber numberWithBool:pushToCM] forKey:@"pushToCM"];
                [receiving setValue:notes forKey:@"notes"];

                [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
                [[NSUserDefaults standardUserDefaults] synchronize];

            }

            NSString * documentId = [[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"];

            NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
            NSFetchRequest *request = [[NSFetchRequest alloc]init];

            NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"status ==%@ AND documentId==%@",@"Created",documentId ];

            [request setPredicate:predicate];

            [request setEntity:entityDesc];

            NSArray *statusTableresults= [context executeFetchRequest:request error:&error];

            for(NSManagedObject * obj in statusTableresults){
                [tempDic setObject:[obj valueForKey:@"date"] forKey:@"DATE"];
                if([obj valueForKey:@"attachmentNo"]){
                    [tempDic setObject:[obj valueForKey:@"attachmentNo"] forKey:@"IMAGE_NO"];
                }
            }

            [[NSUserDefaults standardUserDefaults] setValue:[tempDic valueForKey:@"APP_CODE"] forKey:@"APP_CODE"];
            [[NSUserDefaults standardUserDefaults]setValue:tempDic forKey:@"SAVED_DOC"];
            [[NSUserDefaults standardUserDefaults] synchronize];

        }else if([segue.identifier isEqual:@"continueAdjustment"]){

            NSLog(@"will continue with adjustment");
            //        [self startSpinner:@"Please Wait"];

            if([[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentNo"]isEqualToString:@"reverseAdjustment"]){
                [tempDic setObject:@"REVERSE_ADJUSTMENT" forKey:@"APP_CODE"];
            }else{
                [tempDic setObject:@"ADJUSTMENT" forKey:@"APP_CODE"];
            }

            [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"holdingLocation"] forKey:@"HOLDING_LOCATION"];
            [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"] forKey:@"DOCUMENT_ID"];
            [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"toLocation"] forKey:@"CONSUMING_LOCATION"];
            [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentType"] forKey:@"DOCUMENT_TYPE"];
            [tempDic setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"createdDate"] forKey:@"CREATED_DATE"];

            [[NSUserDefaults standardUserDefaults] setValue:[tempDic valueForKey:@"APP_CODE"] forKey:@"APP_CODE"];
            [[NSUserDefaults standardUserDefaults]setValue:tempDic forKey:@"SAVED_DOC"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            //        [self stopSpinner];
            //        [statusAlert dismissViewControllerAnimated:YES completion:nil];

        }else if([segue.identifier isEqual:@"continueDiscrepency"]){

            NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context];
            NSFetchRequest *request = [[NSFetchRequest alloc]init];

            NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId==%@",[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"]];

            [request setPredicate:predicate];
            [request setEntity:entityDesc];

            NSArray *productResults= [context executeFetchRequest:request error:&error];

            NSMutableArray * allProducts = [[NSMutableArray alloc]init];
            NSMutableArray * notes = [[NSMutableArray alloc]init];

            for(NSDictionary * product in productResults){
                NSMutableDictionary * finalProduct = [[NSMutableDictionary alloc]init];
                [finalProduct setValue:[product valueForKey:@"productId"] forKey:@"ProductId"];
                [finalProduct setValue:[product valueForKey:@"productName"] forKey:@"Name"];
                [finalProduct setValue:[product valueForKey:@"quantityText"] forKey:@"QuantityText"];
                [allProducts addObject:finalProduct];
                if([product valueForKey:@"note"])
                    [notes addObject:[product valueForKey:@"note"]];
            }

            entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
            request = [[NSFetchRequest alloc]init];

            predicate   = [NSPredicate predicateWithFormat:@"documentId==%@",[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"]];

            [request setPredicate:predicate];
            [request setEntity:entityDesc];

            NSArray *docResults= [context executeFetchRequest:request error:&error];
            int imageCounter = [[[docResults objectAtIndex:0]valueForKey:@"attachmentNo"] intValue];

            BOOL pushToCM = [[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"vosCmNote"]?YES:NO;

            if(pushToCM)
                [notes addObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"vosCmNote"]];

            NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
            [receiving setValue:allProducts forKey:@"productList"];
            [receiving setValue:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentId"] forKey:@"documentId"];
            [receiving setValue:[NSNumber numberWithInt:imageCounter] forKey:@"imagecounter"];
            [receiving setValue:[NSNumber numberWithBool:pushToCM] forKey:@"pushToCM"];
            [receiving setValue:notes forKey:@"notes"];

            [[NSUserDefaults standardUserDefaults] setObject:[[self.pendingDocumentArray objectAtIndex:self.selectedRow]valueForKey:@"documentNo"] forKey:@"DOCUMENT_NO"];
            [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
            [[NSUserDefaults standardUserDefaults] setValue:@"VOS_RECEIVE" forKey:@"APP_CODE"];
            [[NSUserDefaults standardUserDefaults] setValue:[[self.pendingDocumentArray objectAtIndex:self.selectedRow] valueForKey:@"receiptNo"] forKey:@"VOS_RECEIPT"];
            [[NSUserDefaults standardUserDefaults] synchronize];

        }else if([segue.identifier isEqual:@"continuePicking"]){

            NSDictionary * pickSavedObject = [self.pendingDocumentArray objectAtIndex:[self.tableView indexPathForSelectedRow].row ];

            NSMutableDictionary * pickingObject = [[NSMutableDictionary alloc]init];

            [pickingObject setValue:[pickSavedObject valueForKey:@"toLocation"] forKey:@"Pick_For_Location"];
            [pickingObject setValue:[pickSavedObject valueForKey:@"holdingLocation"] forKey:@"Pick_Location"];
            NSDictionary * department = [[NSDictionary alloc]initWithObjectsAndKeys:[pickSavedObject valueForKey:@"vosCmNote"], @"Name", [pickSavedObject valueForKey:@"documentNo"], @"DeptId", nil];

            [pickingObject setValue:department forKey:@"Department"];

            [tempDic setObject:[pickSavedObject valueForKey:@"documentId"] forKey:@"DOCUMENT_ID"];
            [tempDic setObject:[pickSavedObject valueForKey:@"createdDate"] forKey:@"CREATED_DATE"];

            [[NSUserDefaults standardUserDefaults] setValue:@"PICKING" forKey:@"APP_CODE"];
            [[NSUserDefaults standardUserDefaults]setValue:tempDic forKey:@"SAVED_DOC"];
            [[NSUserDefaults standardUserDefaults] setObject:pickingObject forKey:@"PICKING"];
            [[NSUserDefaults standardUserDefaults] synchronize];

        }
    }@catch(NSException * ex){
        [self showErrorAlert: [NSString stringWithFormat:@"Error Occured: %@", ex.description]];
    }

}


@end
