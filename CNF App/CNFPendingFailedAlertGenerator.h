//
//  CNFPendingFailedAlertGenerator.h
//  CNF App
//
//  Created by Anik on 2016-09-01.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNFAppDelegate.h"

@interface CNFPendingFailedAlertGenerator : NSObject<NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

-(NSArray*)getAlertDataforPendingDocument;
-(NSArray*)getAlertDataforFailedDocuments;
-(NSArray*)getAlertDataforVosFailedDocuments;

@end
