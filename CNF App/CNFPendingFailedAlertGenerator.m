//
//  CNFPendingFailedAlertGenerator.m
//  CNF App
//
//  Created by Anik on 2016-09-01.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFPendingFailedAlertGenerator.h"

@interface CNFPendingFailedAlertGenerator ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@end

@implementation CNFPendingFailedAlertGenerator{
    NSManagedObjectContext *context;
}

@synthesize context=_context;

-(NSArray*)getAlertDataforPendingDocument{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];

    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"date <= %@ AND status ==%@ AND docUploaded==NO",[[NSDate date] dateByAddingTimeInterval:-5*60], @"Finished"];
    [request setPredicate:searchPredicate];

    @try {
        NSError *error;
        NSArray *results= [context executeFetchRequest:request error:&error];
        
        if(!error){
            if(results.count==0)
                NSLog(@"No currently saved and not uploaded document..");
            else{
                return results;
            }
        }else{
            NSLog(@"Error occured while fetching data from DB");
        }
    } @catch (NSException *exception) {
        NSLog(@"exception %@ occured", exception);
        NSLog(@"Error occured while fetching data from DB");
    }
    
    return NULL;
}


-(NSArray*)getAlertDataforVosFailedDocuments{
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];
    
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"date >= %@ AND status ==%@ AND documentType==%@ AND des contains [cd] %@",[[NSDate date] dateByAddingTimeInterval:-5*60], @"Failed",@"VOS_RECEIVE", @"INVALID_STATUS_TRANSITION"];
    [request setPredicate:searchPredicate];
    
    @try {
        NSError *error;
        NSArray *results= [context executeFetchRequest:request error:&error];
        
        if(!error){
            if(results.count==0)
                NSLog(@"No failed duplicate VOS..");
            else{                
                NSMutableArray * resultArray = [[NSMutableArray alloc]init];
                
                for(NSObject* records in results){
                    NSPredicate * documentPredicate = [NSPredicate predicateWithFormat:@"documentId=%@ AND documentNo!=%@", [records valueForKey:@"documentId"],NULL];
                    [request setPredicate:documentPredicate];
                    NSArray *failedResults= [context executeFetchRequest:request error:&error];
                    
                    if(!error){
                        if(failedResults.count==0){
                            NSLog(@"Could not find related document for failed ones");
                            resultArray=NULL;
                        }else{
                            [resultArray insertObject:[failedResults objectAtIndex:0] atIndex:0];
                        }
                    }else
                        NSLog(@"error occured while trying to get the failed document numbers..");
                }
                return resultArray;
            }
        }else{
            NSLog(@"Error occured while fetching data from DB");
        }
    } @catch (NSException *exception) {
        NSLog(@"exception %@ occured", exception);
        NSLog(@"Error occured while fetching data from DB");
    }
    
    return NULL;
}




-(NSArray*)getAlertDataforFailedDocuments{
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];
    
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"date > %@ AND status ==%@",[[NSDate date] dateByAddingTimeInterval:-60*60], @"Failed"];
    [request setPredicate:searchPredicate];
    

    @try {
        NSError *error;
        NSArray *results= [context executeFetchRequest:request error:&error];
        
        if(!error){
            if(results.count==0){
                NSLog(@"No failed document..");
                return NULL;
            }else{
                NSMutableArray * resultArray = [[NSMutableArray alloc]init];
                
                for(NSObject* records in results){
                    NSPredicate * documentPredicate = [NSPredicate predicateWithFormat:@"documentId=%@ AND documentNo!=%@", [records valueForKey:@"documentId"],NULL];
                    [request setPredicate:documentPredicate];
                    NSArray *failedResults= [context executeFetchRequest:request error:&error];
                    
                    if(!error){
                        if(failedResults.count==0){
//                            NSLog(@"Could not find related document for failed ones");
                            resultArray=NULL;
                        }else{
                            [resultArray insertObject:[failedResults objectAtIndex:0] atIndex:0];
                        }
                    }else
                        NSLog(@"error occured while trying to get the failed document numbers..");
                    
                }
                return resultArray;
            }
        }else{
            NSLog(@"Error occured while fetching data from DB");
        }
    } @catch (NSException *exception) {
        NSLog(@"exception %@ occured", exception);
        NSLog(@"Error occured while fetching data from DB");
    }
    
    return NULL;
}


@end
