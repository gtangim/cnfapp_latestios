//
//  CNFAdjustmentTestViewController.h
//  CNF App
//
//  Created by Anik on 2017-04-24.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import <WebKit/WebKit.h>

@interface CNFPrintDiscardViewController : UIViewController<NSFetchedResultsControllerDelegate, UIPageViewControllerDelegate,UIPrintInteractionControllerDelegate, UIAlertViewDelegate, WKNavigationDelegate>
@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;
- (IBAction)printDiscards:(id)sender;
- (IBAction)quitAndBack:(id)sender;
@property (weak, nonatomic) IBOutlet WKWebView *webView;

@end
