//
//  CNFAdjustmentTestViewController.m
//  CNF App
//
//  Created by Anik on 2017-04-24.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFPrintDiscardViewController.h"
#import <CoreText/CoreText.h>
//#import "PDFRenderer.h"
#import "Reachability.h"
#import "CNFAppEnums.h"
#import "SSKeychain.h"
#import "NFPListObject.h"
#import "CNFGetDocumentId.h"
#import "DiscardShipElement.h"

@interface CNFPrintDiscardViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * tableData;
@property (nonatomic, strong) NSString * fileName;
@property (nonatomic, strong) NSString * documentId;

//second webservice population variables
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSDate * endTime;

@end

@implementation CNFPrintDiscardViewController{
    UIAlertController * statusAlert;
    NSManagedObjectContext * context;
    UIPrintInteractionController *printController;
}

@synthesize context=_context, tableData=_tableData, fileName=_fileName, startTime=_startTime, endTime=_endTime,documentId=_documentId, webView=_webView;

- (void)viewDidLoad {

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    self.webView.navigationDelegate = self;
    [self showCharityNameAlert];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

-(void)putDBEntryandShowPdf{
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYMMDD"];
    self.fileName = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] stringByAppendingString:[formatter stringFromDate:[NSDate date]]] stringByAppendingString:@".pdf"];

    [self getPDFFileFromWeb:self.fileName];
    //make DB entry for this process..
    self.startTime = [NSDate date];
    self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];



    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:@"ADJ_NFP_DISCARD_SHIP" forKey:@"documentType"];
    [newManagedObject setValue:@"Created" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.startTime forKey:@"date"];
    [context save:&error];
}



-(void)getPDFFileFromWeb:(NSString*)fileName{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.navigationItem.leftBarButtonItem setEnabled:false];
    [self.navigationItem.rightBarButtonItem setEnabled:false];
    if([self connectionTester]!=0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startGetDataFromWeb:fileName];
        });
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self.navigationItem.leftBarButtonItem setEnabled:true];
            [self.navigationItem.rightBarButtonItem setEnabled:false];
            [self showErrorAlert:@"No Connection"];
        });
    }
}

#pragma mark - connection tester

-(int)connectionTester{

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];

    NetworkStatus netStatus = [reachability currentReachabilityStatus];

    if(netStatus==1){
        NSLog(@"connection OK...");
    }else{
        NSLog(@"No connection for validation...");
    }

    return netStatus;
}


#pragma mark - alertview and indicator spin handler

//- (void)startSpinner:(NSString *)message {
//    if (!statusAlert){
//        statusAlert = [UIAlertController alertControllerWithTitle: nil
//                                                               message: message
//                                                        preferredStyle: UIAlertControllerStyleAlert];
//
//        UIViewController *customVC     = [[UIViewController alloc] init];
//
//        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [spinner startAnimating];
//        [customVC.view addSubview:spinner];
//
//        [customVC.view addConstraint:[NSLayoutConstraint
//                                      constraintWithItem: spinner
//                                      attribute:NSLayoutAttributeCenterX
//                                      relatedBy:NSLayoutRelationEqual
//                                      toItem:customVC.view
//                                      attribute:NSLayoutAttributeCenterX
//                                      multiplier:1.0f
//                                      constant:0.0f]];
//
//        [customVC.view addConstraint:[NSLayoutConstraint
//                                      constraintWithItem: spinner
//                                      attribute:NSLayoutAttributeCenterY
//                                      relatedBy:NSLayoutRelationEqual
//                                      toItem:customVC.view
//                                      attribute:NSLayoutAttributeCenterY
//                                      multiplier:1.0f
//                                      constant:0.0f]];
//
//        [statusAlert setValue:customVC forKey:@"contentViewController"];
//    }
//    [self presentViewController: statusAlert
//                       animated: true
//                     completion: nil];
//
//}
//
//- (void)stopSpinner {
//    if (statusAlert) {
//        [statusAlert dismissViewControllerAnimated:YES completion:nil];
//        statusAlert = nil;
//    }
//}


-(void)showErrorAlert:(NSString *)message{

    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


#pragma mark - webservice functions

-(void)startGetDataFromWeb:(NSString*)fileName{

    NSLog(@"start getting NFPDiscard pdf from web service......");

    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentDir stringByAppendingPathComponent:fileName];

    NFPListObject * requestObject = [[NFPListObject alloc] init];

    requestObject.userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
    requestObject.holdingLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
    requestObject.deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_ID"];
    requestObject.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"]];

    NSString *post = [requestObject toJSONString];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];

    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getNFPList", appDelegate.serverUrl]]];

    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              NSLog(@"Download Error:%@",error.description);
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                  [self.navigationItem.leftBarButtonItem setEnabled:true];
                                                  [self.navigationItem.rightBarButtonItem setEnabled:false];
                                                  [self showErrorAlert:@"No Connection, please restart"];
                                              });
                                          }else{
                                              if (data) {
                                                  NSDictionary *strings = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
                                                  if([strings valueForKey:@"Message"]==Nil){

                                                      if([strings valueForKey:@"d"]!= [NSNull null]){
                                                          NSData *nsdataFromBase64String = [[NSData alloc] initWithBase64EncodedString:[strings objectForKey:@"d"] options:0];

                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [nsdataFromBase64String writeToFile:filePath atomically:YES];
                                                              NSLog(@"Downloaded data File is saved to %@",filePath);
                                                              [self showPDFFile:fileName];
                                                          });
                                                      }else{
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                              [self.navigationItem.leftBarButtonItem setEnabled:true];
                                                              [self.navigationItem.rightBarButtonItem setEnabled:false];
                                                              self.navigationItem.rightBarButtonItem.enabled = NO;
                                                              self.fileName=NULL;
                                                              [self showErrorAlert:@"No current discards for this location to print"];
                                                          });
                                                      }
                                                  }else{
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          NSLog(@"error msg: %@", [strings valueForKey:@"Message"]);
                                                          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                          [self.navigationItem.leftBarButtonItem setEnabled:true];
                                                          [self.navigationItem.rightBarButtonItem setEnabled:false];
                                                          [self showErrorAlert:[strings valueForKey:@"Message"]];
                                                      });
                                                  }
                                              }
                                          }
                                      }];
    [dataTask resume];

}

-(void)showPDFFile:(NSString*)fileName
{
    NSArray *arrayPaths = NSSearchPathForDirectoriesInDomains(
                                                              NSDocumentDirectory,
                                                              NSUserDomainMask,
                                                              YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];

    NSURL *url = [NSURL fileURLWithPath:pdfFileName];
    //    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //    [self.webView loadRequest:request];
    [self.webView loadFileURL:url allowingReadAccessToURL:url];
    [self.view addSubview:self.webView];
}


-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.navigationItem.leftBarButtonItem setEnabled:true];
    [self.navigationItem.rightBarButtonItem setEnabled:true];
}


-(void)printPdfFile:(NSString*)fileName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSData *myData = [NSData dataWithContentsOfFile: path];

    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    if(pic && [UIPrintInteractionController canPrintData: myData] ) {

        pic.delegate = self;

        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        pic.printInfo = printInfo;
        pic.printingItem = myData;

        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            if(!completed && error){
                NSLog(@"Print Error: %@", error);
                [self showErrorAlert:@"Error occured while printing, Please try again"];
            }else if(completed){
                [self showAcknowledgementAlert];
            }else{

            }
        };
        [pic presentAnimated:YES completionHandler:completionHandler];
    }
}


-(void)showCharityNameAlert{
    UIAlertController *charityNameAlertController = [UIAlertController
                                                     alertControllerWithTitle:nil
                                                     message:[NSString stringWithFormat:@"Please enter the name of the charity this is going to"]
                                                     preferredStyle:UIAlertControllerStyleAlert];

    [charityNameAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"NFP_DISCARD_CHARITY_NAME"];
        [textField becomeFirstResponder];
        textField.keyboardType = UIKeyboardTypeDefault;
    }];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   UITextField * alerttext = [charityNameAlertController.textFields firstObject];
                                   if([alerttext hasText]){
                                       [[NSUserDefaults standardUserDefaults] setValue:[alerttext text] forKey:@"NFP_DISCARD_CHARITY_NAME"];
                                       [[NSUserDefaults standardUserDefaults] synchronize];
                                       [self putDBEntryandShowPdf];
                                   }else{
                                       [self showCharityNameAlert];
                                   }
                               }];

    UIAlertAction *cancelaction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action)
                                   {
                                       //                                   [self dismissViewControllerAnimated:YES completion:^{
                                       [self.navigationController popViewControllerAnimated:YES];
                                       //                                   }];
                                   }];

    [charityNameAlertController addAction:okaction];
    [charityNameAlertController addAction:cancelaction];
    [self presentViewController:charityNameAlertController animated:YES completion:nil];
}


-(void)showAcknowledgementAlert{

    UIAlertController *acknowledgementAlertController = [UIAlertController
                                                         alertControllerWithTitle:nil
                                                         message:[NSString stringWithFormat:@"Did the printer successfully printed discard document?"]
                                                         preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesaction = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {

                                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

                                    self.endTime=[NSDate date];
                                    [self saveAuditToFile];

                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if(self.fileName==NULL){
                                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }else{

                                            if ([self deleteFile:self.fileName]) {
                                                NSLog(@"file deleted");
                                                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                [self.navigationController popViewControllerAnimated:YES];

                                            }else{
                                                NSLog(@"Could not delete file..%@", self.fileName);
                                                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                [self showErrorAlert:@"Could not delete file, Please try again"];

                                            }
                                        }
                                    });
                                }];

    UIAlertAction *noaction = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [acknowledgementAlertController addAction:yesaction];
    [acknowledgementAlertController addAction:noaction];
    [self presentViewController:acknowledgementAlertController animated:YES completion:nil];

}


-(void)saveAuditToFile{

    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:@"ADJ_NFP_DISCARD_SHIP" forKey:@"documentType"];
    [newManagedObject setValue:@"Finished" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.endTime forKey:@"date"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
    [context save:&error];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    NSString *stringFromDate = [formatter stringFromDate:self.endTime];

    DiscardShipElement * discardElement = [[DiscardShipElement alloc]init];
    discardElement.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
    discardElement.CharityName = [[NSUserDefaults standardUserDefaults] valueForKey:@"NFP_DISCARD_CHARITY_NAME"];

    [self writeStringToFile:[discardElement toJSONString] fileName:stringFromDate];
    NSLog(@"File Saved!!!");
}


- (IBAction)printDiscards:(id)sender {
    [self printPdfFile:self.fileName];
}

- (IBAction)quitAndBack:(id)sender {
    [self deleteFileAndGoBack];
}


-(void)deleteFileAndGoBack{

    //    [self startSpinner:@"Deleting File.."];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    dispatch_async(dispatch_get_main_queue(), ^{

        NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];

        NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];

        [request setPredicate:predicate];
        [request setEntity:entityDesc];

        NSError *error;
        NSArray *results= [context executeFetchRequest:request error:&error];

        for (NSManagedObject * obj in results){
            [context deleteObject:obj];
        }
        [context save:&error];

        if(self.fileName==NULL){
            //            [self dismissViewControllerAnimated:YES completion:^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self.navigationController popViewControllerAnimated:YES];
            //            }];
        }else{
            if ([self deleteFile:self.fileName]) {
                NSLog(@"file deleted");
                //                [self dismissViewControllerAnimated:YES completion:^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                [self.navigationController popViewControllerAnimated:YES];
                //                }];
            }else{
                NSLog(@"Could not delete file..%@", self.fileName);
                //                [self dismissViewControllerAnimated:YES completion:^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                [self showErrorAlert:@"Could not delete file, Please try again"];
                //                }];
            }
        }
    });
}


-(BOOL)deleteFile:(NSString*)fileName{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];

    NSString *filePath = [dataPath stringByAppendingPathComponent:fileName];
    NSError *error;
    return [fileManager removeItemAtPath:filePath error:&error];
}

#pragma mark - iphone file system handler

- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];

    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }

    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

@end

