//
//  CNFTestViewController.h
//  CNF App
//
//  Created by Anik on 2017-09-14.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CNFPrintQRCodeViewController : UIViewController<UIPrintInteractionControllerDelegate, UIWebViewDelegate>
@property (nonatomic, strong) NSString * transferNumber;
@property (nonatomic, strong) NSArray * transferNumbers;
@property (weak, nonatomic) IBOutlet UILabel *transferNoLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webview;


@end
