//
//  CNFTestViewController.m
//  CNF App
//
//  Created by Anik on 2017-09-14.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFPrintQRCodeViewController.h"

@interface CNFPrintQRCodeViewController ()

@end

@implementation CNFPrintQRCodeViewController

@synthesize webview=_webView;
@synthesize transferNumber=_transferNumber, transferNoLabel=_transferNoLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [self.navigationItem setHidesBackButton:YES];

    NSMutableArray<UIImage *> *imageArray = [[NSMutableArray alloc]init];

    if(self.transferNumber){
        CIFilter * filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        [filter setDefaults];
        NSData * data = [self.transferNumber dataUsingEncoding:NSUTF8StringEncoding];
        [filter setValue:data forKey:@"inputMessage"];
        CIImage * generatedImage = [filter valueForKey:@"outputImage"];
        CGAffineTransform transform = CGAffineTransformMakeScale(10.0f, 10.0f);
        CIImage *transformedImage = [generatedImage imageByApplyingTransform: transform];
        UIImage *uiImage = [[UIImage alloc] initWithCIImage:transformedImage];

        [imageArray addObject:[self drawText:self.transferNumber inImage:uiImage]];

    }else if(self.transferNumbers){

        for(NSString * transferNumber in self.transferNumbers){
            CIFilter * filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
            [filter setDefaults];
            NSData * data = [transferNumber dataUsingEncoding:NSUTF8StringEncoding];
            [filter setValue:data forKey:@"inputMessage"];
            CIImage * generatedImage = [filter valueForKey:@"outputImage"];
            CGAffineTransform transform = CGAffineTransformMakeScale(10.0f, 10.0f);
            CIImage *transformedImage = [generatedImage imageByApplyingTransform: transform];
            UIImage *uiImage = [[UIImage alloc] initWithCIImage:transformedImage];
            [imageArray addObject:[self drawText:transferNumber inImage:uiImage]];
        }

    }else{

    }
    self.webview.delegate = self;

    NSMutableData * pdfData = [self createPdf:imageArray];
    [self.webview loadData:pdfData MIMEType:@"application/pdf" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Print" style:UIBarButtonItemStylePlain target:self action:@selector(printImage)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Quit" style:UIBarButtonItemStylePlain target:self action:@selector(exitPrinting)];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(exitPrinting)];
    [self showErrorAlert:@"Error while loading file, please try again"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - file system funciton

- (NSMutableData *)createPdf: (NSArray*)images
{
    NSMutableData * pdfData = [[NSMutableData alloc]init];
    UIGraphicsBeginPDFContextToData(pdfData, CGRectZero, nil);

    for (int index = 0; index <[images count] ; index++)
    {
        UIImage *pngImage=[images objectAtIndex:index];;
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, (pngImage.size.width), (pngImage.size.height)), nil);
        [pngImage drawInRect:CGRectMake(0, 0, (pngImage.size.width), (pngImage.size.height))];
    }
    UIGraphicsEndPDFContext();
    return pdfData;
}

-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width, image.size.height+20));
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(0, image.size.height, image.size.width, image.size.height+20);
    [[UIColor whiteColor] set];

    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.lineBreakMode = NSLineBreakByWordWrapping;
    textStyle.alignment = NSTextAlignmentCenter;
    UIFont *textFont = [UIFont systemFontOfSize:16];
    [text drawInRect:CGRectIntegral(rect) withAttributes:@{NSFontAttributeName:textFont, NSParagraphStyleAttributeName:textStyle}];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

#pragma mark - button funtion

-(void)printImage{

    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];

    pic.delegate = self;

    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"nothing";
    printInfo.duplex = UIPrintInfoDuplexLongEdge;
    pic.printInfo = printInfo;
    pic.printFormatter = self.webview.viewPrintFormatter;

    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        if(!completed && error){
            NSLog(@"Print Error: %@", error);
            [self showErrorAlert:@"Error occured while printing, Please try again"];
        }else if(completed){

        }else{

        }
    };
    [pic presentAnimated:YES completionHandler:completionHandler];
}

-(void)exitPrinting{
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSUserDefaults resetStandardUserDefaults];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}


#pragma mark - alert funcitons

-(void)showInfoAlert:(NSString *)title{
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:[NSString stringWithFormat:@"%@", title]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

@end
