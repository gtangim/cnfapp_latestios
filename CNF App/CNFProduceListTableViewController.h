//
//  CNFProduceListTableViewController.h
//  CNF App
//
//  Created by Anik on 2018-06-04.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DataPassDelegate<NSObject>
-(void)returnSelectedProduceList:(NSArray *)produceList;
@end

@interface CNFProduceListTableViewController : UITableViewController<UISearchResultsUpdating, UISearchBarDelegate>
@property(weak,nonatomic)id<DataPassDelegate> delegate;
- (IBAction)addProduct:(id)sender;

@end
