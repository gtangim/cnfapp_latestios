//
//  CNFProduceListTableViewController.m
//  CNF App
//
//  Created by Anik on 2018-06-04.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import "CNFProduceListTableViewController.h"

@interface CNFProduceListTableViewController ()
@property (nonatomic, strong) NSMutableArray * orderedProduceList;
@property (nonatomic, strong) NSMutableArray * allProduceList;
@property (nonatomic, strong) NSMutableArray * displayList;
@property (strong, nonatomic) UISearchController *searchController;
@end

@implementation CNFProduceListTableViewController{
    NSMutableArray *searchResults;
}

@synthesize orderedProduceList=_orderedProduceList,allProduceList=_allProduceList, displayList=_displayList;
@synthesize delegate;


-(NSMutableArray *)displayList{
    if(!_displayList)_displayList = [[NSMutableArray alloc]init];
    return _displayList;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = @"Produce Products";
    if([self populateProduceArrays]){
        if([self loadSearchController]){
            self.displayList = self.orderedProduceList;
            [self.tableView reloadData];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [self.searchController.view removeFromSuperview];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.displayList){
        if([self.searchController isActive]){
            if (searchResults.count>0) {
                return [searchResults count];
            }
            return 0;
        }else{
            return self.displayList.count;
        }
    }
    else return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"produceProductCells" forIndexPath:indexPath];

    NSMutableString * titeText=[[NSMutableString alloc]init];
    NSMutableString * detailedText= [[NSMutableString alloc]init];
    NSNumber * selected;

    if (searchResults.count>0) {
        [titeText appendString:[NSString stringWithFormat:@"%@ %.2f%@", [[searchResults objectAtIndex:indexPath.row] valueForKey:@"ProductName"], [[[searchResults objectAtIndex:indexPath.row] valueForKey:@"Size"]doubleValue], [[searchResults objectAtIndex:indexPath.row] valueForKey:@"UomQty"]]];
        [detailedText appendString:[NSString stringWithFormat:@"Product UPC: %@", [[searchResults objectAtIndex:indexPath.row] valueForKey:@"Upc"]]];
        selected = [NSNumber numberWithBool:[[[searchResults objectAtIndex:indexPath.row]valueForKey:@"Selected"]boolValue]];
    }else{
        [titeText appendString:[NSString stringWithFormat:@"%@ %.2f%@", [[self.displayList objectAtIndex:indexPath.row] valueForKey:@"ProductName"], [[[self.displayList objectAtIndex:indexPath.row] valueForKey:@"Size"]doubleValue], [[self.displayList objectAtIndex:indexPath.row] valueForKey:@"UomQty"]]];
        [detailedText appendString:[NSString stringWithFormat:@"Product UPC: %@", [[self.displayList objectAtIndex:indexPath.row] valueForKey:@"Upc"]]];
        selected = [NSNumber numberWithBool:[[[self.displayList objectAtIndex:indexPath.row]valueForKey:@"Selected"]boolValue]];
    }

    cell.textLabel.text = titeText;
    cell.detailTextLabel.text = detailedText;
    cell.accessoryType = [selected boolValue]? UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;

    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try{

        if(searchResults.count>0){
            NSMutableDictionary * selectedProduct = [[searchResults objectAtIndex:indexPath.row] mutableCopy];
            int orderedIndex;
            if([self.orderedProduceList containsObject:selectedProduct])
                orderedIndex = (int)[self.orderedProduceList indexOfObject:selectedProduct];
            int allIndex = (int)[self.allProduceList indexOfObject:selectedProduct];
            if([selectedProduct valueForKey:@"Selected"])
                [selectedProduct setObject:[NSNumber numberWithBool:FALSE] forKey:@"Selected"];
            else [selectedProduct setObject:[NSNumber numberWithBool:TRUE] forKey:@"Selected"];

            [searchResults replaceObjectAtIndex:indexPath.row withObject:selectedProduct];
            [self.allProduceList replaceObjectAtIndex:allIndex withObject:selectedProduct];
            if([self.orderedProduceList containsObject:selectedProduct])
                [self.orderedProduceList replaceObjectAtIndex:orderedIndex withObject:selectedProduct];
            [self.tableView reloadData];
        }else{
            NSMutableDictionary * selectedProduct = [[self.displayList objectAtIndex:indexPath.row] mutableCopy];
            int orderedIndex;
            if([self.orderedProduceList containsObject:selectedProduct])
                orderedIndex = (int)[self.orderedProduceList indexOfObject:selectedProduct];
            int allIndex = (int)[self.allProduceList indexOfObject:selectedProduct];
            if([[selectedProduct valueForKey:@"Selected"] boolValue])
                [selectedProduct setObject:[NSNumber numberWithBool:FALSE] forKey:@"Selected"];
            else [selectedProduct setObject:[NSNumber numberWithBool:TRUE] forKey:@"Selected"];

            [self.displayList replaceObjectAtIndex:indexPath.row withObject:selectedProduct];
            [self.allProduceList replaceObjectAtIndex:allIndex withObject:selectedProduct];
            if([self.orderedProduceList containsObject:selectedProduct])
                [self.orderedProduceList replaceObjectAtIndex:orderedIndex withObject:selectedProduct];

            [self.tableView reloadData];
        }
    }@catch(NSException *ex){
        NSLog(@"Problem indexing, %@", ex.description);
    }
}


#pragma mark - model funcitons

-(BOOL)populateProduceArrays{
    @try{
        NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"ProduceProductsList"];
        NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];

        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];

        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
        databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];

        [file closeFile];

        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

        NSError * serializerError;
        self.allProduceList = [[NSMutableArray alloc] initWithArray:[NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError]];

        if(serializerError){
            return FALSE;
        }else{
            NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"ProduceProductRecentList"];
            NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];

            NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
            NSFileHandle *file;
            NSData *databuffer;
            file = [NSFileHandle fileHandleForReadingAtPath:filepath];

            [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
            databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];

            [file closeFile];

            NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

            NSArray * orderedProducts = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];

            if(serializerError) return FALSE;
            else{
                self.orderedProduceList = [[NSMutableArray alloc]init];
                if(orderedProducts.count < 1) return TRUE;
                else{
                    for(NSDictionary* product in orderedProducts){
                        NSPredicate * findProduct = [NSPredicate predicateWithFormat:@"ProductId == %@", [product valueForKey:@"ProductId"]];
                        if(!([self.allProduceList filteredArrayUsingPredicate:findProduct].count==0))
                            [self.orderedProduceList addObject: [self.allProduceList filteredArrayUsingPredicate:findProduct].firstObject];
                    }

                    //change self.ordered products filtering based on vendor
                    if([[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"]){
                        NSPredicate * vendorPred = [NSPredicate predicateWithFormat:@"VendorId == %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"]valueForKey:@"VendorId"]];
//                        if(!([self.orderedProduceList filteredArrayUsingPredicate:vendorPred].count==0))
                            [self.orderedProduceList filterUsingPredicate:vendorPred];
                    }

                    return TRUE;
                }

            }
        }
    }@catch(NSException * ex){
        NSLog(@"Error occured at: %@", ex.description);
        return FALSE;
    }
}

-(BOOL)loadSearchController{
    @try{
        self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        self.searchController.searchResultsUpdater = self;
        self.searchController.dimsBackgroundDuringPresentation = NO;
        self.searchController.searchBar.delegate = self;

        self.searchController.searchBar.showsScopeBar = YES;
        self.searchController.searchBar.scopeButtonTitles = [NSArray arrayWithObjects:@"Ordered Produces",@"All Produce", nil];

        self.tableView.tableHeaderView = self.searchController.searchBar;
        self.definesPresentationContext = YES;

        [self.searchController.searchBar sizeToFit];
        [self.searchController.searchBar setValue:@"Done" forKey:@"cancelButtonText"];
        return TRUE;
    }@catch(NSException * ex){
        NSLog(@"Error occured: %@", ex.description);
        return FALSE;
    }
}

#pragma mark - button funciton

- (IBAction)addProduct:(id)sender {
    NSPredicate * selectedProducts = [NSPredicate predicateWithFormat:@"Selected == YES"];
    NSArray * selectedArray = [self.allProduceList filteredArrayUsingPredicate:selectedProducts];
    [delegate returnSelectedProduceList:selectedArray];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - search delegate


- (void)updateSearchResultsForSearchController:(nonnull UISearchController *)searchController {
    NSString *searchString = self.searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    searchBar.showsScopeBar = YES;
    [searchBar sizeToFit];

//    if([_searchController isActive] && searchResults.count>0)
//        self.displayList = searchResults;
//    else{
        if(selectedScope==1){
            self.displayList = self.allProduceList;
        }else{
            self.displayList = self.orderedProduceList;
        }
//    }
    [self.tableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar sizeToFit];
    searchBar.showsScopeBar = YES;
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}


-(void)searchForText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ProductName CONTAINS[cd] %@ OR ProductId CONTAINS[cd] %@ OR Upc CONTAINS[cd] %@", searchText, searchText, searchText];
    searchResults = [[NSMutableArray alloc]initWithArray:[self.displayList filteredArrayUsingPredicate:resultPredicate]];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchController.searchBar.text=@"";
    searchBar.showsScopeBar = YES;
    [self.searchController.searchBar resignFirstResponder];
    searchResults=nil;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView reloadData];
}


@end
