//
//  CNFProducePurchaseProductListTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-10-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFProducePurchaseProductListTableViewController : UITableViewController<UISearchResultsUpdating, UISearchBarDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
