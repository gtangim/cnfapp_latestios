//
//  CNFProducePurchaseProductListTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-10-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFProducePurchaseProductListTableViewController.h"
#import "CNFAppDelegate.h"
#import "CNFGetDocumentId.h"

#import "ProduceItemElement.h"
#import "ProduceTracking.h"

@interface CNFProducePurchaseProductListTableViewController ()
@property (nonatomic, strong) NSMutableArray * produceProductList;
//@property (nonatomic, strong) NSMutableArray * displayArray;
@property (strong, nonatomic) UISearchController *searchController;
@property (nonatomic, strong ) NSManagedObjectContext * context;
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSDate * endTime;
@property (nonatomic, strong) NSString * documentId;
@end

@implementation CNFProducePurchaseProductListTableViewController{
    NSMutableArray *searchResults;
    NSManagedObjectContext * context;
}

@synthesize produceProductList=_produceProductList, searchController=_searchController, context=_context;
@synthesize documentId=_documentId;


- (void)viewDidLoad {
    [super viewDidLoad];

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Quit" style:UIBarButtonItemStylePlain target:self action:@selector(quitAudit)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submitAudit)];

    [self.navigationItem.rightBarButtonItem setEnabled:NO];

    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;

    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;

    [self.searchController.searchBar sizeToFit];
    [self.searchController.searchBar setValue:@"Done" forKey:@"cancelButtonText"];

    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"ProduceProductsList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];

    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];

    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];

    [file closeFile];

    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

    NSError * serializerError;
    NSArray * products = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];

    if(serializerError){
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self showErrorAlert:@"Could not get data from data file, try again later"];
    }else{
        if([self saveInitialEntryToDB]){
            NSSortDescriptor *valueDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"ProductName" ascending:YES];
            self.produceProductList=[[NSMutableArray alloc]initWithArray:[[products sortedArrayUsingDescriptors:@[valueDescriptor]]mutableCopy]];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }else{
            [self showErrorAlert:@"Error saving to DB, Please try again"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DB function

-(BOOL)saveInitialEntryToDB{
    self.startTime = [NSDate date];
    self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];

    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:@"PRODUCE_TRACKING" forKey:@"documentType"];
    [newManagedObject setValue:@"Created" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.startTime forKey:@"date"];
    [context save:&error];

    if(error) return FALSE;
    else return TRUE;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if(self.produceProductList){
        if([self.searchController isActive]){
            if (searchResults.count>0) {
                return [searchResults count];
            }
            return 0;
        }else{
            return self.produceProductList.count;
        }
    }
    else return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"producePurchaseProductCells" forIndexPath:indexPath];

    if (searchResults.count>0) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %.2f%@", [[searchResults objectAtIndex:indexPath.row] valueForKey:@"ProductName"], [[[searchResults objectAtIndex:indexPath.row] valueForKey:@"Size"]doubleValue], [[searchResults objectAtIndex:indexPath.row] valueForKey:@"UomQty"]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Product UPC: %@", [[searchResults objectAtIndex:indexPath.row] valueForKey:@"Upc"]];
        if([[[searchResults objectAtIndex:indexPath.row] valueForKey:@"Selected"] boolValue]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.backgroundColor = [UIColor lightGrayColor];
        }else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
        }
    }else{
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %.2f%@", [[self.produceProductList objectAtIndex:indexPath.row] valueForKey:@"ProductName"], [[[self.produceProductList objectAtIndex:indexPath.row] valueForKey:@"Size"]doubleValue], [[self.produceProductList objectAtIndex:indexPath.row] valueForKey:@"UomQty"]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Product UPC: %@", [[self.produceProductList objectAtIndex:indexPath.row] valueForKey:@"Upc"]];
        if([[[self.produceProductList objectAtIndex:indexPath.row] valueForKey:@"Selected"] boolValue]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.backgroundColor = [UIColor lightGrayColor];
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
        }
    }

    cell.textLabel.numberOfLines =0;

    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (searchResults.count>0) {
        if([[[searchResults objectAtIndex:indexPath.row] valueForKey:@"Selected"]boolValue]){
            NSMutableDictionary * product = [[searchResults objectAtIndex:indexPath.row] mutableCopy];
            NSInteger index = [self.produceProductList indexOfObject:product];
            [product setValue:[NSNumber numberWithBool:NO] forKey:@"Selected"];
            [searchResults replaceObjectAtIndex:indexPath.row withObject:product];
            [self.produceProductList replaceObjectAtIndex:index withObject:product];
        }else{
            NSMutableDictionary * product = [[searchResults objectAtIndex:indexPath.row] mutableCopy];
            NSInteger index = [self.produceProductList indexOfObject:product];
            [product setValue:[NSNumber numberWithBool:YES] forKey:@"Selected"];
            [searchResults replaceObjectAtIndex:indexPath.row withObject:product];
            [self.produceProductList replaceObjectAtIndex:index withObject:product];
        }
    }else{
        if([[[self.produceProductList objectAtIndex:indexPath.row] valueForKey:@"Selected"]boolValue]){
            [[self.produceProductList objectAtIndex:indexPath.row] setValue:[NSNumber numberWithBool:NO] forKey:@"Selected"];
        }else{
            NSMutableDictionary * product = [[self.produceProductList objectAtIndex:indexPath.row] mutableCopy];
            [product setValue:[NSNumber numberWithBool:YES] forKey:@"Selected"];
            [self.produceProductList replaceObjectAtIndex:indexPath.row withObject:product];
        }
    }

    NSPredicate * checkedPred = [NSPredicate predicateWithFormat:@"Selected == YES"];
    if([self.produceProductList filteredArrayUsingPredicate:checkedPred].count==0){
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }else{
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }

    [self.tableView reloadData];
}


#pragma mark - alert controllers funciton

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

#pragma mark - button funciton
-(void)quitAudit{
    UIAlertController *quitAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:@"Sure to quit Creating list?"
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesaction = [UIAlertAction
                               actionWithTitle:@"YES"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                       NSFetchRequest *request = [[NSFetchRequest alloc]init];

                                       NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];

                                       [request setPredicate:predicate];
                                       [request setEntity:entityDesc];

                                       NSError *error;
                                       NSArray *results= [context executeFetchRequest:request error:&error];

                                       for (NSManagedObject * obj in results){
                                           [context deleteObject:obj];
                                       }
                                       [context save:&error];

                                       [NSUserDefaults resetStandardUserDefaults];
                                       [[NSUserDefaults standardUserDefaults]synchronize];

                                       self.produceProductList=Nil;
                                       [self.navigationController popViewControllerAnimated:YES];
                                   });
                               }];

    UIAlertAction *noaction = [UIAlertAction
                                actionWithTitle:@"NO"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }];

    [quitAlertController addAction:yesaction];
    [quitAlertController addAction:noaction];
    [self presentViewController:quitAlertController animated:YES completion:nil];
}

-(void)submitAudit{
    NSPredicate * checkedPred = [NSPredicate predicateWithFormat:@"Selected == YES"];

    UIAlertController *quitAlertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:[NSString stringWithFormat:@"Total %lu products selected, Finish List", (unsigned long)[self.produceProductList filteredArrayUsingPredicate:checkedPred].count]
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesaction = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                                            self.endTime = [NSDate date];
                                            if([self saveAuditToFile]){
                                                [NSUserDefaults resetStandardUserDefaults];
                                                [[NSUserDefaults standardUserDefaults]synchronize];
                                                self.produceProductList=Nil;
                                                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                [self.navigationController popViewControllerAnimated:YES];
                                            }else{
                                                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                [self showErrorAlert:@"Could not save file, Please try again"];
                                            }
                                        });
                                }];

    UIAlertAction *noaction = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [quitAlertController addAction:yesaction];
    [quitAlertController addAction:noaction];
    [self presentViewController:quitAlertController animated:YES completion:nil];
}



#pragma mark - Save file function
-(BOOL)saveAuditToFile{
    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:@"PRODUCE_TRACKING" forKey:@"documentType"];
    [newManagedObject setValue:@"Finished" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.endTime forKey:@"date"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
    [context save:&error];

    if(error) return FALSE;
    else{
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *stringFromDate = [formatter stringFromDate:self.endTime];

        NSMutableArray * element = [[NSMutableArray alloc]init];
        NSPredicate * checkedPred = [NSPredicate predicateWithFormat:@"Selected == YES"];
        for( NSObject * product in [self.produceProductList filteredArrayUsingPredicate:checkedPred]){
            [element addObject:[product valueForKey:@"ProductId"]];
        }
        ProduceItemElement * itemElement = [[ProduceItemElement alloc]init];
        itemElement.ProduceProductsList = element;

        ProduceTracking * tracker = [[ProduceTracking alloc]init];
        tracker.UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
        tracker.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        tracker.ProduceItems = itemElement;

        if([self writeStringToFile:[tracker toJSONString] fileName:stringFromDate]) return TRUE;
        else return FALSE;
    }
}

#pragma mark - iphone file system handler

- (BOOL)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];

    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }

    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];

    if(error) return FALSE;
    else return TRUE;
}


#pragma mark - search filter functions

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

-(void)searchForText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ProductName CONTAINS[cd] %@ OR ProductId CONTAINS[cd] %@ OR Upc CONTAINS[cd] %@", searchText, searchText, searchText];
    searchResults = [[NSMutableArray alloc]initWithArray:[self.produceProductList filteredArrayUsingPredicate:resultPredicate]];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchController.searchBar.text=@"";
    [self.searchController.searchBar resignFirstResponder];

    searchResults=NULL;

    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    [self.tableView reloadData];
}


@end
