//
//  CNFProduceReceiveListTableViewController.h
//  CNF App
//
//  Created by Anik on 2018-06-04.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFProduceListTableViewController.h"

@interface CNFProduceReceiveListTableViewController : UITableViewController<DataPassDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

- (IBAction)addProduce:(id)sender;

@end
