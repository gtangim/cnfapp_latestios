//
//  CNFProduceReceiveListTableViewController.m
//  CNF App
//
//  Created by Anik on 2018-06-04.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import "CNFProduceReceiveListTableViewController.h"
#import "CNFAppDelegate.h"
#import "CNFGetDocumentId.h"
#import "ProduceReceivingModel.h"


@interface CNFProduceReceiveListTableViewController ()
@property (nonatomic, strong) NSMutableArray * receivedProduceList;

//submission properties
@property (nonatomic, strong ) NSManagedObjectContext * context;
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSDate * endTime;
@property (nonatomic, strong) NSString * documentId;
@end

@implementation CNFProduceReceiveListTableViewController{
    NSManagedObjectContext * context;
}
@synthesize receivedProduceList=_receivedProduceList;
@synthesize documentId=_documentId, context=_context;

-(NSMutableArray *)receivedProduceList{
    if(!_receivedProduceList)_receivedProduceList = [[NSMutableArray alloc]init];
    return _receivedProduceList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Produce Receive";
    self.navigationItem.hidesBackButton = YES;

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Finish" style:UIBarButtonItemStylePlain target:self action:@selector(finishReceiving)];

    if(![self registerDatabase])
       [self showErrorAlert:@"Error occured, Please try again.."];
    else [self showErrorAlert:@"Please select produce products by clicking the button on the top right"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)registerDatabase{
    @try{
        CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
        context =[appDelegate managedObjectContext];

        self.startTime = [NSDate date];
        self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];

        NSError *error;
        NSManagedObject *newManagedObject = [NSEntityDescription
                                             insertNewObjectForEntityForName:@"DocStatusInfo"
                                             inManagedObjectContext:context];

        [newManagedObject setValue:@"PRODUCE_RECEIVING" forKey:@"documentType"];
        [newManagedObject setValue:@"Created" forKey:@"status"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [newManagedObject setValue:self.documentId forKey:@"documentId"];
        [newManagedObject setValue:self.startTime forKey:@"date"];
        [context save:&error];

        if(error) return FALSE;
        else return TRUE;

    }@catch(NSException * ex){
        NSLog(@"Error occured: %@", ex.description);
        return FALSE;
    }
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.receivedProduceList?self.receivedProduceList.count:0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"produceReceiceCells" forIndexPath:indexPath];
    if(self.receivedProduceList){
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %.2f%@", [[self.receivedProduceList objectAtIndex:indexPath.row] valueForKey:@"ProductName"], [[[self.receivedProduceList objectAtIndex:indexPath.row] valueForKey:@"Size"]doubleValue], [[self.receivedProduceList objectAtIndex:indexPath.row] valueForKey:@"UomQty"]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Product UPC: %@", [[self.receivedProduceList objectAtIndex:indexPath.row] valueForKey:@"Upc"]];
    }
    return cell;
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self.receivedProduceList removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    return @[deleteAction];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.receivedProduceList removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{

    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"produceListSegue"]) {
        CNFProduceListTableViewController * produceListController = segue.destinationViewController;
        produceListController.delegate = self;
    }
}


#pragma mark - delegate function

-(void)returnSelectedProduceList:(NSArray *)produceList{
    for(NSObject * produce in produceList){
        if(![self.receivedProduceList containsObject:produce])
           [self.receivedProduceList insertObject:produce atIndex:0];
    }
    [self.tableView reloadData];
}


#pragma mark - alert funcitons

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)submitReceipt{
    UIAlertController *quitAlertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:[NSString stringWithFormat:@"Total %lu products selected, Receive List?", (unsigned long)self.receivedProduceList.count]
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesaction = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                                        self.endTime = [NSDate date];
                                        if([self saveAuditToFile]){
                                            [NSUserDefaults resetStandardUserDefaults];
                                            [[NSUserDefaults standardUserDefaults]synchronize];
                                            self.receivedProduceList=Nil;
                                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                            [self.navigationController popToRootViewControllerAnimated:YES];
                                        }else{
                                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                            [self showErrorAlert:@"Could not save file, Please try again"];
                                        }
                                    });
                                }];

    UIAlertAction *noaction = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [quitAlertController addAction:yesaction];
    [quitAlertController addAction:noaction];
    [self presentViewController:quitAlertController animated:YES completion:nil];
}


-(void)quitReceipt{
    UIAlertController *quitAlertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:@"Sure to quit Receiving?"
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesaction = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction *action)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                        NSFetchRequest *request = [[NSFetchRequest alloc]init];

                                        NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];

                                        [request setPredicate:predicate];
                                        [request setEntity:entityDesc];

                                        NSError *error;
                                        NSArray *results= [context executeFetchRequest:request error:&error];

                                        for (NSManagedObject * obj in results){
                                            [context deleteObject:obj];
                                        }
                                        [context save:&error];

                                        [NSUserDefaults resetStandardUserDefaults];
                                        [[NSUserDefaults standardUserDefaults]synchronize];

                                        self.receivedProduceList=Nil;
                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                    });
                                }];

    UIAlertAction *noaction = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [quitAlertController addAction:yesaction];
    [quitAlertController addAction:noaction];
    [self presentViewController:quitAlertController animated:YES completion:nil];
}

#pragma mark - Save file function

-(BOOL)saveAuditToFile{
    @try{
        NSError *error;
        NSManagedObject *newManagedObject = [NSEntityDescription
                                             insertNewObjectForEntityForName:@"DocStatusInfo"
                                             inManagedObjectContext:context];

        [newManagedObject setValue:@"PRODUCE_RECEIVING" forKey:@"documentType"];
        [newManagedObject setValue:@"Finished" forKey:@"status"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [newManagedObject setValue:self.documentId forKey:@"documentId"];
        [newManagedObject setValue:self.endTime forKey:@"date"];
        [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
        [context save:&error];

        if(error) return FALSE;
        else{
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *stringFromDate = [formatter stringFromDate:self.endTime];

            ProduceReceivingModel * produceModel = [[ProduceReceivingModel alloc]init];
            produceModel.UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
            produceModel.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];

            NSMutableArray * idArray = [[NSMutableArray alloc] initWithCapacity:[self.receivedProduceList count]];

            for (NSDictionary *dict in self.receivedProduceList) {
                [idArray addObject:[dict valueForKey:@"ProductId"]];
            }
            produceModel.ProductIdList = idArray;

            if([self writeStringToFile:[produceModel toJSONString] fileName:stringFromDate]) return TRUE;
            else return FALSE;
        }
    }@catch(NSException * ex){
        NSLog(@"error occured: %@", ex.description);
        return FALSE;
    }
}

#pragma mark - iphone file system handler

- (BOOL)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];

    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }

    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];

    if(error) return FALSE;
    else return TRUE;
}


#pragma mark - button function

- (IBAction)addProduce:(id)sender {
    [self performSegueWithIdentifier:@"produceListSegue" sender:self];
}


-(void)finishReceiving{
    UIAlertController *finishAlertController = [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:nil
                                                preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *submitAction = [UIAlertAction
                                   actionWithTitle:@"Submit Receipt"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       if(self.receivedProduceList.count==0) [self showErrorAlert:@"There is no product to receive"];
                                       else [self submitReceipt];
                                   }];

    UIAlertAction *quitAction = [UIAlertAction
                                 actionWithTitle:@"Quit Receipt"
                                 style:UIAlertActionStyleDestructive
                                 handler:^(UIAlertAction *action)
                                 {
                                     [self dismissViewControllerAnimated:YES completion:nil];
                                     [self quitReceipt];
                                 }];

    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];

    [finishAlertController addAction:submitAction];
    [finishAlertController addAction:quitAction];
    [finishAlertController addAction:cancelAction];
    [self presentViewController:finishAlertController animated:YES completion:nil];
}
@end
