//
//  CNFManualReceiveTableViewController.h
//  CNF App Test
//
//  Created by Anik on 2017-08-11.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFFTPScanProduceTableViewController.h"

@interface CNFProduceSummaryTableViewController : UITableViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, BoolPassDelegate>

@property (weak, nonatomic) IBOutlet UITextField *invoiceNumberText;
@property (weak, nonatomic) IBOutlet UITextField *vendorNameText;
@property (weak, nonatomic) IBOutlet UITextField *grandTotalText;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)doneClicked:(id)sender;
- (IBAction)quitClicked:(id)sender;


@end
