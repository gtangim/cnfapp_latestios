//
//  CNFManualReceiveTableViewController.m
//  CNF App Test
//
//  Created by Anik on 2017-08-11.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFProduceSummaryTableViewController.h"
#import "ImageCompression.h"
#import "DLSFTPConnection.h"
#import "DLSFTPUploadRequest.h"
#import "DLSFTPFile.h"


@interface CNFProduceSummaryTableViewController ()
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic) int imagecounter;
@property (nonatomic) BOOL scannerImageAvailable;
@property (strong, nonatomic) DLSFTPConnection *connection;
@end

typedef void (^Completion)(BOOL success, NSString *msg);

@implementation CNFProduceSummaryTableViewController{
    UIAlertController * statusAlert;
}

@synthesize invoiceNumberText=_invoiceNumberText, vendorNameText=_vendorNameText, grandTotalText=_grandTotalText, scrollView=_scrollView, startTime=_startTime, imagecounter=_imagecounter, connection=_connection;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.rightBarButtonItem.enabled=FALSE;
    self.startTime = [NSDate date];
    self.scannerImageAvailable = FALSE;
    //set all delegates
    self.invoiceNumberText.delegate=self.vendorNameText.delegate=self.grandTotalText.delegate=self;
    //remove all files from the directory
    [self removeFilesFormManualDirectory];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    [self checkDoneButton];
    if(self.scannerImageAvailable){
        if(![self removeScrollViewContent]) [self showErrorAlert:@"Error removing content of manual_folder, try again"];
    }
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"]){
        self.vendorNameText.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorName"];
    }
}

#pragma mark - Text field delegate functions

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self checkDoneButton];
    return YES;
}


-(void)checkDoneButton{
    if([self.invoiceNumberText hasText] && [self.vendorNameText hasText] && [self validateGrandTotal] && ((self.scrollView.subviews.count>2) || self.scannerImageAvailable)){
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    }else{
        self.navigationItem.rightBarButtonItem.enabled = FALSE;
    }
}

-(BOOL)validateGrandTotal{
    if([self.grandTotalText hasText]){
        if([self checkforNumeric:self.grandTotalText.text])
            return TRUE;
        else
            return FALSE;
    }else
        return FALSE;
}

-(BOOL) checkforNumeric:(NSString*) str
{
    NSString *strMatchstring=@"\\b([0-9%_.+\\-]+)\\b";
    NSPredicate *textpredicate=[NSPredicate predicateWithFormat:@"SELF MATCHES %@", strMatchstring];
    
    if(![textpredicate evaluateWithObject:str])
    {
        return FALSE;
    }
    return TRUE;
}


#pragma mark - Table view data source

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if(section==1){
        UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, 20.0)] ;

        //add photo button
        UIButton *photoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [photoButton setTitle:@"Photo" forState: UIControlStateNormal];
        [photoButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-100, 5.0, 100.0, 30.0)];
//        photoButton.tag = photo;
        photoButton.hidden = NO;
        [photoButton setBackgroundColor:[UIColor clearColor]];
        [photoButton addTarget:self action:@selector(cameraClicked) forControlEvents:UIControlEventTouchDown];

        //add scan button
        UIButton *scanButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [scanButton setTitle:@"Scan" forState: UIControlStateNormal];
        [scanButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-200, 5.0, 100.0, 30.0)];
//        photoButton.tag = scan;
        scanButton.hidden = NO;
        [scanButton setBackgroundColor:[UIColor clearColor]];
        [scanButton addTarget:self action:@selector(scanClicked) forControlEvents:UIControlEventTouchDown];

        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5.0, 100, 30)];
        title.font = [UIFont systemFontOfSize:13];
        title.text = @"PHOTOS";
        title.textColor = [UIColor grayColor];
        
        [myView addSubview:photoButton];
        [myView addSubview:scanButton];
        [myView addSubview:title];
        
        return myView;
    }
    return NULL;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1 && indexPath.row==0) {
        CGFloat aspectRatio = 350.0f/620.0f;
        return aspectRatio * [UIScreen mainScreen].bounds.size.height;
    }else return 44;
}

#pragma mark - segue callback funciton

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ftpScannerProduceSegue"]) {
        CNFFTPScanProduceTableViewController * produceLScanController = segue.destinationViewController;
        produceLScanController.delegate = self;
    }
}

-(void)returnPhotoSelected:(BOOL)selected{
    if(selected) self.scannerImageAvailable = TRUE;
    else self.scannerImageAvailable = FALSE;
}


#pragma mark - image picker functions

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info{

    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *stringFromDate = [formatter stringFromDate:self.startTime];
    
    NSString * fileName = [NSString stringWithFormat:@"%@-%d.jpg", stringFromDate, self.imagecounter];
    
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    [UIImagePNGRepresentation([[ImageCompression alloc]compressImage:image]) writeToFile:fileAtPath atomically:YES];

    self.imagecounter ++;
    [self showScrollViewOfSavedPhotos];
}


#pragma mark - scrollview population method

-(BOOL)removeScrollViewContent{
    @try{
        for (UIView *subview in self.scrollView.subviews) {
            [subview removeFromSuperview];
        }
        return TRUE;
    }@catch(NSException * ex){
        NSLog(@"Subview remove error: %@", ex.description);
        return FALSE;
    }

}

-(void)showScrollViewOfSavedPhotos{
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    
    self.scrollView.pagingEnabled = YES;
    [self.scrollView setAlwaysBounceVertical:NO];
    //setup internal views
    NSInteger numberOfViews = directoryContent.count;
    for (int i = 0; i < numberOfViews; i++) {
        CGFloat xOrigin = i * self.view.frame.size.width;
        UIImageView *image = [[UIImageView alloc] initWithFrame:
                              CGRectMake(xOrigin, 0,
                                         self.scrollView.frame.size.width,
                                         self.scrollView.frame.size.height)];
        image.image = [UIImage imageWithContentsOfFile:[NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/manual_images/%@", [directoryContent objectAtIndex:i]]]];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [self.scrollView addSubview:image];
    }
    //set the scroll view content size
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width *
                                             numberOfViews,
                                             self.scrollView.frame.size.height);
    
    //check if the done button can be activated
    [self checkDoneButton];
}

- (void)createPdfFile{

    NSString *directory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
    NSError * error;
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    
    NSString *pdfFileName = [directory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@_%@_%@.pdf",[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"],self.invoiceNumberText.text, [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]?[[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]: self.vendorNameText.text, self.grandTotalText.text]];
    
    UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
    
    if(error){
        NSLog(@"pdf creation error..");
        @throw [NSException exceptionWithName:@"PDF Creation Error" reason:error.description userInfo:NULL];
//        return FALSE;
    }else{
        for (int index = 0; index <directoryContent.count ; index++) {
            UIImage *pngImage=[UIImage imageWithContentsOfFile:[directory stringByAppendingPathComponent:[directoryContent objectAtIndex:index]]];
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, (pngImage.size.width), (pngImage.size.height)), nil);
            [pngImage drawInRect:CGRectMake(0, 0, (pngImage.size.width), (pngImage.size.height))];
        }
        UIGraphicsEndPDFContext();
//        return TRUE;
    }
}

-(void)renamePdfFile{

    NSString *directory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
    NSError * error;
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];

    NSString *pdfFileName = [NSString stringWithFormat:@"%@_%@_%@_%@.pdf",[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"],self.invoiceNumberText.text, [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]?[[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]: self.vendorNameText.text, self.grandTotalText.text];

    NSString *oldPath = [NSString stringWithFormat:@"%@/%@", directory, directoryContent.firstObject];
    NSString *newPath = [NSString stringWithFormat:@"%@/%@", directory, pdfFileName];

    NSFileManager *fileMan = [NSFileManager defaultManager];
    if (![fileMan moveItemAtPath:oldPath toPath:newPath error:&error])
    {
        NSLog(@"Failed to move '%@' to '%@': %@", oldPath, newPath, [error localizedDescription]);
        @throw [NSException exceptionWithName:@"PDF Naming Error" reason:error.description userInfo:NULL];
    }

}

#pragma mark - directory funcitons

-(BOOL)removeFilesFormManualDirectory{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
    NSError *error = nil;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:directory]){
        return TRUE;
    }else{
        for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
            BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, file] error:&error];
            if (!success || error)
                return FALSE;
        }
    }
    return TRUE;
}

#pragma mark - button functions

-(void)scanClicked{
    NSLog(@"%lu", (unsigned long)self.scrollView.subviews.count);

    //check for already taken photos
    if(self.scrollView.subviews.count>2){

        //display confirmation alert
        UIAlertController * photoController = [UIAlertController alertControllerWithTitle:nil message:@"Already existing photos, delete and proceed to scan?" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            if([self removeFilesFormManualDirectory])
               [self performSegueWithIdentifier:@"ftpScannerProduceSegue" sender:self];
        }];

        UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {

        }];

        [photoController addAction:yesAction];
        [photoController addAction:noAction];

        [self presentViewController:photoController animated:YES completion:nil];

    }else{
        [self performSegueWithIdentifier:@"ftpScannerProduceSegue" sender:self];
    }
}

- (void)cameraClicked{

    if(!self.scannerImageAvailable){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        [imagePickerController setDelegate:self];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }else{
        UIAlertController * imageController = [UIAlertController alertControllerWithTitle:nil message:@"Already selected image from scan, want to take photos?" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            if([self removeFilesFormManualDirectory]){
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
                }
                [imagePickerController setDelegate:self];
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }else{
                [self showErrorAlert:@"Could not clear directory, try again.."];
            }
        }];

        UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {

        }];

        [imageController addAction:yesAction];
        [imageController addAction:noAction];

        [self presentViewController:imageController animated:YES completion:nil];
    }


}

- (IBAction)doneClicked:(id)sender {
    [self showDoneAlert];
}


- (IBAction)quitClicked:(id)sender {
    
    UIAlertController * quitController = [UIAlertController alertControllerWithTitle:nil message:@"Quit receiving?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [self removeFilesFormManualDirectory];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {

    }];
    
    [quitController addAction:yesAction];
    [quitController addAction:noAction];
    
    [self presentViewController:quitController animated:YES completion:nil];
    
}

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


#pragma mark - SFTP functions for testing connection and uploading file goes here

-(void)FTPConnectionTester{
    
//    [self startSpinner:@"Testing Connection.."];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    self.navigationItem.rightBarButtonItem.enabled = FALSE;
    self.navigationItem.leftBarButtonItem.enabled = FALSE;

    NSString *username = @"CRSMobileFTP";
    NSString *password = @"CRSP@ssw0rd2017!";
    NSInteger port = 22;
    NSString *host = @"ftp.cnfltd.com";
    
    // make a connection object and attempt to connect
    DLSFTPConnection *connection = [[DLSFTPConnection alloc] initWithHostname:host
                                                                         port:port
                                                                     username:username
                                                                     password:password];
    self.connection=connection;
    DLSFTPClientSuccessBlock successBlock = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            // login successful
//            [self stopSpinner];
            NSLog(@"start uploading files");
            [self uploadFIlesFromDirectory];
        });
    };
    
    DLSFTPClientFailureBlock failureBlock = ^(NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.connection=nil;
//            [self stopSpinner];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            self.navigationItem.rightBarButtonItem.enabled = TRUE;
            self.navigationItem.leftBarButtonItem.enabled = TRUE;
            [self showFailureToUploadAlert:[NSString stringWithFormat:@"Ftp connection error: %@", error.localizedDescription]];
        });
    };
    [connection connectWithSuccessBlock:successBlock failureBlock:failureBlock];
}

-(void)uploadFIlesFromDirectory{
    
//    [self startSpinner:@"Uploading Photos.."];

    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];

    NSPredicate * pdfPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", @".pdf"];
    NSArray * pdfArray = [directoryContent filteredArrayUsingPredicate:pdfPredicate];
    
    if(pdfArray.count==0){
        self.connection=nil;
//        [self stopSpinner];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self showFailureToUploadAlert:[NSString stringWithFormat:@"Can not locate PDF file, please try again.."]];
    }else{
        [self uploadfiles:[pdfArray objectAtIndex:0] remoteFileName:[pdfArray objectAtIndex:0]];
    }
}

-(void)uploadfiles:(NSString*)localFileName remoteFileName:(NSString*)remoteFileName{
    
    NSString * remotePath = [@"/Produce_Receipts" stringByAppendingPathComponent:remoteFileName];
    NSString * localPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/manual_images"] stringByAppendingPathComponent:localFileName];
    
    DLSFTPClientProgressBlock progressBlock = ^void(unsigned long long bytesReceived, unsigned long long bytesTotal) {
        
    };
        DLSFTPClientFileTransferSuccessBlock successBlock = ^(DLSFTPFile *file, NSDate *startTime, NSDate *finishTime) {
        dispatch_async(dispatch_get_main_queue(), ^{
//            [self dismissViewControllerAnimated:YES completion:nil];
//            [self stopSpinner];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self performSegueWithIdentifier:@"produceReceiveListSegue" sender:self];
        });
    };
    
    DLSFTPClientFailureBlock failureBlock = ^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
//                [self stopSpinner];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            self.navigationItem.rightBarButtonItem.enabled = TRUE;
            self.navigationItem.leftBarButtonItem.enabled = TRUE;
            [self showFailureToUploadAlert:error.localizedDescription];
        });
    };
    
    DLSFTPUploadRequest * request = [[DLSFTPUploadRequest alloc] initWithRemotePath:remotePath
                                                         localPath:localPath
                                                      successBlock:successBlock
                                                      failureBlock:failureBlock
                                                     progressBlock:progressBlock];
    [self.connection submitRequest:request];
}


-(void)showDoneAlert{
    UIAlertController * doneController = [UIAlertController alertControllerWithTitle:nil message:@"Proceed to Products?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {


        //check if photo or scan
        @try{
            if(!self.scannerImageAvailable) [self createPdfFile];
            else [self renamePdfFile];
            [self FTPConnectionTester];
        }@catch(NSException* ex){
            self.navigationItem.rightBarButtonItem.enabled = TRUE;
            self.navigationItem.leftBarButtonItem.enabled = TRUE;
            [self showFailureToUploadAlert:[NSString stringWithFormat:@"Error Occured: %@", ex.description]];
        }
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {

    }];

    [doneController addAction:yesAction];
    [doneController addAction:noAction];

    [self presentViewController:doneController animated:YES completion:nil];
}


-(void)showFailureToUploadAlert:(NSString*)alertMsg{
    UIAlertController *failureAlertController = [UIAlertController
                                                 alertControllerWithTitle:@"Error Alert"
                                                 message:[NSString stringWithFormat:@"%@", alertMsg]
                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *retryAction = [UIAlertAction
                                  actionWithTitle:@"Retry"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      NSLog(@"Trying to reconnect to the FTP server..");
                                      [self showDoneAlert];
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                    actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        
                                    }];
    
    [failureAlertController addAction:retryAction];
    [failureAlertController addAction:cancelAction];
    
    [self presentViewController:failureAlertController animated:YES completion:nil];
}

-(void)showFinishedAlert{
    UIAlertController *finishedAlertController = [UIAlertController
                                                  alertControllerWithTitle:nil
                                                  message:[NSString stringWithFormat:@"%@", @"File uploaded for produce invoice, click OK to proceed"]
                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"Taking user to main screen..");
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   [self performSegueWithIdentifier:@"produceReceiveListSegue" sender:self];
                               }];
    
    [finishedAlertController addAction:okaction];
    [self presentViewController:finishedAlertController animated:YES completion:nil];
}

@end
