//
//  CNFProductLookupTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-12-01.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFProductLookupTableViewController : UITableViewController<ZBarReaderDelegate, UITextFieldDelegate, UIAlertViewDelegate, DTDeviceDelegate>

@property (weak, nonatomic) IBOutlet UITextField *enterUpcText;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *productBrandLabel;
@property (weak, nonatomic) IBOutlet UILabel *productVendorLabel;

@property (weak, nonatomic) IBOutlet UILabel *productQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCapacityLabel;
@property (weak, nonatomic) IBOutlet UILabel *productVelocityLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productOnOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *productTriggerLabel;

@end
