//
//  CNFProductLookupTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-12-01.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFProductLookupTableViewController.h"
#import "StoreSpecificInfo.h"
#import "Products.h"
#import "BarcodeProcessor.h"
#import "Reachability.h"
#import "getProductDetailRequest.h"

@interface CNFProductLookupTableViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic, strong) NSMutableArray * auditedProducts;
@property (nonatomic) BOOL activateCancelButton;
@property (nonatomic) BOOL scanProduct;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic, strong) Products * scannedProduct;
@property (nonatomic) BOOL isOnlineData;
@end

@implementation CNFProductLookupTableViewController{
    DTDevices *scanner;
    UIAlertController * statusAlert;
}

@synthesize productNameLabel=_productNameLabel, productSizeLabel=_productSizeLabel, context=_context, productBrandLabel=_productBrandLabel, productVendorLabel=_productVendorLabel, scanProduct=_scanProduct, foundBarcode=_foundBarcode,scannerAvailable=_scannerAvailable,scannedProduct=_scannedProduct, enterUpcText=_enterUpcText, scanner=_scanner, isOnlineData=_isOnlineData, productQuantityLabel=_productQuantityLabel, productCapacityLabel=_productCapacityLabel, productVelocityLabel=_productVelocityLabel, productPriceLabel=_productPriceLabel, productOnOrderLabel=_productOnOrderLabel, productTriggerLabel=_productTriggerLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationItem setHidesBackButton:YES];
    
//    [self startSpinner:@"Loading Data..."];
    
    self.activateCancelButton=FALSE;
    self.scanProduct=TRUE;
    self.enterUpcText.delegate=self;
    self.scanner=[DTDevices sharedDevice];
    self.isOnlineData=NO;
    
//    [self stopSpinner];
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailability) userInfo:nil repeats:NO];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [super viewWillAppear:YES];
    [self.scanner addDelegate:self];
    [self.scanner connect];
    
    //check if there is a multiple UPC product selected.
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]){
        [self processBarcode:[[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - butotn functions
- (void)scanShelfTag{
    if(self.scannerAvailable){
        self.productNameLabel.text=self.productSizeLabel.text=self.productVendorLabel.text=self.productBrandLabel.text= @"";
        [self.scanner barcodeStartScan:nil];
        self.foundBarcode = NO;
        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
    }else{
        ZBarReaderViewController *reader = [ZBarReaderViewController new];
        reader.readerDelegate = self;
        [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
        reader.readerView.zoom = 1.0;
//        [self presentModalViewController: reader animated: YES];
        
        [self presentViewController:reader animated:YES completion:nil];
    }
}

#pragma mark - spinner functions

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *customVC     = [[UIViewController alloc] init];
        
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [statusAlert setValue:customVC forKey:@"contentViewController"];
        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }else{
        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:^{
            statusAlert = nil;
        }];
    }
}


#pragma mark - tableview delegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.scannedProduct){
        if(indexPath.section==1 && indexPath.row==3){
            [self showVendorAlert];
        }
        
        if(indexPath.section==2 && indexPath.row==0){
            [self productQuantityAlert];
        }
        
        if(indexPath.section==2 && indexPath.row==1){
            [self productCapacityAlert];
        }
        
        if(indexPath.section==2 && indexPath.row==2){
            [self velocityAlert];
        }
        
        if(indexPath.section==2 && indexPath.row==3){
            [self productPriceAlert];
        }
        
        if(indexPath.section==2 && indexPath.row==5){
            [self triggerAlert];
        }
        
        if(indexPath.section==2 && indexPath.row==4){
            [self onOrderAlert];
        }
    }
}

#pragma mark - text field delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if([self.enterUpcText hasText]){
        if([[self.enterUpcText text]length]<5)
            [self showErrorAlert:@"Please enter a 5 digit code"];
        else
            [self processBarcode:self.enterUpcText.text];
    }else{
        [self showErrorAlert:@"Please Enter Valid product ID or UPC"];
    }
    return NO;
}

#pragma mark - barcode read function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    if(self.scanProduct){
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
        [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self processBarcode:barcode];
    }else{
        int sound[]={3500,200,0,200,3500,200};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
//    [reader dismissModalViewControllerAnimated: YES];
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    
    
    NSString * barcode=symbol.data;
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self processBarcode:barcode];
}

-(void)processBarcode:(NSString *)barcode{

    self.scanProduct=FALSE;
    
    [self.enterUpcText endEditing:YES];
    self.enterUpcText.text=@"";
    
    NSDictionary * resultDic= [[[BarcodeProcessor alloc]init] processBarcode:barcode];
    
    if([resultDic valueForKey:@"errorMsg"]!=NULL){
        NSLog(@"product not found..");
        self.productNameLabel.text=self.productSizeLabel.text=self.productVendorLabel.text=self.productBrandLabel.text=  @"";
        self.productQuantityLabel.text =self.productCapacityLabel.text=self.productVelocityLabel.text=self.productPriceLabel.text=self.productOnOrderLabel.text=self.productTriggerLabel.text = @"";
        self.scannedProduct = nil;
        [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
    }else{
        
        NSArray * products = [resultDic valueForKey:@"productArray"];
        
        if(products.count==0){
            //no product found
            
            NSLog(@"product not found..");
            self.productNameLabel.text=self.productSizeLabel.text=self.productVendorLabel.text=self.productBrandLabel.text=  @"";
            self.productQuantityLabel.text =self.productCapacityLabel.text=self.productVelocityLabel.text=self.productPriceLabel.text=self.productOnOrderLabel.text=self.productTriggerLabel.text = @"";
            self.scannedProduct = nil;
            [self showErrorAlert:@"Product Not Found..."];
            
        }else if(products.count>1){
            //multiple products found
            
            NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
            
            for(NSObject * product in products){
                NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                if([product valueForKey:@"Size"]!=NULL && [product valueForKey:@"UomQty"]!=NULL){
                    [listedProduct setValue:[[[[product valueForKey:@"Size"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[product valueForKey:@"UomQty"]] forKey:@"Size"];
                }
                [listedProductArray addObject:listedProduct];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
            [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"multipleUpcFromLookUpSegue" sender:self];
        }else{
            //only one product found
            
            Products * product = [[Products alloc]init];
            product = [products objectAtIndex:0];
            
            self.scannedProduct = product;
            
            //check connection the online webcall here try to get latest data
            [self testConnectionAndGetData];
        }
    }
}

#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailability{
    if([self.scanner connstate]==1){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(scanShelfTag)];
        self.scannerAvailable=FALSE;
//        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Scan" style:UIBarButtonItemStylePlain target:self action:@selector(scanShelfTag)];
    }
    [self.navigationItem setHidesBackButton:NO];
}

-(void)stopScanner{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}


#pragma mark - alert notification functions

-(void)productQuantityAlert{
    
    self.scanProduct=FALSE;
    
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:[store.Qoh stringValue]] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
    }
    UIAlertController *quantityAlertController = [UIAlertController
                                               alertControllerWithTitle:[@"QOH for " stringByAppendingString:self.scannedProduct.ProductName]
                                               message:[NSString stringWithFormat:@"%@", result]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [quantityAlertController addAction:okaction];
    [self presentViewController:quantityAlertController animated:YES completion:nil];
}


-(void)productPriceAlert{
    self.scanProduct=FALSE;
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:[store.ActivePrice stringValue] ] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
    }

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Price for" message:result preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        self.scanProduct=TRUE;
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated: YES completion: nil];
}


-(void)productCapacityAlert{
    self.scanProduct=FALSE;
    
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:[store.ShelfCapacity stringValue]] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
        
    }
    
    UIAlertController *capacityAlertController = [UIAlertController
                                                  alertControllerWithTitle:[@"Capacity for " stringByAppendingString:self.scannedProduct.ProductName]
                                                  message:[NSString stringWithFormat:@"%@", result]
                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [capacityAlertController addAction:okaction];
    [self presentViewController:capacityAlertController animated:YES completion:nil];
}

-(void)triggerAlert{
    self.scanProduct=FALSE;
    
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:store.TriggerDescription?store.TriggerDescription:@""] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
        
    }
    
    UIAlertController *triggerAlertController = [UIAlertController
                                                  alertControllerWithTitle:[@"Trigger settings for " stringByAppendingString:self.scannedProduct.ProductName]
                                                 message:[NSString stringWithFormat:@"%@", result]
                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [triggerAlertController addAction:okaction];
    [self presentViewController:triggerAlertController animated:YES completion:nil];

}

-(void)velocityAlert{
    self.scanProduct=FALSE;
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        
        NSString * velocityString= [NSString stringWithFormat:@"Previous Day: %@, 7 days: %@, 30 days: %@, 90 days: %@", [store.VelocityPreviousDay stringValue], [store.VelocitySevenDays stringValue], [store.VelocityThirtyDays stringValue], [store.VelocityNinetyDays stringValue]];
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:velocityString] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
        
    }
    UIAlertController *velocityAlertController = [UIAlertController
                                                 alertControllerWithTitle:[@"Velocity info for " stringByAppendingString:self.scannedProduct.ProductName]
                                                 message:[NSString stringWithFormat:@"%@", result]
                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [velocityAlertController addAction:okaction];
    [self presentViewController:velocityAlertController animated:YES completion:nil];
    
}

-(void)onOrderAlert{
    self.scanProduct=FALSE;
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:[store.Noq stringValue] ] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
    }

    UIAlertController *orderAlertController = [UIAlertController
                                                  alertControllerWithTitle:[@"Next Order for " stringByAppendingString:self.scannedProduct.ProductName]
                                                  message:[NSString stringWithFormat:@"%@", result]
                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [orderAlertController addAction:okaction];
    [self presentViewController:orderAlertController animated:YES completion:nil];
    
    
}

-(void)inTransitAlert{
    self.scanProduct=FALSE;
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:[store.Noq stringValue] ] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
    }
    
    UIAlertController *orderAlertController = [UIAlertController
                                               alertControllerWithTitle:[@"Next Order for " stringByAppendingString:self.scannedProduct.ProductName]
                                               message:[NSString stringWithFormat:@"%@", result]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [orderAlertController addAction:okaction];
    [self presentViewController:orderAlertController animated:YES completion:nil];
 
}

-(void)showErrorAlert:(NSString *)message{
    self.scanProduct=FALSE;
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    self.scanProduct=FALSE;
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

-(void)showVendorAlert{
    self.scanProduct=FALSE;
    
    NSString* body = @"";
    
    for(Vendors * vendor in self.scannedProduct.Vendors){
        body=[[body stringByAppendingString:vendor.VendorName]stringByAppendingString:@"\n"];
    }

    UIAlertController *vendorAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Vendors",@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [vendorAlertController addAction:okaction];
    [self presentViewController:vendorAlertController animated:YES completion:nil];
    
}

//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    self.scanProduct=TRUE;
//}


#pragma mark - connection tester

-(int)connectionTester{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];
    
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
    if(netStatus==1){
        NSLog(@"connection OK...");
    }else{
        NSLog(@"No connection for validation...");
    }
    
    return netStatus;
}


-(void)testConnectionAndGetData{
    
    if([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_TIME"]]/60 < [[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_INTERVAL"] intValue]){
        NSLog(@"online data fetching problem from last time, will try offline mode");
        self.isOnlineData=NO;
        [self populateTableWithProduct];
    }else{
        if([self connectionTester]!=0){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Getting data online..");
                [self getProductDataOnline];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //get offline data
                NSLog(@"Getting data offline..");
                self.isOnlineData=NO;
                [self populateTableWithProduct];
            });
        }
    }
}


#pragma mark - webservice functions
-(void)getProductDataOnline{
    
    [self startSpinner:@"Getting Data"];
    
    getProductDetailRequest * productRequest = [[getProductDetailRequest alloc]init];    
    productRequest.productId = self.scannedProduct.ProductId;
    NSString *post = [productRequest toJSONString];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getProductInfoById", appDelegate.serverUrl]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];

    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              NSLog(@"Download Error:%@",error.description);
                                              self.isOnlineData=NO;
                                              //print offline info
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                      [self populateTableWithProduct];
                                                  }];
                                              });
                                          }
                                          if (data) {
                                              NSDictionary *strings = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                              if(error){
                                                  NSLog(@"web service error, can't serialize data..");
                                                  self.isOnlineData=NO;
                                                  //print offline info
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                      [self populateTableWithProduct];
                                                      }];
                                                    });
                                                  
                                              }else{
                                                  if([strings valueForKey:@"Message"]==Nil){
                                                      Products * onlineProduct = [[Products alloc]initWithString:[strings valueForKey:@"d"] error:&error];
                                                      if(!error){
                                                          self.scannedProduct = onlineProduct;
                                                      }
                                                      self.isOnlineData=YES;

                                                      dispatch_async(dispatch_get_main_queue(), ^{
//                                                          [statusAlert dismissViewControllerAnimated:YES completion:^{
//
//                                                          }];
                                                          [self dismissViewControllerAnimated:YES completion:nil];
                                                          [self populateTableWithProduct];
                                                      });
                                                  }else{
                                                      NSLog(@"error msg: %@", [strings valueForKey:@"Message"]);
                                                      //print offline info
                                                      self.isOnlineData=NO;
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                              [self populateTableWithProduct];
                                                          }];
                                                      });
                                                  }
                                              }
                                          }
                                      }];
    
    [dataTask resume];
}

-(void)populateTableWithProduct{
    
    self.scanProduct=TRUE;
    
    if(!self.isOnlineData){
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_TIME"]!=NULL){
            if([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_TIME"]]/60 > [[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_INTERVAL"] intValue]){
                [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"ONLINE_TROUBLE_TIME"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"ONLINE_TROUBLE_TIME"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }

    self.productNameLabel.text = self.scannedProduct.ProductName;
    self.productSizeLabel.text = [[[self.scannedProduct.Size stringValue]stringByAppendingString:@" " ]  stringByAppendingString:self.scannedProduct.Uom];
    self.productBrandLabel.text = self.scannedProduct.Brand ? self.scannedProduct.Brand:@"";
    self.productVendorLabel.text = self.scannedProduct.Supplier.DefaultVendor.VendorName;
    
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]!=[NSNull null]){
        //select storewise info adn populate table
        
        NSPredicate * locationPredicate=[NSPredicate predicateWithFormat:@"Location.LocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
        NSArray * foundLocation = [self.scannedProduct.InfoByStore filteredArrayUsingPredicate:locationPredicate];
        
        if(foundLocation.count==0){
            //do nothing
        }else{
            StoreSpecificInfo * store = [foundLocation objectAtIndex:0];
            
            self.productQuantityLabel.text = [store.Qoh stringValue];
            self.productCapacityLabel.text = [store.ShelfCapacity stringValue];
            self.productVelocityLabel.text = [NSString stringWithFormat:@"%@, %@, %@", [store.VelocitySevenDays stringValue], [store.VelocityThirtyDays stringValue], [store.VelocityNinetyDays stringValue]];
            self.productPriceLabel.text = [store.ListPrice stringValue];
            self.productOnOrderLabel.text = [store.Noq stringValue];
            self.productTriggerLabel.text = store.TriggerDescription;
        }
    }
    [self.tableView reloadData];
}


#pragma mark - tableview functions

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    if(section==2){
        UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
        footer.textLabel.textAlignment = NSTextAlignmentCenter;
    }
}

- (NSString *)tableView:(UITableView *)table titleForFooterInSection:(NSInteger)section{
   
    if(section==2){
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"HH:mm:ss MM/dd/yyyy"];
        
        if(self.isOnlineData){
            return [NSString stringWithFormat:@"Last updated at: %@", [formatter stringFromDate:[NSDate date]]];
        }else{
            return [NSString stringWithFormat:@"Last updated at: %@", [formatter stringFromDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"DATA_FILE_DOWNLOAD_DATE"]]];
        }
    }
    else
        return @"";

}




@end
