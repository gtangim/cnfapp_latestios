//
//  CNFProductLookupViewController.h
//  CNF App
//
//  Created by Anik on 2015-07-24.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFProductLookupViewController : UIViewController<DTDeviceDelegate, ZBarReaderDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *enterUpcText;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *productQOHLabel;
@property (weak, nonatomic) IBOutlet UILabel *productBrandLabel;
@property (weak, nonatomic) IBOutlet UILabel *productVendorLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCapacityLabel;
@property (weak, nonatomic) IBOutlet UIButton *scanShelfTagButton;


@property (weak, nonatomic) IBOutlet UILabel *dateShowLabel;

- (IBAction)backToMain:(id)sender;
- (IBAction)scanShelfTag:(id)sender;

@end
