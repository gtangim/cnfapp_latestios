//
//  CNFProductLookupViewController.m
//  CNF App
//
//  Created by Anik on 2015-07-24.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "CNFProductLookupViewController.h"
#import "StoreSpecificInfo.h"
#import "Products.h"
#import "BarcodeProcessor.h"

@interface CNFProductLookupViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic, strong) NSMutableArray * auditedProducts;
@property (nonatomic) BOOL activateCancelButton;
@property (nonatomic) BOOL scanProduct;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic, strong) Products * scannedProduct;
@property (nonatomic) BOOL shouldScanProduct;

@end

@implementation CNFProductLookupViewController{
    NSManagedObjectContext * context;
    DTDevices *scanner;
    UIAlertView * statusAlert;
}

@synthesize userNameLabel=_userNameLabel, locationLabel=_locationLabel, productNameLabel=_productNameLabel, productSizeLabel=_productSizeLabel, productQOHLabel=_productQOHLabel, context=_context, productBrandLabel=_productBrandLabel, productVendorLabel=_productVendorLabel,dateShowLabel=_dateShowLabel, scanProduct=_scanProduct, foundBarcode=_foundBarcode, productCapacityLabel=_productCapacityLabel,scannerAvailable=_scannerAvailable, scanShelfTagButton=_scanShelfTagButton, scannedProduct=_scannedProduct, enterUpcText=_enterUpcText, scanner=_scanner, shouldScanProduct=_shouldScanProduct;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self startSpinner:@"Loading Data..."];
    
    self.userNameLabel.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"USER_NAME"];
    self.locationLabel.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"OFFICE"];
    
    self.dateShowLabel.text = [@"Data File Version: " stringByAppendingString:[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    self.activateCancelButton=FALSE;
    self.scanProduct=TRUE;
    self.enterUpcText.delegate=self;
    
    [self stopSpinner];
    self.scanner=[DTDevices sharedDevice];
    
    self.productQOHLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *qohtapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(qohtap)];
    [self.productQOHLabel addGestureRecognizer:qohtapGesture];
    
    self.productCapacityLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *capacitytapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(capacitytap)];
    [self.productCapacityLabel addGestureRecognizer:capacitytapGesture];
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailability) userInfo:nil repeats:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [super viewWillAppear:YES];
    [self.scanner addDelegate:self];
    [self.scanner connect];
    
    //check if there is a multiple UPC product selected.
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]){
        [self processBarcode:[[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]];
    }
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backToMain:(id)sender {
    self.scannedProduct=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)scanShelfTag:(id)sender {
    if(self.scannerAvailable){
        self.productNameLabel.text=self.productSizeLabel.text=self.productQOHLabel.text=self.productVendorLabel.text=self.productBrandLabel.text= self.productCapacityLabel.text=@"";
        [self.scanner barcodeStartScan:nil];
        self.foundBarcode = NO;
        
        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
    }else{
        ZBarReaderViewController *reader = [ZBarReaderViewController new];
        reader.readerDelegate = self;
        [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
        reader.readerView.zoom = 1.0;
//        [self presentModalViewController: reader animated: YES];
        [self presentViewController:reader animated:YES completion:nil];
        
        
    }
}


#pragma mark - label tap function

-(void)qohtap{
    if(self.scannedProduct){
        [self productQuantityAlert];
    }else{
        [self showErrorAlert:@"No scanned product"];
    }
}


-(void)capacitytap{
    if(self.scannedProduct){
        [self productCapacityAlert];
    }else{
        [self showErrorAlert:@"No scanned product"];
    }
}

#pragma mark - spinner functions

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(message,@"") message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
        
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator startAnimating];
        
        [statusAlert setValue:indicator forKey:@"accessoryView"];
        [statusAlert show];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissWithClickedButtonIndex:0 animated:YES];
        statusAlert = nil;
    }
}

#pragma mark - text field delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if([self.enterUpcText hasText]){
        if([[self.enterUpcText text]length]<5)
            [self showErrorAlert:@"Please enter a 5 digit code"];
        else
            [self processBarcode:self.enterUpcText.text];
    }else{
        [self showErrorAlert:@"Please Enter Valid product ID or UPC"];
    }
    return NO;
}



#pragma mark - barcode read function

-(void)barcodeData:(NSString *)barcode type:(int)type {
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
        [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self processBarcode:barcode];
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
//    [reader dismissModalViewControllerAnimated: YES];
    [reader dismissViewControllerAnimated:YES completion:nil];
    NSString * barcode=symbol.data;
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self processBarcode:barcode];
}


-(void)processBarcode:(NSString *)barcode{

    [[UIAlertView alloc] dismissWithClickedButtonIndex:0 animated:YES];
    
    NSArray * products= [[[BarcodeProcessor alloc]init] processBarcode:barcode];
    
    if(products.count==0){
        //no product found
        
        NSLog(@"product not found..");
        self.productNameLabel.text=self.productSizeLabel.text=self.productQOHLabel.text=self.productVendorLabel.text=self.productBrandLabel.text= self.productCapacityLabel.text= @"";
        self.scannedProduct = nil;
        [self showErrorAlert:@"Product Not Found..."];
        
    }else if(products.count>1){
        //multiple products found
        
        NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
        
        for(NSObject * product in products){
            NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
            [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
            [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
            
            [listedProductArray addObject:listedProduct];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSegueWithIdentifier:@"multipleUpcFromLookUpSegue" sender:self];
    }else{
        //only one product found
        
        Products * product = [[Products alloc]init];
        product = [products objectAtIndex:0];
        
        self.scannedProduct = product;
        
        self.productBrandLabel.text = [@"  "stringByAppendingString:product.Brand];
        self.productNameLabel.text = [@"  "stringByAppendingString:product.ProductName];
        if(product.Supplier.DefaultVendor.VendorName==nil){
            self.productVendorLabel.text = @"";
        }else{
            self.productVendorLabel.text = [@"  "stringByAppendingString:product.Supplier.DefaultVendor.VendorName];
        }
        NSString * sizeUom = [@" " stringByAppendingString:product.Uom];
        self.productSizeLabel.text = [[@"  "stringByAppendingString:[product.Size stringValue]] stringByAppendingString:sizeUom];

        if([[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]){
            NSPredicate * locationPredicate = [NSPredicate predicateWithFormat:@"Location.LocationId contains[cd] %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];

            StoreSpecificInfo * storeInfo = [[product.InfoByStore filteredArrayUsingPredicate:locationPredicate] objectAtIndex:0];
            
            self.productQOHLabel.text = [@"  "stringByAppendingString:[storeInfo.Qoh stringValue]];
            self.productCapacityLabel.text =[@"  "stringByAppendingString:[storeInfo.ShelfCapacity stringValue]];
        }
    }
}

#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailability{
    if([self.scanner connstate]==1){
        [self.scanShelfTagButton setTitle:@"" forState:UIControlStateNormal];
        [self.scanShelfTagButton setBackgroundColor:nil];
        [self.scanShelfTagButton setImage:[UIImage imageNamed:@"iPhoneCamera.png"] forState:UIControlStateNormal];
        self.scannerAvailable=FALSE;
        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
    
    self.navigationItem.leftBarButtonItem.enabled=NO;
}

-(void)stopScanner
{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}

#pragma mark - alert notification functions

-(void)productQuantityAlert{
    
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:[store.Qoh stringValue]] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
    }
    
    UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:[@"QOH for " stringByAppendingString:self.scannedProduct.ProductName]
                                                         message:result
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [quantityAlert show];
    
}

-(void)productCapacityAlert{
    NSMutableString* result = [NSMutableString stringWithCapacity:150];
    for (StoreSpecificInfo * store in self.scannedProduct.InfoByStore){
        NSString * interString = [[[@" " stringByAppendingString:store.Location.LocationId] stringByAppendingString:@":"] stringByAppendingString:[store.ShelfCapacity stringValue]] ;
        [result appendString:[NSString stringWithFormat:@"%@\r%@", @"",interString]];
 
    }
    
    UIAlertView * capacityAlert = [[UIAlertView alloc] initWithTitle:[@"Capacity for " stringByAppendingString:self.scannedProduct.ProductName]
                                                             message:result
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
    [capacityAlert show];
}

-(void)showErrorAlert:(NSString *)message{
    UIAlertView * errorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(message,@"")
                                                          message:nil
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
    [errorAlert show];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    UIAlertView * infoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title,@"")
                                                         message:body
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [infoAlert show];
}


@end
