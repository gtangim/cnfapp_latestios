//
//  CNFProductionInventoryTableViewController.h
//  CNF App
//
//  Created by Anik on 2017-11-02.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"

@interface CNFProductionInventoryTableViewController : UITableViewController<NSFetchedResultsControllerDelegate, ZBarReaderDelegate>
@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end
