//
//  CNFProductionInventoryTableViewController.m
//  CNF App
//
//  Created by Anik on 2017-11-02.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "CNFProductionInventoryTableViewController.h"
#import "CNFAppDelegate.h"
#import "BarcodeProcessor.h"
#import "Products.h"

#import "CNFGetDocumentId.h"

#import "AdjustmentProduct.h"
#import "AdjustmentItem.h"
#import "BaseAdjustmentRequest.h"

@interface CNFProductionInventoryTableViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSString * receipeName;
@property (nonatomic, strong) NSMutableArray * ingredientList;
@property (nonatomic) int recepieMultiplier;
@property (nonatomic) BOOL ingredientDeplete;
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSDate * endTime;
@property (nonatomic, strong) NSString * documentId;
@end

@implementation CNFProductionInventoryTableViewController{
    NSManagedObjectContext * context;
}

@synthesize context=_context;
@synthesize receipeName=_receipeName, ingredientList=_ingredientList, recepieMultiplier=_recepieMultiplier, ingredientDeplete=_ingredientDeplete;
@synthesize documentId=_documentId;


- (void)viewDidLoad {
    [super viewDidLoad];

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    self.ingredientDeplete = FALSE;

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Quit" style:UIBarButtonItemStylePlain target:self action:@selector(quitAudit)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submitAudit)];

    [self showScanAlert];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.receipeName && self.ingredientList.count>0) return 2;
    else if(self.receipeName && self.ingredientList.count==0) return 1;
    else return 0;
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section==0){
        if(self.ingredientDeplete) return @"recipe Name";
        else return @"Finished Product";
    }else{
        if(self.ingredientDeplete) return @"Ingredient List";
        else return @"Finished Product List";
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section==0) return 1;
    if(section==1) return self.ingredientList.count;
    else return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"productionInventoryCells" forIndexPath:indexPath];

    if(indexPath.section==0){
        cell.textLabel.text = self.receipeName?self.receipeName:@"No recipe Name";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Batches for this recipe: %d", self.recepieMultiplier];
    }else if(indexPath.section==1){
        cell.textLabel.text = [[[self.ingredientList objectAtIndex:indexPath.row] valueForKey:@"Product"] valueForKey:@"ProductName"];

        double totalQuantity = [[self.ingredientList objectAtIndex:indexPath.row] valueForKey:@"revisedQuantity"]?[[[self.ingredientList objectAtIndex:indexPath.row] valueForKey:@"revisedQuantity"]doubleValue]:[[[self.ingredientList objectAtIndex:indexPath.row] valueForKey:@"perQuantity"] doubleValue]*self.recepieMultiplier;

        cell.detailTextLabel.text = [[NSString stringWithFormat:@"Product UPC: %@ \nQuantity for the recipe: %.2f%@ \n",[[[self.ingredientList objectAtIndex:indexPath.row]valueForKey:@"Product"]valueForKey:@"Upc"], totalQuantity, [[self.ingredientList objectAtIndex:indexPath.row]valueForKey:@"uom"]] stringByAppendingString:[NSString stringWithFormat:@"%@",self.ingredientDeplete?[NSString stringWithFormat:@"Note: %@", [[self.ingredientList objectAtIndex:indexPath.row]valueForKey:@"note"]]:@""]];
    }else{

    }

    cell.textLabel.numberOfLines=0;
    cell.detailTextLabel.numberOfLines=0;

    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    if(indexPath.section==0) return YES;
    else return NO;
}


- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    if(indexPath.section==0 && indexPath.row==0){
        UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            self.receipeName=nil;
            self.ingredientList=nil;
            self.recepieMultiplier=1;

            NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
            NSFetchRequest *request = [[NSFetchRequest alloc]init];

            NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];

            [request setPredicate:predicate];
            [request setEntity:entityDesc];

            NSError *error;
            NSArray *results= [context executeFetchRequest:request error:&error];

            for (NSManagedObject * obj in results){
                [context deleteObject:obj];
            }
            [context save:&error];

            [self showScanAlert];
            [self.tableView reloadData];
        }];
        return @[deleteAction];
    }else return  NULL;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0 && indexPath.row==0){
        [self showRecepieMultiplierAlert];
    }
    if(indexPath.section==1){
        [self showIngredientQuantityAlert];
    }
}


#pragma mark - alert fucnitons

-(void)quitAudit{
    UIAlertController *quitAlertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:@"Sure to quit Inventory Adjustment?"
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesaction = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction *action)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                        NSFetchRequest *request = [[NSFetchRequest alloc]init];

                                        NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];

                                        [request setPredicate:predicate];
                                        [request setEntity:entityDesc];

                                        NSError *error;
                                        NSArray *results= [context executeFetchRequest:request error:&error];

                                        for (NSManagedObject * obj in results){
                                            [context deleteObject:obj];
                                        }
                                        [context save:&error];

                                        [NSUserDefaults resetStandardUserDefaults];
                                        [[NSUserDefaults standardUserDefaults]synchronize];

                                        self.ingredientList=nil;
                                        self.receipeName=nil;
                                        [self.navigationController popViewControllerAnimated:YES];
                                    });
                                }];

    UIAlertAction *noaction = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [quitAlertController addAction:yesaction];
    [quitAlertController addAction:noaction];
    [self presentViewController:quitAlertController animated:YES completion:nil];
}

-(void)showRecepieMultiplierAlert{
    UIAlertController *batchAlertController = [UIAlertController
                                               alertControllerWithTitle:self.receipeName
                                               message:@"Enter the number of batches"
                                               preferredStyle:UIAlertControllerStyleAlert];

    [batchAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = [NSString stringWithFormat:@"%d", self.recepieMultiplier];
        textField.keyboardType = UIKeyboardTypeNumberPad;
        [textField becomeFirstResponder];
    }];


    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   UITextField * textField = batchAlertController.textFields.firstObject;
                                   if([textField hasText]){
                                       NSCharacterSet* nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                                       NSRange r = [textField.text rangeOfCharacterFromSet: nonNumbers];
                                       if( r.location == NSNotFound && textField.text.length > 0) {
                                           self.recepieMultiplier = [textField.text intValue];
                                           for(NSObject * ingredients in self.ingredientList){
                                               [ingredients setValue:NULL forKey:@"revisedQuantity"];
                                           }
                                           [self.tableView reloadData];
                                       }
                                       else{
                                           [self showErrorAlert:@"Please enter only numbers"];
                                       }
                                   }else{

                                   }
                               }];

    UIAlertAction *cancelaction = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [batchAlertController addAction:okaction];
    [batchAlertController addAction:cancelaction];
    [self presentViewController:batchAlertController animated:YES completion:nil];
}


-(void)showIngredientQuantityAlert{
    UIAlertController *quantityAlertController = [UIAlertController
                                               alertControllerWithTitle:[[[self.ingredientList objectAtIndex:[self.tableView indexPathForSelectedRow].row]valueForKey:@"Product"]valueForKey:@"ProductName"]
                                               message:[NSString stringWithFormat:@"Enter corrected quantity in %@", [[self.ingredientList objectAtIndex:[self.tableView indexPathForSelectedRow].row]valueForKey:@"uom"]]
                                               preferredStyle:UIAlertControllerStyleAlert];

    [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = [NSString stringWithFormat:@"%.2f", [[self.ingredientList objectAtIndex:[self.tableView indexPathForSelectedRow].row]valueForKey:@"revisedQuantity"]?[[[self.ingredientList objectAtIndex:[self.tableView indexPathForSelectedRow].row]valueForKey:@"revisedQuantity"]doubleValue]:[[[self.ingredientList objectAtIndex:[self.tableView indexPathForSelectedRow].row]valueForKey:@"perQuantity"]doubleValue]*self.recepieMultiplier];
        textField.keyboardType = UIKeyboardTypeDecimalPad;
        [textField becomeFirstResponder];
    }];

    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField * textField = [quantityAlertController.textFields firstObject];
        [[self.ingredientList objectAtIndex:[self.tableView indexPathForSelectedRow].row] setValue:[NSNumber numberWithDouble:[textField.text doubleValue]] forKey:@"revisedQuantity"];
        [self.tableView reloadData];
    }];

    UIAlertAction *cancelaction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
    [quantityAlertController addAction:okAction];
    [quantityAlertController addAction:cancelaction];
    [self presentViewController:quantityAlertController animated:YES completion:nil];

}


-(void)showScanAlert{
    UIAlertController *scanAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:@"Please scan QR-code"
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            ZBarReaderViewController *reader = [ZBarReaderViewController new];
            reader.readerDelegate = self;
            [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 1];
            reader.readerView.zoom = 1.0;
            [self presentViewController:reader animated:YES completion:nil];
    }];


    UIAlertAction *backaction = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   [self.navigationController popToRootViewControllerAnimated:YES];
                               }];

    [scanAlertController addAction:okAction];
    [scanAlertController addAction:backaction];
    [self presentViewController:scanAlertController animated:YES completion:nil];
}

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


-(void)showInvalidBarcodeAlert{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:@"Invalid barcode"
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self showScanAlert];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

#pragma mark - camera function

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];

    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;

    [reader dismissViewControllerAnimated:YES completion:nil];
    NSString * barcode=symbol.data;
    [self processBarcode:barcode];
}

#pragma marm - barcode Processor funciton

-(void)processBarcode:(NSString*)barcode{

    if(!([barcode componentsSeparatedByString:@";"].count>1)){
        [self showInvalidBarcodeAlert];
    }else{
        if(!([[barcode componentsSeparatedByString:@";"].firstObject componentsSeparatedByString:@","].count>1)){
            [self showInvalidBarcodeAlert];
        }else{
            NSLog(@"right barcode found..");

            NSMutableArray * barcodeArray = [[NSMutableArray alloc]initWithArray:[barcode componentsSeparatedByString:@";"]];
            [barcodeArray removeLastObject];

            if([[barcodeArray.firstObject componentsSeparatedByString:@","][0] isEqualToString:@"1"]){
                //scanned recepie
                self.ingredientDeplete = TRUE;
                self.receipeName = [barcodeArray.firstObject componentsSeparatedByString:@","][1];
                self.ingredientList = [[NSMutableArray alloc]init];
                self.recepieMultiplier =1;
                self.navigationItem.title = @"Production Inventory Deplete";
                for(NSString * ingredientString in barcodeArray){
                    Products * product = [[[BarcodeProcessor alloc]init] processBarcodeForProductId:[ingredientString componentsSeparatedByString:@","][1]];
                    if(product == NULL){
                        NSLog(@"Result returned error..Error adding an ingredient");
                    }else{
                        NSMutableDictionary * ingredientDic = [[NSMutableDictionary alloc]init];
                        [ingredientDic setValue:product forKey:@"Product"];
                        [ingredientDic setObject:[ingredientString componentsSeparatedByString:@","][2] forKey:@"uom"];
                        [ingredientDic setObject:[NSNumber numberWithDouble:[[ingredientString componentsSeparatedByString:@","][3]doubleValue]] forKey:@"perQuantity"];
                        [ingredientDic setObject:[ingredientString componentsSeparatedByString:@","][4] forKey:@"note"];
                        [self.ingredientList addObject:ingredientDic];
                    }
                }
            }else{
                //scanned finished good
                NSLog(@"finished product scanned...");
                self.ingredientDeplete = FALSE;
                self.navigationItem.title = @"Production Inventory Increase";
                self.receipeName = @"Finished Product";
                self.ingredientList = [[NSMutableArray alloc]init];
                self.recepieMultiplier =1;
                for(NSString * ingredientString in barcodeArray){
                    Products * product = [[[BarcodeProcessor alloc]init] processBarcodeForProductId:[ingredientString componentsSeparatedByString:@","][1]];
                    if(product == NULL){
                        NSLog(@"Result returned error..Error adding an ingredient");
                    }else{
                        NSMutableDictionary * ingredientDic = [[NSMutableDictionary alloc]init];
                        [ingredientDic setValue:product forKey:@"Product"];
                        [ingredientDic setObject:[ingredientString componentsSeparatedByString:@","][2] forKey:@"uom"];
                        [ingredientDic setObject:[NSNumber numberWithDouble:[[ingredientString componentsSeparatedByString:@","][3]doubleValue]] forKey:@"perQuantity"];
                        [self.ingredientList addObject:ingredientDic];
                    }
                }
            }
            [self saveInitialEntryToDB];
            [self.tableView reloadData];
            NSLog(@"got all products..");
        }
    }
}

#pragma mark - submit function

-(void)saveInitialEntryToDB{
    self.startTime = [NSDate date];
    self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];

    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:self.ingredientDeplete?@"C2_PRODUCTION_INVENTORY_DECREASE":@"C2_PRODUCTION_INVENTORY_INCREASE" forKey:@"documentType"];
    [newManagedObject setValue:@"Created" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.startTime forKey:@"date"];
    [context save:&error];
}

-(void)submitAudit{

    UIAlertController *quitAlertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:[NSString stringWithFormat:@"Submit Request"]
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesaction = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                                        self.endTime = [NSDate date];
                                        if([self saveAuditToFile]){
                                            [NSUserDefaults resetStandardUserDefaults];
                                            [[NSUserDefaults standardUserDefaults]synchronize];
                                            self.ingredientList=Nil;
                                            self.receipeName=Nil;
                                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }else{
                                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                            [self showErrorAlert:@"Could not save file, Please try again"];
                                        }
                                    });
                                }];

    UIAlertAction *noaction = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [quitAlertController addAction:yesaction];
    [quitAlertController addAction:noaction];
    [self presentViewController:quitAlertController animated:YES completion:nil];
}

-(BOOL)saveAuditToFile{
    NSLog(@"saving file..");

    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:self.ingredientDeplete?@"C2_PRODUCTION_INVENTORY_DECREASE":@"C2_PRODUCTION_INVENTORY_INCREASE" forKey:@"documentType"];
    [newManagedObject setValue:@"Finished" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.endTime forKey:@"date"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
    [context save:&error];

    if(error) return FALSE;
    else{
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        NSString *stringFromDate = [formatter stringFromDate:self.endTime];

        NSMutableArray<AdjustmentItem> * productItems = [[NSMutableArray<AdjustmentItem> alloc]init];

        for(NSObject * product in self.ingredientList){

            AdjustmentItem * ProductItem = [[AdjustmentItem alloc]init];

            AdjustmentProduct * adjProduct = [[AdjustmentProduct alloc]init];

            adjProduct.ProductId = [[product valueForKey:@"Product"]  valueForKey:@"ProductId"];
            adjProduct.DepartmentId = [[product valueForKey:@"Product"] valueForKey:@"DepartmentId"];
            adjProduct.SubDepartmentId = [[product valueForKey:@"Product"] valueForKey:@"SubDepartmentId"];
            adjProduct.CategoryId = [[product valueForKey:@"Product"] valueForKey:@"CategoryId"];

            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"Location.LocationId == %@", [[NSUserDefaults standardUserDefaults]valueForKey:@"OFFICE"]];
            NSArray * filteredStoreArray = [[[product valueForKey:@"Product"] valueForKey:@"InfoByStore"] filteredArrayUsingPredicate:predicate];

            adjProduct.ListPrice =  [NSNumber numberWithDouble:[[filteredStoreArray.firstObject valueForKey:@"ListPrice"] doubleValue]];
            adjProduct.ActivePrice = [NSNumber numberWithDouble:[[filteredStoreArray.firstObject valueForKey:@"ActivePrice"] doubleValue]];
            adjProduct.Cost = [NSNumber numberWithDouble:[[[product valueForKey:@"Product"] valueForKey:@"StandardCost"] doubleValue]];

            adjProduct.BottleDeposit = [[product valueForKey:@"Product"] valueForKey:@"BottleDeposit"];
            adjProduct.EnvironmentalFee = [[product valueForKey:@"Product"] valueForKey:@"EnvironmentalFree"];
            adjProduct.Taxable = [[product valueForKey:@"Product"] valueForKey:@"IsTaxable"];
            adjProduct.TaxRate = [[product valueForKey:@"Product"] valueForKey:@"TaxRatePercent"];

            ProductItem.Product = adjProduct;
            double quantity = [product valueForKey:@"revisedQuantity"]?[[product valueForKey:@"revisedQuantity"]doubleValue] :[[product valueForKey:@"perQuantity"]doubleValue] * (double)self.recepieMultiplier;
            ProductItem.Quantity = [NSNumber numberWithDouble:quantity];
            ProductItem.ValueListPrice = [self multiplyNumbers:adjProduct.ListPrice second:ProductItem.Quantity];
            ProductItem.ValueActivePrice = [self multiplyNumbers:adjProduct.ActivePrice second:ProductItem.Quantity];
            ProductItem.ValueCost = [self multiplyNumbers:adjProduct.Cost second:ProductItem.Quantity];
            ProductItem.ValueBottleDeposit = [self multiplyNumbers:adjProduct.BottleDeposit second:ProductItem.Quantity];
            ProductItem.ValueEnvironmentalFee = [self multiplyNumbers:adjProduct.EnvironmentalFee second:ProductItem.Quantity];
            if([adjProduct.Taxable boolValue]){
                ProductItem.ValueActivePriceTax = [NSNumber numberWithDouble:[ProductItem.ValueActivePrice doubleValue] * [adjProduct.TaxRate doubleValue]/100];
                ProductItem.ValueActiveCostTax = [NSNumber numberWithDouble:[ProductItem.ValueCost doubleValue] * [adjProduct.TaxRate doubleValue]/100];
            }
            else{
                ProductItem.ValueActivePriceTax = [NSNumber numberWithDouble:0.00];
                ProductItem.ValueActiveCostTax = [NSNumber numberWithDouble:0.00];
            }
            [productItems addObject:ProductItem];
        }

        BaseAdjustmentRequest * adjustmentRequest = [[BaseAdjustmentRequest alloc]init];
        adjustmentRequest.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
        adjustmentRequest.HoldingLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        adjustmentRequest.ConsumingLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        adjustmentRequest.CreatedDateTime = self.startTime;
        adjustmentRequest.Comment = self.ingredientDeplete?@"Inventory deplete for ingredients for S7":@"Finished Good inventory increase for S7";
        adjustmentRequest.Items = productItems;

        adjustmentRequest.ReversedEntry = [NSNumber numberWithBool:FALSE];
        adjustmentRequest.IsProductionPurpose = [NSNumber numberWithBool:YES];

        if([self writeStringToFile:[adjustmentRequest toJSONString] fileName:stringFromDate]){
            NSLog(@"File Saved for adjustment from %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"]);
            return TRUE;
        }else return FALSE;
        return FALSE;
    }
}

- (BOOL)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];

    if(error){
        return FALSE;
    }else{
        NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
        return TRUE;
    }
}

-(NSNumber*)multiplyNumbers :(NSNumber*)first second:(NSNumber*)second{
    double multiply = [first doubleValue]*[second doubleValue];
    return [NSNumber numberWithDouble:multiply];
}

@end
