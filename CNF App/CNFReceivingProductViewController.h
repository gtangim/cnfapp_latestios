//
//  CNFReceivingProductViewController.h
//  CNF App
//
//  Created by Anik on 2016-01-14.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFReceivingProductViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, NSFetchedResultsControllerDelegate, ZBarReaderDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UILabel *docNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UITableView *productListTable;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITextField *enterUpcText;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

- (IBAction)scanProduct:(id)sender;
- (IBAction)submitAudit:(id)sender;
- (IBAction)goBack:(id)sender;

@end
