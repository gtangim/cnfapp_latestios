//
//  CNFReceivingProductViewController.m
//  CNF App
//
//  Created by Anik on 2016-01-14.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFReceivingProductViewController.h"
#import "CNFVosDiscrepencyTableViewController.h"
#import "CNFAppEnums.h"
#import "Products.h"
#import "CNFGetDocumentId.h"
#import "ImageCompression.h"
#import "BarcodeProcessor.h"


//new addition jan12th 2018

#import "CNFVosVendorSelectTableViewController.h"

//new ones

#import "transferCreationAssociatedProducts.h"
#import "transferReceiveAssociatedProducts.h"
#import "receiptReceiveAssociatedProducts.h"
#import "vosReceiveAssociatedProducts.h"

#import "transferReceiveAuditElement.h"
#import "transferCreationAuditElement.h"
#import "receiptReceiveAuditElement.h"
#import "vosReceiveAuditElement.h"

#import "transferCreationAuditData.h"
#import "transferReceiveAuditData.h"
#import "receiptReceiveAuditData.h"
#import "vosReceiveAuditData.h"

//adjustment models

#import "AdjustmentProduct.h"
#import "AdjustmentItem.h"
#import "BaseAdjustmentRequest.h"

//printing barcode view controller

#import "CNFPrintQRCodeViewController.h"


@interface CNFReceivingProductViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSObject * receivingObject;
@property (nonatomic, strong) NSString * selectedProductId;
@property (nonatomic, strong) NSMutableArray * auditedProducts;
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic) BOOL shouldScanProduct;
//@property (nonatomic, strong) NSMutableArray * productList;
@property (nonatomic, strong) Products * scannedProduct;
@property (nonatomic, strong) NSDate * endTime;
@property (nonatomic, strong) NSNumber * grandTotal;
@property (nonatomic, strong) NSMutableArray * uomArray;
@property (nonatomic) BOOL barcodeRead;
@property (nonatomic) BOOL pushToCM;
@property (nonatomic) BOOL invoicePhoto;
@property (nonatomic, strong) NSString * CMNote;
@property (nonatomic, strong) NSMutableArray * notes;
@property (nonatomic) int imagecounter;
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSString * documentId;
@property (nonatomic) BOOL addQuantity;
@property (nonatomic) BOOL productPhoto;
@property (nonatomic) BOOL editQuantity;
@property (nonatomic) int addEditedQuantity;
@property (nonatomic) BOOL cafeTransfer;

@end

@implementation CNFReceivingProductViewController{
    NSManagedObjectContext * context;
    UIAlertController * statusAlert;
}

@synthesize docNoLabel=_docNoLabel, locationLabel=_locationLabel, productListTable=_productListTable, scanButton=_scanButton, submitButton=_submitButton, receivingObject =_receivingObject, selectedProductId=_selectedProductId, auditedProducts=_auditedProducts, scanner=_scanner,scannerAvailable=_scannerAvailable, foundBarcode=_foundBarcode, enterUpcText=_enterUpcText, scannedProduct=_scannedProduct, context=_context, grandTotal=_grandTotal, uomArray=_uomArray, shouldScanProduct=_shouldScanProduct, barcodeRead=_barcodeRead, pushToCM=_pushToCM, CMNote=_CMNote, notes=_notes, imagecounter=_imagecounter, invoicePhoto=_invoicePhoto, startTime=_startTime, documentId = _documentId, searchButton=_searchButton, addQuantity=_addQuantity, productPhoto=_productPhoto, editQuantity=_editQuantity, addEditedQuantity=_addEditedQuantity, cafeTransfer=_cafeTransfer;

-(NSMutableArray *)auditedProducts{
    if(!_auditedProducts)_auditedProducts=[[NSMutableArray alloc]init];
    return _auditedProducts;
}
-(NSMutableArray *)uomArray{
    if(!_uomArray)_uomArray = [[NSMutableArray alloc] init];
    return _uomArray;
}
-(NSMutableArray *)notes{
    if(!_notes)_notes = [[NSMutableArray alloc] init];
    return _notes;
}

#pragma  mark - veiw controller load funcitons

- (void)viewDidLoad {
    [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:YES];
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [super viewDidLoad];
    
    self.scanner=[DTDevices sharedDevice];
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"VENDOR_PRODUCT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"]){
        
        [[NSUserDefaults standardUserDefaults] setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"APP_CODE"] forKey:@"APP_CODE"];
        [[NSUserDefaults standardUserDefaults] setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"DOCUMENT_NO"] forKey:@"DOCUMENT_NO"];
        [[NSUserDefaults standardUserDefaults] setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"VENDOR"] forKey:@"VENDOR"];
        [[NSUserDefaults standardUserDefaults] setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"TO_LOCATION"] forKey:@"TO_LOCATION"];
        [[NSUserDefaults standardUserDefaults] setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"VOS_RECEIPT"] forKey:@"VOS_RECEIPT"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.documentId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"DOCUMENT_ID"];
        self.startTime = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"DATE"];
        self.imagecounter =[[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"IMAGE_NO"] intValue];
        [self populatePreScannedProducts];
    }else{
        self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];
        self.imagecounter =0;
        self.startTime = [NSDate date];
        [self saveConfigurationsToDB];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"] || [[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"]){
        
        self.searchButton.hidden = NO;
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"]){
            [self finishInvoicePhotoAlert];
        }else{
            [self takePhotoOfInvoiceAlert];
        }

    }else{
        self.searchButton.hidden=YES;
        [self initializeController];
    }
}

-(void)populatePreScannedProducts{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
    NSPredicate * prodPredicate = [NSPredicate predicateWithFormat:@"documentId==%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"DOCUMENT_ID"]];
    [productRequest setEntity:[NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context]];
    [productRequest setPredicate:prodPredicate];
    
    NSError *error;
    NSArray *productResults= [context executeFetchRequest:productRequest error:&error];
    
    if(productResults.count==0){
        NSLog(@"no previously scanned products..start from new..");
    }else{
        for(NSManagedObject * product in productResults){
            NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
            [newProduct setValue:[product valueForKey:@"productName"] forKey:@"Name"];
            [newProduct setValue:[product valueForKey:@"productId"] forKey:@"ProductId"];
            [newProduct setValue:[product valueForKey:@"sku"] forKey:@"Sku"];
            [newProduct setValue:[NSNumber numberWithDouble:[[product valueForKey:@"quantity"] doubleValue]] forKey:@"Quantity"];
            [newProduct setValue:[product valueForKey:@"uom"] forKey:@"Uom"];
            [newProduct setValue:[product valueForKey:@"quantityText"] forKey:@"QuantityText"];
            [newProduct setValue:[product valueForKey:@"note"] forKey:@"note"];
            [newProduct setValue:[product valueForKey:@"uom"] forKey:@"selectedUom"];
            [newProduct setObject:[product valueForKey:@"size"] forKey:@"size"];
            
            //        [self.auditedProducts addObject:newProduct ];
            [self.auditedProducts insertObject:newProduct atIndex:0];
            
            //set new value of document ID to the already saved products
            //        [product setValue:self.documentId forKey:@"documentId"];
        }
        [context save:&error];
    }
}

-(void)saveConfigurationsToDB{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
        NSError *error;
        NSManagedObject *newManagedObject = [NSEntityDescription
                                             insertNewObjectForEntityForName:@"DocStatusInfo"
                                             inManagedObjectContext:context];
        
        [newManagedObject setValue:@"VOS_RECEIVE" forKey:@"documentType"];
        [newManagedObject setValue:@"Created" forKey:@"status"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [newManagedObject setValue:self.documentId forKey:@"documentId"];
        [newManagedObject setValue:self.startTime forKey:@"date"];
        [context save:&error];
        
        //save to temp tables
        
        NSManagedObject *saveManagedObject = [NSEntityDescription
                                              insertNewObjectForEntityForName:@"TempDocStatus"
                                              inManagedObjectContext:context];
        
        [saveManagedObject setValue:@"VOS_RECEIVE" forKey:@"documentType"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"VOS_RECEIPT"] forKey:@"receiptNo"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [saveManagedObject setValue:self.documentId forKey:@"documentId"];
        [saveManagedObject setValue:[NSDate date] forKey:@"createdDate"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] forKey:@"holdingLocation"];
        [context save:&error];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]){
        NSError *error;
        NSManagedObject *newManagedObject = [NSEntityDescription
                                             insertNewObjectForEntityForName:@"DocStatusInfo"
                                             inManagedObjectContext:context];
        
        [newManagedObject setValue:@"RECEIPT_RECEIVE" forKey:@"documentType"];
        [newManagedObject setValue:@"Created" forKey:@"status"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [newManagedObject setValue:self.documentId forKey:@"documentId"];
        [newManagedObject setValue:self.startTime forKey:@"date"];
        [context save:&error];
        
        //save to temp tables
        
        NSManagedObject *saveManagedObject = [NSEntityDescription
                                              insertNewObjectForEntityForName:@"TempDocStatus"
                                              inManagedObjectContext:context];
        
        [saveManagedObject setValue:@"RECEIPT_RECEIVE" forKey:@"documentType"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [saveManagedObject setValue:self.documentId forKey:@"documentId"];
        [saveManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"] forKey:@"vendorId"];
        [saveManagedObject setValue:[NSDate date] forKey:@"createdDate"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] forKey:@"holdingLocation"];
        [context save:&error];
        
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        NSError *error;
        NSManagedObject *newManagedObject = [NSEntityDescription
                                             insertNewObjectForEntityForName:@"DocStatusInfo"
                                             inManagedObjectContext:context];
        
        [newManagedObject setValue:@"TRANSFER_RECEIVE" forKey:@"documentType"];
        [newManagedObject setValue:@"Created" forKey:@"status"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [newManagedObject setValue:self.documentId forKey:@"documentId"];
        [newManagedObject setValue:self.startTime forKey:@"date"];
        [context save:&error];
        
        //save to temp tables
        
        //        NSManagedObject *saveManagedObject = [NSEntityDescription
        //                                              insertNewObjectForEntityForName:@"TempDocStatus"
        //                                              inManagedObjectContext:context];
        //
        //        [saveManagedObject setValue:@"TRANSFER_RECEIVE" forKey:@"documentType"];
        //        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
        //        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        //        [saveManagedObject setValue:self.documentId forKey:@"documentId"];
        //        [context save:&error];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_CREATION"]){
        NSError *error;
        NSManagedObject *newManagedObject = [NSEntityDescription
                                             insertNewObjectForEntityForName:@"DocStatusInfo"
                                             inManagedObjectContext:context];
        
        [newManagedObject setValue:@"TRANSFER_CREATION" forKey:@"documentType"];
        [newManagedObject setValue:@"Created" forKey:@"status"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [newManagedObject setValue:self.documentId forKey:@"documentId"];
        [newManagedObject setValue:self.startTime forKey:@"date"];
        [context save:&error];
        
        //save to temp tables
        
        NSManagedObject *saveManagedObject = [NSEntityDescription
                                              insertNewObjectForEntityForName:@"TempDocStatus"
                                              inManagedObjectContext:context];
        
        [saveManagedObject setValue:@"TRANSFER_CREATION" forKey:@"documentType"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [saveManagedObject setValue:self.documentId forKey:@"documentId"];
        [saveManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"TO_LOCATION"] valueForKey:@"LocationId"] forKey:@"toLocation"];
        [saveManagedObject setValue:[NSDate date] forKey:@"createdDate"];
        [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] forKey:@"holdingLocation"];
        [context save:&error];
        
    }
}

-(void)initializeController{
    
    [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:YES];
    
    self.shouldScanProduct = YES;
    
    self.productListTable.delegate = self;
    self.productListTable.dataSource=self;
    self.enterUpcText.delegate = self;
    
    self.docNoLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"];
    self.locationLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"] || [[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]|| [[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"]){
        [self.submitButton setTitle:@"Receive" forState:UIControlStateNormal];
    }else{
        [self.submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    }
    
    self.scanButton.userInteractionEnabled = YES;
    
    [self initializeProductTable];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        [self preLoadVosData];
    }
    
    [self.productListTable reloadData];
    [self getProductUoms];
    [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailabilityRP) userInfo:nil repeats:NO];
    
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    self.enterUpcText.text=@"";
    
    //check if there is a multiple UPC product selected.
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]){
        [self processBarcode:[[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]];
    }
    
    self.docNoLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"];

    //new function to check if the VOS has been changed
    [self accomodateVOSchange];

    [self.scanner addDelegate:self];
    [self.scanner connect];

}


-(void)viewDidAppear:(BOOL)animated{
    if( [[NSUserDefaults standardUserDefaults]  valueForKey:@"VENDOR_PRODUCT"]){
        [self processBarcode:[[NSUserDefaults standardUserDefaults]  valueForKey:@"VENDOR_PRODUCT"]];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
    
    [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"VENDOR_PRODUCT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}

-(void)accomodateVOSchange{

    NSError * error;
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];

    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@ AND status = %@",self.documentId, @"Created"];

    [request setPredicate:predicate];
    [request setEntity:entityDesc];
    NSArray *docTableResults= [context executeFetchRequest:request error:&error];

    if(docTableResults.count>0){
        //document already exist, check if the vos is the same as the document no in memory
        if(![[docTableResults.firstObject valueForKey:@"documentNo"]isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]]){
            //great,, the VOS changed..so..
            [docTableResults.firstObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKeyPath:@"documentNo"];
            [context save:&error];

            //also change the documentNo field in the TempdocStatus Table

            NSFetchRequest *tempDocRequest = [[NSFetchRequest alloc]init];
            NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];

            [tempDocRequest setPredicate:docPredicate];
            [tempDocRequest setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];

            NSError *error;
            NSArray *results= [context executeFetchRequest:tempDocRequest error:&error];

            if(results.count>0){
                [results.firstObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKeyPath:@"documentNo"];
            }
            [context save:&error];
            //load the VOS data in memory again..
            [self preLoadVosData];

        }else NSLog(@"No change in the VOS data..");

    }else NSLog(@"Could not find any record for VOS");

}



-(void)getProductUoms{
    NSPredicate *uomPredicate = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"UomList"];
    NSObject *uomOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:uomPredicate] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[uomOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[uomOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    
    NSError * serializerError;
    
    self.uomArray = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]
                                                    options:0 error:&serializerError];
    if(serializerError){
        NSLog(@"Serializer error, data file not downloaded properly..");
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }
}

-(void)preLoadVosData{
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
//        [self startSpinner:@"Loading Data..."];
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:YES];
        NSPredicate *vosPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"vosList"];
        NSObject *vosOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:vosPred] objectAtIndex:0];
        
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];
        
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[vosOffset valueForKey:@"O"] intValue]];
        
        databuffer = [file readDataOfLength: [[vosOffset valueForKey:@"L"] intValue]];
        [file closeFile];
        
        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        
        NSError * error;
        NSArray * vosList = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
        
        if(error){
            NSLog(@"Serializer error, data file not downloaded properly..");
            [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
        }else{
            NSPredicate *transferNoPred = [NSPredicate predicateWithFormat:@"VosNumber contains %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]];
            NSArray *filteredVosArray = [vosList filteredArrayUsingPredicate:transferNoPred];
            
            if(filteredVosArray.count==0){
                //no receiving object found in the data file..
                
                [self noVosFoundAlert];
            }else{
                self.receivingObject = [filteredVosArray objectAtIndex:0];
            }
        }
//        [self stopSpinner];
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
    }
}

-(void)initializeProductTable{
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){

    }
    
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"] || [[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_CREATION"]){
        
//        [self startSpinner:@"Loading Data..."];
//        self.productList = [[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"];
//        [self stopSpinner];
    }
    
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
//        [self startSpinner:@"Loading Data..."];
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:YES];
//        self.productList = [[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"];
        
        NSPredicate *transferPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"transferList"];
        NSObject *transferOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:transferPred] objectAtIndex:0];
        
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];
        
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[transferOffset valueForKey:@"O"] intValue]];
        
        databuffer = [file readDataOfLength: [[transferOffset valueForKey:@"L"] intValue]];
        [file closeFile];
        
        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        //        NSArray * transferList = [[NSArray alloc]init];
        
        NSError * error;
        
        NSArray * transferList = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
        
        if(error){
            NSLog(@"Serializer error, data file not downloaded properly..");
            [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
        }else{
            NSPredicate *transferNoPred = [NSPredicate predicateWithFormat:@"TransferNumber contains %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]];
            NSArray *transferNoResult = [transferList filteredArrayUsingPredicate:transferNoPred];
            
            if(transferNoResult.count==0){
//                [self stopSpinner];
                [self noTransferFoundAlert];
            }else{
                self.receivingObject = [transferNoResult objectAtIndex:0];

                if([self.receivingObject valueForKey:@"Note"]!=[NSNull null]){
                    if([[self.receivingObject valueForKey:@"Note"] rangeOfString:[[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] stringByAppendingString:@"_Cafe"]].location !=NSNotFound){
                        self.cafeTransfer=YES;
                    }else{
                        self.cafeTransfer=NO;
                    }
                }else{
                    self.cafeTransfer=NO;
                }
                
                for(NSObject * product in [self.receivingObject valueForKey:@"TransferItems"]){
                    NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
                    [newProduct setValue:[product valueForKey:@"Name"] forKey:@"Name"];
                    [newProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                    [newProduct setValue:[product valueForKey:@"Sku"] forKey:@"Sku"];
                    [newProduct setValue:[NSNumber numberWithDouble:[[product valueForKey:@"Quantity"] doubleValue]] forKey:@"Quantity"];
                    [newProduct setValue:[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] forKey:@"Uom"];
                    [newProduct setValue:[[[[product valueForKey:@"Quantity"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1]] forKey:@"QuantityText"];
                    //            [self.auditedProducts addObject:newProduct];
                    [self.auditedProducts insertObject:newProduct atIndex:0];
                }
            }
        }
//        [self stopSpinner];
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
    }
}

#pragma mark - text field delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if([self.enterUpcText hasText]){
        if([[self.enterUpcText text]length]<5)
            [self showErrorAlert:@"Please enter a 5 digit code"];
        else
            [self processBarcode:self.enterUpcText.text];
        
    }else{
        [self showErrorAlert:@"Please Enter Valid product ID or UPC"];
    }
    return NO;
}

#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailabilityRP{
    if([self.scanner connstate]==1){
        [self.scanButton setTitle:@"" forState:UIControlStateNormal];
        [self.scanButton setBackgroundColor:nil];
        [self.scanButton setImage:[UIImage imageNamed:@"iPhoneCamera.png"] forState:UIControlStateNormal];
        self.scannerAvailable=FALSE;
//        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
}

-(void)stopScanner
{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}


#pragma mark - alertview and indicator spin handler

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];

        UIViewController *customVC     = [[UIViewController alloc] init];

        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];


        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];


        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];

        [statusAlert setValue:customVC forKey:@"contentViewController"];

        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:^{
            statusAlert = nil;
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        return [self.auditedProducts count];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"]){
        return [self.auditedProducts count];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_CREATION"]){
        return [self.auditedProducts count];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        return[self.auditedProducts count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"receivingProductListCell" forIndexPath:indexPath];

        NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Uom"]];
        NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];
        
        if(gotUom.count>0 && [[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){

            if([[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"size"]){
                cell.textLabel.text = [[[[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Name"]stringByAppendingString:@"-"]stringByAppendingString:[[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"size"]]; ;
            }else{
                cell.textLabel.text = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Name"];
            }

//            cell.detailTextLabel.text = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"QuantityText"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%.03f kg", [[[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Quantity"]doubleValue]];

        }else{
            if([[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"size"]){
                cell.textLabel.text = [[[[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Name"]stringByAppendingString:@" - "]stringByAppendingString:[[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"size"]]; ;
            }else{
                cell.textLabel.text = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Name"];
            }
            
//            cell.detailTextLabel.text = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"QuantityText"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ ea", [[[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"Quantity"]stringValue]];

        }
        
        if([[self.auditedProducts objectAtIndex:indexPath.row]valueForKey:@"note"]==nil){
            cell.accessoryType = UITableViewCellAccessoryNone;
        }else{
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    
    cell.textLabel.numberOfLines=0;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        self.selectedProductId = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"ProductId"];
        [self reAuditAlert];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"]){
        self.selectedProductId = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"ProductId"];
        [self reAuditAlert];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_CREATION"]){
        self.selectedProductId = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"ProductId"];
        [self reAuditAlert];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        self.selectedProductId = [[self.auditedProducts objectAtIndex:indexPath.row] valueForKey:@"ProductId"];
        [self reAuditAlert];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self.auditedProducts removeObjectAtIndex:indexPath.row];
        [self.productListTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    
    return @[deleteAction];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.auditedProducts removeObjectAtIndex:indexPath.row];
        [self.productListTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        
    }
}


#pragma mark - alert creation funcitons

-(void)reAuditAlert{

    self.shouldScanProduct=NO;
    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
    NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];

    UIAlertController * reAuditController = [UIAlertController alertControllerWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"] message:[[@"This Product already has a given quantity of " stringByAppendingString:[[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue]] stringByAppendingString:@",change quantity?"] preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * editAction = [UIAlertAction actionWithTitle:@"EDIT" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        self.shouldScanProduct=YES;
        self.addQuantity=NO;
        self.editQuantity=YES;
        self.scannedProduct =[[[[[BarcodeProcessor alloc]init] processBarcode:self.selectedProductId] valueForKey:@"productArray"] objectAtIndex:0];
        [self productQuantityAlert];
    }];
    UIAlertAction * addAction = [UIAlertAction actionWithTitle:@"ADD" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        self.shouldScanProduct=YES;
        self.addQuantity=YES;
        self.editQuantity=NO;
        self.scannedProduct =[[[[[BarcodeProcessor alloc]init] processBarcode:self.selectedProductId] valueForKey:@"productArray"] objectAtIndex:0];
        [self productQuantityAlert];
    }];
    UIAlertAction * noteAction = [UIAlertAction actionWithTitle:@"NOTE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        self.shouldScanProduct=YES;
        [self productNoteAlert];
    }];

    UIAlertAction * photoAction = [UIAlertAction actionWithTitle:@"PHOTO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        self.shouldScanProduct=YES;
        self.productPhoto=YES;
        self.invoicePhoto = NO;
        self.barcodeRead = NO;
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        [imagePickerController setDelegate:self];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];

    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        self.shouldScanProduct=YES;
    }];

    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"]){
        [reAuditController addAction:editAction];
        [reAuditController addAction:addAction];
        [reAuditController addAction:noteAction];
        [reAuditController addAction:photoAction];
        [reAuditController addAction:cancelAction];

    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_CREATION"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        [reAuditController addAction:editAction];
        [reAuditController addAction:addAction];
        [reAuditController addAction:cancelAction];
    }
    [self presentViewController:reAuditController animated:YES completion:nil];

}

//
//-(void)reAuditAlert{
//
//    self.shouldScanProduct=NO;
//
//    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"receiveWithOrderSheet"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"receiveWithoutOrderSheet"]){
//
//        NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
//        NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];
//
//        NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", [[selectedProductResult objectAtIndex:0] valueForKey:@"Uom"]];
//        NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];
//
//        if(gotUom.count>0){
//            if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){
//
//                UIAlertView * reAuditAlert = [[UIAlertView alloc] initWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
//                                                                        message:[[@"This Product already has a given quantity of " stringByAppendingString:[[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue]] stringByAppendingString:@",change quantity?"]
//                                                                       delegate:self
//                                                              cancelButtonTitle:@"NO"
//                                                              otherButtonTitles:@"EDIT",@"ADD",@"NOTE",@"PHOTO", Nil];
//                reAuditAlert.tag = reAuditPrompt;
//                [reAuditAlert show];
//            }else{
//                UIAlertView * reAuditAlert = [[UIAlertView alloc] initWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
//                                                                        message:[[@"This Product already has a given quantity of " stringByAppendingString:[[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue]] stringByAppendingString:@",change quantity?"]
//                                                                       delegate:self
//                                                              cancelButtonTitle:@"NO"
//                                                              otherButtonTitles:@"EDIT",@"ADD", @"NOTE",@"PHOTO", Nil];
//                reAuditAlert.tag = reAuditPrompt;
//                [reAuditAlert show];
//            }
//
//        }else{
//            UIAlertView * reAuditAlert = [[UIAlertView alloc] initWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
//                                                                    message:[[@"This Product already has a given quantity of " stringByAppendingString:[[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue]] stringByAppendingString:@",change quantity?"]
//                                                                   delegate:self
//                                                          cancelButtonTitle:@"NO"
//                                                          otherButtonTitles:@"EDIT",@"ADD",@"NOTE",@"PHOTO", Nil];
//            reAuditAlert.tag = reAuditPrompt;
//            [reAuditAlert show];
//        }
//
//    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"createTransfer"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"receiveTransfer"]){
//
//        NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
//        NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];
//
//        UIAlertView * reAuditAlert = [[UIAlertView alloc] initWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
//                                                                message:[[@"This Product already has a given quantity of " stringByAppendingString:[[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue]] stringByAppendingString:@",change quantity?"]
//                                                               delegate:self
//                                                      cancelButtonTitle:@"NO"
//                                                      otherButtonTitles:@"EDIT",@"ADD", Nil];
//        reAuditAlert.tag = reAuditPrompt;
//        [reAuditAlert show];
//
//    }
//}

//-(void)grandTotalPrompt{
//    UIAlertView * grandTotalAlert = [[UIAlertView alloc] initWithTitle:@"Audit Completed"
//                                                               message:@"Please Enter Grand Total"
//                                                              delegate:self
//                                                     cancelButtonTitle:nil
//                                                     otherButtonTitles:@"Ok",@"Cancel", Nil];
//
//    grandTotalAlert.tag=grandTotalPrompt;
//    grandTotalAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//    [grandTotalAlert textFieldAtIndex:0].placeholder = @"Grand Total";
//    [[grandTotalAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
//    [[grandTotalAlert textFieldAtIndex:0] becomeFirstResponder];
//    [grandTotalAlert show];
//}

-(void)pushToCMPrompt{
    self.shouldScanProduct=NO;
    UIAlertController * pushToCMAlert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to force this invoice to category management?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.pushToCM = YES;
        self.shouldScanProduct=YES;
        [self documentNoteAlert];
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.pushToCM = NO;
        self.shouldScanProduct=YES;
        [self documentNoteAlert];
    }];

    [pushToCMAlert addAction:yesAction];
    [pushToCMAlert addAction:noAction];
    [self presentViewController:pushToCMAlert animated:YES completion:nil];
}

-(void)documentNoteAlert{
    
    self.shouldScanProduct=NO;

    UIAlertController * documentNoteAlert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to add a note for the document? (You must have a note if you wish to push the document to Catagory Management)" preferredStyle:UIAlertControllerStyleAlert];

    [documentNoteAlert addTextFieldWithConfigurationHandler:^(UITextField* noteTextField){
        [noteTextField setKeyboardType:UIKeyboardTypeDefault];
        [noteTextField setAutocorrectionType:UITextAutocorrectionTypeYes];
        noteTextField.placeholder = @"Note for the document";
        [noteTextField becomeFirstResponder];
    }];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        self.shouldScanProduct=YES;
        UITextField * noteText = documentNoteAlert.textFields.firstObject;
        if([noteText hasText]){
            [self.notes addObject:noteText.text];

            //save to DB, note that is for the CM to push the document

            NSFetchRequest *tempDocRequest = [[NSFetchRequest alloc]init];
            NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];

            [tempDocRequest setPredicate:docPredicate];
            [tempDocRequest setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];

            NSError *error;
            NSArray *results= [context executeFetchRequest:tempDocRequest error:&error];

            for (NSManagedObject * obj in results){
                [obj setValue:noteText.text forKey:@"vosCmNote"];
            }
            [context save:&error];

            //push to CM note saved to temp status DB

            [self finishAuditAlert];
        }else{
            [self documentNoteAlert];
        }
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        if(self.notes.count==0 && self.pushToCM)
            [self documentNoteAlert];
        else
            [self finishAuditAlert];
    }];

    [documentNoteAlert addAction:yesAction];
    [documentNoteAlert addAction:noAction];
    [self presentViewController:documentNoteAlert animated:YES completion:nil];

}


-(void)trasnferForCafeAlert{
    
    UIAlertController * cafeTransferAlertController = [UIAlertController alertControllerWithTitle:nil message:@"Is this transfer intended for Cafe?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.notes addObject:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"TO_LOCATION"] valueForKey:@"LocationId"] stringByAppendingString:@"_Cafe"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self transferNoteAlert];
        });
    }];
    
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self transferNoteAlert];
        });
    }];
    
    [cafeTransferAlertController addAction:yesAction];
    [cafeTransferAlertController addAction:noAction];
    
    [self presentViewController:cafeTransferAlertController animated:YES completion:nil];

}


-(void)transferNoteAlert{
    self.shouldScanProduct=NO;

    UIAlertController * transferNoteAlert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to add a note for the transfer?" preferredStyle:UIAlertControllerStyleAlert];

    [transferNoteAlert addTextFieldWithConfigurationHandler:^(UITextField* noteTextField){
        [noteTextField setKeyboardType:UIKeyboardTypeDefault];
        [noteTextField setAutocorrectionType:UITextAutocorrectionTypeYes];
        noteTextField.placeholder = @"Note for the document";
        [noteTextField becomeFirstResponder];
    }];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        self.shouldScanProduct=YES;

        UITextField * noteText = transferNoteAlert.textFields.firstObject;
        if([noteText hasText]){
            [self.notes addObject:noteText.text];
            //not saving this to DB
            [self finishAuditAlert];
        }else{
            [self transferNoteAlert];
        }
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        [self finishAuditAlert];
    }];

    [transferNoteAlert addAction:yesAction];
    [transferNoteAlert addAction:noAction];
    [self presentViewController:transferNoteAlert animated:YES completion:nil];
}

-(void)productQuantityAlert{
    self.shouldScanProduct = NO;

    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
    NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];

    UIAlertController * quantityAlertController ;
    __block NSString * msg = @"Please enter product quantity";

    if(selectedProductResult.count>0){
        quantityAlertController = [UIAlertController alertControllerWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"] message:msg preferredStyle:UIAlertControllerStyleAlert];
        [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            if(self.addQuantity){
                textField.text = @"";
            }else if(self.editQuantity){
                textField.text = [[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue];
            }
            if([self.scannedProduct.Uom isEqualToString:@"ea"]){
                [textField setKeyboardType:UIKeyboardTypeNumberPad];
            }else{
                NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", [[selectedProductResult objectAtIndex:0] valueForKey:@"Uom"]];
                NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];
                if(gotUom.count>0){
                    if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){
                        msg = @"Please enter product quantity in KG";
                    }else{
                        msg = [@"Please enter product quantity in " stringByAppendingString:[[selectedProductResult objectAtIndex:0] valueForKey:@"Uom"]];
                    }
                    [textField setKeyboardType:UIKeyboardTypeDecimalPad];
                }else{
                    [textField setKeyboardType:UIKeyboardTypeNumberPad];
                }
            }
            [textField becomeFirstResponder];
        }];
        quantityAlertController.message = msg;

    }else{
        quantityAlertController = [UIAlertController alertControllerWithTitle:self.scannedProduct.ProductName message:msg preferredStyle:UIAlertControllerStyleAlert];
        [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            if([self.scannedProduct.Uom isEqualToString:@"ea"]){
                [textField setKeyboardType:UIKeyboardTypeNumberPad];
                textField.placeholder = @"Product Quantity";
            }else{
                NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", self.scannedProduct.Uom];
                NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];
                if(gotUom.count>0){
                    if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){
                        msg = @"Please enter product quantity in KG";
                        textField.placeholder = @"Product Quantity in kg";
                    }else{
                        msg = [@"Please enter product quantity in " stringByAppendingString:
                               self.scannedProduct.Uom];
                        textField.placeholder = [@"Product Quantity in " stringByAppendingString:self.scannedProduct.Uom];
                    }
                    [textField setKeyboardType:UIKeyboardTypeDecimalPad];
                }else{
                    [textField setKeyboardType:UIKeyboardTypeNumberPad];
                }
            }
            [textField becomeFirstResponder];
        }];
        quantityAlertController.message = msg;
    }


    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //quantity now entered

        self.shouldScanProduct=YES;
        UITextField * textField = quantityAlertController.textFields.firstObject;

        if([textField hasText]){
            NSLog(@"%@ quantity entered for product: %@",textField.text, self.selectedProductId);

            self.shouldScanProduct = YES;
            self.enterUpcText.text = @"";

            NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
            NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];

            if(alreadyProduct.count>0){

                if([[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]isEqualToString:@"ea"]){

                    double newQuantity = 0.0;

                    if(self.addQuantity)
                        newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[textField text] doubleValue];
                    else if(self.editQuantity)
                        newQuantity = [[textField text] doubleValue];
                    else
                        newQuantity = [[textField text] doubleValue];

                    self.addEditedQuantity = (int)newQuantity;

                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]){

                        NSPredicate * locationPredicate=[NSPredicate predicateWithFormat:@"Location.LocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
                        NSArray * foundLocation = [self.scannedProduct.InfoByStore filteredArrayUsingPredicate:locationPredicate];

                        if(foundLocation.count==0){
                            //do nothing
                        }else{
                            StoreSpecificInfo * store = [foundLocation objectAtIndex:0];
                            if([store.OrderFullCases boolValue]){
                                //this product is ordered in full cases

                                if((int)newQuantity%[self.scannedProduct.CaseQuantity intValue]==0){
                                    //quantity matches..

                                    if(self.addQuantity){
                                        double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[textField text] doubleValue];
                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
                                        [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.00f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                        //                                self.addQuantity=NO;
                                    }else if(self.editQuantity){
                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                                        [[alreadyProduct objectAtIndex:0] setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                        //                                self.editQuantity=NO;
                                    }else{
                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                                        [[alreadyProduct objectAtIndex:0] setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                    }

                                }else{
                                    [self showIncorrectCaseQuantityAlert];
                                }
                            }else{
                                if(self.addQuantity){
                                    double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[textField text] doubleValue];
                                    [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
                                    [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.00f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                    //                                self.addQuantity=NO;
                                }else if(self.editQuantity){
                                    [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                                    [[alreadyProduct objectAtIndex:0] setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                    //                                self.editQuantity=NO;
                                }
                            }
                        }
                    }else{

                        //new added code for transfer addition and edit

                        if(self.addQuantity){
                            double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[textField text] doubleValue];
                            [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
                            [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.00f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                            //                                    self.addQuantity=NO;
                        }else if(self.editQuantity){
                            [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                            [[alreadyProduct objectAtIndex:0] setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                            //                                    self.editQuantity=NO;
                        }
                        [self.productListTable reloadData];

                    }

                    [self.productListTable reloadData];
                }else{
                    NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", [[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]];
                    NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];

                    if(gotUom.count>0){
                        if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){
                            if(self.addQuantity){
                                double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[textField text] doubleValue];
                                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
                                [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.02f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:@"kg"] forKey:@"QuantityText"];
                                //                                        self.addQuantity=NO;
                            }else if(self.editQuantity){
                                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                                [[alreadyProduct objectAtIndex:0] setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:@"kg"] forKey:@"QuantityText"];
                                //                                        self.editQuantity=NO;
                            }
                            [self.productListTable reloadData];

                        }else{
                            if(self.addQuantity){
                                double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[textField text] doubleValue];
                                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
                                [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.02f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                //                                        self.addQuantity=NO;
                            }else if(self.editQuantity){
                                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                                [[alreadyProduct objectAtIndex:0] setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                //                                        self.editQuantity=NO;
                            }
                            [self.productListTable reloadData];
                        }
                    }else{
                        if(self.addQuantity){
                            double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[textField text] doubleValue];
                            [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
                            [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                            //                                    self.addQuantity=NO;
                        }else if(self.editQuantity){
                            [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                            [[alreadyProduct objectAtIndex:0] setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
                            //                                    self.editQuantity=NO;
                        }
                        [self.productListTable reloadData];
                    }
                }

            }else{

                if([self.scannedProduct.Uom isEqualToString:@"ea"]){
                    NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
                    [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
                    [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
                    [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
                    [newProduct setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                    [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
                    [newProduct setValue:self.scannedProduct.Uom forKey:@"selectedUom"];

                    if(self.scannedProduct.Size){
                        [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
                    }

                    [newProduct setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                    //                                [self.auditedProducts addObject:newProduct];
                    [self.auditedProducts insertObject:newProduct atIndex:0];

                    //code for not case quantity

                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]){

                        NSPredicate * locationPredicate=[NSPredicate predicateWithFormat:@"Location.LocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
                        NSArray * foundLocation = [self.scannedProduct.InfoByStore filteredArrayUsingPredicate:locationPredicate];

                        if(foundLocation.count==0){
                            //do nothing
                        }else{
                            StoreSpecificInfo * store = [foundLocation objectAtIndex:0];
                            if([store.OrderFullCases boolValue]){
                                //this product is ordered in full cases

                                if([[textField text]intValue]%[self.scannedProduct.CaseQuantity intValue]==0){
                                    //quantity matches..
                                }else{
                                    self.addQuantity=NO;
                                    self.editQuantity=NO;
                                    [self showIncorrectCaseQuantityAlert];
                                }
                            }else{
                                //do nothing
                            }
                        }
                    }

                    [self.productListTable reloadData];


                }else{

                    NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", self.scannedProduct.Uom];
                    NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];

                    if(gotUom.count>0){
                        if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){

                            NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
                            [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
                            [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
                            [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
                            [newProduct setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                            [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
                            [newProduct setValue:self.scannedProduct.Uom forKey:@"selectedUom"];
                            if(self.scannedProduct.Size){
                                [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
                            }
                            [newProduct setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:@"kg"] forKey:@"QuantityText"];
                            //                                        [self.auditedProducts addObject:newProduct];
                            [self.auditedProducts insertObject:newProduct atIndex:0];
                            [self.productListTable reloadData];

                        }else{

                            NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
                            [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
                            [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
                            [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
                            [newProduct setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                            [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
                            [newProduct setValue:self.scannedProduct.Uom forKey:@"selectedUom"];
                            if(self.scannedProduct.Size){
                                [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
                            }
                            [newProduct setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                            //                                        [self.auditedProducts addObject:newProduct];
                            [self.auditedProducts insertObject:newProduct atIndex:0];
                            [self.productListTable reloadData];

                        }

                    }else{
                        NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
                        [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
                        [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
                        [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
                        [newProduct setValue:[NSNumber numberWithDouble:[[textField text] doubleValue]] forKey:@"Quantity"];
                        [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
                        [newProduct setValue:self.scannedProduct.Uom forKey:@"selectedUom"];
                        if(self.scannedProduct.Size){
                            [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
                        }
                        [newProduct setValue:[[[textField text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                        //                                    [self.auditedProducts addObject:newProduct];
                        [self.auditedProducts insertObject:newProduct atIndex:0];
                        [self.productListTable reloadData];
                    }
                }
            }

            //save product to temp DB

            NSPredicate * quantityTextPredicate = [NSPredicate predicateWithFormat:@"ProductId == %@",self.selectedProductId];
            NSArray * findProduct = [self.auditedProducts filteredArrayUsingPredicate:quantityTextPredicate];

            NSString * quantityText = [[findProduct objectAtIndex:0]valueForKey:@"QuantityText"];

            NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
            NSPredicate *productPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND productId = %@",self.documentId, self.selectedProductId];

            [productRequest setPredicate:productPredicate];
            [productRequest setEntity:[NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context]];

            NSError *error;
            NSArray *results= [context executeFetchRequest:productRequest error:&error];

            if(results.count==0){
                NSManagedObject *saveProduct = [NSEntityDescription
                                                insertNewObjectForEntityForName:@"ReceivingTempProdList"
                                                inManagedObjectContext:context];


                [saveProduct setValue:self.selectedProductId forKey:@"productId"];
                [saveProduct setValue:[NSNumber numberWithDouble:[[[findProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]] forKey:@"quantity"];
                [saveProduct setValue:self.documentId forKey:@"documentId"];
                [saveProduct setValue:[[findProduct objectAtIndex:0] valueForKey:@"Uom"] forKey:@"uom"];
                [saveProduct setValue:[[findProduct objectAtIndex:0] valueForKey:@"Name"] forKey:@"productName"];
                [saveProduct setValue:[[findProduct objectAtIndex:0] valueForKey:@"Sku"] forKey:@"sku"];
                [saveProduct setValue:quantityText forKey:@"quantityText"];
                [saveProduct setValue:[[findProduct objectAtIndex:0] valueForKey:@"size"]?[[findProduct objectAtIndex:0] valueForKey:@"size"]:NULL forKey:@"size"];
                [context save:&error];
            }else{
                for (NSManagedObject * obj in results){
                    [obj setValue:[NSNumber numberWithDouble:[[[findProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]] forKey:@"quantity"];
                    [obj setValue:quantityText forKey:@"quantityText"];
                }
                [context save:&error];
            }
            NSLog(@"product saved to temp DB...");
        }else{
            [self productQuantityAlert];
        }

   }];

    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        [self dismissViewControllerAnimated:YES completion:nil];
        self.selectedProductId=nil;
        self.scannedProduct = nil;

        self.enterUpcText.text = @"";

        if(self.editQuantity || self.addQuantity){

        }else{
            //delete the product if there

            NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
            NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];

            if(alreadyProduct.count>0){
                //delete the product
                [self.auditedProducts removeObject:[alreadyProduct objectAtIndex:0]];
                [self.productListTable reloadData];
            }
        }

    }];

    [quantityAlertController addAction:okAction];
    [quantityAlertController addAction:cancelAction];
    [self presentViewController:quantityAlertController animated:YES completion:nil];

}

//-(void)productQuantityAlert{
//
//    self.shouldScanProduct = NO;
//
//        NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
//        NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];
//
//    if(selectedProductResult.count>0){
//            if([self.scannedProduct.Uom isEqualToString:@"ea"]){
//                UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
//                                                                         message:@"Please enter product quantity"
//                                                                        delegate:self
//                                                               cancelButtonTitle:nil
//                                                               otherButtonTitles:@"Ok", @"Cancel",Nil];
//
//                quantityAlert.tag=quantityPrompt;
//                quantityAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//                if(self.addQuantity){
//                    [quantityAlert textFieldAtIndex:0].text = @"";
//                }else if(self.editQuantity){
//                    [quantityAlert textFieldAtIndex:0].text = [[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue];
//                }
//                [[quantityAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
//                [[quantityAlert textFieldAtIndex:0] becomeFirstResponder];
//                [quantityAlert show];
//
//            }else{
//
//                NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", [[selectedProductResult objectAtIndex:0] valueForKey:@"Uom"]];
//                NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];
//
//                if(gotUom.count>0){
//                    if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){
//                        UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
//                                                                                 message:@"Please enter product quantity in kg"
//                                                                                delegate:self
//                                                                       cancelButtonTitle:nil
//                                                                       otherButtonTitles:@"Ok", @"Cancel",Nil];
//
//                        quantityAlert.tag=quantityPrompt;
//                        quantityAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//
//                        if(self.addQuantity){
//                            [quantityAlert textFieldAtIndex:0].text = @"";
//                        }else if(self.editQuantity){
//                            [quantityAlert textFieldAtIndex:0].text = [[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue];
//                        }
//
//                        [[quantityAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDecimalPad];
//                        [[quantityAlert textFieldAtIndex:0] becomeFirstResponder];
//                        [quantityAlert show];
//
//                    }else{
//                        UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
//                                                                                 message:[@"Please enter product quantity in " stringByAppendingString:[[selectedProductResult objectAtIndex:0] valueForKey:@"Uom"]]
//                                                                                delegate:self
//                                                                       cancelButtonTitle:nil
//                                                                       otherButtonTitles:@"Ok", @"Cancel",Nil];
//                        quantityAlert.tag=quantityPrompt;
//                        quantityAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//
//                        if(self.addQuantity){
//                            [quantityAlert textFieldAtIndex:0].text = @"";
//                        }else if(self.editQuantity){
//                            [quantityAlert textFieldAtIndex:0].text = [[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue];
//                        }
//
//                        [[quantityAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDecimalPad];
//                        [[quantityAlert textFieldAtIndex:0] becomeFirstResponder];
//                        [quantityAlert show];
//                    }
//                }else{
//                    UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"]
//                                                                             message:@"Please enter product quantity"
//                                                                            delegate:self
//                                                                   cancelButtonTitle:nil
//                                                                   otherButtonTitles:@"Ok", @"Cancel",Nil];
//                    quantityAlert.tag=quantityPrompt;
//                    quantityAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//
//                    if(self.addQuantity){
//                        [quantityAlert textFieldAtIndex:0].text = @"";
//                    }else if(self.editQuantity){
//                        [quantityAlert textFieldAtIndex:0].text = [[[selectedProductResult objectAtIndex:0] valueForKey:@"Quantity"] stringValue];
//                    }
//
//                    [[quantityAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
//                    [[quantityAlert textFieldAtIndex:0] becomeFirstResponder];
//                    [quantityAlert show];
//                }
//            }
//
//        }
//
//        else{
//            if([self.scannedProduct.Uom isEqualToString:@"ea"]){
//                UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:self.scannedProduct.ProductName
//                                                                         message:@"Please enter product quantity"
//                                                                        delegate:self
//                                                               cancelButtonTitle:nil
//                                                               otherButtonTitles:@"Ok", @"Cancel",Nil];
//
//                quantityAlert.tag=quantityPrompt;
//                quantityAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//                [quantityAlert textFieldAtIndex:0].placeholder = @"Product Quantity";
//                [[quantityAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
//                [[quantityAlert textFieldAtIndex:0] becomeFirstResponder];
//                [quantityAlert show];
//
//            }else{
//
//                NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", self.scannedProduct.Uom];
//                NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];
//
//                if(gotUom.count>0){
//
//                    if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){
//                        UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:self.scannedProduct.ProductName
//                                                                                 message:@"Please enter product quantity in kg"
//                                                                                delegate:self
//                                                                       cancelButtonTitle:nil
//                                                                       otherButtonTitles:@"Ok", @"Cancel",Nil];
//
//                        quantityAlert.tag=quantityPrompt;
//                        quantityAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//                        [quantityAlert textFieldAtIndex:0].placeholder = @"Product Quantity in kg";
//                        [[quantityAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDecimalPad];
//                        [[quantityAlert textFieldAtIndex:0] becomeFirstResponder];
//                        [quantityAlert show];
//
//                    }else{
//                        UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:self.scannedProduct.ProductName
//                                                                                 message:[@"Please enter product quantity in " stringByAppendingString:self.scannedProduct.Uom]
//                                                                                delegate:self
//                                                                       cancelButtonTitle:nil
//                                                                       otherButtonTitles:@"Ok", @"Cancel",Nil];
//                        quantityAlert.tag=quantityPrompt;
//                        quantityAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//                        [quantityAlert textFieldAtIndex:0].placeholder = [@"Product Quantity in " stringByAppendingString:self.scannedProduct.Uom];
//                        [[quantityAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDecimalPad];
//                        [[quantityAlert textFieldAtIndex:0] becomeFirstResponder];
//                        [quantityAlert show];
//
//                    }
//
//                }else{
//                    UIAlertView * quantityAlert = [[UIAlertView alloc] initWithTitle:self.scannedProduct.ProductName
//                                                                             message:@"Please enter product quantity"
//                                                                            delegate:self
//                                                                   cancelButtonTitle:nil
//                                                                   otherButtonTitles:@"Ok", @"Cancel",Nil];
//
//                    quantityAlert.tag=quantityPrompt;
//                    quantityAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//                    [quantityAlert textFieldAtIndex:0].placeholder = @"Product Quantity";
//                    [[quantityAlert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
//                    [[quantityAlert textFieldAtIndex:0] becomeFirstResponder];
//                    [quantityAlert show];
//                }
//            }
//        }
//    }
//

-(void)uomSelectPrompt{
    self.shouldScanProduct=NO;
    
    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
    NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];
    
    NSPredicate * uomPredicate = [NSPredicate predicateWithFormat:@"UomType ==[c] %@", @"UOM_WEIGHT"];
    NSArray * uoms = [self.uomArray filteredArrayUsingPredicate:uomPredicate];

    if(uoms.count > 0){
        UIAlertController * uomController = [UIAlertController alertControllerWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"] message:@"Please select the preffered uom type" preferredStyle:UIAlertControllerStyleAlert];

        for(NSObject * uom in uoms){
            UIAlertAction * alertAction = [UIAlertAction actionWithTitle:[uom valueForKey:@"Name"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.shouldScanProduct=YES;
                [[selectedProductResult objectAtIndex:0] setValue:[uom valueForKey:@"Name"] forKey:@"selectedUom"];
                [self.productListTable reloadData];
            }];
            [uomController addAction:alertAction];
        }

        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            self.shouldScanProduct=YES;
            [self dismissViewControllerAnimated:YES completion:nil];
        }];

        [uomController addAction:cancelAction];
        [self presentViewController:uomController animated:YES completion:nil];
    }
}

-(void)finishAuditAlert{
    self.shouldScanProduct=NO;

    UIAlertController * finishAlert = [UIAlertController alertControllerWithTitle:nil message:[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"TRANSFER_CREATION"]?@"Do you want to ship this transfer?":@"Do you want to finish receiving?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;

        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){

            NSMutableArray * allProducts = [[NSMutableArray alloc]init];
            for(NSDictionary * product in self.auditedProducts){
                NSMutableDictionary * finalProduct = [[NSMutableDictionary alloc]init];
                [finalProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                [finalProduct setValue:[product valueForKey:@"Name"] forKey:@"Name"];
                [finalProduct setValue:[product valueForKey:@"QuantityText"] forKey:@"QuantityText"];
                [allProducts addObject:finalProduct];
            }

            NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
            [receiving setValue:allProducts forKey:@"productList"];
            [receiving setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"imagecounter"];
            [receiving setValue:[NSNumber numberWithBool:self.pushToCM] forKey:@"pushToCM"];
            [receiving setValue:self.notes forKey:@"notes"];
            [receiving setValue:self.documentId forKey:@"documentId"];

            [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
            [[NSUserDefaults standardUserDefaults] synchronize];


            //change DB for discrepency screen revival

            NSFetchRequest *tempDocRequest = [[NSFetchRequest alloc]init];
            NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];

            [tempDocRequest setPredicate:docPredicate];
            [tempDocRequest setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];

            NSError *error;
            NSArray *results= [context executeFetchRequest:tempDocRequest error:&error];

            for (NSManagedObject * obj in results){
                [obj setValue:@"VOS_RECEIVE_DESCR" forKey:@"documentType"];
            }
            [context save:&error];

            [self performSegueWithIdentifier:@"discrepencySegue" sender:self];
        }else{
            [self startSpinner:@"Saving Data"];
            self.endTime = [NSDate date];
            [self saveAuditToFile];

            dispatch_async(dispatch_get_main_queue(), ^{
                self.auditedProducts=Nil;
                self.scannedProduct=Nil;
                self.selectedProductId=Nil;
                self.receivingObject=Nil;
                self.startTime = nil;
                [self dismissViewControllerAnimated:YES completion:^{

                    //for now stop transfer numbers from being created on the phone side..
                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"TRANSFER_CREATION"])
                        [self performSegueWithIdentifier:@"printBarcodeFromReceivingSegue" sender:self];
                    else
                        [self.navigationController popToRootViewControllerAnimated:YES];
                }];

            });
        }
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
    }];

    [finishAlert addAction:yesAction];
    [finishAlert addAction:noAction];
    [self presentViewController:finishAlert animated:YES completion:nil];

}

-(void)takePhotoOfInvoiceAlert{
    self.shouldScanProduct=NO;

    UIAlertController * photoAlert = [UIAlertController alertControllerWithTitle:nil message:@"Please take a photo of the invoice" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        self.invoicePhoto = YES;
        self.barcodeRead = NO;
        self.productPhoto=NO;
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }

        [imagePickerController setDelegate:self];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];

    [photoAlert addAction:okAction];
    [self presentViewController:photoAlert animated:YES completion:nil];


}

-(void)takeNextInvoicePagePhoto{
    self.shouldScanProduct=NO;

    UIAlertController * photoAlert = [UIAlertController alertControllerWithTitle:nil message:@"More Pages to add?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        self.invoicePhoto = YES;
        self.barcodeRead = NO;
        self.productPhoto=NO;
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        [imagePickerController setDelegate:self];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        self.invoicePhoto = NO;
        self.productPhoto=NO;
        [self initializeController];
    }];

    [photoAlert addAction:yesAction];
    [photoAlert addAction:noAction];
    [self presentViewController:photoAlert animated:YES completion:nil];

}

-(void) finishInvoicePhotoAlert{
    self.shouldScanProduct=NO;

    UIAlertController * finishInvoicePhotoAlert = [UIAlertController alertControllerWithTitle:nil message:@"Are there more pages to add?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        self.invoicePhoto = YES;
        self.barcodeRead = NO;
        self.productPhoto=NO;
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        [imagePickerController setDelegate:self];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        self.invoicePhoto = NO;
        self.productPhoto=NO;
        [self initializeController];
    }];

    [finishInvoicePhotoAlert addAction:yesAction];
    [finishInvoicePhotoAlert addAction:noAction];
    [self presentViewController:finishInvoicePhotoAlert animated:YES completion:nil];
}

-(void)takeOtherPhotoAlert{
    self.shouldScanProduct=NO;
    self.invoicePhoto=NO;
    self.productPhoto=NO;

    UIAlertController * photoAlert = [UIAlertController alertControllerWithTitle:nil message:@"Do you need to add any additional photos?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        self.invoicePhoto = NO;
        self.barcodeRead = NO;
        self.productPhoto=NO;
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        [imagePickerController setDelegate:self];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        if(self.notes.count > 0){
            self.pushToCM = YES;
            [self documentNoteAlert];
        }else{
            [self pushToCMPrompt];
        }
    }];

    [photoAlert addAction:yesAction];
    [photoAlert addAction:noAction];
    [self presentViewController:photoAlert animated:YES completion:nil];

}

-(void)takeNextOtherPhoto{
    self.shouldScanProduct=NO;
    self.invoicePhoto=NO;
    self.productPhoto=NO;

    UIAlertController * photoAlert = [UIAlertController alertControllerWithTitle:nil message:@"Take Another Photo?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        self.invoicePhoto = NO;
        self.barcodeRead = NO;
        self.productPhoto=NO;
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        [imagePickerController setDelegate:self];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        [self finishOptionalPhotoAlert];
    }];

    [photoAlert addAction:yesAction];
    [photoAlert addAction:noAction];
    [self presentViewController:photoAlert animated:YES completion:nil];

}

-(void)finishOptionalPhotoAlert{
    self.shouldScanProduct=NO;

    UIAlertController * photoAlert = [UIAlertController alertControllerWithTitle:nil message:@"Finish Taking Photo?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        if(self.notes.count > 0){
            self.pushToCM = YES;
            [self documentNoteAlert];
        }else{
            [self pushToCMPrompt];
        }
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        self.barcodeRead = NO;
        self.invoicePhoto = NO;
        self.productPhoto=NO;
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        [imagePickerController setDelegate:self];
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];

    [photoAlert addAction:yesAction];
    [photoAlert addAction:noAction];
    [self presentViewController:photoAlert animated:YES completion:nil];

}

-(void)productNoteAlert{
    
    self.shouldScanProduct=NO;
    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
    NSArray *selectedProductResult = [self.auditedProducts filteredArrayUsingPredicate:selectedProdPred];

    UIAlertController * productNoteAlert = [UIAlertController alertControllerWithTitle:[[selectedProductResult objectAtIndex:0] valueForKey:@"Name"] message:@"Enter Note" preferredStyle:UIAlertControllerStyleAlert];

    [productNoteAlert addTextFieldWithConfigurationHandler:^(UITextField* noteTextField){
        [noteTextField setKeyboardType:UIKeyboardTypeDefault];
        [noteTextField setAutocorrectionType:UITextAutocorrectionTypeYes];
        noteTextField.placeholder = @"Product Note";
        if([[selectedProductResult objectAtIndex:0]valueForKey:@"note"]!=nil)
            noteTextField.text =[[selectedProductResult objectAtIndex:0]valueForKey:@"note"];
        [noteTextField becomeFirstResponder];
    }];

    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        UITextField * noteText = productNoteAlert.textFields.firstObject;
        NSObject * product = [selectedProductResult objectAtIndex:0];
        if([noteText hasText]){
            [product setValue:noteText.text forKey:@"note"];
            [self.notes addObject:noteText.text];

            //..save notes to temp DB

            NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
            NSPredicate *productPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND productId = %@",self.documentId, self.selectedProductId];

            [productRequest setPredicate:productPredicate];
            [productRequest setEntity:[NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context]];

            NSError *error;
            NSArray *results= [context executeFetchRequest:productRequest error:&error];

            for (NSManagedObject * obj in results){
                [obj setValue:noteText.text forKey:@"note"];
            }
            [context save:&error];

            NSLog(@"product note saved to temp DB...");

        }else{
            [self productNoteAlert];
        }
        [self.productListTable reloadData];
    }];

    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
    }];

    [productNoteAlert addAction:okAction];
    [productNoteAlert addAction:cancelAction];
    [self presentViewController:productNoteAlert animated:YES completion:nil];
}

-(void)noVosFoundAlert{
    self.shouldScanProduct=NO;
    UIAlertController * noVosAlert = [UIAlertController alertControllerWithTitle:nil message:@"No VOS found from this order. Please get the latest data file from home screen" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        [self deleteAuditandGoBack];
    }];

    [noVosAlert addAction:okAction];
    [self presentViewController:noVosAlert animated:YES completion:nil];
}

-(void)noTransferFoundAlert{
    self.shouldScanProduct=NO;

    UIAlertController * noTransferAlert = [UIAlertController alertControllerWithTitle:nil message:@"No Transfer found in data file. Please get the latest data file from home screen" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        [self deleteAuditandGoBack];
    }];

    [noTransferAlert addAction:okAction];
    [self presentViewController:noTransferAlert animated:YES completion:nil];
}

-(void)productNotInVosAlert{
    self.shouldScanProduct=NO;

    UIAlertController * productNotInVosAlert = [UIAlertController alertControllerWithTitle:nil message:@"This Vendor Order Sheet does not contain this product, Sure to receive it?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        NSPredicate * existingProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.scannedProduct.ProductId];
        if([self.auditedProducts filteredArrayUsingPredicate:existingProdPred].count==0){
            self.selectedProductId = self.scannedProduct.ProductId;
            self.scannedProduct =[[[[[BarcodeProcessor alloc]init] processBarcode:self.selectedProductId] valueForKey:@"productArray"] objectAtIndex:0];
            [self productQuantityAlert];
        }else{
            self.selectedProductId = self.scannedProduct.ProductId;
            [self reAuditAlert];
        }
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
    }];

    [productNotInVosAlert addAction:yesAction];
    [productNotInVosAlert addAction:noAction];
    [self presentViewController:productNotInVosAlert animated:YES completion:nil];
}

-(void)showIncorrectCaseQuantityAlert{
    self.shouldScanProduct=NO;

    NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
    NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];

    UIAlertController * incorrectCaseQuantityAlert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@ is only ordered in cases. Are you sure you're receiving the right quantity?",[alreadyProduct.firstObject valueForKey:@"Name"]?[alreadyProduct.firstObject valueForKey:@"Name"]:self.scannedProduct.ProductName ] preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;

        if(alreadyProduct.count>0){
            if(self.addQuantity){
                double newQuantity = (double)self.addEditedQuantity;
                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
                [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.00f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
            }else if(self.editQuantity){
                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:(double)self.addEditedQuantity] forKey:@"Quantity"];
                [[alreadyProduct objectAtIndex:0] setValue:[[[[NSNumber numberWithDouble:(double)self.addEditedQuantity] stringValue] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
            }else{

            }
        }
        [self.productListTable reloadData];
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
//        self.selectedProductId = self.scannedProduct.ProductId;
        self.scannedProduct =[[[[[BarcodeProcessor alloc]init] processBarcode:self.selectedProductId] valueForKey:@"productArray"] objectAtIndex:0];
        [self productQuantityAlert];
    }];

    [incorrectCaseQuantityAlert addAction:yesAction];
    [incorrectCaseQuantityAlert addAction:noAction];
    [self presentViewController:incorrectCaseQuantityAlert animated:YES completion:nil];

}

-(void)wrongVendorAlert{
    self.shouldScanProduct=NO;
    NSString* body=[[NSString alloc] init];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
        
        body = [NSString stringWithFormat:@"The scanned product is not associated with the Selected Vendor, Are you sure you're receiving a VOS from %@ ? ", [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorName"]];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]) {
        
        body = [NSString stringWithFormat:@"The scanned product is not associated with the Selected Vendor, Are you sure you're receiving a receipt from %@ ?", [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorName"]];
    }

    UIAlertController * wrongVendorAlert = [UIAlertController alertControllerWithTitle:nil message:body preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        NSLog(@"User wants to add this non vendor product to list");

        NSPredicate * existingProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.scannedProduct.ProductId];
        if([self.auditedProducts filteredArrayUsingPredicate:existingProdPred].count==0){
            //                    self.scannedProduct = product;
            self.selectedProductId = self.scannedProduct.ProductId;
            self.scannedProduct =[[[[[BarcodeProcessor alloc]init] processBarcode:self.selectedProductId] valueForKey:@"productArray"] objectAtIndex:0];
            [self productQuantityAlert];
        }else{
            self.selectedProductId = self.scannedProduct.ProductId;
            [self reAuditAlert];
        }
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        NSLog(@"User would quit receiving and start over");
        [self performSegueWithIdentifier:@"vosVendorSelectSegue" sender:self];
    }];

    [wrongVendorAlert addAction:yesAction];
    [wrongVendorAlert addAction:noAction];
    [self presentViewController:wrongVendorAlert animated:YES completion:nil];

}

-(void)showErrorAlert:(NSString *)message{
    self.shouldScanProduct=FALSE;
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.shouldScanProduct=TRUE;
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    self.shouldScanProduct=FALSE;
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.shouldScanProduct=TRUE;
                               }];

    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

-(void)showGoBackPromtAlert{
    self.shouldScanProduct=NO;

    UIAlertController * goBackAlert = [UIAlertController alertControllerWithTitle:@"Are You Sure?" message:@"Clicking yes will delete changes you've made to this receipt" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        [self deleteAuditandGoBack];
    }];

    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
    }];

    [goBackAlert addAction:yesAction];
    [goBackAlert addAction:noAction];
    [self presentViewController:goBackAlert animated:YES completion:nil];

}

-(void)showSaveAndBackAlert{
    self.shouldScanProduct=NO;

    UIAlertController * saveAndgoBackAlert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction * saveAction = [UIAlertAction actionWithTitle:@"Save & Exit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        [self saveAuditandGoBack];
    }];

    UIAlertAction * discardAction = [UIAlertAction actionWithTitle:@"Discard & Exit" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        self.shouldScanProduct=YES;
        [self showGoBackPromtAlert];
    }];

    [saveAndgoBackAlert addAction:saveAction];
    [saveAndgoBackAlert addAction:discardAction];
    [self presentViewController:saveAndgoBackAlert animated:YES completion:nil];
}
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//
////    self.shouldScanProduct = YES;
//
////    if(alertView.tag==transferNotePrompt){
////        if(buttonIndex ==0){
////            if([[alertView textFieldAtIndex:0]hasText]){
////                [self.notes addObject:[alertView textFieldAtIndex:0].text];
////
////                //not saving this to DB
////
////                [self finishAuditAlert];
////            }else{
////                [self transferNoteAlert];
////            }
////        }else{
////            [self finishAuditAlert];
////        }
////    }
//
////    if(alertView.tag==takePhotoOfInvoice){
////        if(buttonIndex==0){
////            self.invoicePhoto = YES;
////            self.barcodeRead = NO;
////            self.productPhoto=NO;
////            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
////            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
////            {
////                [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
////            }
////
////            [imagePickerController setDelegate:self];
////            [self presentViewController:imagePickerController animated:YES completion:nil];
////        }
////    }
//
////    if(alertView.tag==takeAnotherPhotoOfInvoice){
////        if(buttonIndex == 1){
//////            [self finishInvoicePhotoAlert];
////
////            self.invoicePhoto = NO;
////            self.productPhoto=NO;
////            [self initializeController];
////
////        }else if(buttonIndex ==0){
////            self.invoicePhoto = YES;
////            self.barcodeRead = NO;
////            self.productPhoto=NO;
////            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
////            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
////            {
////                [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
////            }
////            [imagePickerController setDelegate:self];
////            [self presentViewController:imagePickerController animated:YES completion:nil];
////        }
////    }
//
////    if(alertView.tag == finishInvoicePhoto){
////        if(buttonIndex == 1){
////            self.invoicePhoto = NO;
////            self.productPhoto=NO;
////            [self initializeController];
////        }else if(buttonIndex ==0){
////            self.invoicePhoto = YES;
////            self.barcodeRead = NO;
////            self.productPhoto=NO;
////            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
////            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
////            {
////                [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
////            }
////            [imagePickerController setDelegate:self];
////            [self presentViewController:imagePickerController animated:YES completion:nil];
////        }
////    }
//
////    if(alertView.tag == takeAnotherPhotoOther){
////        if(buttonIndex == 1){
////            [self finishOptionalPhotoAlert];
////        }else if(buttonIndex ==0){
////            self.invoicePhoto = NO;
////            self.barcodeRead = NO;
////            self.productPhoto=NO;
////            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
////            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
////            {
////                [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
////            }
////            [imagePickerController setDelegate:self];
////            [self presentViewController:imagePickerController animated:YES completion:nil];
////        }
////    }
//
////    if(alertView.tag==wrongVendorPrompt){
////        if(buttonIndex==0){
////            NSLog(@"User wants to add this non vendor product to list");
////
////            NSPredicate * existingProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.scannedProduct.ProductId];
////            if([self.auditedProducts filteredArrayUsingPredicate:existingProdPred].count==0){
////                //                    self.scannedProduct = product;
////                self.selectedProductId = self.scannedProduct.ProductId;
////                [self productQuantityAlert];
////            }else{
////                self.selectedProductId = self.scannedProduct.ProductId;
////                [self reAuditAlert];
////            }
////
////        }else{
////            NSLog(@"User would quit receiving and start over");
////            [self performSegueWithIdentifier:@"vosVendorSelectSegue" sender:self];
////
//////            [self showGoBackPromtAlert];
////        }
////    }
//
////    if(alertView.tag == finishOptionalPhoto){
////        if(buttonIndex == 0){
////            if(self.notes.count > 0){
////                self.pushToCM = YES;
////                [self documentNoteAlert];
////            }else{
////                [self pushToCMPrompt];
////            }
////        }else if(buttonIndex ==1){
////            self.barcodeRead = NO;
////            self.invoicePhoto = NO;
////            self.productPhoto=NO;
////            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
////            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
////            {
////                [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
////            }
////            [imagePickerController setDelegate:self];
////            [self presentViewController:imagePickerController animated:YES completion:nil];
////        }
////    }
//
////    if(alertView.tag== takeOptionalPhoto){
////        if(buttonIndex == 0){
////            self.invoicePhoto = NO;
////            self.barcodeRead = NO;
////            self.productPhoto=NO;
////            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
////            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
////            {
////                [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
////            }
////            [imagePickerController setDelegate:self];
////            [self presentViewController:imagePickerController animated:YES completion:nil];
////        }else if(buttonIndex ==1){
////            if(self.notes.count > 0){
////                self.pushToCM = YES;
////                [self documentNoteAlert];
////            }else{
////                [self pushToCMPrompt];
////            }
////        }
////    }
//
////    if(alertView.tag == cMPushPrompt){
////        if(buttonIndex ==0){
////            self.pushToCM = YES;
////            [self documentNoteAlert];
////        }else if(buttonIndex == 1){
////            self.pushToCM = NO;
////            [self documentNoteAlert];
////        }
////    }
//
////    if(alertView.tag == productNotePrompt){
////        if(buttonIndex ==0){
////            NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
////            NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];
////            NSObject * product = [alreadyProduct objectAtIndex:0];
////            if([[alertView textFieldAtIndex:0]hasText]){
////                [product setValue:[alertView textFieldAtIndex:0].text forKey:@"note"];
////                [self.notes addObject:[alertView textFieldAtIndex:0].text];
////
////                //..save notes to temp DB
////
////                NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
////                NSPredicate *productPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND productId = %@",self.documentId, self.selectedProductId];
////
////                [productRequest setPredicate:productPredicate];
////                [productRequest setEntity:[NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context]];
////
////                NSError *error;
////                NSArray *results= [context executeFetchRequest:productRequest error:&error];
////
////                for (NSManagedObject * obj in results){
////                    [obj setValue:[alertView textFieldAtIndex:0].text forKey:@"note"];
////                }
////                [context save:&error];
////
////                NSLog(@"product note saved to temp DB...");
////
////            }else{
////                [self productNoteAlert];
////            }
////            [self.productListTable reloadData];
////        }
////        else{
////        }
////    }
//
////    if(alertView.tag == documentNotePrompt){
////        if(buttonIndex ==0){
////            if([[alertView textFieldAtIndex:0]hasText]){
////                [self.notes addObject:[alertView textFieldAtIndex:0].text];
////
////                //save to DB, note that is for the CM to push the document
////
////                NSFetchRequest *tempDocRequest = [[NSFetchRequest alloc]init];
////                NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
////
////                [tempDocRequest setPredicate:docPredicate];
////                [tempDocRequest setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
////
////                NSError *error;
////                NSArray *results= [context executeFetchRequest:tempDocRequest error:&error];
////
////                for (NSManagedObject * obj in results){
////                    [obj setValue:[alertView textFieldAtIndex:0].text forKey:@"vosCmNote"];
////                }
////                [context save:&error];
////
////                //push to CM note saved to temp status DB
////
////                [self finishAuditAlert];
////            }else{
////                [self documentNoteAlert];
////            }
////        }else{
////            if(self.notes.count==0 && self.pushToCM)
////                [self documentNoteAlert];
////            else
////                [self finishAuditAlert];
////
////        }
////    }
//
////    if(alertView.tag == uomSelectPrompt){
////
////        NSPredicate * uomPredicate = [NSPredicate predicateWithFormat:@"UomType ==[c] %@", @"UOM_WEIGHT"];
////        NSArray * uoms = [self.uomArray filteredArrayUsingPredicate:uomPredicate];
////
////        NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
////        NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];
////
////        [[alreadyProduct objectAtIndex:0] setValue:[[uoms objectAtIndex:buttonIndex] valueForKey:@"Name"] forKey:@"selectedUom"];
////
////        [self.productListTable reloadData];
////    }
//
////    if(alertView.tag == quantityPrompt){
////            if(buttonIndex ==0){
////                if([[alertView textFieldAtIndex:0]hasText]){
////                    NSLog(@"%@ quantity entered for product: %@",[alertView textFieldAtIndex:0].text, self.selectedProductId);
////
////                    self.shouldScanProduct = YES;
////                    self.enterUpcText.text = @"";
////
////                    NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
////                    NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];
////
////                    if(alreadyProduct.count>0){
////
////                        if([[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]isEqualToString:@"ea"]){
////
////                            double newQuantity = 0.0;
////
////                            if(self.addQuantity)
////                                newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[[alertView textFieldAtIndex:0]text] doubleValue];
////                            else if(self.editQuantity)
////                                newQuantity = [[[alertView textFieldAtIndex:0]text] doubleValue];
////                            else
////                                newQuantity = [[[alertView textFieldAtIndex:0] text] doubleValue];
////
////                            self.addEditedQuantity = (int)newQuantity;
////
////                            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"receiveWithOrderSheet"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"receiveWithoutOrderSheet"]){
////
////                                NSPredicate * locationPredicate=[NSPredicate predicateWithFormat:@"Location.LocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
////                                NSArray * foundLocation = [self.scannedProduct.InfoByStore filteredArrayUsingPredicate:locationPredicate];
////
////                                if(foundLocation.count==0){
////                                    //do nothing
////                                }else{
////                                    StoreSpecificInfo * store = [foundLocation objectAtIndex:0];
////                                    if([store.OrderFullCases boolValue]){
////                                        //this product is ordered in full cases
////
////                                        if((int)newQuantity%[self.scannedProduct.CaseQuantity intValue]==0){
////                                            //quantity matches..
////
////                                            if(self.addQuantity){
////                                                double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[[alertView textFieldAtIndex:0]text] doubleValue];
////                                                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
////                                                [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.00f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                                                //                                self.addQuantity=NO;
////                                            }else if(self.editQuantity){
////                                                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                                [[alreadyProduct objectAtIndex:0] setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                                                //                                self.editQuantity=NO;
////                                            }else{
////                                                [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                                [[alreadyProduct objectAtIndex:0] setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                                            }
////
////                                        }else{
////                                            [self showIncorrectCaseQuantityAlert];
////                                        }
////                                    }else{
////                                        if(self.addQuantity){
////                                            double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[[alertView textFieldAtIndex:0]text] doubleValue];
////                                            [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
////                                            [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.00f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                                            //                                self.addQuantity=NO;
////                                        }else if(self.editQuantity){
////                                            [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                            [[alreadyProduct objectAtIndex:0] setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                                            //                                self.editQuantity=NO;
////                                        }
////                                    }
////                                }
////                            }else{
////
////                                    //new added code for transfer addition and edit
////
////                                    if(self.addQuantity){
////                                        double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[[alertView textFieldAtIndex:0]text] doubleValue];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.00f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                                        //                                    self.addQuantity=NO;
////                                    }else if(self.editQuantity){
////                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                                        //                                    self.editQuantity=NO;
////                                    }
////                                    [self.productListTable reloadData];
////
////                            }
////
////                            [self.productListTable reloadData];
////                        }else{
////                            NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", [[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]];
////                            NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];
////
////                            if(gotUom.count>0){
////                                if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){
////                                    if(self.addQuantity){
////                                        double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[[alertView textFieldAtIndex:0]text] doubleValue];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.02f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:@"kg"] forKey:@"QuantityText"];
//////                                        self.addQuantity=NO;
////                                    }else if(self.editQuantity){
////                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:@"kg"] forKey:@"QuantityText"];
//////                                        self.editQuantity=NO;
////                                    }
////                                    [self.productListTable reloadData];
////
////                                }else{
////                                    if(self.addQuantity){
////                                        double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[[alertView textFieldAtIndex:0]text] doubleValue];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.02f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
//////                                        self.addQuantity=NO;
////                                    }else if(self.editQuantity){
////                                        [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                        [[alreadyProduct objectAtIndex:0] setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
//////                                        self.editQuantity=NO;
////                                    }
////                                    [self.productListTable reloadData];
////                                }
////                            }else{
////                                if(self.addQuantity){
////                                    double newQuantity = [[[alreadyProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]+ [[[alertView textFieldAtIndex:0]text] doubleValue];
////                                    [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
////                                    [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
//////                                    self.addQuantity=NO;
////                                }else if(self.editQuantity){
////                                    [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                    [[alreadyProduct objectAtIndex:0] setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
//////                                    self.editQuantity=NO;
////                                }
////                                [self.productListTable reloadData];
////                            }
////                        }
////
////                    }else{
////
////                        if([self.scannedProduct.Uom isEqualToString:@"ea"]){
////                            NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
////                            [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
////                            [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
////                            [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
////                            [newProduct setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                            [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
////                            [newProduct setValue:self.scannedProduct.Uom forKey:@"selectedUom"];
////
////                            if(self.scannedProduct.Size){
////                                [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
////                            }
////
////                            [newProduct setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
////                            //                                [self.auditedProducts addObject:newProduct];
////                            [self.auditedProducts insertObject:newProduct atIndex:0];
////
////                            //code for not case quantity
////
////                            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"receiveWithOrderSheet"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"receiveWithoutOrderSheet"]){
////
////                                NSPredicate * locationPredicate=[NSPredicate predicateWithFormat:@"Location.LocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
////                                NSArray * foundLocation = [self.scannedProduct.InfoByStore filteredArrayUsingPredicate:locationPredicate];
////
////                                if(foundLocation.count==0){
////                                    //do nothing
////                                }else{
////                                    StoreSpecificInfo * store = [foundLocation objectAtIndex:0];
////                                    if([store.OrderFullCases boolValue]){
////                                        //this product is ordered in full cases
////
////                                        if([[[alertView textFieldAtIndex:0]text]intValue]%[self.scannedProduct.CaseQuantity intValue]==0){
////                                            //quantity matches..
////                                        }else{
////                                            self.addQuantity=NO;
////                                            self.editQuantity=NO;
////                                            [self showIncorrectCaseQuantityAlert];
////                                        }
////                                    }else{
////                                        //do nothing
////                                    }
////                                }
////                            }
////
////                            [self.productListTable reloadData];
////
////
////                        }else{
////
////                            NSPredicate *uomPred = [NSPredicate predicateWithFormat:@"Name == %@", self.scannedProduct.Uom];
////                            NSArray *gotUom = [self.uomArray filteredArrayUsingPredicate:uomPred];
////
////                            if(gotUom.count>0){
////                                if([[[gotUom objectAtIndex:0] valueForKey:@"UomType"]isEqualToString:@"UOM_WEIGHT"]){
////
////                                    NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
////                                    [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
////                                    [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
////                                    [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
////                                    [newProduct setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                    [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
////                                    [newProduct setValue:self.scannedProduct.Uom forKey:@"selectedUom"];
////                                    if(self.scannedProduct.Size){
////                                        [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
////                                    }
////                                    [newProduct setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:@"kg"] forKey:@"QuantityText"];
////                                    //                                        [self.auditedProducts addObject:newProduct];
////                                    [self.auditedProducts insertObject:newProduct atIndex:0];
////                                    [self.productListTable reloadData];
////
////                                }else{
////
////                                    NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
////                                    [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
////                                    [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
////                                    [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
////                                    [newProduct setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                    [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
////                                    [newProduct setValue:self.scannedProduct.Uom forKey:@"selectedUom"];
////                                    if(self.scannedProduct.Size){
////                                        [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
////                                    }
////                                    [newProduct setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
////                                    //                                        [self.auditedProducts addObject:newProduct];
////                                    [self.auditedProducts insertObject:newProduct atIndex:0];
////                                    [self.productListTable reloadData];
////
////                                }
////
////                            }else{
////                                NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
////                                [newProduct setValue:self.scannedProduct.ProductName forKey:@"Name"];
////                                [newProduct setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
////                                [newProduct setValue:self.scannedProduct.Upc forKey:@"Sku"];
////                                [newProduct setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0]text] doubleValue]] forKey:@"Quantity"];
////                                [newProduct setValue:self.scannedProduct.Uom forKey:@"Uom"];
////                                [newProduct setValue:self.scannedProduct.Uom forKey:@"selectedUom"];
////                                if(self.scannedProduct.Size){
////                                    [newProduct setValue:[[[self.scannedProduct.Size stringValue] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"size"];
////                                }
////                                [newProduct setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
////                                //                                    [self.auditedProducts addObject:newProduct];
////                                [self.auditedProducts insertObject:newProduct atIndex:0];
////                                [self.productListTable reloadData];
////                            }
////                        }
////                    }
////
////                    //save product to temp DB
////
////                    NSPredicate * quantityTextPredicate = [NSPredicate predicateWithFormat:@"ProductId == %@",self.selectedProductId];
////                    NSArray * findProduct = [self.auditedProducts filteredArrayUsingPredicate:quantityTextPredicate];
////
////                    NSString * quantityText = [[findProduct objectAtIndex:0]valueForKey:@"QuantityText"];
////
////                    NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
////                    NSPredicate *productPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND productId = %@",self.documentId, self.selectedProductId];
////
////                    [productRequest setPredicate:productPredicate];
////                    [productRequest setEntity:[NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context]];
////
////                    NSError *error;
////                    NSArray *results= [context executeFetchRequest:productRequest error:&error];
////
////                    if(results.count==0){
////                        NSManagedObject *saveProduct = [NSEntityDescription
////                                                        insertNewObjectForEntityForName:@"ReceivingTempProdList"
////                                                        inManagedObjectContext:context];
////
////
////                        [saveProduct setValue:self.selectedProductId forKey:@"productId"];
////                        [saveProduct setValue:[NSNumber numberWithDouble:[[[findProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]] forKey:@"quantity"];
////                        [saveProduct setValue:self.documentId forKey:@"documentId"];
////                        [saveProduct setValue:[[findProduct objectAtIndex:0] valueForKey:@"Uom"] forKey:@"uom"];
////                        [saveProduct setValue:[[findProduct objectAtIndex:0] valueForKey:@"Name"] forKey:@"productName"];
////                        [saveProduct setValue:[[findProduct objectAtIndex:0] valueForKey:@"Sku"] forKey:@"sku"];
////                        [saveProduct setValue:quantityText forKey:@"quantityText"];
////                        [saveProduct setValue:[[findProduct objectAtIndex:0] valueForKey:@"size"]?[[findProduct objectAtIndex:0] valueForKey:@"size"]:NULL forKey:@"size"];
////                        [context save:&error];
////                    }else{
////                        for (NSManagedObject * obj in results){
////                            [obj setValue:[NSNumber numberWithDouble:[[[findProduct objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]] forKey:@"quantity"];
////                            [obj setValue:quantityText forKey:@"quantityText"];
////                        }
////                        [context save:&error];
////                    }
////                    NSLog(@"product saved to temp DB...");
////                }else{
////                    [self productQuantityAlert];
////                }
////            }
////            else{
////                self.enterUpcText.text = @"";
////
////                if(self.editQuantity || self.addQuantity){
////
////                }else{
////                    //delete the product if there
////
////                    NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
////                    NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];
////
////                    if(alreadyProduct.count>0){
////                        //delete the product
////                        [self.auditedProducts removeObject:[alreadyProduct objectAtIndex:0]];
////                        [self.productListTable reloadData];
////                    }
////                }
////            }
////    }
//
////    if(alertView.tag == reAuditPrompt){
////        if(buttonIndex==1){
////            self.addQuantity=NO;
////            self.editQuantity=YES;
////            self.scannedProduct =[[[[[BarcodeProcessor alloc]init] processBarcode:self.selectedProductId] valueForKey:@"productArray"] objectAtIndex:0];
////            [self productQuantityAlert];
////        }else if(buttonIndex==2){
////            self.addQuantity=YES;
////            self.editQuantity=NO;
////            self.scannedProduct =[[[[[BarcodeProcessor alloc]init] processBarcode:self.selectedProductId] valueForKey:@"productArray"] objectAtIndex:0];
////            [self productQuantityAlert];
////        }else if(buttonIndex==3){
////            [self productNoteAlert];
////        }else if(buttonIndex==4){
////            self.productPhoto=YES;
////            self.invoicePhoto = NO;
////            self.barcodeRead = NO;
////            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
////            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
////            {
////                [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
////            }
////
////            [imagePickerController setDelegate:self];
////            [self presentViewController:imagePickerController animated:YES completion:nil];
////        }else if(buttonIndex==5){
////            [self uomSelectPrompt];
////        }
////    }
//
////    if(alertView.tag==finishAuditPrompt){
////        if(buttonIndex==0){
////
////            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"receiveWithOrderSheet"]){
////
////                NSMutableArray * allProducts = [[NSMutableArray alloc]init];
////                for(NSDictionary * product in self.auditedProducts){
////                    NSMutableDictionary * finalProduct = [[NSMutableDictionary alloc]init];
////                    [finalProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
////                    [finalProduct setValue:[product valueForKey:@"Name"] forKey:@"Name"];
////                    [finalProduct setValue:[product valueForKey:@"QuantityText"] forKey:@"QuantityText"];
////                    [allProducts addObject:finalProduct];
////                }
////
////                NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
////                [receiving setValue:allProducts forKey:@"productList"];
////                [receiving setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"imagecounter"];
////                [receiving setValue:[NSNumber numberWithBool:self.pushToCM] forKey:@"pushToCM"];
////                [receiving setValue:self.notes forKey:@"notes"];
////                [receiving setValue:self.documentId forKey:@"documentId"];
////
////                [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
////                [[NSUserDefaults standardUserDefaults] synchronize];
////
////
////                //change DB for discrepency screen revival
////
////                NSFetchRequest *tempDocRequest = [[NSFetchRequest alloc]init];
////                NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
////
////                [tempDocRequest setPredicate:docPredicate];
////                [tempDocRequest setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
////
////                NSError *error;
////                NSArray *results= [context executeFetchRequest:tempDocRequest error:&error];
////
////                for (NSManagedObject * obj in results){
////                    [obj setValue:@"VOS_RECEIVE_DESCR" forKey:@"documentType"];
////                }
////                [context save:&error];
////
////
////                [self performSegueWithIdentifier:@"discrepencySegue" sender:self];
////
////            }else{
////                [self startSpinner:@"Saving Data"];
////                self.endTime = [NSDate date];
////                [self saveAuditToFile];
////
////                dispatch_async(dispatch_get_main_queue(), ^{
//////                    [NSUserDefaults resetStandardUserDefaults];
//////                    [[NSUserDefaults standardUserDefaults]synchronize];
////                    self.auditedProducts=Nil;
////                    self.scannedProduct=Nil;
////                    self.selectedProductId=Nil;
////                    self.receivingObject=Nil;
////                    self.startTime = nil;
//////                    self.documentId=nil;
////
////                    [self stopSpinner];
////
////                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"createTransfer"])
////                        [self performSegueWithIdentifier:@"printBarcodeFromReceivingSegue" sender:self];
////                    else
////                        [self.navigationController popToRootViewControllerAnimated:YES];
////
////                });
////            }
////        }
////    }
//
////    if(alertView.tag == goBackPrompt){
////        if(buttonIndex==0){
////            [self deleteAuditandGoBack];
////        }else{
////        }
////    }
//
////    if(alertView.tag == saveAndGoBackPrompt){
////        if(buttonIndex==0){
////            [self saveAuditandGoBack];
////        }else{
////            [self showGoBackPromtAlert];
////        }
////    }
//
////    if(alertView.tag == noVosFoundPrompt || alertView.tag == noTransferFoundPrompt){
////        if(buttonIndex==0){
////            [self deleteAuditandGoBack];
////        }
////    }
//
////    if(alertView.tag == productNotInVosAlerts){
////        if(buttonIndex == 0){
////            NSPredicate * existingProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.scannedProduct.ProductId];
////            if([self.auditedProducts filteredArrayUsingPredicate:existingProdPred].count==0){
////                self.selectedProductId = self.scannedProduct.ProductId;
////                [self productQuantityAlert];
////            }else{
////                self.selectedProductId = self.scannedProduct.ProductId;
////                [self reAuditAlert];
////            }
////        }
////    }
//
////    if(alertView.tag == wrongCaseQuantityAlerts){
////        if(buttonIndex == 0){
////            NSPredicate * findProdPredicate = [NSPredicate predicateWithFormat:@"ProductId contains %@", self.selectedProductId];
////            NSArray * alreadyProduct = [self.auditedProducts filteredArrayUsingPredicate:findProdPredicate];
////
////            if(alreadyProduct.count>0){
////                if(self.addQuantity){
////                    double newQuantity = (double)self.addEditedQuantity;
////                    [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:newQuantity] forKey:@"Quantity"];
////                    [[alreadyProduct objectAtIndex:0] setValue:[[[NSString stringWithFormat:@"%.00f",newQuantity] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                }else if(self.editQuantity){
////                    [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:(double)self.addEditedQuantity] forKey:@"Quantity"];
////                    [[alreadyProduct objectAtIndex:0] setValue:[[[[NSNumber numberWithDouble:(double)self.addEditedQuantity] stringValue] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                }else{
//////                    [[alreadyProduct objectAtIndex:0] setValue:[NSNumber numberWithDouble:(double)self.addEditedQuantity] forKey:@"Quantity"];
//////                    [[alreadyProduct objectAtIndex:0] setValue:[[[[NSNumber numberWithDouble:(double)self.addEditedQuantity] stringValue] stringByAppendingString:@" "] stringByAppendingString:[[alreadyProduct objectAtIndex:0] valueForKey:@"Uom"]] forKey:@"QuantityText"];
////                }
////            }
////
////            [self.productListTable reloadData];
////
////        }else{
////            self.selectedProductId = self.scannedProduct.ProductId;
////            [self productQuantityAlert];
////        }
////    }
//}


#pragma mark - iphone file system handler

- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

-(void)saveAuditToFile{
    @try {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSString *stringFromDate = [formatter stringFromDate:self.endTime];
            
            NSMutableArray<vosReceiveAssociatedProducts> * allProducts = [[NSMutableArray<vosReceiveAssociatedProducts> alloc]init];
            for(NSDictionary * product in self.auditedProducts){
                vosReceiveAssociatedProducts * finalProduct = [[vosReceiveAssociatedProducts alloc]init];
                finalProduct.ProductId = [product valueForKey:@"ProductId"];
                
//                if([product valueForKey:@"changedQuantity"]){
//                    finalProduct.QuantityText = [product valueForKey:@"changedQuantity"];
//                }else{
                    finalProduct.QuantityText = [product valueForKey:@"QuantityText"];
//                }
                [allProducts addObject:finalProduct];
            }
            
            vosReceiveAuditElement * auditElement = [[vosReceiveAuditElement alloc]init];
            auditElement.ReceiptId = NULL;
            auditElement.ReceiptNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"VOS_RECEIPT"];
            auditElement.VendorId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"];
            auditElement.VosNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"];
            auditElement.VosId=[[NSUserDefaults standardUserDefaults] valueForKey:@"VOS_ID"];
            
            auditElement.GrandTotal = self.grandTotal;
            auditElement.AttachmentCount = [NSNumber numberWithInt:self.imagecounter];
            auditElement.SendToCM = [NSNumber numberWithBool:self.pushToCM];
            auditElement.Note= self.notes;
            auditElement.ProductsList = allProducts;
            
            vosReceiveAuditData * auditData = [[vosReceiveAuditData alloc]init];
            auditData.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
            auditData.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
            auditData.ReceivingType = @"VOS_RECEIVE";
            auditData.AuditItem = auditElement;
            
            [self writeStringToFile:[auditData toJSONString] fileName:stringFromDate];
            NSLog(@"File Saved for %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]);

            NSError *error;
            NSManagedObject *newManagedObject = [NSEntityDescription
                                                 insertNewObjectForEntityForName:@"DocStatusInfo"
                                                 inManagedObjectContext:context];
            
            [newManagedObject setValue:@"VOS_RECEIVE" forKey:@"documentType"];
            [newManagedObject setValue:@"Finished" forKey:@"status"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [newManagedObject setValue:self.documentId forKey:@"documentId"];
            [newManagedObject setValue:self.endTime forKey:@"date"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
            [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
            [context save:&error];
            
        }
        
        else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"]){
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSString *stringFromDate = [formatter stringFromDate:self.endTime];
            
            NSMutableArray<receiptReceiveAssociatedProducts> * allProducts = [[NSMutableArray<receiptReceiveAssociatedProducts> alloc]init];
            for(NSDictionary * product in self.auditedProducts){
                receiptReceiveAssociatedProducts * finalProduct = [[receiptReceiveAssociatedProducts alloc]init];
                finalProduct.ProductId = [product valueForKey:@"ProductId"];
                //            finalProduct.Quantity = [NSNumber numberWithDouble:[[product valueForKey:@"Quantity"] doubleValue]];
                
//                if([product valueForKey:@"changedQuantity"]){
//                    finalProduct.QuantityText = [product valueForKey:@"changedQuantity"];
//                }else{
                    finalProduct.QuantityText = [product valueForKey:@"QuantityText"];
//                }
                
                
                //            finalProduct.QuantityText = [product valueForKey:@"QuantityText"];
                [allProducts addObject:finalProduct];
            }
            
            receiptReceiveAuditElement * auditElement = [[receiptReceiveAuditElement alloc]init];
            auditElement.ReceiptId = NULL;
            auditElement.ReceiptNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"];
            auditElement.VendorId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"];
            auditElement.VosNumber = NULL;
            auditElement.VosId = NULL;
            auditElement.GrandTotal = self.grandTotal;
            auditElement.AttachmentCount = [NSNumber numberWithInt:self.imagecounter];
            auditElement.SendToCM = [NSNumber numberWithBool:self.pushToCM];
            auditElement.Note= self.notes;
            auditElement.ProductsList = allProducts;
            
            receiptReceiveAuditData * auditData = [[receiptReceiveAuditData alloc]init];
            auditData.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
            auditData.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
            auditData.ReceivingType = @"RECEIPT_RECEIVE";
            auditData.AuditItem = auditElement;
            
            [self writeStringToFile:[auditData toJSONString] fileName:stringFromDate];
            NSLog(@"File Saved!!!");
            NSLog(@"File Saved for %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]);
            
            NSError *error;
            NSManagedObject *newManagedObject = [NSEntityDescription
                                                 insertNewObjectForEntityForName:@"DocStatusInfo"
                                                 inManagedObjectContext:context];
            
            [newManagedObject setValue:@"RECEIPT_RECEIVE" forKey:@"documentType"];
            [newManagedObject setValue:@"Finished" forKey:@"status"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [newManagedObject setValue:self.documentId forKey:@"documentId"];
            [newManagedObject setValue:self.endTime forKey:@"date"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
            [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
            
            [context save:&error];
            
        }
        
        else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSString *stringFromDate = [formatter stringFromDate:self.endTime];
            
            NSMutableArray<transferReceiveAssociatedProducts> * allProducts = [[NSMutableArray<transferReceiveAssociatedProducts> alloc]init];
            for(NSDictionary * product in self.auditedProducts){
                transferReceiveAssociatedProducts * finalProduct = [[transferReceiveAssociatedProducts alloc]init];
                finalProduct.ProductId = [product valueForKey:@"ProductId"];
                finalProduct.Quantity = [NSNumber numberWithDouble:[[product valueForKey:@"Quantity"] doubleValue]];
                [allProducts addObject:finalProduct];
            }
            
            transferReceiveAuditElement * auditElement = [[transferReceiveAuditElement alloc]init];
            auditElement.TransferId = [self.receivingObject valueForKey:@"TransferId"];
            auditElement.TransferNumber = [self.receivingObject valueForKey:@"TransferNumber"];
            auditElement.locationId = [self.receivingObject valueForKey:@"FromLocationId"];
            auditElement.ProductsList = allProducts;
            
            transferReceiveAuditData * auditData = [[transferReceiveAuditData alloc]init];
            auditData.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
            auditData.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
            auditData.ReceivingType = @"TRANSFER_RECEIVE";
            auditData.AuditItem = auditElement;
            
            [self writeStringToFile:[auditData toJSONString] fileName:stringFromDate];
            NSLog(@"File Saved for %@", [self.receivingObject valueForKey:@"TransferNumber"]);
            
            NSError *error;
            NSManagedObject *newManagedObject = [NSEntityDescription
                                                 insertNewObjectForEntityForName:@"DocStatusInfo"
                                                 inManagedObjectContext:context];
            
            [newManagedObject setValue:@"TRANSFER_RECEIVE" forKey:@"documentType"];
            [newManagedObject setValue:@"Finished" forKey:@"status"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [newManagedObject setValue:self.documentId forKey:@"documentId"];
            [newManagedObject setValue:self.endTime forKey:@"date"];
            [newManagedObject setValue:[self.receivingObject valueForKey:@"TransferNumber"] forKey:@"documentNo"];
            [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
            [context save:&error];
            
            if(self.cafeTransfer){
                [self saveFileForCafeAdjustment];
            }
        }
        
        else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_CREATION"]){
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSString *stringFromDate = [formatter stringFromDate:self.endTime];
            
            NSMutableArray<transferCreationAssociatedProducts> * allProducts = [[NSMutableArray<transferCreationAssociatedProducts> alloc]init];
            for(NSDictionary * product in self.auditedProducts){
                transferCreationAssociatedProducts * finalProduct = [[transferCreationAssociatedProducts alloc]init];
                finalProduct.ProductId = [product valueForKey:@"ProductId"];
                finalProduct.Quantity = [NSNumber numberWithDouble:[[product valueForKey:@"Quantity"] doubleValue]];
                [allProducts addObject:finalProduct];
            }
            
            transferCreationAuditElement * auditElement = [[transferCreationAuditElement alloc]init];
            auditElement.locationId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"TO_LOCATION"] valueForKey:@"LocationId"];
            auditElement.ProductsList = allProducts;
            
            NSMutableString * noteString = [[NSMutableString alloc] init];
            for (NSString * note in self.notes){
                [noteString appendString:note];
                [noteString appendString:@","];
            }
            auditElement.Note = noteString.length>0? noteString:NULL;

            NSString * transfer = [NSString stringWithFormat:@"T%@%@%@", [[NSUserDefaults standardUserDefaults] valueForKey:@
                                                                          "OFFICE"], [[[NSUserDefaults standardUserDefaults] valueForKey:@"TO_LOCATION"] valueForKey:@"LocationId"], [self.documentId componentsSeparatedByString:@"_"][1]];
            auditElement.TransferNumber = transfer;
            
            transferCreationAuditData * auditData = [[transferCreationAuditData alloc]init];
            auditData.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
            auditData.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
            auditData.ReceivingType = @"TRANSFER_CREATION";
            auditData.AuditItem = auditElement;
            
            [self writeStringToFile:[auditData toJSONString] fileName:stringFromDate];
            NSLog(@"File Saved for new transfer creation");
            
            NSError *error;
            NSManagedObject *newManagedObject = [NSEntityDescription
                                                 insertNewObjectForEntityForName:@"DocStatusInfo"
                                                 inManagedObjectContext:context];
            
            [newManagedObject setValue:@"TRANSFER_CREATION" forKey:@"documentType"];
            [newManagedObject setValue:@"Finished" forKey:@"status"];
            [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
            [newManagedObject setValue:self.documentId forKey:@"documentId"];
            [newManagedObject setValue:self.endTime forKey:@"date"];
            [newManagedObject setValue:@"New Transfer" forKey:@"documentNo"];
            [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
            [context save:&error];
            
        }
        
        NSError *error;
        //delete from DocStatusInfo table
        
        NSFetchRequest *allDocs = [[NSFetchRequest alloc] init];
        [allDocs setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
        NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
        [allDocs setPredicate:docPredicate];
        NSArray *docs = [context executeFetchRequest:allDocs error:&error];
        for (NSManagedObject *doc in docs) {
            [context deleteObject:doc];  }
        NSError *saveError = nil;
        [context save:&saveError];
        
        //delete from ReceivingTempProdList table
        
        NSFetchRequest *allProds = [[NSFetchRequest alloc] init];
        [allProds setEntity:[NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context]];
        NSPredicate *prodPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
        [allProds setPredicate:prodPredicate];
        NSArray *prods = [context executeFetchRequest:allProds error:&error];
        for (NSManagedObject *prod in prods) {
            [context deleteObject:prod];  }
        [context save:&saveError];
        
        NSLog(@"temp DB entries deleted..");
    }
    @catch (NSException *exception) {
        NSLog(@"exeption thrown while saving file for receiving:%@", exception);
        [self showErrorAlert:@"An error has occurred – please restart the application and contact IT"];
    }
}


-(void)saveFileForCafeAdjustment{
    
    NSLog(@"saving adjustment file for cafe transfer");
    
    @try {
        NSString* documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];

        NSError *error;
        NSManagedObject *createManagedObject = [NSEntityDescription
                                                insertNewObjectForEntityForName:@"DocStatusInfo"
                                                inManagedObjectContext:context];
        
        [createManagedObject setValue:@"C2_CAFE" forKey:@"documentType"];
        [createManagedObject setValue:@"Created" forKey:@"status"];
        [createManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [createManagedObject setValue:documentId forKey:@"documentId"];
        [createManagedObject setValue:self.startTime forKey:@"date"];
        [context save:&error];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *stringFromDate = [formatter stringFromDate:[self.endTime dateByAddingTimeInterval:5.0]];
        
        NSMutableArray<AdjustmentItem> * productItems = [[NSMutableArray<AdjustmentItem> alloc]init];
        
        for(NSObject * receivedProduct in self.auditedProducts){
            
            AdjustmentItem * ProductItem = [[AdjustmentItem alloc]init];
            AdjustmentProduct * adjProduct = [[AdjustmentProduct alloc]init];
            
            Products * product = [[[[[BarcodeProcessor alloc]init] processBarcode:[receivedProduct valueForKey:@"ProductId"]] valueForKey:@"productArray"] objectAtIndex:0];
            
            adjProduct.ProductId = product.ProductId;
            adjProduct.DepartmentId = product.DepartmentId;
            adjProduct.SubDepartmentId = product.SubDepartmentId;
            adjProduct.CategoryId = product.CategoryId;
            adjProduct.BottleDeposit = product.BottleDeposit;
            adjProduct.EnvironmentalFee = product.EnvironmentalFree;
            adjProduct.Taxable = product.IsTaxable;
            adjProduct.TaxRate = product.TaxRatePercent;
            
            NSPredicate * locationPredicate=[NSPredicate predicateWithFormat:@"Location.LocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
            NSArray * foundLocation = [product.InfoByStore filteredArrayUsingPredicate:locationPredicate];
            
            StoreSpecificInfo * store = [foundLocation objectAtIndex:0];
            
            adjProduct.ListPrice =  store.ListPrice;
            adjProduct.ActivePrice = store.ActivePrice;
            adjProduct.Cost = product.StandardCost;
            
            ProductItem.Product = adjProduct;
            
            ProductItem.Quantity = [NSNumber numberWithDouble:[[receivedProduct valueForKey:@"Quantity"] doubleValue]];
            ProductItem.ValueListPrice = [self multiplyNumbers:ProductItem.Product.ListPrice second:ProductItem.Quantity];
            ProductItem.ValueActivePrice = [self multiplyNumbers:ProductItem.Product.ActivePrice second:ProductItem.Quantity];
            ProductItem.ValueCost = [self multiplyNumbers:ProductItem.Product.Cost second:ProductItem.Quantity];
            ProductItem.ValueBottleDeposit = [self multiplyNumbers:ProductItem.Product.BottleDeposit second:ProductItem.Quantity];
            ProductItem.ValueEnvironmentalFee = [self multiplyNumbers:ProductItem.Product.EnvironmentalFee second:ProductItem.Quantity];

            if([product.IsTaxable boolValue]){
                ProductItem.ValueActivePriceTax = [NSNumber numberWithDouble:[ProductItem.ValueActivePrice doubleValue] * [adjProduct.TaxRate doubleValue]/100];
                ProductItem.ValueActiveCostTax = [NSNumber numberWithDouble:[ProductItem.ValueCost doubleValue] * [adjProduct.TaxRate doubleValue]/100];
            }
            else{
                ProductItem.ValueActivePriceTax = [NSNumber numberWithDouble:0.00];
                ProductItem.ValueActiveCostTax = [NSNumber numberWithDouble:0.00];
            }

            [productItems addObject:ProductItem];
        }
        
        BaseAdjustmentRequest * adjustmentRequest = [[BaseAdjustmentRequest alloc]init];
        adjustmentRequest.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
        adjustmentRequest.HoldingLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        adjustmentRequest.ConsumingLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        adjustmentRequest.CreatedDateTime = self.startTime;
        adjustmentRequest.Comment = @"Cafe_Adjustment";
        adjustmentRequest.Items = productItems;
        adjustmentRequest.ReversedEntry = [NSNumber numberWithBool:FALSE];
        adjustmentRequest.IsProductionPurpose = [NSNumber numberWithBool:NO];
        
        [self writeStringToFile:[adjustmentRequest toJSONString] fileName:stringFromDate];
        
        //    NSError *error;
        NSManagedObject *finishManagedObject = [NSEntityDescription
                                                insertNewObjectForEntityForName:@"DocStatusInfo"
                                                inManagedObjectContext:context];
        
        [finishManagedObject setValue:@"C2_CAFE" forKey:@"documentType"];
        [finishManagedObject setValue:@"Finished" forKey:@"status"];
        [finishManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [finishManagedObject setValue:documentId forKey:@"documentId"];
        [finishManagedObject setValue:[self.endTime dateByAddingTimeInterval:5.0] forKey:@"date"];
        [finishManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
        [context save:&error];
        
        NSLog(@"File Saved for adjustment from %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"]);
    } @catch (NSException *exception) {
        NSLog(@"Exception happened while trying to create adjustment for cafe transfer: %@", exception.description);
    }

}

-(NSNumber*)multiplyNumbers :(NSNumber*)first second:(NSNumber*)second{
    double multiply = [first doubleValue]*[second doubleValue];
    return [NSNumber numberWithDouble:multiply];
}


#pragma mark - barcode read function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    if(self.shouldScanProduct){
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
        [self processBarcode:barcode];
    }else{
        int sound[]={3500,200,0,200,3500,200};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    //add code here
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_CREATION"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        if(self.productPhoto){
            self.productPhoto=NO;
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        if(self.invoicePhoto){
            if(self.imagecounter>0){
                [self dismissViewControllerAnimated:YES completion:nil];
                [self finishInvoicePhotoAlert];
            }else{
                [self dismissViewControllerAnimated:YES completion:nil];
                [self takePhotoOfInvoiceAlert];
            }
        }else{
            //different than invoice photo
            [self dismissViewControllerAnimated:YES completion:nil];
            [self finishOptionalPhotoAlert];
        }
    }
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    if(self.barcodeRead){
        id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
        
        ZBarSymbol *symbol = nil;
        for(symbol in results)
            break;
        
        [reader dismissViewControllerAnimated:YES completion:Nil];
        NSString * barcode=symbol.data;
        if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
            barcode = [symbol.data substringFromIndex:1];
        }
        [self processBarcode:barcode];
    }else{
//        [self startSpinner:@"Please wait..."];
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *stringFromDate = [formatter stringFromDate:self.startTime];
        
        NSString * fileName = [[[stringFromDate stringByAppendingString:@"."]stringByAppendingString:[NSString stringWithFormat:@"%d",self.imagecounter]] stringByAppendingString:@".jpg"];
        
        NSError * error;
        NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
        NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        
        [UIImagePNGRepresentation([[ImageCompression alloc]compressImage:image]) writeToFile:fileAtPath atomically:YES];
        
        self.imagecounter ++;
        
        NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        
        NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@ AND status = %@",self.documentId, @"Created"];
        
        [request setPredicate:predicate];
        [request setEntity:entityDesc];
        NSArray *results= [context executeFetchRequest:request error:&error];
        
        for (NSManagedObject * obj in results){
            [obj setValue:@"Saved" forKeyPath:@"attachmentStatus"];
            [obj setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"attachmentNo"];
        }
        [context save:&error];
        
//        [self stopSpinner];
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
        
        if(self.invoicePhoto){
            [self takeNextInvoicePagePhoto];
        }else if(self.productPhoto){
            
        }else{
            [self takeNextOtherPhoto];
        }
    }
}

-(void)processBarcode:(NSString *)barcode{
    
    [self.enterUpcText endEditing:YES];
    self.enterUpcText.text=@"";
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"]){

        NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcodeWithVendor:barcode vendor:[[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]];
        
        if([resultDic valueForKey:@"errorMsg"]!=NULL){
            NSLog(@"Result returned error..");
            [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
        }else{
            
            NSArray * products = [resultDic valueForKey:@"productArray"];
            
            if(products.count==0){
                //no product returned
                //try for different vendor
                
                resultDic = [[[BarcodeProcessor alloc]init]processBarcode:barcode];
                
                if([resultDic valueForKey:@"errorMsg"]!=NULL){
                    NSLog(@"Result returned error..");
                    [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
                    
                }else{
                
                    NSArray * products = [resultDic valueForKey:@"productArray"];
                    
                    if(products.count==0){
                        //no product found
                        
                        NSLog(@"product not found..");
                        [self showErrorAlert:@"Product Not Found..."];
                        
                    }else if(products.count>1){
                        //multiple products from other vendors
                        
                        NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
                        
                        for(NSObject * product in products){
                            NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                            [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                            [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                            if([product valueForKey:@"Size"]!=NULL && [product valueForKey:@"UomQty"]!=NULL){
                                [listedProduct setValue:[[[[product valueForKey:@"Size"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[product valueForKey:@"UomQty"]] forKey:@"Size"];
                            }
                            [listedProductArray addObject:listedProduct];
                        }
                        
                        [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [self performSegueWithIdentifier:@"multipleUpcFromReceivingSegue" sender:self];
                        
                    }else if(products.count==1){
                        
                        Products * product = [[Products alloc]init];
                        product = [products objectAtIndex:0];
                        
                        self.scannedProduct = product;
                        [self wrongVendorAlert];
                    }
                }
                
            }else if(products.count>1){
                //multiple products from choosen vendors
                
                NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
                
                for(NSObject * product in products){
                    NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                    [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                    [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                    if([product valueForKey:@"Size"]!=NULL && [product valueForKey:@"UomQty"]!=NULL){
                        [listedProduct setValue:[[[[product valueForKey:@"Size"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[product valueForKey:@"UomQty"]] forKey:@"Size"];
                    }
                    [listedProductArray addObject:listedProduct];
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self performSegueWithIdentifier:@"multipleUpcFromReceivingSegue" sender:self];
                
            }else if(products.count==1){
                
                Products * product = [[Products alloc]init];
                product = [products objectAtIndex:0];
                
                NSPredicate * existingProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", product.ProductId];
                if([self.auditedProducts filteredArrayUsingPredicate:existingProdPred].count==0){
                    self.scannedProduct = product;
                    self.selectedProductId = product.ProductId;
                    [self productQuantityAlert];
                }else{
                    self.selectedProductId = product.ProductId;
                    [self reAuditAlert];
                }
            }
        }
    }
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
    
        
        NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcodeWithVendor:barcode vendor:[[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]];
        
        if([resultDic valueForKey:@"errorMsg"]!=NULL){
            NSLog(@"Result returned error..");
            [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
        }else{
        
            NSArray * products = [resultDic valueForKey:@"productArray"];
            
            if(products.count==0){
                //no product returned
                //try for different vendor
                
                
                
                NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcode:barcode];
                
                if([resultDic valueForKey:@"errorMsg"]!=NULL){
                    NSLog(@"Result returned error..");
                    [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
                }else{
                    NSArray * products = [resultDic valueForKey:@"productArray"];
                
                    if(products.count==0){
                        //no product found
                        
                        NSLog(@"product not found..");
                        [self showErrorAlert:@"Product Not Found..."];
                        
                    }else if(products.count>1){
                        //multiple products from other vendors
                        
                        NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
                        
                        for(NSObject * product in products){
                            NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                            [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                            [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                            if([product valueForKey:@"Size"]!=NULL && [product valueForKey:@"UomQty"]!=NULL){
                                [listedProduct setValue:[[[[product valueForKey:@"Size"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[product valueForKey:@"UomQty"]] forKey:@"Size"];
                            }
                            [listedProductArray addObject:listedProduct];
                        }
                        
                        [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [self performSegueWithIdentifier:@"multipleUpcFromReceivingSegue" sender:self];
                        
                    }else if(products.count==1){
                        Products * product = [[Products alloc]init];
                        product = [products objectAtIndex:0];
                        self.scannedProduct = product;
                        [self wrongVendorAlert];
                    }
                
                }

            }else if(products.count>1){
                //multiple products from choosen vendors
                
                NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
                
                for(NSObject * product in products){
                    NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                    [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                    [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                    if([product valueForKey:@"Size"]!=NULL && [product valueForKey:@"UomQty"]!=NULL){
                        [listedProduct setValue:[[[[product valueForKey:@"Size"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[product valueForKey:@"UomQty"]] forKey:@"Size"];
                    }
                    [listedProductArray addObject:listedProduct];
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self performSegueWithIdentifier:@"multipleUpcFromReceivingSegue" sender:self];
                
            }else if(products.count==1){
                
                Products * product = [[Products alloc]init];
                product = [products objectAtIndex:0];
                
                NSPredicate * productPred = [NSPredicate predicateWithFormat:@"ProductId == %@",product.ProductId];
                NSArray * foundProduct = [[self.receivingObject valueForKey:@"VosItems"] filteredArrayUsingPredicate:productPred];
                
                if(foundProduct.count==0){
                    //item not in VOS, prompt not in vos alert
                    
                    self.scannedProduct = product;
                    self.selectedProductId = product.ProductId;
                    
                    [self productNotInVosAlert];

                }else{
                    NSPredicate * existingProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", product.ProductId];
                    if([self.auditedProducts filteredArrayUsingPredicate:existingProdPred].count==0){
                        self.scannedProduct = product;
                        self.selectedProductId = product.ProductId;
                        [self productQuantityAlert];
                    }else{
                        self.selectedProductId = product.ProductId;
                        [self reAuditAlert];
                    }
                }
            }
        }
    }
    
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"TRANSFER_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"TRANSFER_CREATION"]){
        
        NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcode:barcode];
        
        if([resultDic valueForKey:@"errorMsg"]!=NULL){
            NSLog(@"Result returned error..");
            [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
        }else{
            
            NSArray * products = [resultDic valueForKey:@"productArray"];
            
            if(products.count==0){
                //no product found
                
                NSLog(@"product not found..");
                [self showErrorAlert:@"Product Not Found..."];
                
            }else if(products.count>1){
                //multiple products found
                
                NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
                
                for(NSObject * product in products){
                    NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                    [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                    [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                    if([product valueForKey:@"Size"]!=NULL && [product valueForKey:@"UomQty"]!=NULL){
                        [listedProduct setValue:[[[[product valueForKey:@"Size"] stringValue] stringByAppendingString:@" "] stringByAppendingString:[product valueForKey:@"UomQty"]] forKey:@"Size"];
                    }
                    [listedProductArray addObject:listedProduct];
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self performSegueWithIdentifier:@"multipleUpcFromReceivingSegue" sender:self];
                
            }else if(products.count==1){
                //one product found
                
                Products * product = [[Products alloc]init];
                product = [products objectAtIndex:0];
                
                NSPredicate * existingProdPred = [NSPredicate predicateWithFormat:@"ProductId contains %@", product.ProductId];
                if([self.auditedProducts filteredArrayUsingPredicate:existingProdPred].count==0){
                    self.scannedProduct = product;
                    self.selectedProductId = product.ProductId;
                    [self productQuantityAlert];
                }else{
                    self.selectedProductId = product.ProductId;
                    [self reAuditAlert];
                }
            }
        }
    }
}



#pragma  mark - Button funcitons
- (IBAction)scanProduct:(id)sender {
    if(self.scannerAvailable){
        if(self.shouldScanProduct){
            [self.scanner barcodeStartScan:nil];
            self.foundBarcode = NO;
            [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
        }
    }else{
        if(self.shouldScanProduct){
            self.barcodeRead = YES;
            ZBarReaderViewController *reader = [ZBarReaderViewController new];
            reader.readerDelegate = self;
            [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
            reader.readerView.zoom = 1.0;
            [self presentViewController:reader animated:YES completion:Nil];
        }
    }
}

- (IBAction)submitAudit:(id)sender {
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"] || [[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"RECEIPT_RECEIVE"]){
        [self takeOtherPhotoAlert];
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        [self finishAuditAlert];
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_CREATION"]){
        [self trasnferForCafeAlert];
    }
}

- (IBAction)goBack:(id)sender {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"TRANSFER_RECEIVE"])
        [self showGoBackPromtAlert];
    else
        [self showSaveAndBackAlert];
}


-(void)saveAuditandGoBack{
    [self startSpinner:@"Saving Receipt.."];
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSUserDefaults resetStandardUserDefaults];
        [[NSUserDefaults standardUserDefaults]synchronize];
        self.auditedProducts=Nil;
        self.scannedProduct=Nil;
        self.selectedProductId=Nil;
        self.receivingObject=Nil;
        self.startTime = nil;
        self.documentId=nil;
//        [self stopSpinner];
        [statusAlert dismissViewControllerAnimated:YES completion:^{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
    });
}


-(void)deleteAuditandGoBack{

    [self startSpinner:@"Deleting Receipt.."];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //delete from DocStatusInfo table
        NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        
        NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];
        
        [request setPredicate:predicate];
        [request setEntity:entityDesc];
        
        NSError *error;
        NSArray *results= [context executeFetchRequest:request error:&error];
        
        for (NSManagedObject * obj in results){
            [context deleteObject:obj];
        }
        [context save:&error];
        
        //delete from TempDocStatus table
        
        NSFetchRequest *allDocs = [[NSFetchRequest alloc] init];
        [allDocs setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
        NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
        [allDocs setPredicate:docPredicate];
        NSArray *docs = [context executeFetchRequest:allDocs error:&error];
        for (NSManagedObject *doc in docs) {
            [context deleteObject:doc];  }
        NSError *saveError = nil;
        [context save:&saveError];
        
        //delete from ReceivingTempProdList table
        
        NSFetchRequest *allProds = [[NSFetchRequest alloc] init];
        [allProds setEntity:[NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context]];
        NSPredicate *prodPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
        [allProds setPredicate:prodPredicate];
        NSArray *prods = [context executeFetchRequest:allProds error:&error];
        for (NSManagedObject *prod in prods) {
            [context deleteObject:prod];  }
        [context save:&saveError];
        
        [NSUserDefaults resetStandardUserDefaults];
        [[NSUserDefaults standardUserDefaults]synchronize];
        self.auditedProducts=Nil;
        self.scannedProduct=Nil;
        self.selectedProductId=Nil;
        self.receivingObject=Nil;
        self.startTime = nil;
        self.documentId=nil;
        [self dismissViewControllerAnimated:YES completion:^{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
    });
}

#pragma mark - Storyboard segue functions


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"printBarcodeFromReceivingSegue"]){
        CNFPrintQRCodeViewController * qrCodeViewContr = segue.destinationViewController;
        NSString * transfer = [NSString stringWithFormat:@"T%@%@%@", [[NSUserDefaults standardUserDefaults] valueForKey:@
                                                                      "OFFICE"], [[[NSUserDefaults standardUserDefaults] valueForKey:@"TO_LOCATION"] valueForKey:@"LocationId"], [self.documentId componentsSeparatedByString:@"_"][1]];
        qrCodeViewContr.transferNumber = transfer;
    }
}






@end
