//
//  CNFSubmitAttachment.m
//  CNF App
//
//  Created by Anik on 2016-04-14.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFSubmitAttachment.h"
#import "UserElement.h"
#import "SSKeychain.h"
#import "SubmitDataElement.h"
#import "ValidUserItems.h"
#import "submitAttachment.h"
#import "CNFGetDocStatus.h"
#import "CNFAppSyncSystem.h"
#import "SSKeychain.h"

@interface CNFSubmitAttachment ()
@property (nonatomic, strong) NSMutableArray * listOfItemsToUpload;
@property (nonatomic, strong) NSMutableDictionary * itemToUpload;
@end

@implementation CNFSubmitAttachment{
    NSManagedObjectContext *context;
}

@synthesize listOfItemsToUpload = _listOfItemsToUpload, itemToUpload=_itemToUpload;

-(NSMutableArray *)listOfItemsToUpload{
    if(!_listOfItemsToUpload)_listOfItemsToUpload = [[NSMutableArray alloc]init];
    return _listOfItemsToUpload;
}


-(void)startSubmittingAttchment{
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"BACK_PROCESS"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [[NSUserDefaults standardUserDefaults] setValue:[formatter stringFromDate:[NSDate date]] forKey:@"QUEUE_START_TIME"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSLog(@"Is Queue running : %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"BACK_PROCESS"]);
    
    [self sortOutUnsibmittedDoc];
    
    if(self.listOfItemsToUpload.count==0){
        NSLog(@"no new attachment to submit..procede next..");
        
        self.listOfItemsToUpload=nil;
        self.itemToUpload=nil;
        
        CNFAppSyncSystem *sync=[[CNFAppSyncSystem alloc]init];
        [sync startSync];
        
    }else{
        for (NSDictionary *item in self.listOfItemsToUpload) {
            if([[item valueForKey:@"fileNames"] count]!=0)
                [self submitAttachment:item];
            else
                NSLog(@"No file found to submit for docId :%@", [item valueForKey:@"docId"]);
        }
        NSLog(@"finished uploading attachments.., switch to next task..");
        
        self.listOfItemsToUpload=nil;
        self.itemToUpload=nil;
        
        CNFAppSyncSystem *sync=[[CNFAppSyncSystem alloc]init];
        [sync startSync];
    }
}


-(void)sortOutUnsibmittedDoc{
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@" (attachmentStatus==%@ OR attachmentStatus==%@) AND status ==%@ AND uploadCount!=attachmentNo", @"Saved",@"Partially Submitted",@"Created"];
    
    [request setPredicate:predicate];
    
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *results= [context executeFetchRequest:request error:&error];
    
    for (NSManagedObject *obj in results) {
        
        self.itemToUpload = [[NSMutableDictionary alloc]init];
        
        if([obj valueForKey:@"documentType"]!=NULL){
            [self.itemToUpload setObject:[obj valueForKey:@"documentType"] forKey:@"docType"];
        }
        
        if([obj valueForKey:@"userName"]!=NULL){
            [self.itemToUpload setObject:[obj valueForKey:@"userName"] forKey:@"userName"];
        }
        
        if([obj valueForKey:@"date"]!=NULL){
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *stringFromDate = [formatter stringFromDate:[obj valueForKey:@"date"]];
            [self.itemToUpload setObject:[obj valueForKey:@"date"] forKey:@"date"];
            [self.itemToUpload setObject:[obj valueForKey:@"documentId"] forKey:@"docId"];
            [self.itemToUpload setObject:[self getListofFiles:stringFromDate] forKey:@"fileNames"];
        }
        [self.itemToUpload setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_ID"] forKey:@"deviceId"];
        [self.listOfItemsToUpload addObject:self.itemToUpload];
    }
}

- (NSString*)readDataFromFile:(NSString*)file {
    // Build the path...
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:file];
    NSData * pdfData=[NSData dataWithContentsOfFile:fileAtPath];
    return [pdfData base64EncodedStringWithOptions:0];
    
}

-(NSArray *)getListofFiles:(NSString*)fileName{
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    
    NSArray *filteredArray = [directoryContent filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains %@", fileName]];
    
    return filteredArray;
}

#pragma mark - web service functions

-(void)submitAttachment:(NSDictionary *)item{

    for(NSString* fileName in [item valueForKey:@"fileNames"]){
        submitAttachment * submitAttachmentFile = [[submitAttachment alloc]init];
        submitAttachmentFile.userName = [item objectForKey:@"userName"];
        submitAttachmentFile.deviceId = [item objectForKey:@"deviceId"];
        submitAttachmentFile.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];
        submitAttachmentFile.extension = [@"." stringByAppendingString:[fileName componentsSeparatedByString:@"."] [2]];
        submitAttachmentFile.docId = [item objectForKey:@"docId"];
        submitAttachmentFile.fileContent = [self readDataFromFile:fileName];
        submitAttachmentFile.attachmentCounter = [fileName componentsSeparatedByString:@"."][1];
        
        NSString * post = [submitAttachmentFile toJSONString];
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
        NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/submitAttachmentFile", appDelegate.serverUrl]]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:len forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];

        NSLog(@"attachement %@ start to submit at %@", fileName, [NSDate date]);

        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                          
                                              if(error){
                                                  NSLog(@"error getitng submit attachment response, will try later..");
                                              }else{
                                                  @try {
                                                      id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                      
                                                      if(error){
                                                          NSLog(@"Error serializing web service call..");
                                                      }else{
                                                          if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                              NSLog(@"web service error : %@", [dictionary valueForKey:@"Message"]);
                                                              NSLog(@"attachement %@ submission failed at %@", fileName, [NSDate date]);
                                                              
                                                              [self tryToResubmitWithDifferentToken:fileName docId:[item objectForKey:@"docId"]];
                                                              
                                                          }else{
                                                              
                                                              NSString * validationResult = [dictionary valueForKey:@"d"];
                                                              
                                                              if(![[dictionary valueForKey:@"d"] boolValue]){
                                                                  
                                                                  NSLog(@"attachment submisison error : %@", validationResult);
                                                                  NSLog(@"attachement %@ submission failed at %@, will try to resubmit the document", fileName, [NSDate date]);
                                                                  
                                                                  for(int i =0; i<2;i++){
                                                                      
                                                                      //wait 20 seconds for the next upload
                                                                      [NSThread sleepForTimeInterval:20.0];
                                                                      
                                                                      NSURLResponse* newResponse = nil;
                                                                      
                                                                      NSError * resubmisisonError;
                                                                      NSLog(@"trying to resubmit 0k image, attempt %d",i+1);
                                                                      NSData* newData = [NSURLConnection sendSynchronousRequest:request returningResponse:&newResponse error:&resubmisisonError];

                                                                      
////                                                                      NSURLSession *session = [NSURLSession sharedSession];
//                                                                      NSURLSessionDataTask *resubmissionDataTask = [session dataTaskWithRequest:request
//                                                                                                                  completionHandler:^(NSData *data, NSURLResponse *newResponse, NSError *resubmisisonError)
//                                                                                                        {
//                                                                                                        
//                                                                                                        }];
//                                                                      [resubmissionDataTask resume];
                                                                      
                                                                      if(resubmisisonError){
                                                                          NSLog(@"error getitng submit attachment response, will try later..");
                                                                          continue;
                                                                      }else{
                                                                          id dictionary = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&resubmisisonError];
                                                                          if(resubmisisonError){
                                                                              NSLog(@"Resubmission error, could not serialize web data, attachment submission failed..");
                                                                              continue;
                                                                          }else{
                                                                              if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                                                  NSLog(@"web service error : %@", [dictionary valueForKey:@"Message"]);
                                                                                  NSLog(@"attachement %@ submission failed at %@", fileName, [NSDate date]);
                                                                                  continue;
                                                                              }else{
                                                                                  if(![[dictionary valueForKey:@"d"] boolValue]){
                                                                                      NSLog(@"attachment submisison error : %@ on attempt %d", [dictionary valueForKey:@"d"], i+1);
                                                                                      continue;
                                                                                  }else{
                                                                                      NSLog(@"attachment successfully submitted on %d attempt", i+1);
                                                                                      break;
                                                                                  }
                                                                              }
                                                                          }
                                                                      }
                                                                  }
                                                                  
                                                                  NSLog(@"0k problem..file submitted: %@ with result:%@", fileName, validationResult);
                                                                  NSLog(@"attachement %@ submission ended at %@", fileName, [NSDate date]);
                                                                  
                                                                  NSError *error;
                                                                  
                                                                  NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                                                  NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                                                  NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId ==%@ AND status ==%@ AND (attachmentStatus==%@ OR attachmentStatus==%@)", [item objectForKey:@"docId"], @"Created", @"Saved", @"Partially Submitted"];
                                                                  [request setPredicate:predicate];
                                                                  [request setEntity:entityDesc];
                                                                  
                                                                  NSArray *results= [context executeFetchRequest:request error:&error];
                                                                  
                                                                  for (NSManagedObject *obj in results) {
                                                                      
                                                                      int newValue = [[obj valueForKey:@"uploadCount"]intValue]+1;
                                                                      
                                                                      [obj setValue:[NSNumber numberWithInt:newValue ] forKey:@"uploadCount"];
                                                                      
                                                                      NSLog(@"upload count value set to : %d",newValue );
                                                                      
                                                                      if(newValue>=[[obj valueForKey:@"attachmentNo"] intValue]){
                                                                          [obj setValue:@"Submitted" forKey:@"attachmentStatus"];
                                                                      }else{
                                                                          [obj setValue:@"Partially Submitted" forKey:@"attachmentStatus"];
                                                                      }
                                                                  }
                                                                  [context save:&error];
                                                                  
                                                                  if(!error){
                                                                      NSLog(@"DB entry for docId: %@ created", [item objectForKey:@"docId"]);
                                                                      
                                                                      NSString* dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"];
                                                                      if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                                                                          [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
                                                                      
                                                                      NSString* originPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"] stringByAppendingPathComponent:fileName];
                                                                      NSString* finalPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"] stringByAppendingPathComponent:fileName];
                                                                      
                                                                      if([[NSFileManager defaultManager] copyItemAtPath:originPath toPath:finalPath error:&error]){
                                                                          if([[NSFileManager defaultManager] removeItemAtPath:originPath error:&error]){
                                                                              NSLog(@"%@ moved and deleted..", fileName);
                                                                          }else{
                                                                              NSLog(@"%@ moved but not deleted..", fileName);
                                                                          }
                                                                      }else{
                                                                          NSLog(@"%@ could not be moved or deleted..", fileName);
                                                                      }
                                                                  }else{
                                                                      NSLog(@"Error occured in puting entry in DB for docId: %@, filename: %@", [item objectForKey:@"docId"], fileName);
                                                                      NSLog(@"Will try uploading this image again..");
                                                                  }
                                                                  
                                                                  
                                                              }
                                                              
                                                              
                                                              else{
                                                                  
                                                                  NSLog(@"file submitted: %@ with result:%@", fileName, validationResult);
                                                                  NSLog(@"attachement %@ submission successfully ended at %@", fileName, [NSDate date]);
                                                                  
                                                                  NSError *error;
                                                                  
                                                                  NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                                                  NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                                                  NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId ==%@ AND status ==%@ AND (attachmentStatus==%@ OR attachmentStatus==%@)", [item objectForKey:@"docId"], @"Created", @"Saved", @"Partially Submitted"];
                                                                  [request setPredicate:predicate];
                                                                  [request setEntity:entityDesc];
                                                                  
                                                                  NSArray *results= [context executeFetchRequest:request error:&error];
                                                                  
                                                                  for (NSManagedObject *obj in results) {
                                                                      
                                                                      int newValue = [[obj valueForKey:@"uploadCount"]intValue]+1;
                                                                      
                                                                      [obj setValue:[NSNumber numberWithInt:newValue ] forKey:@"uploadCount"];
                                                                      
                                                                      NSLog(@"upload count value set to : %d",newValue );
                                                                      
                                                                      if(newValue>=[[obj valueForKey:@"attachmentNo"] intValue]){
                                                                          [obj setValue:@"Submitted" forKey:@"attachmentStatus"];
                                                                      }else{
                                                                          [obj setValue:@"Partially Submitted" forKey:@"attachmentStatus"];
                                                                      }
                                                                  }
                                                                  [context save:&error];
                                                                  
                                                                  if(!error){
                                                                      NSLog(@"DB entry for docId: %@ created", [item objectForKey:@"docId"]);
                                                                      
                                                                      NSString* dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"];
                                                                      if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                                                                          [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
                                                                      
                                                                      NSString* originPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"] stringByAppendingPathComponent:fileName];
                                                                      NSString* finalPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"] stringByAppendingPathComponent:fileName];
                                                                      
                                                                      if([[NSFileManager defaultManager] copyItemAtPath:originPath toPath:finalPath error:&error]){
                                                                          if([[NSFileManager defaultManager] removeItemAtPath:originPath error:&error]){
                                                                              NSLog(@"%@ moved and deleted..", fileName);
                                                                          }else{
                                                                              NSLog(@"%@ moved but not deleted..", fileName);
                                                                          }
                                                                      }else{
                                                                          NSLog(@"%@ could not be moved or deleted..", fileName);
                                                                      }
                                                                  }else{
                                                                      NSLog(@"Error occured in puting entry in DB for docId: %@, filename: %@", [item objectForKey:@"docId"], fileName);
                                                                      NSLog(@"Will try uploading this image again..");
                                                                  }
                                                              }
                                                          }
                                                      }
                                                      
                                                  } @catch (NSException *exception) {
                                                      NSLog(@"exception %@ occured while submitting and processing document: %@", exception, [item valueForKey:@"docId"]);
                                                  }
                                              }
                                          }];
        [dataTask resume];
    }
}

-(void)tryToResubmitWithDifferentToken:(NSString*)fileName docId:(NSString*)docId{

        submitAttachment * submitAttachmentFile = [[submitAttachment alloc]init];
        submitAttachmentFile.userName = @"CNFMobileSystem";
        submitAttachmentFile.deviceId = @"TestDevice";
        submitAttachmentFile.token = @"YJVLNEVFZA";
        submitAttachmentFile.extension = [@"." stringByAppendingString:[fileName componentsSeparatedByString:@"."] [2]];
        submitAttachmentFile.docId = docId;
        submitAttachmentFile.fileContent = [self readDataFromFile:fileName];
        submitAttachmentFile.attachmentCounter = [fileName componentsSeparatedByString:@"."][1];
        
        NSString * post = [submitAttachmentFile toJSONString];
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
        NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/submitAttachmentFile", appDelegate.serverUrl]]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:len forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];

        NSLog(@"attachement %@ start to submit at %@", fileName, [NSDate date]);

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(error){
                                              NSLog(@"error getitng submit attachment response, will try later..");
                                          }else{
                                              
                                              @try {
                                                  id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                  
                                                  if(error){
                                                      NSLog(@"Error serializing web service call");
                                                  }else{
                                                      if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                          NSLog(@"web service error : %@", [dictionary valueForKey:@"Message"]);
                                                          NSLog(@"attachement %@ submission failed at %@", fileName, [NSDate date]);
                                                          
                                                      }else{
                                                          NSString * validationResult = [dictionary valueForKey:@"d"];
                                                          if(![[dictionary valueForKey:@"d"] boolValue]){
                                                              
                                                              NSLog(@"attachment submisison error : %@", validationResult);
                                                              NSLog(@"attachement %@ submission failed at %@, will try to resubmit the document", fileName, [NSDate date]);
                                                              
                                                              for(int i =0; i<2;i++){
                                                                  
                                                                  //wait 20 seconds for the next upload
                                                                  [NSThread sleepForTimeInterval:20.0];
                                                                  
                                                                  NSURLResponse* newResponse = nil;
                                                                  
                                                                  NSError * resubmisisonError;
                                                                  NSLog(@"trying to resubmit 0k image, attempt %d",i+1);
                                                                  NSData* newData = [NSURLConnection sendSynchronousRequest:request returningResponse:&newResponse error:&resubmisisonError];
                                                                  
                                                                  if(resubmisisonError){
                                                                      NSLog(@"error getitng submit attachment response, will try later..");
                                                                      continue;
                                                                  }else{
                                                                      id dictionary = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&resubmisisonError];
                                                                      if(resubmisisonError){
                                                                          NSLog(@"Resubmission error, could not serialize web data, attachment submission failed..");
                                                                          continue;
                                                                      }else{
                                                                          if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                                              NSLog(@"web service error : %@", [dictionary valueForKey:@"Message"]);
                                                                              NSLog(@"attachement %@ submission failed at %@", fileName, [NSDate date]);
                                                                              continue;
                                                                          }else{
                                                                              if(![[dictionary valueForKey:@"d"] boolValue]){
                                                                                  NSLog(@"attachment submisison error : %@ on attempt %d", [dictionary valueForKey:@"d"], i+1);
                                                                                  continue;
                                                                              }else{
                                                                                  NSLog(@"attachment successfully submitted on %d attempt", i+1);
                                                                                  break;
                                                                              }
                                                                          }
                                                                      }
                                                                  }
                                                              }
                                                              
                                                              NSLog(@"0k problem..file submitted: %@ with result:%@", fileName, validationResult);
                                                              NSLog(@"attachement %@ submission ended at %@", fileName, [NSDate date]);
                                                              
                                                              NSError *error;
                                                              
                                                              NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                                              NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                                              NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId ==%@ AND status ==%@ AND (attachmentStatus==%@ OR attachmentStatus==%@)", docId, @"Created", @"Saved", @"Partially Submitted"];
                                                              [request setPredicate:predicate];
                                                              [request setEntity:entityDesc];
                                                              
                                                              NSArray *results= [context executeFetchRequest:request error:&error];
                                                              
                                                              for (NSManagedObject *obj in results) {
                                                                  
                                                                  int newValue = [[obj valueForKey:@"uploadCount"]intValue]+1;
                                                                  
                                                                  [obj setValue:[NSNumber numberWithInt:newValue ] forKey:@"uploadCount"];
                                                                  
                                                                  NSLog(@"upload count value set to : %d",newValue );
                                                                  
                                                                  if(newValue>=[[obj valueForKey:@"attachmentNo"] intValue]){
                                                                      [obj setValue:@"Submitted" forKey:@"attachmentStatus"];
                                                                  }else{
                                                                      [obj setValue:@"Partially Submitted" forKey:@"attachmentStatus"];
                                                                  }
                                                              }
                                                              [context save:&error];
                                                              
                                                              if(!error){
                                                                  NSLog(@"DB entry for docId: %@ created", docId);
                                                                  
                                                                  NSString* dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"];
                                                                  if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                                                                      [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
                                                                  
                                                                  NSString* originPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"] stringByAppendingPathComponent:fileName];
                                                                  NSString* finalPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"] stringByAppendingPathComponent:fileName];
                                                                  
                                                                  if([[NSFileManager defaultManager] copyItemAtPath:originPath toPath:finalPath error:&error]){
                                                                      if([[NSFileManager defaultManager] removeItemAtPath:originPath error:&error]){
                                                                          NSLog(@"%@ moved and deleted..", fileName);
                                                                      }else{
                                                                          NSLog(@"%@ moved but not deleted..", fileName);
                                                                      }
                                                                  }else{
                                                                      NSLog(@"%@ could not be moved or deleted..", fileName);
                                                                  }
                                                              }else{
                                                                  NSLog(@"Error occured in puting entry in DB for docId: %@, filename: %@", docId, fileName);
                                                                  NSLog(@"Will try uploading this image again..");
                                                              }
                                                              
                                                          }
                                                          
                                                          else{
                                                              
                                                              NSLog(@"file submitted: %@ with result:%@", fileName, validationResult);
                                                              NSLog(@"attachement %@ submission successfully ended at %@", fileName, [NSDate date]);
                                                              
                                                              NSError *error;
                                                              
                                                              NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                                              NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                                              NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId ==%@ AND status ==%@ AND (attachmentStatus==%@ OR attachmentStatus==%@)", docId, @"Created", @"Saved", @"Partially Submitted"];
                                                              [request setPredicate:predicate];
                                                              [request setEntity:entityDesc];
                                                              
                                                              NSArray *results= [context executeFetchRequest:request error:&error];
                                                              
                                                              for (NSManagedObject *obj in results) {
                                                                  
                                                                  int newValue = [[obj valueForKey:@"uploadCount"]intValue]+1;
                                                                  
                                                                  [obj setValue:[NSNumber numberWithInt:newValue ] forKey:@"uploadCount"];
                                                                  
                                                                  NSLog(@"upload count value set to : %d",newValue );
                                                                  
                                                                  if(newValue>=[[obj valueForKey:@"attachmentNo"] intValue]){
                                                                      [obj setValue:@"Submitted" forKey:@"attachmentStatus"];
                                                                  }else{
                                                                      [obj setValue:@"Partially Submitted" forKey:@"attachmentStatus"];
                                                                  }
                                                              }
                                                              [context save:&error];
                                                              
                                                              if(!error){
                                                                  NSLog(@"DB entry for docId: %@ created", docId);
                                                                  
                                                                  NSString* dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"];
                                                                  if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                                                                      [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
                                                                  
                                                                  NSString* originPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"] stringByAppendingPathComponent:fileName];
                                                                  NSString* finalPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"] stringByAppendingPathComponent:fileName];
                                                                  
                                                                  if([[NSFileManager defaultManager] copyItemAtPath:originPath toPath:finalPath error:&error]){
                                                                      if([[NSFileManager defaultManager] removeItemAtPath:originPath error:&error]){
                                                                          NSLog(@"%@ moved and deleted..", fileName);
                                                                      }else{
                                                                          NSLog(@"%@ moved but not deleted..", fileName);
                                                                      }
                                                                  }else{
                                                                      NSLog(@"%@ could not be moved or deleted..", fileName);
                                                                  }
                                                              }else{
                                                                  NSLog(@"Error occured in puting entry in DB for docId: %@, filename: %@", docId, fileName);
                                                                  NSLog(@"Will try uploading this image again..");
                                                              }
                                                          }
                                                      }
                                                      
                                                  }
                                                  
                                              } @catch (NSException *exception) {
                                                  NSLog(@"exception %@ occured while submitting and processing document: %@", exception, docId);
                                              }
                                          }
                                      
                                      }];
    [dataTask resume];
    
    
    
    
}


@end
