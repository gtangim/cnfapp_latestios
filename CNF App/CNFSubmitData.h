//
//  CNFSubmitData.h
//  CNF App
//
//  Created by Anik on 2015-05-08.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNFAppDelegate.h"

@interface CNFSubmitData : NSObject<NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

-(void)startSubmittingData;


@end
