//
//  CNFSubmitData.m
//  CNF App
//
//  Created by Anik on 2015-05-08.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "CNFSubmitData.h"
#import "UserElement.h"
#import "SSKeychain.h"
#import "SubmitDataElement.h"
#import "ValidUserItems.h"
#import "CNFGetDocStatus.h"
#import "OutOfStockAuditData.h"

#import "transferCreationAuditData.h"
#import "transferReceiveAuditData.h"
#import "receiptReceiveAuditData.h"
#import "vosReceiveAuditData.h"
#import "vosCreationAuditData.h"

#import "transferCreationSubmitDataElement.h"
#import "transferReceiveSubmitDataElement.h"
#import "vosCreationSubmitDataElement.h"
#import "vosReceiveSubmitDataElement.h"
#import "receiptReceiveSubmitDataElement.h"

#import "SupplySubmitDataElement.h"
#import "SupplyAuditData.h"

#import "adjustmentSubmitDataElement.h"
#import "adjustmentAuditData.h"

#import "DiscardShipElement.h"
#import "DiscardShipSubmitDataElement.h"

#import "BaseAdjustmentRequest.h"

#import "WarehousePicking.h"
#import "WarehousePickingSubmitRequest.h"

#import "CNFSubmitAttachment.h"

#import "ProduceTracking.h"
#import "ProduceTrackingSubmitRequest.h"

#import "ShelfTagBatchData.h"
#import "ShelfTagBatchSubmit.h"

#import "ProduceReceivingModel.h"
#import "ProduceReceiveSubmitDataElement.h"

@interface CNFSubmitData ()
@property (nonatomic, strong) NSMutableArray * listOfItemsToUpload;
@property (nonatomic, strong) NSMutableDictionary * itemToUpload;
@end


@implementation CNFSubmitData{
    NSManagedObjectContext *context;
}


@synthesize listOfItemsToUpload = _listOfItemsToUpload, itemToUpload=_itemToUpload;


-(NSMutableArray *)listOfItemsToUpload{
    if(!_listOfItemsToUpload)_listOfItemsToUpload = [[NSMutableArray alloc]init];
    return _listOfItemsToUpload;
}

-(void)startSubmittingData{

     NSLog(@"Is Queue running : %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"BACK_PROCESS"]);
    
    [self sortOutUnsibmittedDoc];

    if(self.listOfItemsToUpload.count!=0){
        for (NSDictionary *item in self.listOfItemsToUpload) {
            [self submitItem:item];
        }
    }
    self.listOfItemsToUpload=nil;
    self.itemToUpload=nil;

    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BACK_PROCESS"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    NSLog(@"Background process for uploading doc finished...");


//    if(self.listOfItemsToUpload.count==0){
//        NSLog(@"no document to submit..proceede next..");
//
//        self.listOfItemsToUpload=nil;
//        self.itemToUpload=nil;
//
//        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BACK_PROCESS"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//
//        NSLog(@"Background process for uploading doc finished...");
//
//    }else{
//        for (NSDictionary *item in self.listOfItemsToUpload) {
//            [self submitItem:item];
//        }
//
//        self.listOfItemsToUpload=nil;
//        self.itemToUpload=nil;
//        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BACK_PROCESS"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//
//        NSLog(@"Background process for uploading doc finished...");
//    }
}


-(void)sortOutUnsibmittedDoc{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];

    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"status ==%@ AND docUploaded==NO", @"Finished"];

    [request setPredicate:predicate];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *results= [context executeFetchRequest:request error:&error];
    
    for (NSManagedObject *obj1 in results) {
        
        NSFetchRequest *request2 = [[NSFetchRequest alloc]init];
        NSPredicate *predicate2   = [NSPredicate predicateWithFormat:@"status ==%@ AND documentId==%@ AND uploadCount==attachmentNo", @"Created", [obj1 valueForKey:@"documentId"]];
        
        [request2 setPredicate:predicate2];
        [request2 setEntity:entityDesc];
        NSError *error;
        NSArray *results2= [context executeFetchRequest:request2 error:&error];
        
        for (NSManagedObject *obj2 in results2){
            self.itemToUpload = [[NSMutableDictionary alloc]init];
            
            if([obj1 valueForKey:@"documentId"]!=NULL){
                [self.itemToUpload setObject:[obj1 valueForKey:@"documentId"] forKey:@"docId"];
            }
            
            if([obj1 valueForKey:@"documentType"]!=NULL){
                [self.itemToUpload setObject:[obj1 valueForKey:@"documentType"] forKey:@"docType"];
            }
            
            if([obj1 valueForKey:@"userName"]!=NULL){
                [self.itemToUpload setObject:[obj1 valueForKey:@"userName"] forKey:@"userName"];
            }
            
            if([obj1 valueForKey:@"date"]!=NULL){
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *stringFromDate = [formatter stringFromDate:[obj1 valueForKey:@"date"]];
                [self.itemToUpload setObject:stringFromDate forKey:@"fileName"];
                [self.itemToUpload setObject:[obj1 valueForKey:@"date"] forKey:@"date"];
            }
            [self.itemToUpload setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_ID"] forKey:@"deviceId"];
            [self.listOfItemsToUpload addObject:self.itemToUpload];
            
        }
    }
}

- (NSString*)readStringFromFile:(NSString*)file {
    
    // Build the path...
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    NSString* fileName = file;
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    // The main act...
    return [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
}

#pragma mark - web service functions

-(void)submitItem:(NSDictionary*)item{
    NSString * post;
    NSError * error;
    @try {
        if([[item objectForKey:@"docType"]isEqualToString:@"OUT_OF_STOCK_AUDIT_SUBMISSION"]){
            
            OutOfStockAuditData * auditData = [[OutOfStockAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            SubmitDataElement * submitItem = [[SubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<OutOfStockAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;
            
            post = [submitItem toJSONString];
            
        }
        else if([[item objectForKey:@"docType"]isEqualToString:@"PICKING"]){
            
            WarehousePicking * auditData = [[WarehousePicking alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            WarehousePickingSubmitRequest * submitItem = [[WarehousePickingSubmitRequest alloc]init];
            
            submitItem.rcvdItems = [[NSArray<WarehousePicking> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];
            post = [submitItem toJSONString];
            
        }


        else if([[item objectForKey:@"docType"]isEqualToString:@"SHELF_TAG_BATCH_CREATE"]){
            ShelfTagBatchData * auditData = [[ShelfTagBatchData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];

            ShelfTagBatchSubmit * submitItem = [[ShelfTagBatchSubmit alloc]init];

            submitItem.rcvdItems = [[NSArray<ShelfTagBatchData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;

            post = [submitItem toJSONString];
        }


        else if([[item objectForKey:@"docType"]isEqualToString:@"PRODUCE_TRACKING"]){

            ProduceTracking * auditData = [[ProduceTracking alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];

            ProduceTrackingSubmitRequest * submitItem = [[ProduceTrackingSubmitRequest alloc]init];

            submitItem.rcvdItems = [[NSArray<ProduceTracking> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];
            post = [submitItem toJSONString];

        }


        else if([[item objectForKey:@"docType"]isEqualToString:@"PRODUCE_RECEIVING"]){

            ProduceReceivingModel * auditData = [[ProduceReceivingModel alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];

            ProduceReceiveSubmitDataElement * submitItem = [[ProduceReceiveSubmitDataElement alloc]init];

            submitItem.rcvdItems = [[NSArray<ProduceReceivingModel> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];
            post = [submitItem toJSONString];

        }

        else if([[item objectForKey:@"docType"]isEqualToString:@"ADJ_NFP_DISCARD_SHIP"]){
            
            DiscardShipElement * auditelement = [[DiscardShipElement alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            DiscardShipSubmitDataElement * submitItem = [[DiscardShipSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<DiscardShipElement> alloc] initWithObjects:auditelement, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"QOH_ADJUSTMENT_SUBMISSION"]){
            
            SupplyAuditData * auditData = [[SupplyAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            SupplySubmitDataElement * submitItem = [[SupplySubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<SupplyAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"TRANSFER_CREATION"]){
            
            transferCreationAuditData * auditData = [[transferCreationAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            transferCreationSubmitDataElement * submitItem = [[transferCreationSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<transferCreationAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"TRANSFER_RECEIVE"]){
            
            transferReceiveAuditData * auditData = [[transferReceiveAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            transferReceiveSubmitDataElement * submitItem = [[transferReceiveSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<transferReceiveAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"RECEIPT_RECEIVE"]){
            
            receiptReceiveAuditData * auditData = [[receiptReceiveAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            receiptReceiveSubmitDataElement * submitItem = [[receiptReceiveSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<receiptReceiveAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = @"RECEIPT_RECEIVE";
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"VOS_RECEIVE"]){
            
            vosReceiveAuditData * auditData = [[vosReceiveAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            vosReceiveSubmitDataElement * submitItem = [[vosReceiveSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<vosReceiveAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"VOS_CREATION"]){
            
            vosCreationAuditData * auditData = [[vosCreationAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            vosCreationSubmitDataElement * submitItem = [[vosCreationSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<vosCreationAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];;
            
            post = [submitItem toJSONString];
        }
        
        
        //checking adjustment documents seperately from this point
        
        else if([[item objectForKey:@"docType"]isEqualToString:@"C2_CAFE"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_MARKETING"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_NFP_DISCARD"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_RET_DISCARD"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_SPOILED"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_STAFF_BENEFIT"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_STAFF_ERROR"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_VENDOR_ERROR"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_PRODUCTION_INVENTORY_DECREASE"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_PRODUCTION_INVENTORY_INCREASE"]){
            
            BaseAdjustmentRequest * auditData = [[BaseAdjustmentRequest alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            adjustmentSubmitDataElement * submitItem = [[adjustmentSubmitDataElement alloc]init];
            
//            for(AdjustmentItem * item in auditData.Items){
//                item.Product.ListPrice = [NSDecimalNumber numberWithDouble:[item.Product.ListPrice doubleValue]];
//                item.Product.ActivePrice = [NSDecimalNumber numberWithDouble:[item.Product.ActivePrice doubleValue]];
//                item.Product.Cost = [NSDecimalNumber numberWithDouble:[item.Product.Cost doubleValue]];
//                item.Product.BottleDeposit = [NSDecimalNumber numberWithDouble:[item.Product.BottleDeposit doubleValue]];
//                item.Product.EnvironmentalFee = [NSDecimalNumber numberWithDouble:[item.Product.EnvironmentalFee doubleValue]];
//                item.Product.TaxRate = [NSDecimalNumber numberWithDouble:[item.Product.TaxRate doubleValue]];
//                
//                item.ValueCost = [NSDecimalNumber numberWithDouble:[item.ValueCost doubleValue]];
//                item.ValueListPrice = [NSDecimalNumber numberWithDouble:[item.ValueListPrice doubleValue]];
//                item.ValueActivePrice = [NSDecimalNumber numberWithDouble:[item.ValueActivePrice doubleValue]];
//                item.ValueBottleDeposit = [NSDecimalNumber numberWithDouble:[item.ValueBottleDeposit doubleValue]];
//                item.ValueTax = [NSDecimalNumber numberWithDouble:[item.ValueTax doubleValue]];
//                item.ValueEnvironmentalFee = [NSDecimalNumber numberWithDouble:[item.ValueEnvironmentalFee doubleValue]];
//            }
            
            submitItem.rcvdItems = [[NSArray<BaseAdjustmentRequest> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = [item objectForKey:@"userName"];
            submitItem.deviceId = [item objectForKey:@"deviceId"];
            submitItem.token = [SSKeychain  passwordForService:@"CNFApp-Token" account:[item objectForKey:@"userName"]];
            post = [submitItem toJSONString];
        }
        
        if(error){
            [NSException raise:@"JSON model population raised error" format:@"%@", error];
        }
    } @catch (NSException *exception) {
        NSLog(@"exception %@ thrown by the JSON model converted in submitData class",exception);
    }

    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];

    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/submitData", appDelegate.serverUrl]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
//    NSURLResponse* response = nil;
    
    NSLog(@"doc %@ start to submit at %@", [item objectForKey:@"docId"], [NSDate date]);
    
//    NSError* webError;
//    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&webError];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *webError)
                                      {
                                          if(webError){
                                              NSLog(@"error getitng user token for submit doc..");
                                              NSLog(@"doc %@ failed to submit at %@", [item objectForKey:@"docId"], [NSDate date]);
                                          }else{
                                              @try {
                                                  NSError * parseError;
                                                  id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                  
                                                  if(parseError){
                                                      NSLog(@"Error serializing data from web service, Submit data failed..");
                                                  }else{
                                                      if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                          NSLog(@"web service error submit: %@", [dictionary valueForKey:@"Message"]);
                                                          NSLog(@"doc %@ failed to submit at %@", [item objectForKey:@"docId"], [NSDate date]);
                                                          
                                                          [self tryToResubmitWithDifferentToken:item];
                                                          
                                                      }else{
                                                          NSLog(@"%@", [dictionary valueForKey:@"d"]);
                                                          NSLog(@"doc %@ successfully submitted at %@", [item objectForKey:@"docId"], [NSDate date]);
                                                          
                                                          //            save changes to DB
                                                          CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
                                                          context =[appDelegate managedObjectContext];
                                                          
                                                          NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                                          NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                                          
                                                          NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@ AND status ==%@ AND docUploaded==NO",[item valueForKey:@"docId"], @"Finished"];
                                                          
                                                          [request setPredicate:predicate];
                                                          [request setEntity:entityDesc];
                                                          
                                                          NSError *error;
                                                          NSArray *results= [context executeFetchRequest:request error:&error];
                                                          
                                                          for (NSManagedObject * obj in results){
                                                              [obj setValue:[NSNumber numberWithBool:YES] forKey:@"docUploaded"];
                                                          }
                                                          [context save:&error];
                                                          if(!error)
                                                              NSLog(@"doc Uploaded and saved to DB..");
                                                          else
                                                              NSLog(@"error saving the doc to DB..");
                                                      }
                                                  }
                                                  
                                              } @catch (NSException *exception) {
                                                  NSLog(@"Exception happened during doc submission : %@", [item objectForKey:@"docId"]);
                                              }
                                          }
                                      }];
    [dataTask resume];
    
    }


-(void)tryToResubmitWithDifferentToken:(NSDictionary*)item{

    NSString * post;
    NSError * error;
    @try {
        if([[item objectForKey:@"docType"]isEqualToString:@"OUT_OF_STOCK_AUDIT_SUBMISSION"]){
            
            OutOfStockAuditData * auditData = [[OutOfStockAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            SubmitDataElement * submitItem = [[SubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<OutOfStockAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
            
        }
        else if([[item objectForKey:@"docType"]isEqualToString:@"PICKING"]){
            WarehousePicking * auditData = [[WarehousePicking alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            WarehousePickingSubmitRequest * submitItem = [[WarehousePickingSubmitRequest alloc]init];
            
            submitItem.rcvdItems = [[NSArray<WarehousePicking> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
        }


        else if([[item objectForKey:@"docType"]isEqualToString:@"SHELF_TAG_BATCH_CREATE"]){
            ShelfTagBatchData * auditData = [[ShelfTagBatchData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];

            ShelfTagBatchSubmit * submitItem = [[ShelfTagBatchSubmit alloc]init];

            submitItem.rcvdItems = [[NSArray<ShelfTagBatchData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";

            post = [submitItem toJSONString];
        }


        else if([[item objectForKey:@"docType"]isEqualToString:@"PRODUCE_TRACKING"]){

            ProduceTracking * auditData = [[ProduceTracking alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];

            ProduceTrackingSubmitRequest * submitItem = [[ProduceTrackingSubmitRequest alloc]init];

            submitItem.rcvdItems = [[NSArray<ProduceTracking> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";

            post = [submitItem toJSONString];

        }

        else if([[item objectForKey:@"docType"]isEqualToString:@"PRODUCE_RECEIVING"]){

            ProduceReceivingModel * auditData = [[ProduceReceivingModel alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];

            ProduceReceiveSubmitDataElement * submitItem = [[ProduceReceiveSubmitDataElement alloc]init];

            submitItem.rcvdItems = [[NSArray<ProduceReceivingModel> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            post = [submitItem toJSONString];

        }


        else if([[item objectForKey:@"docType"]isEqualToString:@"ADJ_NFP_DISCARD_SHIP"]){
            
            DiscardShipElement * auditelement = [[DiscardShipElement alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            DiscardShipSubmitDataElement * submitItem = [[DiscardShipSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<DiscardShipElement> alloc] initWithObjects:auditelement, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"QOH_ADJUSTMENT_SUBMISSION"]){
            
            SupplyAuditData * auditData = [[SupplyAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            SupplySubmitDataElement * submitItem = [[SupplySubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<SupplyAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"TRANSFER_CREATION"]){
            
            transferCreationAuditData * auditData = [[transferCreationAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            transferCreationSubmitDataElement * submitItem = [[transferCreationSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<transferCreationAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"TRANSFER_RECEIVE"]){
            
            transferReceiveAuditData * auditData = [[transferReceiveAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            transferReceiveSubmitDataElement * submitItem = [[transferReceiveSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<transferReceiveAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"RECEIPT_RECEIVE"]){
            
            receiptReceiveAuditData * auditData = [[receiptReceiveAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            receiptReceiveSubmitDataElement * submitItem = [[receiptReceiveSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<receiptReceiveAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = @"RECEIPT_RECEIVE";
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"VOS_RECEIVE"]){
            
            vosReceiveAuditData * auditData = [[vosReceiveAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            vosReceiveSubmitDataElement * submitItem = [[vosReceiveSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<vosReceiveAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
            
        }else if([[item objectForKey:@"docType"]isEqualToString:@"VOS_CREATION"]){
            
            vosCreationAuditData * auditData = [[vosCreationAuditData alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            vosCreationSubmitDataElement * submitItem = [[vosCreationSubmitDataElement alloc]init];
            
            submitItem.rcvdItems = [[NSArray<vosCreationAuditData> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];
        }
        
        else if([[item objectForKey:@"docType"]isEqualToString:@"C2_CAFE"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_MARKETING"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_NFP_DISCARD"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_RET_DISCARD"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_SPOILED"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_STAFF_BENEFIT"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_STAFF_ERROR"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_VENDOR_ERROR"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_PRODUCTION_INVENTORY_DECREASE"]||[[item objectForKey:@"docType"]isEqualToString:@"C2_PRODUCTION_INVENTORY_INCREASE"]){
            
            BaseAdjustmentRequest * auditData = [[BaseAdjustmentRequest alloc]initWithString:[self readStringFromFile:[item valueForKey:@"fileName"]] error:&error];
            
            adjustmentSubmitDataElement * submitItem = [[adjustmentSubmitDataElement alloc]init];
            
//            for(AdjustmentItem * item in auditData.Items){
//                item.Product.ListPrice = [NSDecimalNumber numberWithDouble:[item.Product.ListPrice doubleValue]];
//                item.Product.ActivePrice = [NSDecimalNumber numberWithDouble:[item.Product.ActivePrice doubleValue]];
//                item.Product.Cost = [NSDecimalNumber numberWithDouble:[item.Product.Cost doubleValue]];
//                item.Product.BottleDeposit = [NSDecimalNumber numberWithDouble:[item.Product.BottleDeposit doubleValue]];
//                item.Product.EnvironmentalFee = [NSDecimalNumber numberWithDouble:[item.Product.EnvironmentalFee doubleValue]];
//                item.Product.TaxRate = [NSDecimalNumber numberWithDouble:[item.Product.TaxRate doubleValue]];
//
//                item.ValueCost = [NSDecimalNumber numberWithDouble:[item.ValueCost doubleValue]];
//                item.ValueListPrice = [NSDecimalNumber numberWithDouble:[item.ValueListPrice doubleValue]];
//                item.ValueActivePrice = [NSDecimalNumber numberWithDouble:[item.ValueActivePrice doubleValue]];
//                item.ValueBottleDeposit = [NSDecimalNumber numberWithDouble:[item.ValueBottleDeposit doubleValue]];
//                item.ValueTax = [NSDecimalNumber numberWithDouble:[item.ValueTax doubleValue]];
//                item.ValueEnvironmentalFee = [NSDecimalNumber numberWithDouble:[item.ValueEnvironmentalFee doubleValue]];
//
//            }

            submitItem.rcvdItems = [[NSArray<BaseAdjustmentRequest> alloc] initWithObjects:auditData, nil];
            submitItem.docId = [item objectForKey:@"docId"];
            submitItem.docType = [item objectForKey:@"docType"];
            submitItem.userName = @"CNFMobileSystem";
            submitItem.deviceId = @"TestDevice";
            submitItem.token = @"YJVLNEVFZA";
            
            post = [submitItem toJSONString];

        }
        
        if(error){
            [NSException raise:@"JSON model population raised error" format:@"%@", error];
        }
    } @catch (NSException *exception) {
        NSLog(@"exception %@ thrown by the JSON model converted in submitData class",exception);
    }
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/submitData", appDelegate.serverUrl]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
//    NSURLResponse* response = nil;
    
    NSLog(@"doc %@ start to submit at %@", [item objectForKey:@"docId"], [NSDate date]);
    
//    NSError* webError;
//    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&webError];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *webError)
                                      {
                                          if(webError){
                                              NSLog(@"error getitng user token for submit doc..");
                                              NSLog(@"doc %@ failed to submit at %@", [item objectForKey:@"docId"], [NSDate date]);
                                          }else{
                                              @try {
                                                  NSError * parseError;
                                                  id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                  
                                                  if(parseError){
                                                      NSLog(@"Failed to serialize data from web service, parse error, submit data failed");
                                                  }else{
                                                      if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                          NSLog(@"web service error submit: %@", [dictionary valueForKey:@"Message"]);
                                                          NSLog(@"doc %@ failed to submit at %@", [item objectForKey:@"docId"], [NSDate date]);
                                                          
                                                      }else{
                                                          NSLog(@"%@", [dictionary valueForKey:@"d"]);
                                                          NSLog(@"doc %@ successfully submitted at %@", [item objectForKey:@"docId"], [NSDate date]);
                                                          
                                                          //            save changes to DB
                                                          CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
                                                          context =[appDelegate managedObjectContext];
                                                          
                                                          NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                                          NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                                          
                                                          NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@ AND status ==%@ AND docUploaded==NO",[item valueForKey:@"docId"], @"Finished"];
                                                          
                                                          [request setPredicate:predicate];
                                                          [request setEntity:entityDesc];
                                                          
                                                          NSError *error;
                                                          NSArray *results= [context executeFetchRequest:request error:&error];
                                                          
                                                          for (NSManagedObject * obj in results){
                                                              [obj setValue:[NSNumber numberWithBool:YES] forKey:@"docUploaded"];
                                                          }
                                                          [context save:&error];
                                                          if(!error)
                                                              NSLog(@"doc Uploaded and saved to DB..");
                                                          else
                                                              NSLog(@"error saving the doc to DB..");
                                                      }
                                                  }
                                                  
                                              } @catch (NSException *exception) {
                                                  NSLog(@"Exception happened during doc submission : %@", [item objectForKey:@"docId"]);
                                              }
                                          }
                                      }];
    [dataTask resume];
    
    

}



@end
