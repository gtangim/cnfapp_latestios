//
//  CNFUserLoginTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-08-12.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNFUserLoginTableViewController : UITableViewController<UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UITextField *userNameText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;

- (IBAction)logIn:(id)sender;

@end
