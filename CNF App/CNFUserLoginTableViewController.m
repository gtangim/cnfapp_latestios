//
//  CNFUserLoginTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-08-12.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFUserLoginTableViewController.h"
#import "Reachability.h"
#import "ValidUserItems.h"
#import "UserElement.h"
#import "SSKeychain.h"
#import "CNFAppDelegate.h"
#import "CNFAppEnums.h"

@interface CNFUserLoginTableViewController ()
@property (nonatomic, strong) NSString * webServiceName;
@property (nonatomic, strong) NSString * auditLocation;
@end


@implementation CNFUserLoginTableViewController{
    UIAlertController * statusAlert;
}

@synthesize tableView=_tableView,userNameText=_userNameText,passwordText=_passwordText, webServiceName=_webServiceName,auditLocation=_auditLocation;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userNameText.delegate=self;
    self.passwordText.delegate=self;
    
    [self.navigationItem setTitle:@"User Login"];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    
    self.userNameText.delegate=self;
    self.passwordText.delegate=self;
    
    self.webServiceName = @"";
    
    self.userNameText.text=@"";
    self.passwordText.text=@"";

    self.auditLocation = @"Default";
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}


#pragma mark - text field delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)logIn:(id)sender {
    if([self.userNameText hasText] && [self.passwordText hasText]){
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

        //disable the back and login button
        [self.navigationItem.leftBarButtonItem setEnabled:FALSE];
        [self.navigationItem.rightBarButtonItem setEnabled:FALSE];

        if([self connectionTester]!=0){
            [self validateUser];
        }else{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
            [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
            //new function added for offline validation of user login
            [self validateAgainstKeychain:[self.userNameText.text lowercaseString] password:self.passwordText.text];
        }
    }else{
        [self showErrorAlert:@"Please enter valid Username and Password"];
    }
}

-(void)showErrorAlert:(NSString *)message{
    
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
    
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    UIAlertController *infoAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(title,@"")
                                               message:[NSString stringWithFormat:@"%@", body]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

#pragma mark - connection tester

-(int)connectionTester{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];

    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
    if(netStatus==1){
        NSLog(@"connection OK...");
    }else{
        NSLog(@"No connection for validation...");
    }
    
    return netStatus;
}

-(void)validateUser{

    self.webServiceName = @"validateUser";
    
    UserElement * userElement = [[UserElement alloc]init];
    userElement.user = [[User alloc]init];
    
    userElement.user.UserName = [self.userNameText.text lowercaseString];
    userElement.user.Password= self.passwordText.text;
    userElement.user.DeviceId = [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_ID"];
    
    NSString *post = [userElement toJSONString];
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/ValidateUser", appDelegate.serverUrl]]];

    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(error){
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//                                                  [self showErrorAlert:@"Connection error with server"];
                                                  //new function added for offline validation of user login
                                                  [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
                                                  [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                                  [self validateAgainstKeychain:[self.userNameText.text lowercaseString] password:self.passwordText.text];
                                              });
                                          }else{
                                              if([self.webServiceName isEqualToString:@"validateUser"]){
                                                  id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                  if(error){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//                                                          [self showErrorAlert:@"Error serializing data from web service.."];
                                                          //new function added for offline validation of user login
                                                          [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
                                                          [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                                          [self validateAgainstKeychain:[self.userNameText.text lowercaseString] password:self.passwordText.text];
                                                      });
                                                  }else{
                                                      if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                              [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
                                                              [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                                              [self showErrorAlert:[dictionary valueForKey:@"Message"]];
                                                          });
                                                      }else{
                                                              NSString * validationResult = [dictionary valueForKey:@"d"];
                                                              NSError * error;
                                                              ValidUserItems * validUser = [[ValidUserItems alloc] initWithString:validationResult error:&error];
                                                              
                                                              if(!error){
                                                                  [[NSUserDefaults standardUserDefaults] setObject:userElement.user.UserName forKey:@"USER_NAME"];
                                                                  [[NSUserDefaults standardUserDefaults] setObject:validUser.userGroups forKey:@"USERGROUPS"];
                                                                  [[NSUserDefaults standardUserDefaults]setObject:validUser.DeviceShortId forKey:@"DEVICE_SHORT_ID"];
                                                                  [[NSUserDefaults standardUserDefaults] setObject:validUser.Office forKey:@"OFFICE"];
                                                                  
                                                                  //add the userinfo in the memory structure for future offline use
                                                                  
                                                                  NSMutableArray * copiedArray = [[NSMutableArray alloc]init];
                                                                  NSMutableDictionary * userInfo = [[NSMutableDictionary alloc]init];
                                                                  
                                                                  [userInfo setValue:userElement.user.UserName forKey:@"userName"];
                                                                  [userInfo setValue:validUser.userGroups forKey:@"userGroups"];
                                                                  [userInfo setValue:validUser.Office forKey:@"userOffice"];
                                                                  [userInfo setValue:[[NSDate date] dateByAddingTimeInterval:60*60*24*3] forKey:@"userExpire"];

                                                                  if([[NSUserDefaults standardUserDefaults] valueForKey:@"IN_MEMORY_USERS"]){
                                                                      copiedArray = [[[NSUserDefaults standardUserDefaults] valueForKey:@"IN_MEMORY_USERS"] mutableCopy];
                                                                      NSPredicate * userPredicate = [NSPredicate predicateWithFormat:@"userName == %@", userElement.user.UserName];
                                                                      NSArray * filteredArray = [copiedArray filteredArrayUsingPredicate:userPredicate];
                                                                      
                                                                      if(filteredArray.count>0){
                                                                          //user found replace new dictionary
                                                                          NSUInteger index = [copiedArray indexOfObject:[filteredArray objectAtIndex:0]];
                                                                          [copiedArray replaceObjectAtIndex:index withObject:userInfo];

                                                                      }else{
                                                                          //user not found, create new dictionary and add
                                                                          [copiedArray addObject:userInfo];
                                                                      }
                                                                  }else{
                                                                      [copiedArray addObject:userInfo];
                                                                  }
                                                                  
                                                                  [[NSUserDefaults standardUserDefaults]setObject:copiedArray forKey:@"IN_MEMORY_USERS"];
                                                                  [[NSUserDefaults standardUserDefaults] synchronize];
    
                                                                  NSLog(@"User Validated..");
                                                                  [SSKeychain setPassword:userElement.user.Password forService:@"CNFApp" account:userElement.user.UserName];
                                                                  [SSKeychain setPassword:validUser.userToken.TokenValue forService:@"CNFApp-Token" account:userElement.user.UserName];
                                                                  NSLog(@"Account added to key chain..");

                                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                                      [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                                      [self performSegueWithIdentifier:@"pickLocationSegue" sender:self];
                                                                  });
                                                              }else{
                                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                                      [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                                      [self.navigationItem.leftBarButtonItem setEnabled:TRUE];
                                                                      [self.navigationItem.rightBarButtonItem setEnabled:TRUE];
                                                                      [self showErrorAlert:@"Error serializing data from web service.."];
                                                                  });
                                                              }
                                                      }
                                                  }
                                              }
                                          }
                                      }];
    [dataTask resume];
}

-(void)validateAgainstKeychain:(NSString*)userName password:(NSString*)password{
    if([password isEqualToString:[SSKeychain  passwordForService:@"CNFApp" account:[userName lowercaseString]]]){
        //password matched, check if they logged in in the past 3 days
        
        NSMutableArray * copiedArray = [[[NSUserDefaults standardUserDefaults] valueForKey:@"IN_MEMORY_USERS"] mutableCopy];
        NSPredicate * userPredicate = [NSPredicate predicateWithFormat:@"userName == %@", [self.userNameText.text lowercaseString]];
        NSArray * filteredArray = [copiedArray filteredArrayUsingPredicate:userPredicate];
        
        if(filteredArray.count>0){
            //user found with matching profile
            
            if ([[NSDate date] compare:[[filteredArray objectAtIndex:0] valueForKey:@"userExpire"]] == NSOrderedDescending) {
                NSLog(@"user Credential Expired...");
                [self showErrorAlert:@"User did not login in the past 3 days, Password expired"];
                
            } else if ([[NSDate date] compare:[[filteredArray objectAtIndex:0] valueForKey:@"userExpire"]] == NSOrderedAscending) {
                NSLog(@"user has up-to date credential");

                //set user defaults
                
                [[NSUserDefaults standardUserDefaults] setObject:[userName lowercaseString] forKey:@"USER_NAME"];
                [[NSUserDefaults standardUserDefaults] setObject:[[filteredArray objectAtIndex:0] valueForKey:@"userGroups"] forKey:@"USERGROUPS"];
                [[NSUserDefaults standardUserDefaults] setObject:[[filteredArray objectAtIndex:0] valueForKey:@"userOffice"] forKey:@"OFFICE"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    [self performSegueWithIdentifier:@"pickLocationSegue" sender:self];
                });
                
            } else {
                NSLog(@"dates are the same");
            }
        }else{
            [self showErrorAlert:@"Cannot find user in the local Memory"];
        }
    }else{
        [self showErrorAlert:@"Incorrect Username or Password"];
    }
    
}


@end
