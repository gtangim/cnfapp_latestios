//
//  CNFUserStatusTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-08-09.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFAppEnums.h"
#import "CNFGetDocumentId.h"
#import "CNFUserStatusTableViewController.h"
#import "vosReceiveAuditData.h"
#import "receiptReceiveAuditData.h"

@interface CNFUserStatusTableViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * tableData;
@property NSUInteger selectedIndex;
@end

@implementation CNFUserStatusTableViewController{
    NSManagedObjectContext * context;
//    UIAlertView * statusAlert;
}

@synthesize context=_context, tableData=_tableData, selectedIndex=_selectedIndex;

- (void)viewDidLoad {
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];

    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [super viewDidLoad];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    [refreshControl addTarget:self action:@selector(updateTable) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;

    @try{
        [self deleteEntriesBefor10Days];
        [self getTableDataReady];
        [self deleteListedFiles];
    }@catch(NSException * ex){
        NSLog(@"Expection Occured: %@", ex.description);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateTable{
    [self getTableDataReady];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}

-(void)deleteEntriesBefor10Days{
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSDate* startDate=[[NSDate date] dateByAddingTimeInterval:-7*24*60*60];
    NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date < %@",startDate];
    [request setPredicate:datePredicate];
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *results= [context executeFetchRequest:request error:&error];
    
    if(error)
        NSLog(@"error getting DB records");
    else{
        if(results.count==0){
            NSLog(@"no data to delete more than 10 days old");
        }else{
            for(NSManagedObject * objects in results){
                [context deleteObject:objects];
            }
            NSError * saveError;
            [context save:&saveError];
            if(!saveError)
                NSLog(@"context save successful after deleted entries before 7 days");
            else
                NSLog(@"context save unsuccessful for entry deletion..");
        }
    }
}

-(void)getTableDataReady{

    @try{

        self.tableData = [[NSMutableArray alloc]init];

        NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];

        NSPredicate * userPredicate  = [NSPredicate predicateWithFormat:@"userName==%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"]];
        NSDate* startDate=[[NSDate date] dateByAddingTimeInterval:-5*24*60*60];
        NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date >= %@",startDate];
        NSPredicate * dateNuserPredicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[userPredicate, datePredicate]];

        [request setPredicate:dateNuserPredicate];

        NSSortDescriptor *eventSorter=[[NSSortDescriptor alloc]initWithKey:@"date" ascending:NO];
        [request setSortDescriptors:[[NSArray alloc] initWithObjects:eventSorter, nil]];
        [request setEntity:entityDesc];

        NSError *error;
        NSArray *results= [context executeFetchRequest:request error:&error];

        NSMutableArray *resultArray = [NSMutableArray new];
        NSArray *groups = [results valueForKeyPath:@"@distinctUnionOfObjects.documentId"];

        NSMutableArray * unsortedArray = [[NSMutableArray alloc]init];

        for(NSString * groupId in groups){
            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"documentId == %@",groupId];
            NSArray * new = [results filteredArrayUsingPredicate:predicate];
            NSMutableDictionary * newProduct = [[NSMutableDictionary alloc]init];
            [newProduct setValue:groupId forKey:@"id"];
            [newProduct setValue:[[new objectAtIndex:0] valueForKey:@"date"]  forKey:@"time"];
            [unsortedArray addObject:newProduct];
        }

        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                            initWithKey: @"time" ascending: NO];
        NSArray *sortedArray = [unsortedArray sortedArrayUsingDescriptors: [NSArray arrayWithObject:sortDescriptor]];

        NSArray * sortedGroups = [sortedArray valueForKeyPath:@"id"];

        for (NSString *documentId in sortedGroups)
        {
            NSMutableDictionary *entry = [NSMutableDictionary new];
            [entry setObject:documentId forKey:@"documentId"];
            NSArray *groupNames = [results filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"documentId = %@", documentId]];
            [entry setObject:[[groupNames objectAtIndex:0] valueForKey:@"documentType"] forKey:@"documentType"];
            for (int i = 0; i < groupNames.count; i++)
            {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *date = [formatter stringFromDate:[[groupNames objectAtIndex:i] valueForKey:@"date"]];
                [entry setObject:date forKey:[[groupNames objectAtIndex:i] valueForKey:@"status"]];

                if([[groupNames objectAtIndex:i] valueForKey:@"attachmentStatus"]!=nil){
                    [entry setObject:[[groupNames objectAtIndex:i] valueForKey:@"attachmentStatus"] forKey:@"attachmentStatus"];
                }

                if([[groupNames objectAtIndex:i] valueForKey:@"documentNo"]!=nil){
                    [entry setObject:[[groupNames objectAtIndex:i] valueForKey:@"documentNo"] forKey:@"documentNo"];
                }

                if(([[groupNames objectAtIndex:i] valueForKey:@"des"]!=nil) && (![[[groupNames objectAtIndex:i] valueForKey:@"des"] isEqualToString:@""])){
                    [entry setObject:[[groupNames objectAtIndex:i] valueForKey:@"des"] forKey:@"des"];
                }
            }
            [resultArray addObject:entry];
        }
        self.tableData = resultArray;
    }@catch(NSException * ex){
        NSLog(@"Error occured: %@", ex.description);
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableData.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statusCells" forIndexPath:indexPath];
    
    if (self.tableData.count ==0){
        
    }else{
        
        NSManagedObject *obj=[self.tableData objectAtIndex:indexPath.row];
        
        if([obj valueForKey:@"documentNo"]){
            if([[obj valueForKey:@"documentType"]isEqualToString:@"TRANSFER_CREATION"]&&[obj valueForKey:@"Processed"]){
                cell.textLabel.text = [obj valueForKey:@"des"];
            }else{
                cell.textLabel.text = [obj valueForKey:@"documentNo"];
            }
        }else{
            cell.textLabel.text = [obj valueForKey:@"documentType"];
        }
        
        if([obj valueForKey:@"Failed"]){
            cell.detailTextLabel.text = [@" Failed at: " stringByAppendingString:[obj valueForKey:@"Failed"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"button-no.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"Processed"]){
            cell.detailTextLabel.text = [@" Processed successfully at: " stringByAppendingString:[obj valueForKey:@"Processed"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"modify.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"PreValidated"]){
            cell.detailTextLabel.text = [@" PreValidated at: " stringByAppendingString:[obj valueForKey:@"PreValidated"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"modify.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"Submitted"]){
            cell.detailTextLabel.text = [@" Submitted at: " stringByAppendingString:[obj valueForKey:@"Submitted"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"upload.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"Finished"]){
            cell.detailTextLabel.text = [@" Finished at: " stringByAppendingString:[obj valueForKey:@"Finished"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"sync.png"] scaledToSize:CGSizeMake(30,30)];
        }else if([obj valueForKey:@"Created"]){
            cell.detailTextLabel.text = [@" Created at: " stringByAppendingString:[obj valueForKey:@"Created"]];
            cell.imageView.image=[self imageWithImage:[UIImage imageNamed:@"sync.png"] scaledToSize:CGSizeMake(30,30)];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *obj =[self.tableData objectAtIndex:indexPath.row];
    
    NSString * failedText = ([obj valueForKey:@"Failed"]) ? ([@"\r Failed at: " stringByAppendingString:[obj valueForKey:@"Failed"]]) : (@"");
    NSString * processedText = ([obj valueForKey:@"Processed"]) ? ([@"\r Processed at: " stringByAppendingString:[obj valueForKey:@"Processed"]]) : (@"");
    NSString * prevalidatedText = ([obj valueForKey:@"PreValidated"]) ? ([@"\r PreValidated at: " stringByAppendingString:[obj valueForKey:@"PreValidated"]]) : (@"");
    NSString * submittedText = ([obj valueForKey:@"Submitted"]) ? ([@"\r Submitted at: " stringByAppendingString:[obj valueForKey:@"Submitted"]]) : (@"");
    
    NSString * finishedText = ([obj valueForKey:@"Finished"]) ? ([@"\r Finished at: " stringByAppendingString:[obj valueForKey:@"Finished"]]) : (@"");
    NSString * createdText = ([obj valueForKey:@"Created"]) ? ([@"\r Created at: " stringByAppendingString:[obj valueForKey:@"Created"]]) : (@"");
    NSString * attachmentText = ([obj valueForKey:@"attachmentStatus"]) ? ([@"\r Attachment: " stringByAppendingString:[obj valueForKey:@"attachmentStatus"]]) : (@"");
    NSString * docNoText = ([obj valueForKey:@"documentNo"]) ? ([@"\r Document No: " stringByAppendingString:[obj valueForKey:@"documentNo"]]) : (@"");
    NSString * desText = ([obj valueForKey:@"des"]) ? ([@"\r Details: " stringByAppendingString:[obj valueForKey:@"des"]]) : (@"");
    
    
    if([obj valueForKey:@"Failed"]&&[[obj valueForKey:@"documentType"] isEqualToString:@"VOS_RECEIVE"]&&[obj valueForKey:@"des"]&&[obj valueForKey:@"documentNo"]){
        
        if([[obj valueForKey:@"des"] rangeOfString:@"INVALID_STATUS_TRANSITION"].location == NSNotFound){
            NSString * detailedMsg = [[[[[[[[docNoText stringByAppendingString:failedText]stringByAppendingString:processedText]stringByAppendingString:prevalidatedText]stringByAppendingString:attachmentText] stringByAppendingString:submittedText]stringByAppendingString:finishedText] stringByAppendingString:createdText]stringByAppendingString:desText];
            

            UIAlertController *auditDetailsController = [UIAlertController
                                                       alertControllerWithTitle:NSLocalizedString(@"Detailed Info",@"")
                                                       message:detailedMsg
                                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okaction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }];
            
            [auditDetailsController addAction:okaction];
            [self presentViewController:auditDetailsController animated:YES completion:nil];
            
        }else{
            NSString * detailedMsg = [[[[[[[[docNoText stringByAppendingString:failedText]stringByAppendingString:processedText]stringByAppendingString:prevalidatedText]stringByAppendingString:attachmentText] stringByAppendingString:submittedText]stringByAppendingString:finishedText] stringByAppendingString:createdText]stringByAppendingString:desText];

            self.selectedIndex =indexPath.row;
            UIAlertController *auditDetailsController = [UIAlertController
                                                         alertControllerWithTitle:NSLocalizedString(@"Detailed Info",@"")
                                                         message:detailedMsg
                                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *reSubmitAction = [UIAlertAction
                                       actionWithTitle:@"Re-Submit"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           
                                           NSDate * startTime = [NSDate date];
                                           NSManagedObject *obj =[self.tableData objectAtIndex:self.selectedIndex];
                                           
                                           CNFGetDocumentId * newIDGenerator = [[CNFGetDocumentId alloc]init];
                                           NSString * newDocId = [newIDGenerator getDocumentId];
                                           
                                           NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"];
                                           NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
                                           
                                           NSArray *filteredArray = [directoryContent filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains %@", [obj valueForKey:@"Created"]]];
                                           
                                           if(filteredArray.count==0){
                                               NSLog(@"no images for the failed document..");
                                           }else{
                                               
                                               NSUInteger  imageNumber = filteredArray.count;
                                               
                                               NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                               [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                               NSString * toSaveFileName = [formatter stringFromDate:startTime];
                                               
                                               NSError * fileMoveError;
                                               for(NSString * fileName in filteredArray){
                                                   NSString * imageCounter = [fileName componentsSeparatedByString:@"."][1];
                                                   
                                                   NSString* finalPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/temp_images"] stringByAppendingPathComponent:[[toSaveFileName stringByAppendingString:@"."] stringByAppendingString:imageCounter]];
                                                   NSString* originPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"] stringByAppendingPathComponent:fileName];
                                                   
                                                   if([[NSFileManager defaultManager] copyItemAtPath:originPath toPath:finalPath error:&fileMoveError]){
                                                       NSLog(@"%@ moved from saved to temp folder..", fileName);
                                                   }else{
                                                       NSLog(@"%@ could not be moved or deleted..error occured %@", fileName, fileMoveError);
                                                   }
                                               }
                                               
                                               if(!fileMoveError){
                                                   NSLog(@"files saved to temp folder again..get doc Id and save to DB now..");
                                                   
                                                   NSError * dbSaveError;
                                                   NSManagedObject *newManagedObject = [NSEntityDescription
                                                                                        insertNewObjectForEntityForName:@"DocStatusInfo"
                                                                                        inManagedObjectContext:context];
                                                   
                                                   [newManagedObject setValue:@"RECEIPT_RECEIVE" forKey:@"documentType"];
                                                   [newManagedObject setValue:@"Created" forKey:@"status"];
                                                   [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
                                                   [newManagedObject setValue:newDocId forKey:@"documentId"];
                                                   [newManagedObject setValue:startTime forKey:@"date"];
                                                   [newManagedObject setValue:@"Saved" forKey:@"attachmentStatus"];
                                                   [newManagedObject setValue:[NSNumber numberWithInt:(int)imageNumber] forKey:@"attachmentNo"];
                                                   [context save:&dbSaveError];
                                                   
                                                   if(!dbSaveError){
                                                       NSLog(@"no error in db saving..will continue next..make document to receipt receive from vos..");
                                                       
                                                       NSError * fileReadError;
                                                       receiptReceiveAuditData * auditData = [[receiptReceiveAuditData alloc]initWithString:[self readStringFromFile:[obj valueForKey:@"Finished"]] error:&fileReadError];
                                                       
                                                       NSMutableArray * notes = [[NSMutableArray alloc] initWithArray:auditData.AuditItem.Note];
                                                       [notes addObject:[NSString stringWithFormat:@"This is the later part of vos: %@ and receipt : %@",auditData.AuditItem.VosNumber, auditData.AuditItem.ReceiptNumber]];
                                                       
                                                       auditData.ReceivingType=@"RECEIPT_RECEIVE";
                                                       auditData.AuditItem.ReceiptNumber=[auditData.AuditItem.ReceiptNumber stringByAppendingString:@"a"];
                                                       auditData.AuditItem.VosNumber=NULL;
                                                       auditData.AuditItem.VosId=NULL;
                                                       auditData.AuditItem.Note = notes;
                                                       
                                                       NSDate * endTime = [NSDate date];
                                                       
                                                       @try {
                                                           [self writeStringToFile:[auditData toJSONString] fileName:[formatter stringFromDate:endTime]];
                                                           
                                                           NSLog(@"File %@ save to File folder", [auditData.AuditItem.ReceiptNumber stringByAppendingString:@"a"]);
                                                           
                                                           //save entry to DB...
                                                           
                                                           NSError *finalDbSave;
                                                           NSManagedObject *newManagedObject = [NSEntityDescription
                                                                                                insertNewObjectForEntityForName:@"DocStatusInfo"
                                                                                                inManagedObjectContext:context];
                                                           
                                                           [newManagedObject setValue:@"RECEIPT_RECEIVE" forKey:@"documentType"];
                                                           [newManagedObject setValue:@"Finished" forKey:@"status"];
                                                           [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
                                                           [newManagedObject setValue:newDocId forKey:@"documentId"];
                                                           [newManagedObject setValue:endTime forKey:@"date"];
                                                           [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
                                                           
                                                           [newManagedObject setValue:auditData.AuditItem.ReceiptNumber forKey:@"documentNo"];
                                                           
                                                           [context save:&finalDbSave];
                                                           
                                                           if(!finalDbSave){
                                                               NSLog(@"task completed resaving the file....");

                                                               UIAlertController *completionAlertController = [UIAlertController
                                                                                                          alertControllerWithTitle:@"Re Submit Completed"
                                                                                                          message:[NSString stringWithFormat:@"The VOS %@ has been resubmitted as receipt %@",[obj valueForKey:@"documentNo"],auditData.AuditItem.ReceiptNumber]
                                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                                                               
                                                               UIAlertAction *okaction = [UIAlertAction
                                                                                          actionWithTitle:@"OK"
                                                                                          style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction *action)
                                                                                          {
                                                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                                                          }];
                                                               
                                                               [completionAlertController addAction:okaction];
                                                               [self presentViewController:completionAlertController animated:YES completion:nil];
                                                               
                                                           }else{
                                                               NSLog(@"Error happened while trying to save the file to DB..process terminated..");
//                                                               UIAlertView * failedAlert = [[UIAlertView alloc]initWithTitle:@"Re Submit Failed" message:[NSString stringWithFormat:@"The VOS %@ could not be resubmitted",[obj valueForKey:@"documentNo"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:NULL];
//                                                               [failedAlert show];
                                                               
                                                               
                                                               UIAlertController *failedAlertController = [UIAlertController
                                                                                                               alertControllerWithTitle:@"Re Submit Failed"
                                                                                                               message:[NSString stringWithFormat:@"The VOS %@ could not be resubmitted",[obj valueForKey:@"documentNo"]]
                                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                               
                                                               UIAlertAction *okaction = [UIAlertAction
                                                                                          actionWithTitle:@"OK"
                                                                                          style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction *action)
                                                                                          {
                                                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                                                          }];
                                                               
                                                               [failedAlertController addAction:okaction];
                                                               [self presentViewController:failedAlertController animated:YES completion:nil];
                                                               
                                                           }
                                                           
                                                           
                                                       } @catch (NSException *exception) {
                                                           NSLog(@"Expection happened %@, while trying to save file for RECEIPI_RECEIVE..", exception);
//                                                           UIAlertView * failedAlert = [[UIAlertView alloc]initWithTitle:@"Re Submit Failed" message:[NSString stringWithFormat:@"The VOS %@ could not be resubmitted",[obj valueForKey:@"documentNo"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:NULL];
//                                                           [failedAlert show];
                                                           
                                                           
                                                           UIAlertController *failedAlertController = [UIAlertController
                                                                                                       alertControllerWithTitle:@"Re Submit Failed"
                                                                                                       message:[NSString stringWithFormat:@"The VOS %@ could not be resubmitted",[obj valueForKey:@"documentNo"]]
                                                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                                           
                                                           UIAlertAction *okaction = [UIAlertAction
                                                                                      actionWithTitle:@"OK"
                                                                                      style:UIAlertActionStyleDefault
                                                                                      handler:^(UIAlertAction *action)
                                                                                      {
                                                                                          [self dismissViewControllerAnimated:YES completion:nil];
                                                                                      }];
                                                           
                                                           [failedAlertController addAction:okaction];
                                                           [self presentViewController:failedAlertController animated:YES completion:nil];

                                                       }

                                                   }else{
                                                       NSLog(@"error occured while saving entry to DB, %@", dbSaveError);
//                                                       UIAlertView * failedAlert = [[UIAlertView alloc]initWithTitle:@"Re Submit Failed" message:[NSString stringWithFormat:@"The VOS %@ could not be resubmitted",[obj valueForKey:@"documentNo"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:NULL];
//                                                       [failedAlert show];
                                                       
                                                       UIAlertController *failedAlertController = [UIAlertController
                                                                                                   alertControllerWithTitle:@"Re Submit Failed"
                                                                                                   message:[NSString stringWithFormat:@"The VOS %@ could not be resubmitted",[obj valueForKey:@"documentNo"]]
                                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                                       
                                                       UIAlertAction *okaction = [UIAlertAction
                                                                                  actionWithTitle:@"OK"
                                                                                  style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction *action)
                                                                                  {
                                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                                  }];
                                                       
                                                       [failedAlertController addAction:okaction];
                                                       [self presentViewController:failedAlertController animated:YES completion:nil];
                                                   }
                                               }else{
                                                   NSLog(@"error occured while moving file to temp folder..will abort operation...");
//                                                   UIAlertView * failedAlert = [[UIAlertView alloc]initWithTitle:@"Re Submit Failed" message:[NSString stringWithFormat:@"The VOS %@ could not be resubmitted",[obj valueForKey:@"documentNo"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:NULL];
//                                                   [failedAlert show];
                                                   
                                                   UIAlertController *failedAlertController = [UIAlertController
                                                                                               alertControllerWithTitle:@"Re Submit Failed"
                                                                                               message:[NSString stringWithFormat:@"The VOS %@ could not be resubmitted",[obj valueForKey:@"documentNo"]]
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                   
                                                   UIAlertAction *okaction = [UIAlertAction
                                                                              actionWithTitle:@"OK"
                                                                              style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction *action)
                                                                              {
                                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                              }];
                                                   
                                                   [failedAlertController addAction:okaction];
                                                   [self presentViewController:failedAlertController animated:YES completion:nil];

                                               }
                                           }
                                       }];
            
            
            UIAlertAction *cancelAction = [UIAlertAction
                                             actionWithTitle:@"Cancel"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction *action)
                                             {
                                                 [self dismissViewControllerAnimated:YES completion:nil];
                                             }];
            
            
            [auditDetailsController addAction:reSubmitAction];
            [auditDetailsController addAction:cancelAction];
            [self presentViewController:auditDetailsController animated:YES completion:nil];
            
        }
    }else{
        NSString * detailedMsg = [[[[[[[[docNoText stringByAppendingString:failedText]stringByAppendingString:processedText]stringByAppendingString:prevalidatedText]stringByAppendingString:attachmentText] stringByAppendingString:submittedText]stringByAppendingString:finishedText] stringByAppendingString:createdText]stringByAppendingString:desText];
        
//        UIAlertView * auditDetails = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Detailed Info",@"")
//                                                                message:detailedMsg
//                                                               delegate:self
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//        [auditDetails show];
        
        UIAlertController *auditDetailsController = [UIAlertController
                                                     alertControllerWithTitle:NSLocalizedString(@"Detailed Info",@"")
                                                     message:detailedMsg
                                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okaction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [auditDetailsController addAction:okaction];
        [self presentViewController:auditDetailsController animated:YES completion:nil];

    }
}


-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



#pragma mark - Delete Files more than 3 months old

-(NSArray *)listFileAtPath:(NSString*)filePath
{
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:filePath];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    return directoryContent;
}

-(void)deleteListedFiles{
    NSArray * fileListing = [self listFileAtPath:@"Documents/AuditFiles"];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    for(NSString * name in fileListing){
        if(!([name rangeOfString:@"pdf" options:NSCaseInsensitiveSearch].length==0)){
        }else{
            NSDate * date = [dateFormatter dateFromString:name];
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [gregorianCalendar components: (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear )
                                                                fromDate:date
                                                                  toDate:[NSDate date]
                                                                 options:0];
            if([components month]>2){
                [self deleteAuditFile:name];
            }
        }
    }
    
    //delete processed image files
    for(NSDictionary* object in self.tableData){
        if([object valueForKey:@"Processed"]){
            if([object valueForKey:@"Created"]){
                [self deleteImageFiles:[object valueForKey:@"Created"]];
            }
        }
    }
    
    //delete saved images from more than 2 week old
    
    NSArray * imagefileListing = [self listFileAtPath:@"Documents/saved_images"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    for(NSString * name in imagefileListing){
        NSString * dateString = [name componentsSeparatedByString:@"."][0];
        
        NSDate * date = [dateFormatter dateFromString:dateString];
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components: (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear )
                                                            fromDate:date
                                                              toDate:[NSDate date]
                                                             options:0];
        if([components day]>14){
            NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"];
            NSString *filePath = [dataPath stringByAppendingPathComponent:name];
            NSError *error;
            BOOL success = [fileManager removeItemAtPath:filePath error:&error];
            if (success) {
                NSLog(@"file deleted");
            }
            else
            {
                NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
            }
        }
    }
    
}

- (void)deleteAuditFile:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    
    NSString *filePath = [dataPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"file deleted");
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

-(void)deleteImageFiles:(NSString* )fileName{
    
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    NSArray *filteredArray = [directoryContent filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains %@", fileName]];
    NSError *error;
    for(NSString* name in filteredArray){
        NSString* finalPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/saved_images"] stringByAppendingPathComponent:name];
        [[NSFileManager defaultManager] removeItemAtPath:finalPath error:&error];
        NSLog(@"images removed..");
    }
}


#pragma mark - resubmit audit funcitons

- (NSString*)readStringFromFile:(NSString*)file {
    
    // Build the path...
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    NSString* fileName = file;
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    // The main act...
    return [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
}


- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

- (NSData*)readDataFromFile:(NSString*)file {
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    NSString* fileName = [file stringByAppendingString:@".pdf"];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    NSData * pdfData = [NSData dataWithContentsOfFile:fileAtPath];
    return pdfData;
}

- (IBAction)goBack:(id)sender {
    self.tableData = nil;
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
