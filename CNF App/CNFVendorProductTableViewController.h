//
//  CNFVendorProductTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-07-06.
//  Copyright © 2016 CNFIT. All rights reserved.


#import <UIKit/UIKit.h>

@interface CNFVendorProductTableViewController :  UITableViewController<UISearchDisplayDelegate, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (IBAction)back:(id)sender;

@end
