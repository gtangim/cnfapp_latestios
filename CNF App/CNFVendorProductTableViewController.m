//
//  CNFVendorProductTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-07-06.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFVendorProductTableViewController.h"

@interface CNFVendorProductTableViewController ()
@property (nonatomic, strong) NSArray * productArray;
@property (strong, nonatomic) UISearchController *searchController;

@end

@implementation CNFVendorProductTableViewController{
    NSArray *searchResults;
//    UIAlertView * statusAlert;
}

@synthesize productArray=_productArray, searchBar=_searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    [self.searchController.searchBar sizeToFit];
    
    
//    [self startSpinner:@"loading data"];
    
    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"VendorProductList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSError * error;
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    NSArray * vendorProductArray = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
    
    if(error){
        NSLog(@"Serializer error, data file not downloaded properly..");
        
    }else{
        NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"VendorId==%@", [[[NSUserDefaults standardUserDefaults]valueForKey:@"VENDOR"] valueForKey:@"VendorId"]];
        
        self.productArray = [vendorProductArray filteredArrayUsingPredicate:vendorPredicate];
        
        vendorPredicate=nil;
        vendorProductArray=nil;
        
//        [self stopSpinner];
        
        if(self.productArray.count==0){
            [self showNoProductAlert];
        }
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - spinner functions

//- (void)startSpinner:(NSString *)message {
//    if (!statusAlert){
//        statusAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(message,@"") message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
//        
//        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [indicator startAnimating];
//        
//        [statusAlert setValue:indicator forKey:@"accessoryView"];
//        [statusAlert show];
//    }
//}
//
//- (void)stopSpinner {
//    if (statusAlert) {
//        [statusAlert dismissWithClickedButtonIndex:0 animated:YES];
//        statusAlert = nil;
//    }
//}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (searchResults.count>0) {
        return [searchResults count];
        
    } else {
        return self.productArray.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vendorProductCell"];

    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"vendorProductCell"];
    }
    
    if (searchResults.count>0) {
        cell.textLabel.text = [[searchResults objectAtIndex:indexPath.row]valueForKey:@"ProductName"];
    } else {
        cell.textLabel.text = [[self.productArray objectAtIndex:indexPath.row] valueForKey:@"ProductName"];
    }
    cell.textLabel.numberOfLines=0;
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (searchResults.count>0) {
//        indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
        [[NSUserDefaults standardUserDefaults] setObject:[[searchResults objectAtIndex:indexPath.row] valueForKey:@"ProductId"] forKey:@"VENDOR_PRODUCT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:[[self.productArray objectAtIndex:indexPath.row] valueForKey:@"ProductId"] forKey:@"VENDOR_PRODUCT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - search filter functions

//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
//{
//    [self filterContentForSearchText:searchString
//                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                      objectAtIndex:[self.searchDisplayController.searchBar
//                                                     selectedScopeButtonIndex]]];
//    return YES;
//}
//
//
//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ProductName CONTAINS[cd] %@", searchText];
//    searchResults = [self.productArray filteredArrayUsingPredicate:resultPredicate];
//}


- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

-(void)searchForText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ProductName CONTAINS[cd] %@", searchText];
    searchResults = [self.productArray filteredArrayUsingPredicate:resultPredicate];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchController.searchBar.text=@"";
    [self.searchController.searchBar resignFirstResponder];
    
    searchResults=NULL;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView reloadData];
}



-(void)showNoProductAlert{
//    UIAlertView * noProductAlert = [[UIAlertView alloc]initWithTitle:@"No Product" message:@"This Particular Vendor does not have product that you can add without barcode scanning" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [noProductAlert show];
//    
    
    UIAlertController *noProductAlertController = [UIAlertController
                                               alertControllerWithTitle:@"No Product"
                                               message:@"This Particular Vendor does not have product that you can add without barcode scanning"
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
    
    [noProductAlertController addAction:okaction];
    [self presentViewController:noProductAlertController animated:YES completion:nil];
    
    
    
}


- (IBAction)back:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"VENDOR_PRODUCT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
