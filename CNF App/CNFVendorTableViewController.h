//
//  CNFVendorTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-01-14.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFVendorTableViewController : UITableViewController<UISearchDisplayDelegate, UISearchBarDelegate, UISearchControllerDelegate, ZBarReaderDelegate, UISearchResultsUpdating, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *scanButton;

- (IBAction)back:(id)sender;
- (IBAction)scanProduct:(id)sender;

@end
