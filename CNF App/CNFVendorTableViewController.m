//
//  CNFVendorTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-01-14.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFVendorTableViewController.h"
#import "CNFAppEnums.h"
#import "Products.h"
#import "BarcodeProcessor.h"

@interface CNFVendorTableViewController ()
@property (nonatomic, strong) NSArray * vendorArray;
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic, strong) NSMutableArray * filteredVendorArray;
@property (nonatomic) BOOL scanProduct;
@property (strong, nonatomic) UISearchController *searchController;
@end

@implementation CNFVendorTableViewController{
    NSArray *searchResults;
    UIAlertController * statusAlert;
}

@synthesize vendorArray=_vendorArray, searchBar=_searchBar, scanner=_scanner,scannerAvailable=_scannerAvailable,  foundBarcode=_foundBarcode, filteredVendorArray=_filteredVendorArray, scanButton=_scanButton, scanProduct=_scanProduct;

-(NSMutableArray *)filteredVendorArray{
    if(!_filteredVendorArray)_filteredVendorArray=[[NSMutableArray alloc]init];
    return _filteredVendorArray;
}

- (void)viewDidLoad {
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [super viewDidLoad];
    
//    [self startSpinner:@"loading data"];
    
    self.scannerAvailable = TRUE;

    self.scanProduct=TRUE;
    self.scanner=[DTDevices sharedDevice];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    [self.searchController.searchBar sizeToFit];
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailability) userInfo:nil repeats:NO];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationItem.leftBarButtonItem.enabled=NO;
    
    NSPredicate *vendorPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"VendorList"];
    NSObject *vendorOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:vendorPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[vendorOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[vendorOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSError * error;
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    self.vendorArray = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]
                                                       options:0 error:&error];
    
    if(error){
        NSLog(@"Serializer error, data file not downloaded properly..");
//        [self stopSpinner];
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }else{
//        [self stopSpinner];
        [self showReceiptNoAlert];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [self.scanner addDelegate:self];
    [self.scanner connect];
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
    [super viewWillDisappear:animated];
}


#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailability{
    if([self.scanner connstate]==1){
        self.scannerAvailable=FALSE;
//        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
    
    self.navigationItem.leftBarButtonItem.enabled=YES;
}

-(void)stopScanner{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.filteredVendorArray.count>0){
        return self.filteredVendorArray.count;
    }else{
        if (searchResults.count>0) {
            return [searchResults count];
            
        } else {
            return self.vendorArray.count;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vendorCell"];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"vendorCell"];
    }
    
    if(self.filteredVendorArray.count>0){
        cell.textLabel.text = [[self.filteredVendorArray objectAtIndex:indexPath.row]valueForKey:@"VendorName"];
    }else{
        if (searchResults.count>0) {
            cell.textLabel.text = [[searchResults objectAtIndex:indexPath.row]valueForKey:@"VendorName"];
        } else {
            cell.textLabel.text = [[self.vendorArray objectAtIndex:indexPath.row] valueForKey:@"VendorName"];
        }
    }
    cell.textLabel.numberOfLines=0;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    [self startSpinner:@"Please Wait.."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if(self.filteredVendorArray.count>0){
            [[NSUserDefaults standardUserDefaults] setObject:[self.filteredVendorArray objectAtIndex:indexPath.row] forKey:@"VENDOR"];
        }else{
            if (searchResults.count>0) {
                [[NSUserDefaults standardUserDefaults] setObject:[searchResults objectAtIndex:indexPath.row] forKey:@"VENDOR"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:[self.vendorArray objectAtIndex:indexPath.row] forKey:@"VENDOR"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        dispatch_async (dispatch_get_main_queue(), ^{
            NSLog(@"user %@ started receiving %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"],[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]);
//            [self stopSpinner];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self performSegueWithIdentifier:@"createReceiptToScannerSegue" sender:self];
            });
            
        });
    });
}

#pragma mark - search filter functions

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

-(void)searchForText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"VendorName CONTAINS[cd] %@", searchText];
    searchResults = [self.vendorArray filteredArrayUsingPredicate:resultPredicate];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchController.searchBar.text=@"";
    [self.searchController.searchBar resignFirstResponder];
    
    searchResults=NULL;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView reloadData];
}


#pragma mark - button functions

-(void)showReceiptNoAlert{
    
    self.scanProduct=FALSE;
    UIAlertController *getReceiptNoAlertController = [UIAlertController alertControllerWithTitle:@"Receipt Receive" message:@"Please Enter Receipt Number" preferredStyle:UIAlertControllerStyleAlert];
    
    [getReceiptNoAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField1) {
        textField1.placeholder = @"Receipt #";
        textField1.keyboardType = UIKeyboardTypeDefault;
        [textField1 becomeFirstResponder];
    }];
    
    [getReceiptNoAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField2) {
        textField2.placeholder = @"Confirm Receipt #";
        textField2.keyboardType = UIKeyboardTypeDefault;
    }];
    
    
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        self.scanProduct=TRUE;
        UITextField * textField1 = getReceiptNoAlertController.textFields.firstObject;
        UITextField * textField2 = [getReceiptNoAlertController.textFields objectAtIndex:1];
        
        if([textField1 hasText] && [textField2 hasText]){
            if([textField1.text isEqualToString:textField2.text]){
                [[NSUserDefaults standardUserDefaults] setObject:textField1.text forKey:@"DOCUMENT_NO"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                        [self showReceiptNoAlert];
                });
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                    [self showReceiptNoAlert];
            });
        }
    }];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
        });
        
        
    }];
    
    [getReceiptNoAlertController addAction:okAction];
    [getReceiptNoAlertController addAction:cancelAction];
    
    [self presentViewController:getReceiptNoAlertController animated:YES completion:nil];
    
}

//-(void)selectVendorAlert{
//    UIAlertView * vendorAlert = [[UIAlertView alloc] initWithTitle:@"Select Vendor"
//                                                           message:@"Please select the vendor this receipt is coming from"
//                                                          delegate:self
//                                                 cancelButtonTitle:@"OK"
//                                                 otherButtonTitles:nil];
//    vendorAlert.tag = receiptWithOrderSheet;
//    [vendorAlert show];
//}

-(void)showErrorAlert:(NSString *)message{
    self.scanProduct=FALSE;
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    self.scanProduct=FALSE;
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    
//    self.scanProduct=TRUE;
//    
//    if(alertView.tag == receiptWithoutOrderSheet){
//        if(buttonIndex ==0){
//            [self.navigationController popViewControllerAnimated:YES];
//        }else{
//            if([[alertView textFieldAtIndex:0]hasText] && [[alertView textFieldAtIndex:1]hasText]){
//                if([[alertView textFieldAtIndex:0].text isEqualToString:[alertView textFieldAtIndex:1].text]){
//                    [[NSUserDefaults standardUserDefaults] setObject:[alertView textFieldAtIndex:1].text forKey:@"DOCUMENT_NO"];
//                    [[NSUserDefaults standardUserDefaults] synchronize];
////                    [self selectVendorAlert];
//                    
//                }else{
//                    [self showReceiptNoAlert];
//                }
//            }else{
//                [self showReceiptNoAlert];
//            }
//        }
//    }else{
//        [alertView dismissWithClickedButtonIndex:0 animated:YES];
//    }
//}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)scanProduct:(id)sender {
    
    if([self.scanButton.title isEqualToString:@"Cancel"]){
        self.filteredVendorArray=nil;
        [self.scanButton setTitle:@"Scan"];
        [self.tableView reloadData];
    }else{
        if(self.scannerAvailable){
            [self.scanner barcodeStartScan:nil];
            self.foundBarcode = NO;
            [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
        }else{
            ZBarReaderViewController *reader = [ZBarReaderViewController new];
            reader.readerDelegate = self;
            [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
            reader.readerView.zoom = 1.0;
            [self presentViewController:reader animated:YES completion:nil];
        }
    }
}


#pragma  mark - barcode scanner function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    if(self.scanProduct){
        self.foundBarcode =YES;
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
//            [self startSpinner:@"Please wait.."];
            [self processBarcode:barcode];
        });
    }else{
        int sound[]={3500,200,0,200,3500,200};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
    
    
}

#pragma mark - camera function

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    
    NSString * barcode=symbol.data;
    
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        [self startSpinner:@"Please wait.."];
        [self processBarcode:barcode];
    });
}

#pragma mark - barcode processing function

-(void)processBarcode:(NSString *)barcode{
    
    self.filteredVendorArray=nil;
    //    [self.tableView reloadData];
    
    NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcode:barcode];
    
    if([resultDic valueForKey:@"errorMsg"]!=NULL){
        NSLog(@"Result returned error..");
//        [self stopSpinner];
        [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
    }else{
        NSArray * products = [resultDic valueForKey:@"productArray"];
        
        if(products.count==0){
            //no product found
            NSLog(@"product not found..");
            
            dispatch_async(dispatch_get_main_queue(), ^{
//                [self stopSpinner];
                [self showErrorAlert:@"Product Not Found..."];
            });
            
        }else if(products.count>0){
            Products * product = [products objectAtIndex:0];
            for(Vendors * vendor in product.Vendors){
                NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"SELF.VendorId = %@", vendor.VendorId];
                NSArray * filteredArray = [self.vendorArray filteredArrayUsingPredicate:vendorPredicate];
                [self.filteredVendorArray addObjectsFromArray:filteredArray];
            }
            
            if(self.filteredVendorArray.count==0){
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [self stopSpinner];
                    [self showErrorAlert:@"No Associated Vendor Found For This Product"];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [self stopSpinner];
                    [self.tableView reloadData];
                    [self.scanButton setTitle:@"Cancel"];
                });
            }
        }
    }
}


@end
