//
//  CNFVosDiscrepencyTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-12-12.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFVosDiscrepencyTableViewController : UITableViewController<  UIAlertViewDelegate, NSFetchedResultsControllerDelegate,  UINavigationControllerDelegate,  UIActionSheetDelegate, DTDeviceDelegate, ZBarReaderDelegate, UITextFieldDelegate>

@end
