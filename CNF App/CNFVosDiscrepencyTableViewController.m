//
//  CNFVosDiscrepencyTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-12-12.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFVosDiscrepencyTableViewController.h"
#import "CNFAppEnums.h"
#import "Products.h"
#import "BarcodeProcessor.h"

#import "vosReceiveAssociatedProducts.h"
#import "vosReceiveAuditElement.h"
#import "vosReceiveAuditData.h"

@interface CNFVosDiscrepencyTableViewController ()

@property (nonatomic, strong) NSMutableArray * productList;
@property (nonatomic) int imagecounter;
@property (nonatomic) BOOL pushToCM;
@property (nonatomic, strong) NSMutableArray * notes;
@property (nonatomic, strong) NSString * documentId;


@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic, strong) Products * scannedProduct;
@property (nonatomic) BOOL shouldScanProduct;


@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * vosProductList;
@property (nonatomic, strong) NSMutableArray * discrepencyArray;
@property (nonatomic) NSInteger row;
@property (nonatomic) NSInteger section;
@property (nonatomic, strong) NSMutableArray * notOrdered;
@property (nonatomic, strong) NSMutableArray * notReceived;
@property (nonatomic, strong) NSMutableArray * misMatched;

@end

@implementation CNFVosDiscrepencyTableViewController{
    UIAlertController * statusAlert;
    NSManagedObjectContext * context;
    UIAlertController *addProductAlert;
}


@synthesize productList=_productList, imagecounter=_imagecounter, pushToCM=_pushToCM, notes=_notes, documentId=_documentId, context=_context, vosProductList=_vosProductList, discrepencyArray=_discrepencyArray, row=_row, notOrdered=_notOrdered, notReceived=_notReceived, misMatched=_misMatched, section=_section, scanner=_scanner,scannerAvailable=_scannerAvailable,foundBarcode=_foundBarcode, scannedProduct=_scannedProduct, shouldScanProduct=_shouldScanProduct;

-(NSMutableArray *)vosProductList{
    if(!_vosProductList)_vosProductList=[[NSMutableArray alloc]init];
    return _vosProductList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(submitOrder)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editOrder)];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationItem.hidesBackButton=YES;
    [self.navigationItem setTitle:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]];
    
    //document no/VOS for the order might change, check and save to DB
    
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId==%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"RECEIVING_OBJECT"] valueForKey:@"documentId"]];
    
    [request setPredicate:predicate];
    [request setEntity:entityDesc];
    
    NSError * error;
    NSArray *docResults= [context executeFetchRequest:request error:&error];
    
    [[docResults objectAtIndex:0]setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
    [context save:&error];

    
    self.shouldScanProduct=YES;
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            });
            [self processBarcode:[[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]];
        });
    }
    [self loadObjectsForView];

}

-(void)viewWillDisappear:(BOOL)animated{
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
    [super viewWillDisappear:animated];
}


-(void)loadObjectsForView{
    self.productList = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"RECEIVING_OBJECT"] valueForKey:@"productList"] mutableCopy];
    self.imagecounter = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"RECEIVING_OBJECT"] valueForKey:@"imagecounter"]intValue];
    self.pushToCM = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"RECEIVING_OBJECT"] valueForKey:@"pushToCM"]boolValue];
    self.notes = [[[NSUserDefaults standardUserDefaults] valueForKey:@"RECEIVING_OBJECT"] valueForKey:@"notes"];;
    self.documentId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"RECEIVING_OBJECT"] valueForKey:@"documentId"];
    
    [self populateVosProductList];
}


-(void)viewDidAppear:(BOOL)animated{
    self.scanner=[DTDevices sharedDevice];
    [self.scanner addDelegate:self];
    [self.scanner connect];
    
    if([self.scanner connstate]!=2)
        [self.scanner connect];
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)populateVosProductList{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSPredicate *receiptPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"VosList"];
    NSObject *receiptOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:receiptPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[receiptOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[receiptOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    NSError * error;
    NSArray * receiptList = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
    
    if(error){
        NSLog(@"Serializer error, data file not downloaded properly..");
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }else{
        NSPredicate * vosPredicate = [NSPredicate predicateWithFormat:@"VosNumber == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]];
        
        NSArray * vosArray = [[receiptList filteredArrayUsingPredicate:vosPredicate] mutableCopy];
        
        if(vosArray.count==0){
            [self showErrorAlert:@"Can not find Order Sheet, Please check if the order is already received."];
        }else{
            self.vosProductList=[[[vosArray objectAtIndex:0]valueForKey:@"VosItems"] mutableCopy];
            if(![[NSUserDefaults standardUserDefaults] valueForKey:@"VOS_ID"]){
                [[NSUserDefaults standardUserDefaults] setValue:[[vosArray objectAtIndex:0] valueForKey:@"Id"] forKey:@"VOS_ID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]){
                NSMutableDictionary * vendor = [[NSMutableDictionary alloc]init];
                [vendor setValue:[[vosArray objectAtIndex:0] valueForKey:@"VendorId"] forKey:@"VendorId"];
                [vendor setValue:[[vosArray objectAtIndex:0] valueForKey:@"VendorName"] forKey:@"VendorName"];
                [[NSUserDefaults standardUserDefaults] setObject:vendor forKey:@"VENDOR"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        
        vosArray=nil;
        receiptList=nil;
        
        [self loadDiscrepencyArray];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

-(void)loadDiscrepencyArray{
    
    self.discrepencyArray= [[NSMutableArray alloc]init];
    
    for(NSObject* product in self.productList){
        NSPredicate * findProductPredicate = [NSPredicate predicateWithFormat:@"ProductId == %@", [product valueForKey:@"ProductId"]];
        
        NSArray * foundProductArray = [self.vosProductList filteredArrayUsingPredicate:findProductPredicate];
        
        if(foundProductArray.count==0){
            NSMutableDictionary * discrepentProduct = [[NSMutableDictionary alloc]init];
            [discrepentProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
            [discrepentProduct setValue:[product valueForKey:@"Name"] forKey:@"Name"];
            [discrepentProduct setValue:[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][0] forKey:@"ReceivedQuantity"];
            [discrepentProduct setValue:@"0" forKey:@"OrderedQuantity"];
            [discrepentProduct setValue:[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][1] forKey:@"Uom"];
            [self.discrepencyArray addObject:discrepentProduct];
            
        }else{
            
            NSString * uom = [[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][1];
            
            if([uom isEqualToString:@"ea"]){
                
                //each product UOM, no conversion needed
                
                if([[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][0]doubleValue]==[[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]){
                    
                }else{
                    NSMutableDictionary * discrepentProduct = [[NSMutableDictionary alloc]init];
                    [discrepentProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                    [discrepentProduct setValue:[product valueForKey:@"Name"] forKey:@"Name"];
                    [discrepentProduct setValue:[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][0] forKey:@"ReceivedQuantity"];
                    [discrepentProduct setValue:[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][1] forKey:@"Uom"];
                    [self.discrepencyArray addObject:discrepentProduct];
                }
            }else{
                
                //weighable product, received in KG, conversion needed..
                
                NSString * vosProductUom = [[[foundProductArray objectAtIndex:0] valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1];
                
                if([uom isEqualToString:vosProductUom]){
                    
                    //uom same, just compare the quantities
                    if([[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][0]doubleValue]==[[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"] doubleValue]){
                        
                    }else{
                        NSMutableDictionary * discrepentProduct = [[NSMutableDictionary alloc]init];
                        [discrepentProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                        [discrepentProduct setValue:[product valueForKey:@"Name"] forKey:@"Name"];
                        [discrepentProduct setValue:[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][0] forKey:@"ReceivedQuantity"];
                        [discrepentProduct setValue:[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"] forKey:@"OrderedQuantity"];
                        [discrepentProduct setValue:[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][1] forKey:@"Uom"];
                        [self.discrepencyArray addObject:discrepentProduct];
                    }
                    
                }else{
                    //uom not same, do conversion
                    
                    double quantity;
                    
                    if([vosProductUom isEqualToString:@"100g"]){
                         quantity = [[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"]doubleValue] /10;
                    }else if([vosProductUom isEqualToString:@"250g"]){
                         quantity = [[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"]doubleValue] /4;
                    }else if([vosProductUom isEqualToString:@"500g"]){
                         quantity = [[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"]doubleValue] /2;
                    }else if([vosProductUom isEqualToString:@"g"]){
                         quantity = [[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"]doubleValue] /1000;
                    }else if([vosProductUom isEqualToString:@"kg"]){
                         quantity = [[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"]doubleValue] /1;
                    }else if([vosProductUom isEqualToString:@"lb"]){
                         quantity = [[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"]doubleValue] /2.20462;
                    }else if([vosProductUom isEqualToString:@"mg"]){
                         quantity = [[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"]doubleValue] /1000000;
                    }else{
                       quantity = [[[foundProductArray objectAtIndex:0] valueForKey:@"Quantity"]doubleValue];
                    }
                    
                    if([[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][0] doubleValue]==[[NSString stringWithFormat:@"%0.2f",quantity]doubleValue]){
                        
                    }else{
                        NSMutableDictionary * discrepentProduct = [[NSMutableDictionary alloc]init];
                        [discrepentProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                        [discrepentProduct setValue:[product valueForKey:@"Name"] forKey:@"Name"];
                        [discrepentProduct setValue:[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][0] forKey:@"ReceivedQuantity"];
                        [discrepentProduct setValue:[NSString stringWithFormat:@"%.02f", quantity] forKey:@"OrderedQuantity"];
                        [discrepentProduct setValue:[[product valueForKey:@"QuantityText"] componentsSeparatedByString:@" "][1] forKey:@"Uom"];
                        [self.discrepencyArray addObject:discrepentProduct];
                    }
                }
            }
        }
    }
    
    for(NSObject* product in self.vosProductList){
        NSPredicate * findProductPredicate = [NSPredicate predicateWithFormat:@"ProductId == %@", [product valueForKey:@"ProductId"]];
        NSArray * foundProductArray = [self.productList filteredArrayUsingPredicate:findProductPredicate];
        
        if(foundProductArray.count==0){
            NSMutableDictionary * discrepentProduct = [[NSMutableDictionary alloc]init];
            [discrepentProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
            [discrepentProduct setValue:[product valueForKey:@"Name"] forKey:@"Name"];
            [discrepentProduct setValue:@"0" forKey:@"ReceivedQuantity"];
            
            if( [[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] isEqualToString:@"ea"]){
                [discrepentProduct setValue:[[product valueForKey:@"Quantity"] stringValue] forKey:@"OrderedQuantity"];
                [discrepentProduct setValue:[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] forKey:@"Uom"];
            }else{
                double quantity;
                if([[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] isEqualToString:@"100g"]){
                    quantity = [[product valueForKey:@"Quantity"]doubleValue] /10;
                    [discrepentProduct setValue:[NSString stringWithFormat:@"%.02f",quantity] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:@"kg" forKey:@"Uom"];
                }else if([[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] isEqualToString:@"250g"]){
                    quantity = [[product valueForKey:@"Quantity"]doubleValue] /4;
                    [discrepentProduct setValue:[NSString stringWithFormat:@"%.02f",quantity] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:@"kg" forKey:@"Uom"];
                }else if([[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] isEqualToString:@"500g"]){
                    quantity = [[product valueForKey:@"Quantity"]doubleValue] /2;
                    [discrepentProduct setValue:[NSString stringWithFormat:@"%.02f",quantity] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:@"kg" forKey:@"Uom"];
                }else if([[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] isEqualToString:@"g"]){
                    quantity = [[product valueForKey:@"Quantity"]doubleValue] /1000;
                    [discrepentProduct setValue:[NSString stringWithFormat:@"%.02f",quantity] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:@"kg" forKey:@"Uom"];
                }else if([[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] isEqualToString:@"kg"]){
                    quantity = [[product valueForKey:@"Quantity"]doubleValue] /1;
                    [discrepentProduct setValue:[NSString stringWithFormat:@"%.02f",quantity] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:@"kg" forKey:@"Uom"];
                }else if([[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] isEqualToString:@"lb"]){
                    quantity = [[product valueForKey:@"Quantity"]doubleValue] /2.20462;
                    [discrepentProduct setValue:[NSString stringWithFormat:@"%.02f",quantity] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:@"kg" forKey:@"Uom"];
                }else if([[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] isEqualToString:@"mg"]){
                    quantity = [[product valueForKey:@"Quantity"]doubleValue] /1000000;
                    [discrepentProduct setValue:[NSString stringWithFormat:@"%.02f",quantity] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:@"kg" forKey:@"Uom"];
                }else{
                    [discrepentProduct setValue:[product valueForKey:@"Quantity"] forKey:@"OrderedQuantity"];
                    [discrepentProduct setValue:[[product valueForKey:@"Uom"] componentsSeparatedByString:@"_"][1] forKey:@"Uom"];
                }
            }
            [self.discrepencyArray addObject:discrepentProduct];
        }else{
            
        }
    }
    
    if(self.discrepencyArray.count==0){
        [self noDiscrepencyFinish];
    }else{
        self.notOrdered= [[NSMutableArray alloc]init];
        self.notReceived = [[NSMutableArray alloc]init];
        self.misMatched = [[NSMutableArray alloc]init];
        
        for(NSObject * product in self.discrepencyArray){
            if([[product valueForKey:@"ReceivedQuantity"] doubleValue]==0){
                if([[product valueForKey:@"OrderedQuantity"] doubleValue]==0){}
                else [self.notReceived addObject:product];
            }else if([[product valueForKey:@"OrderedQuantity"] doubleValue]==0){
                if([[product valueForKey:@"ReceivedQuantity"] doubleValue]==0){}
                else [self.notOrdered addObject:product];
            }else{
                [self.misMatched addObject:product];
            }
        }
        [self.tableView reloadData];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)stopScanner
{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0){
        return @"Not Ordered Items";
    }else if(section==1){
        return @"Not Received Items";
    }else{
        return @"Quantity Mismatched Items";
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(section==0){
        if(self.notOrdered.count==0){
            return 1;
        }else return self.notOrdered.count;
    }else if(section ==1){
        if(self.notReceived.count==0){
            return 1;
        }else return self.notReceived.count;
    }else{
        if(self.misMatched.count==0){
            return 1;
        }else return self.misMatched.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"discrepencyCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"discrepencyCell"];
    }
    
    cell.textLabel.numberOfLines=0;
    cell.detailTextLabel.numberOfLines=0;
    
    if(indexPath.section==0){
        if(self.notOrdered.count==0){
            cell.textLabel.text = @"No Item";
            cell.detailTextLabel.text = @"No currently received but not ordered item";
        }else{
            cell.textLabel.text = [[self.notOrdered objectAtIndex:indexPath.row] valueForKey:@"Name"];
            cell.detailTextLabel.text = [self getUomTextForCell:[[self.notOrdered objectAtIndex:indexPath.row] valueForKey:@"OrderedQuantity"] receivedQuantity:[[self.notOrdered objectAtIndex:indexPath.row] valueForKey:@"ReceivedQuantity"] uom:[[self.notOrdered objectAtIndex:indexPath.row] valueForKey:@"Uom"]];
        }
    }else if(indexPath.section==1){
        if(self.notReceived.count==0){
            cell.textLabel.text = @"No Item";
            cell.detailTextLabel.text = @"No currently ordered but not received item";
        }else{
            cell.textLabel.text = [[self.notReceived objectAtIndex:indexPath.row] valueForKey:@"Name"];
            cell.detailTextLabel.text = [self getUomTextForCell:[[self.notReceived objectAtIndex:indexPath.row] valueForKey:@"OrderedQuantity"] receivedQuantity:[[self.notReceived objectAtIndex:indexPath.row] valueForKey:@"ReceivedQuantity"] uom:[[self.notReceived objectAtIndex:indexPath.row] valueForKey:@"Uom"]];
        }
    }else{
        if(self.misMatched.count==0){
            cell.textLabel.text = @"No Item";
            cell.detailTextLabel.text = @"No currently quantity mismatched item";
        }else{
            cell.textLabel.text = [[self.misMatched objectAtIndex:indexPath.row] valueForKey:@"Name"];
            cell.detailTextLabel.text = [self getUomTextForCell:[[self.misMatched objectAtIndex:indexPath.row] valueForKey:@"OrderedQuantity"] receivedQuantity:[[self.misMatched objectAtIndex:indexPath.row] valueForKey:@"ReceivedQuantity"] uom:[[self.misMatched objectAtIndex:indexPath.row] valueForKey:@"Uom"]];
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==0 && indexPath.row==0 && self.notOrdered.count==0){
        
    }else if(indexPath.section==1 && indexPath.row==0 && self.notReceived.count==0){
        
    }else if(indexPath.section==2 && indexPath.row==0 && self.misMatched.count==0){
        
    }else{
        self.section = indexPath.section;
        self.row=indexPath.row;
        [self discrepencyQuantityAlert];
    }
}


#pragma mark - alertview and indicator spin handler

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                               message: message
                                                        preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *customVC     = [[UIViewController alloc] init];
        
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [statusAlert setValue:customVC forKey:@"contentViewController"];
    }
    [self presentViewController: statusAlert
                       animated: true
                     completion: nil];
    
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:nil];
        statusAlert = nil;
    }
}



#pragma mark - picker view delegates

-(void)showActionList{
    
    UIAlertController * popUpAlertController = [UIAlertController alertControllerWithTitle:nil message:@"Select an action" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * changeVos = [UIAlertAction actionWithTitle:@"Change Order Sheet" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self changeVos];
    }];
    
    UIAlertAction * addProduct = [UIAlertAction actionWithTitle:@"Add Product" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self addNewProduct];
    }];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
    }];
    
    [popUpAlertController addAction:changeVos];
    [popUpAlertController addAction:addProduct];
    [popUpAlertController addAction:cancel];
    
    [self presentViewController:popUpAlertController animated:YES completion:nil];
}


-(void)changeVos{
    NSLog(@"user will choose different vendor Order Sheet");
    [self performSegueWithIdentifier:@"vosSelectDiscrepencySegue" sender:self];
}

-(void)addNewProduct{
    NSLog(@"Add new product");


    //changed this portion for the scanner in adding product..
//    self.shouldScanProduct=NO;
    self.shouldScanProduct = YES;

    addProductAlert = [UIAlertController alertControllerWithTitle:@"Add Product" message:[NSString stringWithFormat:@"Please put product code in the text box to add product"] preferredStyle:UIAlertControllerStyleAlert];
    
    [addProductAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter UPC/Product Code";
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [textField setReturnKeyType:UIReturnKeyGo];
        [textField resignFirstResponder];
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        self.shouldScanProduct=TRUE;

        UITextField * alertTextField = [addProductAlert.textFields firstObject];
        
        if([alertTextField hasText]){
            if([[alertTextField text]length]<5)
                [self showErrorAlert:@"Please Enter Valid product ID or UPC"];
            else{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                [self processBarcode:alertTextField.text];
            }
        }else{
            [self showErrorAlert:@"Please Enter Valid product ID or UPC"];
        }
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        self.shouldScanProduct=TRUE;
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [addProductAlert addAction:okAction];
    [addProductAlert addAction:cancelAction];
    [self presentViewController:addProductAlert animated:YES completion:nil];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}


#pragma mark - barcode read function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    
    if(self.shouldScanProduct){
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
//        if([self doesAlertViewExist])
            [self dismissViewControllerAnimated:YES completion:nil];
        [self processBarcode:barcode];
    }else{
        int sound[]={3500,200,0,200,3500,200};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    [reader dismissViewControllerAnimated:YES completion:nil];
    NSString * barcode=symbol.data;
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        });
        [self processBarcode:barcode];
    });
}

-(void)processBarcode:(NSString *)barcode{

    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcode:barcode];
    
    if([resultDic valueForKey:@"errorMsg"]!=NULL){
        NSLog(@"Result returned error..");
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            self.shouldScanProduct=YES;
            [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
        });
    }else{
        NSArray * products = [resultDic valueForKey:@"productArray"];
        
        if(products.count==0){
            
            //no product found
            
            NSLog(@"product not found..");
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                self.shouldScanProduct=YES;
                [self showErrorAlert:@"Product Not Found..."];
            });
            
        }else if(products.count>1){
            
            //multiple products found
            
            NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];
            
            for(NSObject * product in products){
                NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];
                
                [listedProductArray addObject:listedProduct];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.scanner disconnect];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                [self performSegueWithIdentifier:@"multipleUpcFromDiscrepencySegue" sender:self];
            });
            
        }else{
            
            //returned 1 product
            
            Products * product = [[Products alloc]init];
            product = [products objectAtIndex:0];
            
            self.scannedProduct=product;
            
            NSPredicate * productPredicate = [NSPredicate predicateWithFormat:@"ProductId == %@", self.scannedProduct.ProductId];
            NSArray * productArray = [self.productList filteredArrayUsingPredicate:productPredicate];
            
            if(productArray.count>0){
                //already exists
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    self.shouldScanProduct=YES;
                    [self showErrorAlert:@"Product already in the list"];
                });
            }else{
                //add product
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    NSLog(@"adding product..");
                    [self newProductQuantityAlert];
                });
            }
        }
    }
}


#pragma mark - alertview delegate

-(BOOL) doesAlertViewExist {
    if ([[UIApplication sharedApplication].keyWindow isMemberOfClass:[UIWindow class]])
    {
        return NO;//AlertView does not exist on current window
    }
    return YES;//AlertView exist on current window
}

-(void)showErrorAlert:(NSString *)message{
    
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)newProductQuantityAlert{
    self.shouldScanProduct=NO;
  
    UIAlertController * quantityAlertController = [UIAlertController
                                                   alertControllerWithTitle:self.scannedProduct.ProductName
                                                   message:[self.scannedProduct.Uom isEqualToString:@"ea"]?@"Please enter received quantity in EA":@"Please enter received quantity in KG"
                                                   preferredStyle:UIAlertControllerStyleAlert];

    [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField* quantityTextField){
        [quantityTextField becomeFirstResponder];
        quantityTextField.placeholder = @"Product Quantity";
        if([self.scannedProduct.Uom isEqualToString:@"ea"]){
            [quantityTextField setKeyboardType:UIKeyboardTypeNumberPad];
        }else{
            [quantityTextField setKeyboardType:UIKeyboardTypeDecimalPad];
        }
    }];
    
    UIAlertAction *okaction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   self.shouldScanProduct=TRUE;
                                   
                                   UITextField * alertText = [quantityAlertController.textFields firstObject];
                               
                                   if([alertText hasText]){
                                       NSMutableDictionary * newProd = [[NSMutableDictionary alloc] init];
                                       [newProd setValue:self.scannedProduct.ProductName forKey:@"Name"];
                                       [newProd setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
                                       
                                       if([self.scannedProduct.Uom isEqualToString:@"ea"]){
                                           [newProd setValue:[[[alertText text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                                       }else if([self.scannedProduct.Uom isEqualToString:@"100g"] || [self.scannedProduct.Uom isEqualToString:@"250g"]||[self.scannedProduct.Uom isEqualToString:@"500g"]||[self.scannedProduct.Uom isEqualToString:@"g"]||[self.scannedProduct.Uom isEqualToString:@"kg"]||[self.scannedProduct.Uom isEqualToString:@"lb"]||[self.scannedProduct.Uom isEqualToString:@"mg"]){
                                           [newProd setValue:[[[alertText text] stringByAppendingString:@" "] stringByAppendingString:@"kg"] forKey:@"QuantityText"];
                                       }else{
                                           [newProd setValue:[[[alertText text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
                                       }
                                       
                                       [self.productList addObject:newProd];

                                       
                                       //save product to receiving temp product DB
                                       
                                       NSError *error;
                                       NSManagedObject *newManagedObject = [NSEntityDescription
                                                                            insertNewObjectForEntityForName:@"ReceivingTempProdList"
                                                                            inManagedObjectContext:context];
                                       
                                       [newManagedObject setValue:self.documentId forKey:@"documentId"];
                                       [newManagedObject setValue:self.scannedProduct.ProductId forKey:@"productId"];
                                       [newManagedObject setValue:self.scannedProduct.ProductName forKey:@"productName"];
                                       [newManagedObject setValue:[[[alertText text]stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"quantityText"];
                                       [newManagedObject setValue:self.scannedProduct.Uom forKey:@"uom"];
                                       [newManagedObject setValue:[NSNumber numberWithDouble:[[alertText text] doubleValue]] forKey:@"quantity"];
                                       
                                       [context save:&error];
                                       
                                       
                                       //regenerate receiving object
                                       
                                       NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
                                       [receiving setValue:self.productList forKey:@"productList"];
                                       [receiving setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"imagecounter"];
                                       [receiving setValue:[NSNumber numberWithBool:self.pushToCM] forKey:@"pushToCM"];
                                       [receiving setValue:self.notes forKey:@"notes"];
                                       [receiving setValue:self.documentId forKey:@"documentId"];
                                       
                                       [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
                                       [[NSUserDefaults standardUserDefaults] synchronize];
                                       
                                       [self loadObjectsForView];
                                       
                                   }else{
                                       [self newProductQuantityAlert];
                                   }
                               }];
    
    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                     handler:^(UIAlertAction *action)
                               {
                                   self.shouldScanProduct=TRUE;
                                   
                               }];
    
    [quantityAlertController addAction:okaction];
    [quantityAlertController addAction:cancelaction];
    
    [self presentViewController:quantityAlertController animated:YES completion:nil];
    
}

-(void)discrepencyQuantityAlert{

    self.shouldScanProduct=NO;
    NSObject * foundProduct = [[NSObject alloc]init];
    
    if(self.section==0){
        foundProduct = [self.notOrdered objectAtIndex:self.row];
    }else if(self.section==1){
        foundProduct = [self.notReceived objectAtIndex:self.row];
    }else{
        foundProduct = [self.misMatched objectAtIndex:self.row];
    }
    
    UIAlertController * quantityAlertController = [UIAlertController
                               alertControllerWithTitle:[foundProduct valueForKey:@"Name"]
                               message:@"Please enter the correted quantity for this product and press OK"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    [quantityAlertController addTextFieldWithConfigurationHandler:^(UITextField* quantityTextField){
        [quantityTextField becomeFirstResponder];
        
        if([[foundProduct valueForKey:@"Uom"]isEqualToString:@"ea"]){
            [quantityTextField setKeyboardType:UIKeyboardTypeNumberPad];
            quantityTextField.placeholder = [foundProduct valueForKey:@"ReceivedQuantity"];
        }else{
            [quantityTextField setKeyboardType:UIKeyboardTypeDecimalPad];
            
            if([[foundProduct valueForKey:@"Uom"]isEqualToString:@"100g"]){
                double quantity = [[foundProduct valueForKey:@"ReceivedQuantity"]doubleValue] /10;
                quantityTextField.placeholder = [NSString stringWithFormat:@"%.02f", quantity];
            }else if([[foundProduct valueForKey:@"Uom"]isEqualToString:@"250g"]){
                double quantity = [[foundProduct valueForKey:@"ReceivedQuantity"]doubleValue] /4;
                quantityTextField.placeholder = [NSString stringWithFormat:@"%.02f", quantity];
            }else if([[foundProduct valueForKey:@"Uom"]isEqualToString:@"500g"]){
                double quantity = [[foundProduct valueForKey:@"ReceivedQuantity"]doubleValue] /2;
                quantityTextField.placeholder = [NSString stringWithFormat:@"%.02f", quantity];
            }else if([[foundProduct valueForKey:@"Uom"]isEqualToString:@"g"]){
                double quantity = [[foundProduct valueForKey:@"ReceivedQuantity"]doubleValue] /1000;
                quantityTextField.placeholder = [NSString stringWithFormat:@"%.02f", quantity];
            }else if([[foundProduct valueForKey:@"Uom"]isEqualToString:@"kg"]){
                double quantity = [[foundProduct valueForKey:@"ReceivedQuantity"]doubleValue] /1;
                quantityTextField.placeholder = [NSString stringWithFormat:@"%.02f", quantity];
            }else if([[foundProduct valueForKey:@"Uom"]isEqualToString:@"lb"]){
                double quantity = [[foundProduct valueForKey:@"ReceivedQuantity"]doubleValue] /2.20462;
                quantityTextField.placeholder = [NSString stringWithFormat:@"%.02f", quantity];
            }else if([[foundProduct valueForKey:@"Uom"]isEqualToString:@"mg"]){
                double quantity = [[foundProduct valueForKey:@"ReceivedQuantity"]doubleValue] /1000000;
                quantityTextField.placeholder = [NSString stringWithFormat:@"%.02f", quantity];
            }else{
                quantityTextField.placeholder = [foundProduct valueForKey:@"ReceivedQuantity"];
            }
        }
    }];
    
    
    UIAlertAction *okaction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action)
                               {
                                   self.shouldScanProduct=TRUE;
                                   
                                   UITextField * alertText = [quantityAlertController.textFields firstObject];
                                   
                                   if([alertText hasText]){
                                       [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                                       
                                       NSObject * foundProduct = [[NSObject alloc]init];
                                       if(self.section==0){
                                           foundProduct = [self.notOrdered objectAtIndex:self.row];
                                       }else if(self.section==1){
                                           foundProduct = [self.notReceived objectAtIndex:self.row];
                                       }else{
                                           foundProduct = [self.misMatched objectAtIndex:self.row];
                                       }
                                       
                                       NSPredicate * predicate = [NSPredicate predicateWithFormat:@"ProductId == %@", [foundProduct valueForKey:@"ProductId"]];
                                       NSArray * filteredArray = [self.productList filteredArrayUsingPredicate:predicate];
                                       
                                       if(filteredArray.count==0){
                                           //add this product to product list with entered quantity
                                           
                                           NSMutableDictionary * product = [[NSMutableDictionary alloc]init];
                                           [product setValue:[foundProduct valueForKey:@"ProductId"] forKey:@"ProductId"];
                                           [product setValue:[foundProduct valueForKey:@"Name"] forKey:@"Name"];
                                           [product setValue:[[[alertText text]stringByAppendingString:@" "] stringByAppendingString:[foundProduct valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                           
                                           [self.productList addObject:product];
                                           
                                           //save product to receiving temp product DB
                                           
                                           NSError *error;
                                           NSManagedObject *newManagedObject = [NSEntityDescription
                                                                                insertNewObjectForEntityForName:@"ReceivingTempProdList"
                                                                                inManagedObjectContext:context];
                                           
                                           [newManagedObject setValue:self.documentId forKey:@"documentId"];
                                           [newManagedObject setValue:[foundProduct valueForKey:@"ProductId"] forKey:@"productId"];
                                           [newManagedObject setValue:[foundProduct valueForKey:@"Name"] forKey:@"productName"];
                                           [newManagedObject setValue:[[[alertText text]stringByAppendingString:@" "] stringByAppendingString:[foundProduct valueForKey:@"Uom"]] forKey:@"quantityText"];
                                           [newManagedObject setValue:[foundProduct valueForKey:@"Uom"] forKey:@"uom"];
                                           [newManagedObject setValue:[NSNumber numberWithDouble:[[alertText text] doubleValue]] forKey:@"quantity"];
                                           
                                           [context save:&error];
                                           
                                           
                                           //regenerate receiving object
                                           
                                           NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
                                           [receiving setValue:self.productList forKey:@"productList"];
                                           [receiving setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"imagecounter"];
                                           [receiving setValue:[NSNumber numberWithBool:self.pushToCM] forKey:@"pushToCM"];
                                           [receiving setValue:self.notes forKey:@"notes"];
                                           [receiving setValue:self.documentId forKey:@"documentId"];
                                           
                                           [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                           
                                       }else{
                                           //change quantity in existing product
                                           
                                           NSObject * productToModify = [[filteredArray objectAtIndex:0] mutableCopy];
                                           
                                           int index = (int)[self.productList indexOfObject:productToModify];
                                           [productToModify setValue:[[[alertText text]stringByAppendingString:@" "] stringByAppendingString:[foundProduct valueForKey:@"Uom"]] forKey:@"QuantityText"];
                                           
                                           [self.productList replaceObjectAtIndex:index withObject:productToModify];
                                           
                                           
                                           //change the quantity text for the current product
                                           
                                           NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context];
                                           NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                           
                                           NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId==%@ AND productId==%@",self.documentId, [productToModify valueForKey:@"ProductId"]];
                                           
                                           [request setPredicate:predicate];
                                           [request setEntity:entityDesc];
                                           
                                           NSError * error;
                                           NSArray *productResults= [context executeFetchRequest:request error:&error];
                                           
                                           [[productResults objectAtIndex:0]setValue:[[[alertText text]stringByAppendingString:@" "] stringByAppendingString:[foundProduct valueForKey:@"Uom"]] forKey:@"quantityText"];
                                           [[productResults objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[alertText text] doubleValue]] forKey:@"quantity"];
                                           [context save:&error];
                                           
                                           //regenerate receiving object
                                           
                                           NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
                                           [receiving setValue:self.productList forKey:@"productList"];
                                           [receiving setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"imagecounter"];
                                           [receiving setValue:[NSNumber numberWithBool:self.pushToCM] forKey:@"pushToCM"];
                                           [receiving setValue:self.notes forKey:@"notes"];
                                           [receiving setValue:self.documentId forKey:@"documentId"];
                                           
                                           [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                       }
                                       [self loadObjectsForView];
                                   }else{
                                       [self discrepencyQuantityAlert];
                                   }
                               }];
    
    UIAlertAction *cancelaction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action)
                                   {
                                       self.shouldScanProduct=TRUE;
                                   }];
    
    [quantityAlertController addAction:okaction];
    [quantityAlertController addAction:cancelaction];
    [self presentViewController:quantityAlertController animated:YES completion:nil];
}

-(void)finishReceiving{
    self.shouldScanProduct=NO;

    UIAlertController *finishAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:nil
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *finishaction = [UIAlertAction
                               actionWithTitle:@"Finish Receiving"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self startSpinner:@"Saving Data"];
                                   [self saveVosToFile];
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [NSUserDefaults resetStandardUserDefaults];
                                       [[NSUserDefaults standardUserDefaults]synchronize];
                                       self.context=Nil;
                                       self.vosProductList=Nil;
                                       self.discrepencyArray=Nil;
                                       self.documentId=nil;
                                       self.productList=nil;
                                       self.notes=nil;
                                       
                                       [statusAlert dismissViewControllerAnimated:YES completion:^{
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                       }];
                                   });

                               }];
    
    UIAlertAction *saveExitaction = [UIAlertAction
                                   actionWithTitle:@"Save & Exit"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self startSpinner:@"Saving Data"];
                                       
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [NSUserDefaults resetStandardUserDefaults];
                                           [[NSUserDefaults standardUserDefaults]synchronize];
                                           self.context=Nil;
                                           self.vosProductList=Nil;
                                           self.discrepencyArray=Nil;
                                           self.documentId=nil;
                                           self.productList=nil;
                                           self.notes=nil;
                                           
                                           [statusAlert dismissViewControllerAnimated:YES completion:^{
                                               [self.navigationController popToRootViewControllerAnimated:YES];
                                           }];
                                       });
                                   }];
    
    UIAlertAction *cancelaction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       self.shouldScanProduct=YES;
                                   }];
    
    [finishAlertController addAction:finishaction];
    [finishAlertController addAction:saveExitaction];
    [finishAlertController addAction:cancelaction];
    [self presentViewController:finishAlertController animated:YES completion:nil];
}


-(void)noDiscrepencyFinish{
   
    self.shouldScanProduct=NO;
    UIAlertController *finishAlertController = [UIAlertController
                                                alertControllerWithTitle:@"No Discrepency found, Click YES to finish Receiving"
                                                message:nil
                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesaction = [UIAlertAction
                                   actionWithTitle:@"YES"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self startSpinner:@"Saving Data"];
                                       [self saveVosToFile];
                                       
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [NSUserDefaults resetStandardUserDefaults];
                                           [[NSUserDefaults standardUserDefaults]synchronize];
                                           self.context=Nil;
                                           self.vosProductList=Nil;
                                           self.discrepencyArray=Nil;
                                           self.documentId=nil;
                                           self.productList=nil;
                                           self.notes=nil;
                                           
                                           [statusAlert dismissViewControllerAnimated:YES completion:^{
                                               [self.navigationController popToRootViewControllerAnimated:YES];
                                           }];
                                       });
                                   }];
    
    UIAlertAction *noaction = [UIAlertAction
                                actionWithTitle:@"NO"
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction *action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }];
    
    [finishAlertController addAction:yesaction];
    [finishAlertController addAction:noaction];
    [self presentViewController:finishAlertController animated:YES completion:nil];
  
}

//
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    self.shouldScanProduct=YES;
//
//    if(alertView.tag==discrepencyQuantityAlertPrompt){
//        if(buttonIndex==0){
//            if([[alertView textFieldAtIndex:0]hasText]){
//                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//
//                NSObject * foundProduct = [[NSObject alloc]init];
//                if(self.section==0){
//                    foundProduct = [self.notOrdered objectAtIndex:self.row];
//                }else if(self.section==1){
//                    foundProduct = [self.notReceived objectAtIndex:self.row];
//                }else{
//                    foundProduct = [self.misMatched objectAtIndex:self.row];
//                }
//
//                NSPredicate * predicate = [NSPredicate predicateWithFormat:@"ProductId == %@", [foundProduct valueForKey:@"ProductId"]];
//                NSArray * filteredArray = [self.productList filteredArrayUsingPredicate:predicate];
//
//                if(filteredArray.count==0){
//                    //add this product to product list with entered quantity
//
//                    NSMutableDictionary * product = [[NSMutableDictionary alloc]init];
//                    [product setValue:[foundProduct valueForKey:@"ProductId"] forKey:@"ProductId"];
//                    [product setValue:[foundProduct valueForKey:@"Name"] forKey:@"Name"];
//                    [product setValue:[[[[alertView textFieldAtIndex:0] text]stringByAppendingString:@" "] stringByAppendingString:[foundProduct valueForKey:@"Uom"]] forKey:@"QuantityText"];
//
//
//                    [self.productList addObject:product];
//
//                    //save product to receiving temp product DB
//
//                    NSError *error;
//                    NSManagedObject *newManagedObject = [NSEntityDescription
//                                                         insertNewObjectForEntityForName:@"ReceivingTempProdList"
//                                                         inManagedObjectContext:context];
//
//                    [newManagedObject setValue:self.documentId forKey:@"documentId"];
//                    [newManagedObject setValue:[foundProduct valueForKey:@"ProductId"] forKey:@"productId"];
//                    [newManagedObject setValue:[foundProduct valueForKey:@"Name"] forKey:@"productName"];
//                    [newManagedObject setValue:[[[[alertView textFieldAtIndex:0] text]stringByAppendingString:@" "] stringByAppendingString:[foundProduct valueForKey:@"Uom"]] forKey:@"quantityText"];
//                    [newManagedObject setValue:[foundProduct valueForKey:@"Uom"] forKey:@"uom"];
//                    [newManagedObject setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0] text] doubleValue]] forKey:@"quantity"];
//
//                    [context save:&error];
//
//
//                    //regenerate receiving object
//
//                    NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
//                    [receiving setValue:self.productList forKey:@"productList"];
//                    [receiving setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"imagecounter"];
//                    [receiving setValue:[NSNumber numberWithBool:self.pushToCM] forKey:@"pushToCM"];
//                    [receiving setValue:self.notes forKey:@"notes"];
//                    [receiving setValue:self.documentId forKey:@"documentId"];
//
//                    [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
//                    [[NSUserDefaults standardUserDefaults] synchronize];
//
//                }else{
//                    //change quantity in existing product
//
//                    NSObject * productToModify = [[filteredArray objectAtIndex:0] mutableCopy];
//
//                    int index = (int)[self.productList indexOfObject:productToModify];
//                    [productToModify setValue:[[[[alertView textFieldAtIndex:0] text]stringByAppendingString:@" "] stringByAppendingString:[foundProduct valueForKey:@"Uom"]] forKey:@"QuantityText"];
//
//                    [self.productList replaceObjectAtIndex:index withObject:productToModify];
//
//
//                    //change the quantity text for the current product
//
//                    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context];
//                    NSFetchRequest *request = [[NSFetchRequest alloc]init];
//
//                    NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId==%@ AND productId==%@",self.documentId, [productToModify valueForKey:@"ProductId"]];
//
//                    [request setPredicate:predicate];
//                    [request setEntity:entityDesc];
//
//                    NSError * error;
//                    NSArray *productResults= [context executeFetchRequest:request error:&error];
//
//                    [[productResults objectAtIndex:0]setValue:[[[[alertView textFieldAtIndex:0] text]stringByAppendingString:@" "] stringByAppendingString:[foundProduct valueForKey:@"Uom"]] forKey:@"quantityText"];
//                    [[productResults objectAtIndex:0] setValue:[NSNumber numberWithDouble:[[[alertView textFieldAtIndex:0] text] doubleValue]] forKey:@"quantity"];
//                    [context save:&error];
//
//                    //regenerate receiving object
//
//                    NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
//                    [receiving setValue:self.productList forKey:@"productList"];
//                    [receiving setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"imagecounter"];
//                    [receiving setValue:[NSNumber numberWithBool:self.pushToCM] forKey:@"pushToCM"];
//                    [receiving setValue:self.notes forKey:@"notes"];
//                    [receiving setValue:self.documentId forKey:@"documentId"];
//
//                    [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
//                    [[NSUserDefaults standardUserDefaults] synchronize];
//
//                }
//
//                [self loadObjectsForView];
//
//            }else{
//                [self discrepencyQuantityAlert];
//            }
//        }
//    }
//    if(alertView.tag==newProductQuantityAlertPrompt){
//        self.shouldScanProduct=YES;
//        if(buttonIndex==0){
//            if([[alertView textFieldAtIndex:0]hasText]){
//                NSMutableDictionary * newProd = [[NSMutableDictionary alloc] init];
//                [newProd setValue:self.scannedProduct.ProductName forKey:@"Name"];
//                [newProd setValue:self.scannedProduct.ProductId forKey:@"ProductId"];
//
//                if([self.scannedProduct.Uom isEqualToString:@"ea"]){
//                    [newProd setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
//                }else if([self.scannedProduct.Uom isEqualToString:@"100g"] || [self.scannedProduct.Uom isEqualToString:@"250g"]||[self.scannedProduct.Uom isEqualToString:@"500g"]||[self.scannedProduct.Uom isEqualToString:@"g"]||[self.scannedProduct.Uom isEqualToString:@"kg"]||[self.scannedProduct.Uom isEqualToString:@"lb"]||[self.scannedProduct.Uom isEqualToString:@"mg"]){
//                    [newProd setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:@"kg"] forKey:@"QuantityText"];
//                }else{
//                    [newProd setValue:[[[[alertView textFieldAtIndex:0] text] stringByAppendingString:@" "] stringByAppendingString:self.scannedProduct.Uom] forKey:@"QuantityText"];
//                }
//
//                [self.productList addObject:newProd];
//
//                NSMutableDictionary * receiving = [[NSMutableDictionary alloc]init];
//                [receiving setValue:self.productList forKey:@"productList"];
//                [receiving setValue:[NSNumber numberWithInt:self.imagecounter] forKey:@"imagecounter"];
//                [receiving setValue:[NSNumber numberWithBool:self.pushToCM] forKey:@"pushToCM"];
//                [receiving setValue:self.notes forKey:@"notes"];
//                [receiving setValue:self.documentId forKey:@"documentId"];
//
//                [[NSUserDefaults standardUserDefaults] setObject:receiving forKey:@"RECEIVING_OBJECT"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
//
//                [self loadObjectsForView];
//
//            }else{
//                [self newProductQuantityAlert];
//            }
//        }
//    }
//    if(alertView.tag==finishAuditPrompt){
//        if(buttonIndex==0){
//            [self startSpinner:@"Saving Data"];
//            [self saveVosToFile];
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [NSUserDefaults resetStandardUserDefaults];
//                [[NSUserDefaults standardUserDefaults]synchronize];
//                self.context=Nil;
//                self.vosProductList=Nil;
//                self.discrepencyArray=Nil;
//                self.documentId=nil;
//                self.productList=nil;
//                self.notes=nil;
//
//                [statusAlert dismissViewControllerAnimated:YES completion:^{
//                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }];
//            });
//        }else if(buttonIndex==1){
//
//            [self startSpinner:@"Saving Data"];
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [NSUserDefaults resetStandardUserDefaults];
//                [[NSUserDefaults standardUserDefaults]synchronize];
//                self.context=Nil;
//                self.vosProductList=Nil;
//                self.discrepencyArray=Nil;
//                self.documentId=nil;
//                self.productList=nil;
//                self.notes=nil;
//
//                [self stopSpinner];
//                [self.navigationController popToRootViewControllerAnimated:YES];
//            });
//
//        }else if(buttonIndex==2){
//
//        }
//
//    }
//    if(alertView.tag==discrepencyErrorAlerts){
//
//    }
//}


#pragma mark - button funciton

-(void)submitOrder{
    [self finishReceiving];
}

-(void)editOrder{
    [self showActionList];
}


-(NSString*)getUomTextForCell:(NSString*)orderedQuantity receivedQuantity:(NSString*)receivedQuantity uom:(NSString*)uom{
    NSString * returnText = @"";
        returnText = [returnText stringByAppendingString:[NSString stringWithFormat:@"Ordered Quantity: %@ %@  received Quantity: %@ %@", orderedQuantity, uom,receivedQuantity, uom]];

    return returnText;
}




-(void)saveVosToFile{
    @try {
        NSDate * endTime = [NSDate date];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *stringFromDate = [formatter stringFromDate:endTime];
        
        NSMutableArray<vosReceiveAssociatedProducts> * allProducts = [[NSMutableArray<vosReceiveAssociatedProducts> alloc]init];
        for(NSDictionary * product in self.productList){
            vosReceiveAssociatedProducts * finalProduct = [[vosReceiveAssociatedProducts alloc]init];
            finalProduct.ProductId = [product valueForKey:@"ProductId"];
            finalProduct.QuantityText = [product valueForKey:@"QuantityText"];
            [allProducts addObject:finalProduct];
        }
        
        vosReceiveAuditElement * auditElement = [[vosReceiveAuditElement alloc]init];
        auditElement.ReceiptId = NULL;
        auditElement.ReceiptNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"VOS_RECEIPT"];
        auditElement.VendorId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"];
        auditElement.VosNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"];
        auditElement.VosId=[[NSUserDefaults standardUserDefaults] valueForKey:@"VOS_ID"];
        
        auditElement.GrandTotal = nil;
        auditElement.AttachmentCount = [NSNumber numberWithInt:self.imagecounter];
        auditElement.SendToCM = [NSNumber numberWithBool:self.pushToCM];
        auditElement.Note= self.notes;
        auditElement.ProductsList = allProducts;
        
        vosReceiveAuditData * auditData = [[vosReceiveAuditData alloc]init];
        auditData.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
        auditData.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        auditData.ReceivingType = @"VOS_RECEIVE";
        auditData.AuditItem = auditElement;
        
        
        [self writeStringToFile:[auditData toJSONString] fileName:stringFromDate];
        NSLog(@"File Saved for %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]);
        
        NSError *error;
        NSManagedObject *newManagedObject = [NSEntityDescription
                                             insertNewObjectForEntityForName:@"DocStatusInfo"
                                             inManagedObjectContext:context];
        
        [newManagedObject setValue:@"VOS_RECEIVE" forKey:@"documentType"];
        [newManagedObject setValue:@"Finished" forKey:@"status"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
        [newManagedObject setValue:self.documentId forKey:@"documentId"];
        [newManagedObject setValue:endTime forKey:@"date"];
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"] forKey:@"documentNo"];
        [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
        [context save:&error];
        
        //delete from DocStatusInfo table
        
        NSFetchRequest *allDocs = [[NSFetchRequest alloc] init];
        [allDocs setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
        NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
        [allDocs setPredicate:docPredicate];
        NSArray *docs = [context executeFetchRequest:allDocs error:&error];
        for (NSManagedObject *doc in docs) {
            [context deleteObject:doc];  }
        NSError *saveError = nil;
        [context save:&saveError];
        
        //delete from ReceivingTempProdList table
        
        NSFetchRequest *allProds = [[NSFetchRequest alloc] init];
        [allProds setEntity:[NSEntityDescription entityForName:@"ReceivingTempProdList" inManagedObjectContext:context]];
        NSPredicate *prodPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
        [allProds setPredicate:prodPredicate];
        NSArray *prods = [context executeFetchRequest:allProds error:&error];
        for (NSManagedObject *prod in prods) {
            [context deleteObject:prod];  }
        [context save:&saveError];
        
        NSLog(@"temp DB entries deleted..");
        
    } @catch (NSException *exception) {
        NSLog(@"exeption thrown while saving file for vos:%@", exception);
        [self showErrorAlert:@"An error has occurred – please restart the application and contact IT"];
    }
}


- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}



@end
