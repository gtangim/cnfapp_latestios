//
//  CNFVosTransferSelectTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-03-24.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"


@interface CNFVosTransferSelectTableViewController : UITableViewController<UISearchDisplayDelegate, UISearchBarDelegate, UISearchControllerDelegate, ZBarReaderDelegate, NSFetchedResultsControllerDelegate, DTDeviceDelegate, UIAlertViewDelegate, UISearchResultsUpdating, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *scanButton;

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

- (IBAction)backButton:(id)sender;
- (IBAction)scanProduct:(id)sender;

@end
