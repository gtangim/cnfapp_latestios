//
//  CNFVosTransferSelectTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-03-24.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFVosTransferSelectTableViewController.h"
#import "Products.h"

#import "CNFAppEnums.h"
#import "BarcodeProcessor.h"
#import "Reachability.h"
#import "vosGetRequest.h"
#import "transferGetRequest.h"
#import "GZIP.h"
#import "BRLineReader.h"


@interface CNFVosTransferSelectTableViewController ()
@property (nonatomic, strong ) NSManagedObjectContext * context;
@property (nonatomic, strong) NSMutableArray * vosArray;
@property (nonatomic, strong) NSMutableArray * transferArray;
@property (nonatomic) NSInteger index;
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic, strong) NSArray * filterTransferArray;
@property (nonatomic, strong) NSMutableArray * filterVosArray;
@property (nonatomic) BOOL isOnlineData;
@property (nonatomic) BOOL scanProduct;
@property (nonatomic) BOOL dataFileDownloaded;
@property (nonatomic, strong) NSString * interVersion;
@property (nonatomic, strong) NSString * transferNo;
@property (nonatomic, strong) NSString * receiptNo;
@property (nonatomic, strong) NSObject * vos;
@property (strong, nonatomic) UISearchController *searchController;
@end

@implementation CNFVosTransferSelectTableViewController{
    NSArray *searchResults;
    UIAlertController * statusAlert;
    NSManagedObjectContext * context;
}

@synthesize  vosArray = _vosArray, transferArray = _transferArray, index =_index, scanner=_scanner,scannerAvailable=_scannerAvailable, foundBarcode=_foundBarcode, scanButton=_scanButton, context = _context, filterTransferArray=_filterTransferArray, filterVosArray=_filterVosArray, isOnlineData = _isOnlineData, scanProduct=_scanProduct, dataFileDownloaded=_dataFileDownloaded, interVersion=_interVersion, transferNo=_transferNo, receiptNo=_receiptNo, vos=_vos;


-(NSMutableArray *)vosArray{
    if(!_vosArray)_vosArray=[[NSMutableArray alloc]init];
    return _vosArray;
}

-(NSMutableArray *)transferArray{
    if(!_transferArray)_transferArray=[[NSMutableArray alloc]init];
    return _transferArray;
}

-(NSMutableArray *)filterVosArray{
    if(!_filterVosArray)_filterVosArray=[[NSMutableArray alloc]init];
    return _filterVosArray;
}

- (void)viewDidLoad {
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationItem.leftBarButtonItem.enabled=NO;

    [super viewDidLoad];
    self.isOnlineData=NO;
    self.scannerAvailable = TRUE;
    self.scanProduct=YES;
    self.dataFileDownloaded=FALSE;
    
    self.scanner=[DTDevices sharedDevice];
    [self.scanner addDelegate:self];
    
    [self testConnectionAndGetData];
    
}


-(void)initiateSearchBar{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
        self.searchController.searchBar.showsScopeBar=NO;
    }else{
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"Note CONTAINS[cd] %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] stringByAppendingString:@"_Cafe"]];
        if([self.transferArray filteredArrayUsingPredicate:resultPredicate].count>0){
            self.searchController.searchBar.showsScopeBar = YES;
        }else{
            self.searchController.searchBar.showsScopeBar = NO;
        }
        self.searchController.searchBar.scopeButtonTitles = [NSArray arrayWithObjects:@"All",@"Cafe Transfers", nil];
    }
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    [self.searchController setActive:NO];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    [self.searchController setActive:FALSE];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    searchResults=nil;
    [self.scanner addDelegate:self];
    [self.scanner connect];
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailability) userInfo:nil repeats:NO];
    [super viewWillAppear:animated];
}


-(void)viewWillDisappear:(BOOL)animated{
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
    [super viewWillDisappear:animated];
}

-(void)dealloc {
    [self.searchController.view removeFromSuperview];
}

#pragma mark - connection tester

-(int)connectionTester{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];
    
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
    if(netStatus==1){
        NSLog(@"connection OK...");
    }else{
        NSLog(@"No connection...");
    }
    
    return netStatus;
}


#pragma mark - spinner functions

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *customVC     = [[UIViewController alloc] init];
        
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [statusAlert setValue:customVC forKey:@"contentViewController"];
        
        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:nil];
        statusAlert=nil;
    }
}

#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailability{
    if([self.scanner connstate]==1){
        self.scannerAvailable=FALSE;
//        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
    self.navigationItem.leftBarButtonItem.enabled=YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        
        if(self.filterVosArray.count>0){
            return self.filterVosArray.count;
        }
        
        if (searchResults.count>0) {
            return [searchResults count];
        } else {
            return self.vosArray.count;
        }
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        if(self.filterTransferArray.count>0){
            return self.filterTransferArray.count;
        }
        
        if (searchResults.count>0) {
            return [searchResults count];
        } else {
            return  self.transferArray.count;
        }
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vosTransferCells"];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"vosTransferCells"];
    }
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/dd/YYYY HH:mm:ss"];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        if(self.filterVosArray.count>0){
            
            cell.textLabel.text = [[self.filterVosArray objectAtIndex:indexPath.row]valueForKey:@"VosNumber"];
            
            if(self.isOnlineData){
                cell.detailTextLabel.text= [NSString stringWithFormat:@"From vendor: %@, Total Quantity: %@, Created on: %@", [[self.filterVosArray objectAtIndex:indexPath.row]valueForKey:@"VendorName"], [[[self.filterVosArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue], [dateFormatter stringFromDate:[self getDateFromJSON:[[self.filterVosArray objectAtIndex:indexPath.row]valueForKey:@"CreatedOn"]]]];
            }else{
                cell.detailTextLabel.text= [NSString stringWithFormat:@"From vendor: %@, Total Quantity: %@, Created on: %@", [[self.filterVosArray objectAtIndex:indexPath.row]valueForKey:@"VendorName"], [[[self.filterVosArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue], [[[[self.filterVosArray objectAtIndex:indexPath.row]valueForKey:@"CreatedOn"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]];
            }
            
        }else{
            if (searchResults.count>0) {
                cell.textLabel.text = [[searchResults objectAtIndex:indexPath.row]valueForKey:@"VosNumber"];
                
                
                if(self.isOnlineData){
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"From vendor: %@, Total Quantity: %@, Created on: %@", [[searchResults objectAtIndex:indexPath.row]valueForKey:@"VendorName"], [[[searchResults objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue], [dateFormatter stringFromDate:[self getDateFromJSON:[[searchResults objectAtIndex:indexPath.row]valueForKey:@"CreatedOn"]]]];
                }else{
                    
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"From vendor: %@, Total Quantity: %@, Created on: %@", [[searchResults objectAtIndex:indexPath.row]valueForKey:@"VendorName"], [[[searchResults objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue],[[[[searchResults objectAtIndex:indexPath.row]valueForKey:@"CreatedOn"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]];
                }
                
            } else {
                cell.textLabel.text = [[self.vosArray objectAtIndex:indexPath.row]valueForKey:@"VosNumber"];
                
                if(self.isOnlineData){
                    
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"From vendor: %@, Total Quantity: %@, Created on: %@", [[self.vosArray objectAtIndex:indexPath.row]valueForKey:@"VendorName"], [[[self.vosArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue], [dateFormatter stringFromDate:[self getDateFromJSON:[[self.vosArray objectAtIndex:indexPath.row]valueForKey:@"CreatedOn"]]]];
                }else{
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"From vendor: %@, Total Quantity: %@, Created on: %@", [[self.vosArray objectAtIndex:indexPath.row]valueForKey:@"VendorName"], [[[self.vosArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue],[[[[self.vosArray objectAtIndex:indexPath.row]valueForKey:@"CreatedOn"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]];
                }
            }
        }
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        if(self.filterTransferArray.count>0){
            cell.textLabel.text = [[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"TransferNumber"];
            
            if(self.isOnlineData){
                
                if([[self.filterTransferArray objectAtIndex:indexPath.row] valueForKey:@"Note"]){
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@, Note: %@", [[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"FromLocation"], [[[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue], [dateFormatter stringFromDate:[self getDateFromJSON:[[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"CreatedDate"]]], [[self.filterTransferArray objectAtIndex:indexPath.row] valueForKey:@"Note"]];
                    
                }else
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@", [[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"FromLocation"], [[[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue], [dateFormatter stringFromDate:[self getDateFromJSON:[[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"CreatedDate"]]]];
                
            }else{
                
                if([[self.filterTransferArray objectAtIndex:indexPath.row] valueForKey:@"Note"]){
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@, Note: %@", [[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"FromLocationId"], [[[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue], [[[[self.filterTransferArray objectAtIndex:indexPath.row] valueForKey:@"CreationDate"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "], [[self.filterTransferArray objectAtIndex:indexPath.row] valueForKey:@"Note"]];
                    
                }else
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@", [[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"FromLocationId"], [[[self.filterTransferArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue], [[[[self.filterTransferArray objectAtIndex:indexPath.row] valueForKey:@"CreationDate"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]];
            }
            
        }else{
            if (searchResults.count>0) {
                
                cell.textLabel.text = [[searchResults objectAtIndex:indexPath.row]valueForKey:@"TransferNumber"];
                
                if(self.isOnlineData){
                    
                    if([[searchResults objectAtIndex:indexPath.row]valueForKey:@"Note"]!=[NSNull null])
                        cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@ Note: %@", [[searchResults objectAtIndex:indexPath.row]valueForKey:@"FromLocation"], [[[searchResults objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue] , [dateFormatter stringFromDate:[self getDateFromJSON:[[searchResults objectAtIndex:indexPath.row]valueForKey:@"CreatedDate"]]], [[searchResults objectAtIndex:indexPath.row]valueForKey:@"Note"]];
                    
                    else
                        cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@", [[searchResults objectAtIndex:indexPath.row]valueForKey:@"FromLocation"], [[[searchResults objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue] , [dateFormatter stringFromDate:[self getDateFromJSON:[[searchResults objectAtIndex:indexPath.row]valueForKey:@"CreatedDate"]]]];
                    
                }else{
                    
                    if([[searchResults objectAtIndex:indexPath.row]valueForKey:@"Note"]!=[NSNull null])
                        cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@ Note: %@", [[searchResults objectAtIndex:indexPath.row]valueForKey:@"FromLocationId"], [[[searchResults objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue] , [[[[searchResults objectAtIndex:indexPath.row]valueForKey:@"CreationDate"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "], [[searchResults objectAtIndex:indexPath.row]valueForKey:@"Note"]];
                    
                    else
                        cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@", [[searchResults objectAtIndex:indexPath.row]valueForKey:@"FromLocationId"], [[[searchResults objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue] ,[[[[searchResults objectAtIndex:indexPath.row]valueForKey:@"CreationDate"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]];
                    
                }
                
            } else {
                
                cell.textLabel.text = [[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"TransferNumber"];
                
                if(self.isOnlineData){
                    if([[self.transferArray objectAtIndex:indexPath.row] valueForKey:@"Note"]!=[NSNull null])
                        cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@ Note: %@", [[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"FromLocation"], [[[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue] , [dateFormatter stringFromDate:[self getDateFromJSON:[[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"CreatedDate"]]], [[self.transferArray objectAtIndex:indexPath.row] valueForKey:@"Note"]];
                    
                    else
                        cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@", [[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"FromLocation"], [[[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue] , [dateFormatter stringFromDate:[self getDateFromJSON:[[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"CreatedDate"]]]];
                }else{
                    if([[self.transferArray objectAtIndex:indexPath.row] valueForKey:@"Note"]!=[NSNull null])
                        cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@ Note: %@", [[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"FromLocationId"], [[[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue] , [[[[self.transferArray objectAtIndex:indexPath.row] valueForKey:@"CreationDate"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "], [[self.transferArray objectAtIndex:indexPath.row] valueForKey:@"Note"]];
                    
                    else
                        cell.detailTextLabel.text= [NSString stringWithFormat:@"From location: %@, Total Quantity: %@, Created on: %@", [[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"FromLocationId"], [[[self.transferArray objectAtIndex:indexPath.row]valueForKey:@"TotalQuantity"] stringValue] , [[[[self.transferArray objectAtIndex:indexPath.row] valueForKey:@"CreationDate"] valueForKey:@"DateTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]];
                }
                
                
            }
        }
    }
    
    cell.textLabel.numberOfLines=0;
    cell.detailTextLabel.numberOfLines=0;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        if(self.filterTransferArray.count>0){
            self.index=indexPath.row;
            [self confirmVosTransferAlert];
        }else{
            if (searchResults.count>0) {
                self.index=indexPath.row;
                [self confirmVosTransferAlert];
            }else {
                self.index=indexPath.row;
                [self confirmVosTransferAlert];
            }
        }
    }else{
        if(self.filterVosArray.count>0){
            self.index=indexPath.row;
            [self confirmVosTransferAlert];
        }else{
            if (searchResults.count>0) {
                self.index=indexPath.row;
                [self confirmVosTransferAlert];
            }else {
                self.index=indexPath.row;
                [self confirmVosTransferAlert];
            }
        }
    }
}


#pragma  mark - alert views

-(void)showErrorAlert:(NSString *)message{
    self.scanProduct=FALSE;
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


-(void)confirmVosTransferAlert{
    self.scanProduct=FALSE;
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        
        NSObject * vos = [[NSObject alloc]init];
        
        if(self.filterVosArray.count>0){
            vos = [self.filterVosArray objectAtIndex:self.index];
        }else{
            if(searchResults.count>0){
                vos = [searchResults objectAtIndex:self.index];
            }else{
                vos = [self.vosArray objectAtIndex:self.index];
            }
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[vos valueForKey:@"VosNumber"] message:[NSString stringWithFormat:@"This VOS is from: %@,Please type the receipt number and click YES to proceed with this order", [vos valueForKey:@"VendorName"]] preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"Receipt Number";
            textField.keyboardType = UIKeyboardTypeDefault;
        }];
        
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            self.scanProduct=TRUE;
            
            if([[alertController.textFields objectAtIndex:0]hasText]){
                if([self checkVosTransferExistanceInDataFile:[vos valueForKey:@"VosNumber"]]){
                    [self navigateToReceivingPageForVos:vos receiptNo:[alertController.textFields objectAtIndex:0].text];
                }else{
                    //get new data file here..
                    if(!self.dataFileDownloaded){
                        NSLog(@"getting new data file..");
                        self.vos=vos;
                        self.receiptNo=[alertController.textFields objectAtIndex:0].text;
                        [self testConnectionAndGetDataFile];
                    }else
                        [self showErrorAlert:@"Data File does not contain VOS, please try again later"];
                }
            }else{
                [self confirmVosTransferAlert];
            }
        }];
        
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            self.scanProduct=TRUE;
        }];
        
        [alertController addAction:yesAction];
        [alertController addAction:noAction];
        
        [self presentViewController:alertController animated: YES completion: nil];
        
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        NSObject * transfer = [[NSObject alloc]init];
        
        if(self.filterTransferArray.count>0){
            transfer = [self.filterTransferArray objectAtIndex:self.index];
        }else{
            if(searchResults.count>0){
                transfer = [searchResults objectAtIndex:self.index];
            }else{
                transfer = [self.transferArray objectAtIndex:self.index];
            }
        }
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[transfer valueForKey:@"TransferNumber"] message:[NSString stringWithFormat:@"Continue receiving trasnfer with total item: %@ from %@", [[transfer valueForKey:@"TotalQuantity"] stringValue], [transfer valueForKey:@"FromLocation"]] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            self.scanProduct=TRUE;
            self.transferNo=[transfer valueForKey:@"TransferNumber"];
            
            if([self checkVosTransferExistanceInDataFile:[transfer valueForKey:@"TransferNumber"]]){
                [self navigateToReceivingPageForTransfer:[transfer valueForKey:@"TransferNumber"]];
            }else{
                //get new data file here
                if(!self.dataFileDownloaded){
                    NSLog(@"getting new data file..");
                    self.transferNo=[transfer valueForKey:@"TransferNumber"];
                    [self testConnectionAndGetDataFile];
                }else
                    [self showErrorAlert:@"Data File does not contain Transfer, please try again later"];
            }
            
        }];
        
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            self.scanProduct=TRUE;
        }];
        [alertController addAction:yesAction];
        [alertController addAction:noAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
}


-(void)navigateToReceivingPageForVos:(NSObject*)vos receiptNo:(NSString*)receiptNo{
    [[NSUserDefaults standardUserDefaults] setObject:[vos valueForKey:@"VosNumber"] forKey:@"DOCUMENT_NO"];
    [[NSUserDefaults standardUserDefaults] setObject:receiptNo forKey:@"VOS_RECEIPT"];
    NSMutableDictionary * vendor = [[NSMutableDictionary alloc]init];
    [vendor setValue:[vos valueForKey:@"VendorId"] forKey:@"VendorId"];
    [vendor setValue:[vos valueForKey:@"VendorName"] forKey:@"VendorName"];
    [[NSUserDefaults standardUserDefaults] setValue:vendor forKey:@"VENDOR"];
    [[NSUserDefaults standardUserDefaults] setValue:[vos valueForKey:@"Id"] forKey:@"VOS_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"user %@ started receiving %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"],[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopSpinner];
        [self performSegueWithIdentifier:@"vosSelectToScannerSegue" sender:self];
    });
}


-(void)navigateToReceivingPageForTransfer:(NSString*)documentNo{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [[NSUserDefaults standardUserDefaults] setValue:documentNo forKey:@"DOCUMENT_NO"];
        [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"VENDOR"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"user %@ started receiving %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"],[[NSUserDefaults standardUserDefaults] valueForKey:@"DOCUMENT_NO"]);
        dispatch_async (dispatch_get_main_queue(), ^{
            [self stopSpinner];
            [self performSegueWithIdentifier:@"receivingProductVosTransferSague" sender:self];
        });
    });
}


-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    self.scanProduct=FALSE;
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}


#pragma  mark - barcode scanner function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    if(self.scanProduct){
        self.foundBarcode =YES;
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self processBarcode:barcode];
        });
    }else{
        int sound[]={3500,200,0,200,3500,200};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }
}

#pragma mark - camera function

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    
    NSString * barcode=symbol.data;
    
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self processBarcode:barcode];
    });
}


#pragma mark - webservice functions

-(void)testConnectionAndGetData{
    
    self.scanProduct=FALSE;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        if([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_TIME"]]/60 < [[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_INTERVAL"] intValue]){
            NSLog(@"online data fetching problem from last time, will try offline mode");
            [self getDataOffline];
        }else{
            if([self connectionTester]!=0){
                [self getVOSTransferDataOnline];
            }else{
                //get offline data
                [self getDataOffline];
            }
        }
}


#pragma mark - webservice get data functions

-(void)getVOSTransferDataOnline{

    NSLog(@"getting online data..");
    
    NSString *post=@"";
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        vosGetRequest * getRequest = [[vosGetRequest alloc]init];
        getRequest.locationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        post = [getRequest toJSONString];
    }else{
        transferGetRequest * getRequest = [[transferGetRequest alloc]init];
        getRequest.toLocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
        post = [getRequest toJSONString];
    }
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getOpenOrderSheets", appDelegate.serverUrl]]];
    }else{
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getEnrouteTransfers", appDelegate.serverUrl]]];
    }
    
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              NSLog(@"Download Error:%@",error.description);
                                              //get data from file, error downloading
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self getDataOffline];
                                              });

                                          }else{
                                              if (data) {
                                                  NSDictionary *strings = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                  if(error){
                                                      NSLog(@"web service error, can't serialize data..");
                                                      //get data from file, error downloading
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                              [self getDataOffline];
                                                      });

                                                  }else{
                                                      if([strings valueForKey:@"Message"]==Nil){
                                                          
                                                          if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
                                                              self.vosArray = [NSJSONSerialization JSONObjectWithData:[[strings valueForKey:@"d"] dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
                                                          }else{
                                                              self.transferArray = [NSJSONSerialization JSONObjectWithData:[[strings valueForKey:@"d"] dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
                                                          }
                                                          
                                                          if(!error){
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  [self stopSpinner];
//                                                                  if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"receiveWithOrderSheet"]){
//                                                                      
//                                                                      NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"Note CONTAINS[cd] %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] stringByAppendingString:@"_Cafe"]];
//                                                                      if([self.transferArray filteredArrayUsingPredicate:resultPredicate].count>0){
//                                                                          self.searchController.searchBar.showsScopeBar=YES;
//                                                                      }
//                                                                  }
                                                                  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                                  [self initiateSearchBar];
                                                                      [self.tableView reloadData];
                                                                      self.isOnlineData = YES;
                                                                      self.scanProduct=TRUE;
                                                              });

                                                          }else{
                                                              NSLog(@"error msg: %@", error.description);
                                                              //get data from file, error downloading
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                      [self getDataOffline];
                                                              });
                                                          }
                                                      }else{
                                                          NSLog(@"error msg: %@", [strings valueForKey:@"Message"]);
//                                                          get data from file, error downloading
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                                  [self getDataOffline];
                                                          });
                                                      }
                                                  }
                                              }
                                          }
                                      }];
    
    [dataTask resume];

}


-(void)getDataOffline{
    NSLog(@"getting offilne data..");

    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_TIME"]!=NULL){
        if([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_TIME"]]/60 > [[[NSUserDefaults standardUserDefaults] objectForKey:@"ONLINE_TROUBLE_INTERVAL"] intValue]){
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"ONLINE_TROUBLE_TIME"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"ONLINE_TROUBLE_TIME"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSMutableArray * docNoSet = [[NSMutableArray alloc]init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    [request setEntity:entityDesc];
    
    NSError *error;
    NSArray *results= [context executeFetchRequest:request error:&error];
    
    for(NSManagedObject * dbObjects in results){
        if([dbObjects valueForKey:@"documentNo"]!=nil){
            [docNoSet addObject:[dbObjects valueForKey:@"documentNo"]];
        }
    }
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        
        NSPredicate *receiptPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"VosList"];
        NSObject *receiptOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:receiptPred] objectAtIndex:0];
        
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];
        
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[receiptOffset valueForKey:@"O"] intValue]];
        
        databuffer = [file readDataOfLength: [[receiptOffset valueForKey:@"L"] intValue]];
        [file closeFile];
        
        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        
        NSError * error;
        
        NSArray * receiptList = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
        
        
        if(error){
            NSLog(@"Serializer error, data file not downloaded properly..");
            dispatch_async(dispatch_get_main_queue(), ^{
                    [self showErrorAlert:@"Serializer error, Please try again"];
            });
        }else{
            NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"LocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
            
            self.vosArray = [[receiptList filteredArrayUsingPredicate:locationPredicate] mutableCopy];
            
            receiptList=nil;
            
            NSMutableArray *toDelete = [[NSMutableArray alloc]init];
            
            for(NSObject * vos in self.vosArray){
                if([docNoSet containsObject:[vos valueForKey:@"VosNumber"]]){
                    [toDelete addObject:vos];
                }
            }
            [self.vosArray removeObjectsInArray:toDelete];
        }
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        NSPredicate *transferPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"transferList"];
        NSObject *transferOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:transferPred] objectAtIndex:0];
        
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];
        
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[transferOffset valueForKey:@"O"] intValue]];
        
        databuffer = [file readDataOfLength: [[transferOffset valueForKey:@"L"] intValue]];
        [file closeFile];
        
        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        
        NSError * error;
        
        NSArray * transferList = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
        
        if(error){
            NSLog(@"Serializer error, data file not downloaded properly..");
                [self showErrorAlert:@"Serializer error, Please try again"];
        }else{
            NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"ToLocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
            
            self.transferArray = [[transferList filteredArrayUsingPredicate:locationPredicate] mutableCopy];
            
            transferList=nil;
            
            NSMutableArray *toDelete = [[NSMutableArray alloc]init];
            for(NSObject * transfer in self.transferArray){
                if([docNoSet containsObject:[transfer valueForKey:@"TransferNumber"]]){
                    [toDelete addObject:transfer];
                }
            }
            [self.transferArray removeObjectsInArray:toDelete];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self initiateSearchBar];
            self.isOnlineData=NO;
            self.scanProduct=TRUE;
            [self.tableView reloadData];
    });
}



#pragma mark - barcode processing function

-(void)processBarcode:(NSString *)barcode{

    self.filterTransferArray=nil;
    self.filterVosArray=nil;

    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"] && [[barcode substringToIndex:1]isEqualToString:@"T"]){
        //receiving transfer and transfer barcode scanned

        NSPredicate * findProductInTransferPred = [NSPredicate predicateWithFormat:@"TransferNumber == %@",barcode];
        self.filterTransferArray = [self.transferArray filteredArrayUsingPredicate:findProductInTransferPred];

        if(self.filterTransferArray.count==0){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showErrorAlert:@"No Transfer Found With This Barcode"];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self.scanButton setTitle:@"Cancel"];
            });
        }
    }else{
        NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcode:barcode];

        if([resultDic valueForKey:@"errorMsg"]!=NULL){
            NSLog(@"Result returned error..");
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
            });
        }else{
            NSArray * products = [resultDic valueForKey:@"productArray"];

            if(products.count==0){
                //no product found
                NSLog(@"product not found..");

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showErrorAlert:@"Product Not Found..."];
                });

            }else if(products.count>0){
                //multiple products found

                Products * product = [[Products alloc]init];
                product = [products objectAtIndex:0];

                NSMutableArray * newArray = [[NSMutableArray alloc]init];

                if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){

                    for(Vendors * vendor in product.Vendors){

                        NSCompoundPredicate * vendorAndProductPredicate = [[NSCompoundPredicate alloc]init];

                        if(!self.isOnlineData){
                            NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"SELF.VendorId = %@", vendor.VendorId];
                            NSPredicate * productIdPredicate = [NSPredicate predicateWithFormat:@"ANY %K.%K CONTAINS[c] %@", @"VosItems", @"ProductId",product.ProductId];
                            vendorAndProductPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[[NSArray alloc] initWithObjects:vendorPredicate, productIdPredicate, nil]];
                        }else{
                            NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"SELF.VendorId = %@", vendor.VendorId];
                            NSPredicate * productIdPredicate = [NSPredicate predicateWithFormat:@"ANY %K CONTAINS[c] %@", @"VosProducts",product.ProductId];
                            vendorAndProductPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[[NSArray alloc] initWithObjects:vendorPredicate, productIdPredicate, nil]];
                        }
                        NSArray * filteredArray = [self.vosArray filteredArrayUsingPredicate:vendorAndProductPredicate];
                        [newArray addObjectsFromArray:filteredArray];
                    }
                    self.filterVosArray = [[NSMutableArray alloc]init];
                    [self.filterVosArray addObjectsFromArray:newArray];

                    if(self.filterVosArray.count==0){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self showErrorAlert:@"No Order Sheet Found With This Product"];
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableView reloadData];
                            [self.scanButton setTitle:@"Cancel"];
                        });
                    }

                }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){

                    if(!self.isOnlineData){
                        NSPredicate * findProductInTransferPred = [NSPredicate predicateWithFormat:@"ANY %K.%K CONTAINS[c] %@",@"TransferItems", @"ProductId",product.ProductId];
                        self.filterTransferArray = [self.transferArray filteredArrayUsingPredicate:findProductInTransferPred];
                    }else{
                        NSPredicate * findProductInTransferPred = [NSPredicate predicateWithFormat:@"ANY %K CONTAINS[c] %@",@"TransferProducts",product.ProductId];
                        self.filterTransferArray = [self.transferArray filteredArrayUsingPredicate:findProductInTransferPred];
                    }

                    if(self.filterTransferArray.count==0){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self showErrorAlert:@"No Transfer Found With This Product"];
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableView reloadData];
                            [self.scanButton setTitle:@"Cancel"];
                        });
                    }
                }
            }
        }

    }


}

#pragma mark - search filter functions

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    searchBar.showsScopeBar = YES;
    [searchBar sizeToFit];
    if(selectedScope==1){
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"Note CONTAINS[cd] %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] stringByAppendingString:@"_Cafe"]];
        searchResults = [self.transferArray filteredArrayUsingPredicate:resultPredicate];
    }else{
        searchResults=nil;
    }
    [self.tableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"Note CONTAINS[cd] %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] stringByAppendingString:@"_Cafe"]];
        if([self.transferArray filteredArrayUsingPredicate:resultPredicate].count>0){
            searchBar.showsScopeBar = YES;
        }else{
            searchBar.showsScopeBar = NO;
            searchBar.scopeButtonTitles=nil;
        }
    }
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"Note CONTAINS[cd] %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] stringByAppendingString:@"_Cafe"]];
        if([self.transferArray filteredArrayUsingPredicate:resultPredicate].count>0){
            searchBar.showsScopeBar = YES;
        }else{
            searchBar.showsScopeBar = NO;
        }
    }
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}


-(void)searchForText:(NSString*)searchText{
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"VosNumber CONTAINS[cd] %@", searchText];
        searchResults = [self.vosArray filteredArrayUsingPredicate:resultPredicate];
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"TransferNumber CONTAINS[cd] %@", searchText];
        searchResults = [self.transferArray filteredArrayUsingPredicate:resultPredicate];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchResults=nil;
    [self.tableView reloadData];
}



#pragma mark - button funtions

- (IBAction)backButton:(id)sender {
    self.vosArray = nil;
    self.transferArray = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)scanProduct:(id)sender {
    
    if([self.scanButton.title isEqualToString:@"Cancel"]){
        self.filterTransferArray=nil;
        self.filterVosArray=nil;
        [self.scanButton setTitle:@"Scan"];
        [self.tableView reloadData];
        
    }else{
        if(self.scannerAvailable){
            [self.scanner barcodeStartScan:nil];
            self.foundBarcode = NO;
            [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
        }else{
            ZBarReaderViewController *reader = [ZBarReaderViewController new];
            reader.readerDelegate = self;
            [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 1];
            reader.readerView.zoom = 1.0;
            [self presentViewController:reader animated: YES completion:nil];
        }
    }
}

-(void)stopScanner
{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}


- (NSDate*) getDateFromJSON:(NSString *)dateString{
    // Expect date in this format "/Date(1268123281843)/"
    int startPos = (int)[dateString rangeOfString:@"("].location+1;
    int endPos = (int)[dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    //    NSLog(@"%llu",milliseconds);
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}


-(BOOL)checkVosTransferExistanceInDataFile:(NSString*)documentNo{
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){

        NSPredicate *vosPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"vosList"];
        NSObject *vosOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:vosPred] objectAtIndex:0];
        
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];
        
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[vosOffset valueForKey:@"O"] intValue]];
        
        databuffer = [file readDataOfLength: [[vosOffset valueForKey:@"L"] intValue]];
        [file closeFile];
        
        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        
        NSError * error;
        NSArray * vosList = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [statusAlert dismissViewControllerAnimated:YES completion:^{
                    NSLog(@"Serializer error, data file not downloaded properly..");
                    [self showErrorAlert:@"Serializer error, Please try again"];
                }];
            });
            return FALSE;
        }else{
            NSPredicate *transferNoPred = [NSPredicate predicateWithFormat:@"VosNumber contains %@", documentNo];
            NSArray *filteredVosArray = [vosList filteredArrayUsingPredicate:transferNoPred];

            if(filteredVosArray.count==0){
                //no receiving object found in the data file..
                return FALSE;
            }else{
                return TRUE;
            }
        }
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        
        NSPredicate *transferPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"transferList"];
        NSObject *transferOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:transferPred] objectAtIndex:0];
        
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];
        
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[transferOffset valueForKey:@"O"] intValue]];
        
        databuffer = [file readDataOfLength: [[transferOffset valueForKey:@"L"] intValue]];
        [file closeFile];
        
        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        
        NSError * error;
        
        NSArray * transferList = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
        
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Serializer error, data file not downloaded properly..");
                    [self showErrorAlert:@"Serializer error, Please try again.."];
            });
            return FALSE;
        }else{
            NSPredicate *transferNoPred = [NSPredicate predicateWithFormat:@"TransferNumber contains %@", documentNo];
            NSArray *transferNoResult = [transferList filteredArrayUsingPredicate:transferNoPred];

            if(transferNoResult.count==0){
                return FALSE;
            }else{
                return TRUE;
                
            }
        }
    }
    return FALSE;
}

#pragma mark - new data file get functions

-(void)testConnectionAndGetDataFile{
    if([self connectionTester]!=0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getDataFileVersion];
        });
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showErrorAlert:@"No Connection for Data Fetch..Please Try Again.."];
        });
    }
}


-(void)getDataFileVersion{
    [self startSpinner:@"Getting Data File.."];
    NSString *post =  @"{\"userName\":\"CNFMobileSystem\",\"token\":\"YJVLNEVFZA\", \"deviceId\":\"TestDevice\", \"appId\":\"CNFMobileV1\"}";
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getDataFileVersionV1", appDelegate.serverUrl]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                      NSLog(@"Download Error:%@",error.description);
                                                  }];
                                              });
                                          }else{
                                              id dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                              if(error){
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                          NSLog(@"web service error, can't serialize data..");
                                                          [self showErrorAlert:@"Data File version can not be determined.."];
                                                      }];
                                                  });
                                                  //changes in this file
                                              }else{
                                                  if ([dictionary valueForKey:@"Message"]!=NULL) {
                                                      NSLog(@"%@", [dictionary valueForKey:@"Message"]);
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                              [self showErrorAlert:[NSString stringWithFormat:@"Data File version query error %@", [dictionary valueForKey:@"Message"]]];
                                                          }];
                                                      });
                                                  }else{
                                                      NSLog(@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]);
                                                      if([[[NSUserDefaults standardUserDefaults] valueForKey:@"FILE_VERSION"]isEqualToString:[dictionary valueForKey:@"d"]]){
                                                          NSLog(@"Data file up to date");
                                                          
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                                  [self showErrorAlert:@"Data File up to date and does not contain necessary info, Please try again later"];
                                                              }];
                                                          });
                                                      }else{
                                                          self.interVersion = [dictionary valueForKey:@"d"];
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [self getNewData];
                                                          });
                                                      }
                                                  }
                                              }
                                          }
                                      }];
    [dataTask resume];
}


-(void)getNewData{
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentDir stringByAppendingPathComponent:@"db.json.gz"];
    
    NSString *post =   @"{\"userName\":\"CNFMobileSystem\",\"token\":\"YJVLNEVFZA\", \"applicationName\":\"cnfmobile\",\"dataFileVersion\":\"1\",\"deviceId\":\"TestDevice\"}";
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getDataV1", appDelegate.serverUrl]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              NSLog(@"Download Error:%@",error.description);
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                      [self showErrorAlert:@"Data File could not be downloaded.."];
                                                  }];
                                              });
                                          }
                                          if (data) {
                                              NSDictionary *strings = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                              if(error){
                                                  NSLog(@"web service error, can't serialize data..");
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                          [self showErrorAlert:[NSString stringWithFormat:@"Data File serialization error %@", error]];
                                                      }];
                                                  });

                                              }else{
                                                  if([strings valueForKey:@"Message"]==Nil){
                                                      NSData *nsdataFromBase64String = [[NSData alloc] initWithBase64EncodedString:[strings objectForKey:@"d"] options:0];
                                                      
                                                      [nsdataFromBase64String writeToFile:filePath atomically:YES];
                                                      NSLog(@"Downloaded data File is saved to %@",filePath);
                                                      [self unzipFile];
                                                  }else{
                                                      NSLog(@"error msg: %@", [strings valueForKey:@"Message"]);
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                             [self showErrorAlert:[NSString stringWithFormat:@"Data File download error %@", [strings valueForKey:@"Message"]]];
                                                          }];
                                                      });
                                                  }
                                              }
                                          }
                                      }];
    [dataTask resume];
}

#pragma mark - unzip function

-(void)unzipFile{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json.gz"];
    NSString *destinationPath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir,@"db.json"];
    
    NSData *gzData = [NSData dataWithContentsOfFile:filepath];
    NSData *ungzippedData = [gzData gunzippedData];
    
    [ungzippedData writeToFile:destinationPath atomically:YES];
    
    NSLog(@"done unzipping file..");
    
    if([self validateFileSizeEnd])
        [self readFileHeader];
    else{
        NSLog(@"File Corrupted, please restart app..");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusAlert dismissViewControllerAnimated:YES completion:^{
                [self showErrorAlert:[NSString stringWithFormat:@"Data File corrupted, please try re-downloading"]];
            }];
        });
    }
}

#pragma mark - read file header

-(void)readFileHeader{
    NSLog(@"start loading to memory..");
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    
    BRLineReader * line =[[BRLineReader alloc]initWithFile:filepath encoding:NSUTF8StringEncoding];
    NSString * firstLine= [line readLine];
    
    NSError * serializerError;
    
    NSData *data = [firstLine dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary * jsonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&serializerError];
    
    if(serializerError){
        NSLog(@"Could not serialize..");
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusAlert dismissViewControllerAnimated:YES completion:^{
               [self showErrorAlert:@"Data File serialization Error, Please try re-downloading"];
            }];
        });
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithLong:firstLine.length+6] forKey:@"START_OFFSET"];
            [[NSUserDefaults standardUserDefaults] setObject:[jsonDic objectForKey:@"DataList"] forKey:@"DATA_LIST"];
            [[NSUserDefaults standardUserDefaults] setValue:self.interVersion forKey:@"FILE_VERSION"];
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"DATA_FILE_DOWNLOAD_DATE"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"Initial Produres completed..");
           [self stopSpinner];
        });
    }
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"VOS_RECEIVE"]){
        if([self checkVosTransferExistanceInDataFile:[self.vos valueForKey:@"VosNumber"]])
            [self navigateToReceivingPageForVos:self.vos receiptNo:self.receiptNo];
        else{
            NSLog(@"Vos can't be found in the data file...");
            [self showErrorAlert:@"VOS can't be found in the data file, please try after some time"];
        }
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"TRANSFER_RECEIVE"]){
        if([self checkVosTransferExistanceInDataFile:self.transferNo])
            [self navigateToReceivingPageForTransfer:self.transferNo];
        else{
            NSLog(@"Transfer can't be found in the data file...");
            [self showErrorAlert:@"Transfer can't be found in the data file, please try after some time"];
        }
    }
}

-(BOOL)validateFileSizeEnd{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    [file seekToFileOffset:[file seekToEndOfFile]-15];
    NSData *databuffer = [file readDataOfLength:15];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    if([jsonStr rangeOfString:@"####"].location != NSNotFound){
        NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        long long fileSize= [[[jsonStr componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""] longLongValue];
        if(fileSize == [file seekToEndOfFile]){
            [file closeFile];
            return TRUE;
        }else{
            [file closeFile];
            return FALSE;
        }
    }else{
        [file closeFile];
        return FALSE;
    }
}



@end
