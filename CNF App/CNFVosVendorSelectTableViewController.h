//
//  CNFVosVendorSelectTableViewController.h
//  CNF App
//
//  Created by Anik on 2016-11-17.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"

@interface CNFVosVendorSelectTableViewController : UITableViewController<UISearchDisplayDelegate,UISearchBarDelegate, UISearchControllerDelegate, ZBarReaderDelegate , UIAlertViewDelegate, UISearchResultsUpdating, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
- (IBAction)scanProduct:(id)sender;

@end
