//
//  CNFVosVendorSelectTableViewController.m
//  CNF App
//
//  Created by Anik on 2016-11-17.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "CNFVosVendorSelectTableViewController.h"
#import "CNFAppEnums.h"
#import "BarcodeProcessor.h"
#import "Products.h"

@interface CNFVosVendorSelectTableViewController ()
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic, strong) NSMutableArray * vendorArray;
@property (nonatomic, strong) NSMutableArray * vosArray;
@property (nonatomic) BOOL scanProduct;
@property (strong, nonatomic) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray * filterVosArray;
@property (nonatomic, strong) NSMutableArray * filteredVendorArray;
@end

@implementation CNFVosVendorSelectTableViewController{
    NSArray *searchResults;
}

@synthesize vendorArray=_vendorArray, searchBar=_searchBar, scanner=_scanner,scannerAvailable=_scannerAvailable,  foundBarcode=_foundBarcode, scanProduct=_scanProduct, filterVosArray=_filterVosArray, filteredVendorArray=_filteredVendorArray;

-(NSMutableArray *)vosArray{
    if(!_vosArray)_vosArray=[[NSMutableArray alloc]init];
    return _vosArray;
}

-(NSMutableArray *)vendorArray{
    if(!_vendorArray)_vendorArray=[[NSMutableArray alloc]init];
    return _vendorArray;
}

-(NSMutableArray *)filterVosArray{
    if(!_filterVosArray)_filterVosArray=[[NSMutableArray alloc]init];
    return _filterVosArray;
}

-(NSMutableArray *)filteredVendorArray{
    if(!_filteredVendorArray)_filteredVendorArray=[[NSMutableArray alloc]init];
    return _filteredVendorArray;
}

- (void)viewDidLoad {
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [super viewDidLoad];
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    self.scanner=[DTDevices sharedDevice];
    [self.scanner addDelegate:self];
    [self.scanner connect];
    self.scanProduct=TRUE;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    [self.searchController.searchBar sizeToFit];
    
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailability) userInfo:nil repeats:NO];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
        
        //show current voslist for the location
        
        NSPredicate *receiptPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"VosList"];
        NSObject *receiptOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:receiptPred] objectAtIndex:0];
        
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];
        
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[receiptOffset valueForKey:@"O"] intValue]];
        
        databuffer = [file readDataOfLength: [[receiptOffset valueForKey:@"L"] intValue]];
        [file closeFile];
        
        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        NSError * error;
        
        NSArray * receiptList = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&error];
        
        if(error){
            NSLog(@"Serializer error, data file not downloaded properly..");
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
        }else{
            NSPredicate *locationPredicate = [NSPredicate predicateWithFormat:@"LocationId == %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]];
            self.vosArray = [[receiptList filteredArrayUsingPredicate:locationPredicate] mutableCopy];
        }
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"PRODUCE_RECEIVE"]|| [[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"INVOICE_LOOKUP"]){
        
        //show current vendor list
        
        NSPredicate *vendorPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"VendorList"];
        NSObject *vendorOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:vendorPred] objectAtIndex:0];
        
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
        NSFileHandle *file;
        NSData *databuffer;
        file = [NSFileHandle fileHandleForReadingAtPath:filepath];
        
        [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[vendorOffset valueForKey:@"O"] intValue]];
        
        databuffer = [file readDataOfLength: [[vendorOffset valueForKey:@"L"] intValue]];
        [file closeFile];
        
        NSError * error;
        
        NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        self.vendorArray = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]
                                                           options:0 error:&error];
        
        if(error){
            NSLog(@"Serializer error, data file not downloaded properly..");
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
        }
    }

    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
}


-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [super viewWillAppear:YES];
    [self.scanner addDelegate:self];
    [self.scanner connect];
    
    //check if there is a multiple UPC product selected.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
        if(self.filterVosArray.count>0){
            return self.filterVosArray.count;
        }else{
            if (searchResults.count>0) {
                return [searchResults count];
            } else {
                return self.vosArray.count;
            }
        }
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"PRODUCE_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"INVOICE_LOOKUP"]){
        if(self.filteredVendorArray.count>0){
            return self.filteredVendorArray.count;
        }else{
            if (searchResults.count>0) {
                return [searchResults count];
                
            } else {
                return self.vendorArray.count;
            }
        }
    }else{
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vosVendorSelectCell"];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"vendorCell"];
    }
    
    cell.textLabel.numberOfLines=0;
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
        
        if(self.filterVosArray.count>0){
            cell.textLabel.text = [[self.filterVosArray objectAtIndex:indexPath.row]valueForKey:@"VosNumber"];
        }else{
            if (searchResults.count>0) {
                cell.textLabel.text = [[searchResults objectAtIndex:indexPath.row]valueForKey:@"VosNumber"];
            } else {
                cell.textLabel.text = [[self.vosArray objectAtIndex:indexPath.row]valueForKey:@"VosNumber"];
            }
        }
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"PRODUCE_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"INVOICE_LOOPUP"]){
        if(self.filteredVendorArray.count>0){
            cell.textLabel.text = [[self.filteredVendorArray objectAtIndex:indexPath.row]valueForKey:@"VendorName"];
        }else{
            if (searchResults.count>0) {
                cell.textLabel.text = [[searchResults objectAtIndex:indexPath.row]valueForKey:@"VendorName"];
            } else {
                cell.textLabel.text = [[self.vendorArray objectAtIndex:indexPath.row] valueForKey:@"VendorName"];
            }
        }
    }
    
    cell.textLabel.numberOfLines=0;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){

        NSObject * vos = [[NSObject alloc]init];
        if(self.filterVosArray.count>0){
            vos = [self.filterVosArray objectAtIndex:indexPath.row];
        }else{
            if(searchResults.count>0){
                vos = [searchResults objectAtIndex:indexPath.row];
            }else{
                vos = [self.vosArray objectAtIndex:indexPath.row];
            }
        }

        NSMutableDictionary * vendor = [[NSMutableDictionary alloc]init];
        [vendor setValue:[vos valueForKey:@"VendorId"] forKey:@"VendorId"];
        [vendor setValue:[vos valueForKey:@"VendorName"] forKey:@"VendorName"];
        [[NSUserDefaults standardUserDefaults] setValue:vendor forKey:@"VENDOR"];
        [[NSUserDefaults standardUserDefaults] setValue:[vos valueForKey:@"VosNumber"] forKey:@"DOCUMENT_NO"];
        [[NSUserDefaults standardUserDefaults] setValue:[vos valueForKey:@"Id"] forKey:@"VOS_ID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"User changed the current Vendor for VOS receive to %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]);
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"PRODUCE_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"INVOICE_LOOKUP"]){
        
        if(self.filteredVendorArray.count>0){
            [[NSUserDefaults standardUserDefaults] setObject:[self.filteredVendorArray objectAtIndex:indexPath.row] forKey:@"VENDOR"];
            [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"VOS_ID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else{
            if (searchResults.count>0) {
                [[NSUserDefaults standardUserDefaults] setObject:[searchResults objectAtIndex:indexPath.row] forKey:@"VENDOR"];
                [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"VOS_ID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:[self.vendorArray objectAtIndex:indexPath.row] forKey:@"VENDOR"];
                [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"VOS_ID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        NSLog(@"User changed the current Vendor for receipt receive to %@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"VENDOR"] valueForKey:@"VendorId"]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - search filter functions

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

-(void)searchForText:(NSString*)searchText{
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
        
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"VosNumber CONTAINS[cd] %@", searchText];
        searchResults = [self.vosArray filteredArrayUsingPredicate:resultPredicate];
        
    }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"PRODUCE_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"INVOICE_LOOKUP"]){
        
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"VendorName CONTAINS[cd] %@", searchText];
        searchResults = [self.vendorArray filteredArrayUsingPredicate:resultPredicate];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchController.searchBar.text=@"";
    [self.searchController.searchBar resignFirstResponder];
    
    searchResults=NULL;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView reloadData];
}

#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailability{
    if([self.scanner connstate]==1){
        self.scannerAvailable=FALSE;
//        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
}

-(void)stopScanner{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}


#pragma mark - alertview fucntions

-(void)showErrorAlert:(NSString *)message{
    self.scanProduct=FALSE;
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    self.scanProduct=FALSE;
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.scanProduct=TRUE;
                               }];
    
    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

#pragma  mark - barcode scanner function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    if(self.scanProduct){
        self.foundBarcode =YES;
        int sound[]={2000,100};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
        [self processBarcode:barcode];
    }else{
        int sound[]={3500,200,0,200,3500,200};
        [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    }

}

#pragma mark - camera function

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
//    [reader dismissModalViewControllerAnimated: YES];
    [reader dismissViewControllerAnimated:YES completion:nil];
    
    NSString * barcode=symbol.data;
    
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    [self processBarcode:barcode];
}

#pragma mark - barcode processing function

-(void)processBarcode:(NSString *)barcode{
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc]init]processBarcode:barcode];
    
    if([resultDic valueForKey:@"errorMsg"]!=NULL){
        NSLog(@"Result returned error..");
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
    }else{
        NSArray * products = [resultDic valueForKey:@"productArray"];
        
        Products * product = [[Products alloc]init];
        if(products.count==0){
            //no product found
            NSLog(@"product not found..");
            [self showErrorAlert:@"Product Not Found..."];
        }else if(products.count>1||products.count==1){
            
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            
            //products found
            product = [products objectAtIndex:0];
            
            NSMutableArray * newArray = [[NSMutableArray alloc]init];
            
            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"VOS_RECEIVE"]){
                for(Vendors * vendor in product.Vendors){
                    
                    NSCompoundPredicate * vendorAndProductPredicate = [[NSCompoundPredicate alloc]init];

                    NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"SELF.VendorId = %@", vendor.VendorId];
                    NSPredicate * productIdPredicate = [NSPredicate predicateWithFormat:@"ANY %K CONTAINS[c] %@", @"VosProducts",product.ProductId];
                    vendorAndProductPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[[NSArray alloc] initWithObjects:vendorPredicate, productIdPredicate, nil]];

                    NSArray * filteredArray = [self.vosArray filteredArrayUsingPredicate:vendorAndProductPredicate];
                    [newArray addObjectsFromArray:filteredArray];
                }
                self.filterVosArray = [[NSMutableArray alloc]init];
                [self.filterVosArray addObjectsFromArray:newArray];
                
                if(self.filterVosArray.count==0){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self showErrorAlert:@"No Order Sheet Found With This Product"];
                    });
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"RECEIPT_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"]isEqualToString:@"PRODUCE_RECEIVE"]||[[[NSUserDefaults standardUserDefaults] valueForKey:@"APP_CODE"] isEqualToString:@"INVOICE_LOOKUP"]){
                
                for(Vendors * vendor in product.Vendors){
                    NSPredicate * vendorPredicate = [NSPredicate predicateWithFormat:@"SELF.VendorId = %@", vendor.VendorId];
                    NSArray * filteredArray = [self.vendorArray filteredArrayUsingPredicate:vendorPredicate];
                    [self.filteredVendorArray addObjectsFromArray:filteredArray];
                }
                
                if(self.filteredVendorArray.count==0){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self showErrorAlert:@"No Associated Vendor Found For This Product"];
                    });
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }
    }
}


#pragma mark - button funcitons

- (IBAction)scanProduct:(id)sender {
    if(self.scannerAvailable){
        [self.scanner barcodeStartScan:nil];
        self.foundBarcode = NO;
        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
    }else{
        ZBarReaderViewController *reader = [ZBarReaderViewController new];
        reader.readerDelegate = self;
        [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
        reader.readerView.zoom = 1.0;
        [self presentViewController:reader animated:YES completion:nil];
    }
}


@end
