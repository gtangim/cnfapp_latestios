//
//  DateTime.h
//  CNF App
//
//  Created by Anik on 2015-06-09.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface DateTime : JSONModel

@property (strong, nonatomic) NSString * DateTime;

@end
