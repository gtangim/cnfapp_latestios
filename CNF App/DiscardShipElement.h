//
//  DiscardShipElement.h
//  CNF App
//
//  Created by Anik on 2017-04-27.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
@protocol DiscardShipElement @end

@interface DiscardShipElement : JSONModel
@property (nonatomic, retain) NSString * LocationId;
@property (nonatomic, retain) NSString * CharityName;
@end
