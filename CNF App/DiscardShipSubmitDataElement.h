//
//  DiscardShipSubmitDataElement.h
//  CNF App
//
//  Created by Anik on 2017-04-27.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "DiscardShipElement.h"

@interface DiscardShipSubmitDataElement : JSONModel

@property (strong, nonatomic) NSArray<DiscardShipElement>* rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;

@end
