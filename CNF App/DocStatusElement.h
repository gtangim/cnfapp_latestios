//
//  DocStatusElement.h
//  CNF App
//
//  Created by Anik on 2015-05-15.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface DocStatusElement : JSONModel

@property (nonatomic,retain) NSString * userName;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic,retain) NSString * deviceId;
@property (nonatomic, retain) NSString * token;

@end
