//
//  GetDataElement.h
//  CNF App
//
//  Created by Anik on 2015-04-13.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface GetDataElement : JSONModel

@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * deviceId;
@property (nonatomic, retain) NSString * token;

@end
