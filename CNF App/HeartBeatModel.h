//
//  HeartBeat.h
//  CNF App Test
//
//  Created by Anik on 2017-08-01.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface HeartBeatModel : JSONModel

@property (nonatomic, retain) NSString * deviceLongId;
@property (nonatomic, retain) NSString * dataFileVersion;
@property (nonatomic, retain) NSNumber * numDocsInQueue;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSString * loggedInUser;
@property (nonatomic, retain) NSString * appName;
@end
