//
//  ImageCompression.h
//  CNF App
//
//  Created by Anik on 2016-07-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageCompression : NSObject

-(UIImage*)compressImage:(UIImage*)image;

@end
