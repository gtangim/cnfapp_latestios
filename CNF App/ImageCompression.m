//
//  ImageCompression.m
//  CNF App
//
//  Created by Anik on 2016-07-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "ImageCompression.h"

@implementation ImageCompression

-(UIImage*)compressImage:(UIImage*)image{
    UIImage * newImage= [self convertImageToGrayScale:[self shrinkImage:image]];
    return newImage;
}

- (UIImage *)convertImageToGrayScale:(UIImage *)image {
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef contextGre = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(contextGre, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(contextGre);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(contextGre);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage;
}


- (UIImage*)shrinkImage:(UIImage*)image
{
    CGSize cgsize = CGSizeMake(960, 1280);
    UIGraphicsBeginImageContext( cgsize );
    [image drawInRect:CGRectMake(0,0,cgsize.width,cgsize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
