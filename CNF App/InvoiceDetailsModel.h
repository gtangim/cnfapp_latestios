//
//  InvoiceDetailsModel.h
//  CNF App
//
//  Created by Anik on 2017-09-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface InvoiceDetailsModel : JSONModel

@property (nonatomic, retain) NSString * locationId;
@property (nonatomic, retain) NSString * invoiceNumber;
@property (nonatomic, retain) NSString * vendorId;

@end
