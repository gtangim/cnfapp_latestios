//
//  Locations.h
//  CNF App
//
//  Created by Anik on 2015-03-03.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface Locations : JSONModel

@property (nonatomic, retain) NSString * LocationId;
@property (nonatomic, retain) NSString * Description;
@property (nonatomic, retain) NSString * LocationType;

@end
