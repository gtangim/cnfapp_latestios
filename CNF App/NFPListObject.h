//
//  NFPListObject.h
//  CNF App
//
//  Created by Anik on 2017-04-27.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface NFPListObject : JSONModel

@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * token;
@property (nonatomic, retain) NSString * deviceId;
@property (nonatomic, retain) NSString * holdingLocationId;


@end
