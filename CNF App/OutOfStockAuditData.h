//
//  OutOfStockAuditData.h
//  CNF App
//
//  Created by Anik on 2015-04-14.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "ReportElement.h"

@protocol OutOfStockAuditData @end

@interface OutOfStockAuditData : JSONModel

@property (strong, nonatomic) NSString * UserId;
@property (strong, nonatomic) NSString * LocationId;
@property (strong, nonatomic) NSDate * StartTime;
@property (strong, nonatomic) NSDate * EndTime;

@property (strong, nonatomic) NSArray<ReportElement>* AuditList;

@end
