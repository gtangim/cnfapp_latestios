//
//  PickDepartmentSelectTableViewController.h
//  CNF App Test
//
//  Created by Anik on 2017-05-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickDepartmentSelectTableViewController : UITableViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *departmentPicker;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;

- (IBAction)selectDepartmentAndProcede:(id)sender;

@end
