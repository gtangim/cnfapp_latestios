//
//  PickDepartmentSelectTableViewController.m
//  CNF App Test
//
//  Created by Anik on 2017-05-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "PickDepartmentSelectTableViewController.h"

@interface PickDepartmentSelectTableViewController ()
@property (nonatomic, strong) NSMutableArray * departmentArray;
@end

@implementation PickDepartmentSelectTableViewController

@synthesize nextButton=_nextButton, departmentPicker=_departmentPicker, departmentArray=_departmentArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    
    self.departmentPicker.dataSource=self;
    self.departmentPicker.delegate=self;
    
    [self.nextButton setEnabled:NO];
    
    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"DepartmentList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    NSError * serializerError;
    
    NSArray * locations = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];
    
    if(serializerError){
        NSLog(@"Serializer error, data file not downloaded properly..");
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }else{
        self.departmentArray =[locations mutableCopy];

//        int deptId;
//        if([[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"] isKindOfClass:[NSString class]]){
//            deptId = [[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"] intValue];
//        }else{
//            deptId = (int)[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"];
//        }

//        if(![[[[NSUserDefaults standardUserDefaults]valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"]){
            [self.departmentPicker selectRow:0 inComponent:0 animated:YES];
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
//        }else{
//            [self.departmentPicker selectRow:[[self.departmentArray valueForKey:@"DeptId"] indexOfObject:[[[[NSUserDefaults standardUserDefaults]valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"]] inComponent:0 animated:YES];
//            [self.navigationItem.rightBarButtonItem setEnabled:YES];
//        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - pickerview functions
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component{
    return[self.departmentArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [[[[[self.departmentArray objectAtIndex:row] valueForKey:@"DeptId"] stringValue] stringByAppendingString:@"-"] stringByAppendingString:[[self.departmentArray objectAtIndex:row] valueForKey:@"Name"]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
    
    if(pickerView==self.departmentPicker){
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
}

#pragma mark - alert notification functions

-(void)showErrorAlert:(NSString *)message{

    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}




- (IBAction)selectDepartmentAndProcede:(id)sender {
    
    NSMutableDictionary * pickingObject = [[NSMutableDictionary alloc]init];

    [pickingObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_For_Location"] forKey:@"Pick_For_Location"];
    [pickingObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_Location"] forKey:@"Pick_Location"];
    [pickingObject setValue:[self.departmentArray objectAtIndex:[self.departmentPicker selectedRowInComponent:0]] forKey:@"Department"];
    
    [[NSUserDefaults standardUserDefaults] setObject:pickingObject forKey:@"PICKING"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSegueWithIdentifier:@"pickingProductSegue" sender:self];
    
}
@end
