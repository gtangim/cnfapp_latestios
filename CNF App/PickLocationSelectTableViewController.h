//
//  LocationDepartmentSelectTableViewController.h
//  CNF App Test
//
//  Created by Anik on 2017-05-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickLocationSelectTableViewController : UITableViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *locationPicker;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;

- (IBAction)selectLocationAndNext:(id)sender;

@end
