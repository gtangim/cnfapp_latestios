//
//  LocationDepartmentSelectTableViewController.m
//  CNF App Test
//
//  Created by Anik on 2017-05-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "PickLocationSelectTableViewController.h"

@interface PickLocationSelectTableViewController ()
@property (nonatomic, strong) NSMutableArray * locationArray;
@end

@implementation PickLocationSelectTableViewController{
}

@synthesize nextButton=_nextButton, locationPicker=_locationPicker, locationArray=_locationArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.nextButton setEnabled:FALSE];
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    
    self.locationPicker.delegate=self;
    self.locationPicker.dataSource=self;

    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"LocationList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    NSError * serializerError;
    
    NSArray * locations = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];
    
    if(serializerError){
        NSLog(@"Serializer error, data file not downloaded properly..");
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }else{
        self.locationArray =[locations mutableCopy];
        
        if(![[[NSUserDefaults standardUserDefaults]valueForKey:@"PICKING_OBJECT"] valueForKey:@"Pick_For_Location"]){
            [self.locationPicker selectRow:0 inComponent:0 animated:YES];
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
        }else{
            [self.locationPicker selectRow:[[self.locationArray valueForKey:@"LocationId"] indexOfObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"PICKER_OBJECT"] valueForKey:@"Pick_For_Location"]] inComponent:0 animated:YES];
            [self.navigationItem.rightBarButtonItem setEnabled:YES];
        }
    }
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"PICKING_OBJECT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - pickerview functions
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component{
    return[self.locationArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [[[[self.locationArray objectAtIndex:row] valueForKey:@"LocationId"] stringByAppendingString:@"-"] stringByAppendingString:[[self.locationArray objectAtIndex:row] valueForKey:@"Description"]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
    
    if(pickerView==self.locationPicker){
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
}

#pragma mark - alert notification functions

-(void)showErrorAlert:(NSString *)message{

    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


- (IBAction)selectLocationAndNext:(id)sender {
    
    if([[[self.locationArray objectAtIndex:[self.locationPicker selectedRowInComponent:0]] valueForKey:@"LocationId"]isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]]){
        [self showErrorAlert:@"Cannot pick for the same location, Please choose a different location"];
    }else{
        NSMutableDictionary * pickingObject = [[NSMutableDictionary alloc]init];
        [pickingObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"] forKey:@"Pick_Location"];
        [pickingObject setValue:[[self.locationArray objectAtIndex:[self.locationPicker selectedRowInComponent:0]] valueForKey:@"LocationId"] forKey:@"Pick_For_Location"];
        [pickingObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] forKey:@"Department"];
        
        [[NSUserDefaults standardUserDefaults] setObject:pickingObject forKey:@"PICKING"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSegueWithIdentifier:@"pickingDepartmentSelectSegue" sender:self];
    }
}
@end
