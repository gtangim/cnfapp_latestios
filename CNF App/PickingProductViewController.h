//
//  PickingProductViewController.h
//  CNF App
//
//  Created by Anik on 2018-04-27.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"

//this file is now changed to make a commit


@interface PickingProductViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UITableView *productTableView;
@property (weak, nonatomic) IBOutlet UILabel *pickUserLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickLocationLabel;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *productIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *productUpcLabel;
@property (weak, nonatomic) IBOutlet UILabel *wareHouseQohLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalDemandLabel;
@property (weak, nonatomic) IBOutlet UILabel *eachOrCaseLabel;

@property (weak, nonatomic) IBOutlet UITextField *quantityText;

- (IBAction)pickProduct:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *pickProductButton;

- (IBAction)submitPick:(id)sender;

@end
