//
//  PickingProductViewController.m
//  CNF App
//
//  Created by Anik on 2018-04-27.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import "PickingProductViewController.h"
#import "CNFAppEnums.h"
#import "CNFGetDocumentId.h"
#import "WarehousePicking.h"
#import "QuantityDetails.h"
#import "WarehousePickingItem.h"
#import "CNFPrintQRCodeViewController.h"


@interface PickingProductViewController ()
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) NSString * documentId;
@property (nonatomic, strong) NSDate * startTime;
@property (nonatomic, strong) NSDate * endTime;
@property (nonatomic, strong) NSMutableArray * pickProducts;
@property (nonatomic, strong) NSString * selectedProductId;
@end

@implementation PickingProductViewController{
    NSManagedObjectContext * context;
    UIAlertController * statusAlert;
}

@synthesize pickProducts=_pickProducts, context=_context, selectedProductId=_selectedProductId,  startTime=_startTime, endTime=_endTime, documentId=_documentId, quantityText=_quantityText;


-(NSMutableArray *)pickProducts{
    if(!_pickProducts)_pickProducts=[[NSMutableArray alloc]init];
    return _pickProducts;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [self.navigationItem setTitle:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"Name"]];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Quit" style:UIBarButtonItemStylePlain target:self action:@selector(showReturnToDepartmentAlert)];

    self.pickUserLabel.text = [NSString stringWithFormat:@"%@ PICKING FOR", [[[NSUserDefaults standardUserDefaults]valueForKey:@"USER_NAME"]uppercaseString]];
    self.pickLocationLabel.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_For_Location"];

    // Do any additional setup after loading the view.
    self.productTableView.delegate = self;
    self.productTableView.dataSource = self;
    self.quantityText.delegate = self;

    [self getDataToPopulateTable];

}


-(void)getDataToPopulateTable{
    [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:YES];

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];

    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"PickingOrderList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];

    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];

    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];

    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    [file closeFile];

    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];

    NSError * serializerError;

    NSArray * pickingOrders = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];

    if(serializerError){
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
        NSLog(@"Serializer error, data file not downloaded properly..");
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }else{
        NSArray * pickingListArray =[pickingOrders mutableCopy];

        NSString *deptId;

        if([[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"] isKindOfClass:[NSString class]]){
            deptId = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"];
        }else{
            deptId = [[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"]stringValue];
        }

        NSPredicate * departmentPredicate = [NSPredicate predicateWithFormat:@"FromLocationId == %@ AND ToLocationId == %@ AND DepartmentId==%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_Location"], [[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_For_Location"], deptId];

        self.pickProducts=[[pickingListArray filteredArrayUsingPredicate:departmentPredicate] mutableCopy];
    }

    if(self.pickProducts.count==0){
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
        [self showNoProductAlert];
    }else{
        NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"WarehouseSeqNumber" ascending:YES];
        NSArray *sortDescriptors = @[seqSort];

        self.pickProducts = [[NSMutableArray alloc]initWithArray:[self.pickProducts sortedArrayUsingDescriptors:sortDescriptors]];


        if([[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"]){
            if(![self initializeSavedPick]){
                [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
                [self showErrorInitialingAlert];
            }else{
                [self.productTableView reloadData];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.productTableView selectRowAtIndexPath:indexPath
                                                   animated:YES
                                             scrollPosition:UITableViewScrollPositionNone];
                [self tableView:self.productTableView didSelectRowAtIndexPath:indexPath];
            }

        }else{
            if(![self initializePicking]){
                [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
                [self showErrorInitialingAlert];
            }else{

                [self.productTableView reloadData];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.productTableView selectRowAtIndexPath:indexPath
                                                   animated:YES
                                             scrollPosition:UITableViewScrollPositionNone];
                [self tableView:self.productTableView didSelectRowAtIndexPath:indexPath];
            }
        }
        [UIApplication.sharedApplication setNetworkActivityIndicatorVisible:NO];
    }
}

-(BOOL)initializePicking{

    self.startTime = [NSDate date];
    self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];

    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:@"PICKING" forKey:@"documentType"];
    [newManagedObject setValue:@"Created" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.startTime forKey:@"date"];
    [context save:&error];


    //save to temp tables

    NSManagedObject *saveManagedObject = [NSEntityDescription
                                          insertNewObjectForEntityForName:@"TempDocStatus"
                                          inManagedObjectContext:context];

    [saveManagedObject setValue:@"PICKING" forKey:@"documentType"];
    [saveManagedObject setValue:[[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"]stringValue] forKey:@"documentNo"];
    [saveManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_Location"] forKey:@"holdingLocation"];
    [saveManagedObject setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_For_Location"] forKey:@"toLocation"];
    [saveManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [saveManagedObject setValue:self.documentId forKey:@"documentId"];
    [saveManagedObject setValue:[NSDate date] forKey:@"createdDate"];
    [saveManagedObject setValue:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"Name"] forKey:@"vosCmNote"];
    [context save:&error];


    if(error)
        return FALSE;
    else
        return TRUE;
}

-(BOOL)initializeSavedPick{

    self.startTime = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"CREATED_DATE"];
    self.documentId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SAVED_DOC"] valueForKey:@"DOCUMENT_ID"];

    NSError *error;

    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];

    NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
    NSPredicate * prodPredicate = [NSPredicate predicateWithFormat:@"documentId==%@",self.documentId];
    [productRequest setEntity:[NSEntityDescription entityForName:@"PickTempProductList" inManagedObjectContext:context]];
    [productRequest setPredicate:prodPredicate];

    NSArray *productResults= [context executeFetchRequest:productRequest error:&error];

    if(productResults.count==0){
        NSLog(@"no previously scanned products..start from new..");
    }else{
        for(NSManagedObject * product in productResults){
            NSPredicate * prodPred = [NSPredicate predicateWithFormat:@"ProductId == %@", [product valueForKey:@"productId"]];
            NSArray * foundProductAry = [self.pickProducts filteredArrayUsingPredicate:prodPred];

            if(foundProductAry.count>0){
                NSMutableDictionary * foundProduct = [foundProductAry.firstObject mutableCopy];
                int index = (int)[self.pickProducts indexOfObject:foundProduct];
                [self.pickProducts removeObject:foundProduct];

                if([[foundProduct valueForKey:@"ShipCaseQty"]boolValue]){
                    [foundProduct setValue:[NSNumber numberWithBool:YES] forKey:@"Picked"];
                    [foundProduct setValue:[NSNumber numberWithDouble:[[product valueForKey:@"pickedNumCases"]doubleValue]] forKey:@"PickedNumCases"];
                    [foundProduct setValue:[product valueForKey:@"pickedQuantity"] forKey:@"QuantityPicked"];
                }else{
                    [foundProduct setValue:[NSNumber numberWithBool:YES] forKey:@"Picked"];
                    [foundProduct setValue:[product valueForKey:@"pickedQuantity"] forKey:@"QuantityPicked"];
                    [foundProduct setValue:NULL forKey:@"PickedNumCases"];
                }

                [self.pickProducts insertObject:foundProduct atIndex:index];
            }else continue;
        }
    }

    if(error)
        return FALSE;
    else
        return TRUE;
}




-(void)reloadDeatailsView{
    if(self.selectedProductId.length>0){
        //populate the fields..

        NSDictionary * selectedProduct = [self.pickProducts objectAtIndex:[self.productTableView indexPathForSelectedRow].row];

        //poulate common fields

        self.productNameLabel.text = [selectedProduct valueForKey:@"ProductName"];

        self.productSizeLabel.text = [NSString stringWithFormat:@"%@ %@ - %@", [selectedProduct valueForKey:@"Size"]?[selectedProduct valueForKey:@"Size"]:@"", [[selectedProduct valueForKey:@"UomId"] componentsSeparatedByString:@"_"][1], [selectedProduct valueForKey:@"Brand"]?[selectedProduct valueForKey:@"Brand"]:@""];

        self.productUpcLabel.text = [NSString stringWithFormat:@"SKU: [%@]",  [selectedProduct valueForKey:@"Upc"]];
        self.productIdLabel.text = [NSString stringWithFormat:@"ID: %@ ", [selectedProduct valueForKey:@"ProductId"]];

        [self.productUpcLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [self.productSizeLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [self.productIdLabel setFont:[UIFont boldSystemFontOfSize:16]];

        double requestedQuantity = [[selectedProduct valueForKey:@"QuantityRequested"] doubleValue];
        double caseQuantity = [[selectedProduct valueForKey:@"CaseQty"] doubleValue];
        double pickedQuantity = [selectedProduct valueForKey:@"QuantityPicked"]? [[selectedProduct valueForKey:@"QuantityPicked"]doubleValue] : 0.0;
        double warehouseQoh = [[selectedProduct valueForKey:@"WarehosueQoh"] doubleValue];
        double totalDemand = [[selectedProduct valueForKey:@"TotalDemand"] doubleValue];

        //set warehouse quantity text red if less than total demand
        if(warehouseQoh<totalDemand) self.wareHouseQohLabel.textColor = [UIColor redColor];
        else self.wareHouseQohLabel.textColor = [UIColor blackColor];

        if([[selectedProduct valueForKey:@"UomId"]isEqualToString:@"OTH_ea"]){
            //each quantity, round values up..
            self.totalDemandLabel.text = [NSString stringWithFormat:@"Total demand: %0f", round(totalDemand)];
            self.wareHouseQohLabel.text = [NSString stringWithFormat:@"Warehouse QOH: %0f",round(warehouseQoh)];
        }else{
            self.totalDemandLabel.text = [NSString stringWithFormat:@"Total demand: %2f", totalDemand];
            self.wareHouseQohLabel.text = [NSString stringWithFormat:@"Total warehouse QOH: %2f", warehouseQoh];
        }

        //Populate Variable fields

        if([[selectedProduct valueForKey:@"UomId"]isEqualToString:@"OTH_ea"]){
            //each quantity..
            if([[selectedProduct valueForKey:@"ShipCaseQty"]boolValue]){
                self.eachOrCaseLabel.text = [NSString stringWithFormat:@" X %.0f = %.0f", round(caseQuantity), [selectedProduct valueForKey:@"QuantityPicked"]? round(pickedQuantity):round(requestedQuantity)];
                self.quantityText.text = [selectedProduct valueForKey:@"QuantityPicked"]? [NSString stringWithFormat:@"%.0f", round(pickedQuantity/caseQuantity)]: [NSString stringWithFormat:@"%.0f", round(requestedQuantity/caseQuantity)];
            }else{
                self.eachOrCaseLabel.text = @"";
                self.quantityText.text = [selectedProduct valueForKey:@"QuantityPicked"]? [NSString stringWithFormat:@"%.0f", round(pickedQuantity)] : [NSString stringWithFormat:@"%.0f", round(requestedQuantity)];
            }
        }else{
            //not each quantity, dont round..
            if([[selectedProduct valueForKey:@"ShipCaseQty"]boolValue]){
                self.eachOrCaseLabel.text = [NSString stringWithFormat:@" X %.2f = %.2f", caseQuantity, [selectedProduct valueForKey:@"QuantityPicked"]?pickedQuantity:requestedQuantity];
                self.quantityText.text = [selectedProduct valueForKey:@"QuantityPicked"]? [NSString stringWithFormat:@"%.2f", pickedQuantity/caseQuantity]: [NSString stringWithFormat:@"%.2f", requestedQuantity/caseQuantity];
            }else{
                self.eachOrCaseLabel.text = @"";
                self.quantityText.text = [selectedProduct valueForKey:@"QuantityPicked"]? [NSString stringWithFormat:@"%.2f", pickedQuantity] : [NSString stringWithFormat:@"%.2f", requestedQuantity];
            }
        }

        [self.pickProductButton setUserInteractionEnabled:TRUE];

    }else{
        //No product selected, show empty
        self.productNameLabel.text = self.productSizeLabel.text = self.productUpcLabel.text = self.wareHouseQohLabel.text = self.totalDemandLabel.text = self.quantityText.text = @"";

        [self.pickProductButton setUserInteractionEnabled:FALSE];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Tableview functions


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pickProducts.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pickProductCells" forIndexPath:indexPath];

    if([[self.pickProducts objectAtIndex:indexPath.row]valueForKey:@"Size"]){
        cell.textLabel.text =[[[[self.pickProducts objectAtIndex:indexPath.row]valueForKey:@"ProductName"] stringByAppendingString:@"-"] stringByAppendingString:[[[[self.pickProducts objectAtIndex:indexPath.row]valueForKey:@"Size"] stringValue] stringByAppendingString:[[[self.pickProducts objectAtIndex:indexPath.row]valueForKey:@"UomId"] componentsSeparatedByString:@"_"][1]]];
    }else{
        cell.textLabel.text =[[self.pickProducts objectAtIndex:indexPath.row]valueForKey:@"ProductName"];
    }

    if([[self.pickProducts objectAtIndex:indexPath.row] valueForKey:@"Picked"]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.textLabel.textColor = [UIColor blueColor];
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.textColor = [UIColor orangeColor];
    }

    cell.textLabel.numberOfLines=0;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    self.selectedProductId = [[self.pickProducts objectAtIndex:indexPath.row] valueForKey:@"ProductId"];
    [self reloadDeatailsView];
}


#pragma mark - Alert view functions

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];

        UIViewController *customVC     = [[UIViewController alloc] init];

        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];


        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];


        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];

        [statusAlert setValue:customVC forKey:@"contentViewController"];

        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:nil];
        statusAlert = nil;
    }
}




-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showNoProductAlert{

    UIAlertController *noProductAlert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:[NSString stringWithFormat:@"No product to pick from Department: %@ for store: %@",[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"Name"],[[[NSUserDefaults standardUserDefaults]valueForKey:@"PICKING"] valueForKey:@"Pick_For_Location"]]
                                         preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
    [noProductAlert addAction:okAction];
    [self presentViewController:noProductAlert animated:YES completion:nil];

}

-(void)showQuantityTooHighAlert{
    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId == %@", self.selectedProductId];
    NSArray *selectedProductResult = [self.pickProducts filteredArrayUsingPredicate:selectedProdPred];

    UIAlertController *noProductAlert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:[NSString stringWithFormat:@"Quantity you entered for %@ is higher than 10000 or more than 1000 cases, please select an appropriate value",[[selectedProductResult objectAtIndex:0] valueForKey:@"ProductName"]]
                                         preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    [noProductAlert addAction:okAction];
    [self presentViewController:noProductAlert animated:YES completion:nil];
}


-(void)showReturnToDepartmentAlert{
    UIAlertController *returnToDepartAlert = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *saveExitAction = [UIAlertAction
                                actionWithTitle:@"Save and Exit"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                    [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"APP_CODE"];
                                    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"SAVED_DOC"];
                                    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"PICKING"];
                                    [[NSUserDefaults standardUserDefaults]synchronize];

                                    self.pickProducts=Nil;
                                    self.documentId=nil;
                                    self.selectedProductId=nil;
                                    [self.navigationController popToRootViewControllerAnimated:YES];
                                }];
    UIAlertAction *discardExitAction = [UIAlertAction
                               actionWithTitle:@"Discard and Exit"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {

                                   //delete the data and go back

                                   //delete from doc status info
                                   NSEntityDescription *entityDesc= [NSEntityDescription entityForName:@"DocStatusInfo" inManagedObjectContext:context];
                                   NSFetchRequest *request = [[NSFetchRequest alloc]init];

                                   NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"documentId == %@  AND status = %@",self.documentId, @"Created"];

                                   [request setPredicate:predicate];
                                   [request setEntity:entityDesc];

                                   NSError *error;
                                   NSArray *results= [context executeFetchRequest:request error:&error];

                                   for (NSManagedObject * obj in results){
                                       [context deleteObject:obj];
                                   }
                                   [context save:&error];

                                   //delete from tempDocList table

                                   NSFetchRequest *allDocs = [[NSFetchRequest alloc] init];
                                   [allDocs setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
                                   NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
                                   [allDocs setPredicate:docPredicate];
                                   NSArray *docs = [context executeFetchRequest:allDocs error:&error];
                                   for (NSManagedObject *doc in docs) {
                                       [context deleteObject:doc];  }
                                   NSError *saveError = nil;
                                   [context save:&saveError];

                                   //delete from TempProdList table

                                   NSFetchRequest *allProds = [[NSFetchRequest alloc] init];
                                   [allProds setEntity:[NSEntityDescription entityForName:@"PickTempProductList" inManagedObjectContext:context]];
                                   NSPredicate *prodPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
                                   [allProds setPredicate:prodPredicate];
                                   NSArray *prods = [context executeFetchRequest:allProds error:&error];
                                   for (NSManagedObject *prod in prods) {
                                       [context deleteObject:prod];  }
                                   [context save:&saveError];

                                   [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"APP_CODE"];
                                   [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"SAVED_DOC"];
                                   [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"PICKING"];
                                   [[NSUserDefaults standardUserDefaults]synchronize];

                                   self.pickProducts=Nil;
                                   self.documentId=nil;
                                   self.selectedProductId=nil;
                                   [self.navigationController popToRootViewControllerAnimated:YES];

                               }];

    UIAlertAction *cancelAction = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                                         [self dismissViewControllerAnimated:YES completion:nil];
                                     }];

    [returnToDepartAlert addAction:saveExitAction];
    [returnToDepartAlert addAction:discardExitAction];
    [returnToDepartAlert addAction:cancelAction];
    [self presentViewController:returnToDepartAlert animated:YES completion:nil];
}


-(void)showErrorInitialingAlert{
    UIAlertController *returnToDepartAlert = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:@"No product picked, Please pick products to submit"
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
    [returnToDepartAlert addAction:okAction];
    [self presentViewController:returnToDepartAlert animated:YES completion:nil];
}

-(void)showNoPickedProductAlert{
    UIAlertController *finishAlertController = [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:@"No product picked, Please pick products to submit"
                                                preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    [finishAlertController addAction:okAction];
    [self presentViewController:finishAlertController animated:YES completion:nil];

}

-(void)showQuantityHigherThanDemandAlert{
    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId == %@", self.selectedProductId];
    NSArray *selectedProductResult = [[self.pickProducts filteredArrayUsingPredicate:selectedProdPred] mutableCopy];

    UIAlertController *highQuantityAlert = [UIAlertController
                                            alertControllerWithTitle:nil
                                            message:[NSString stringWithFormat:@"Quantity you entered for %@ is higher than original Demand, Are you sure to pick it?",[[selectedProductResult objectAtIndex:0] valueForKey:@"ProductName"]]
                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesAction = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                    [self modifyProductPickQuantity];
                                }];
    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [highQuantityAlert addAction:yesAction];
    [highQuantityAlert addAction:noAction];
    [self presentViewController:highQuantityAlert animated:YES completion:nil];

}

-(void)showModelValidationAlert{

    UIAlertController *finishAlertController = [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:@"Model Validation Failed, Please try again"
                                                preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    [finishAlertController addAction:okAction];
    [self presentViewController:finishAlertController animated:YES completion:nil];
}


-(void)showFinishPickAlert{

    UIAlertController *finishAlertController = [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:@"Are you sure you want to finish picking and submit?"
                                                preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesAction = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                    NSPredicate * pickPredicate = [NSPredicate predicateWithFormat:@"Picked == YES"];
                                    NSArray * pickedProducts = [self.pickProducts filteredArrayUsingPredicate:pickPredicate];

                                    if(pickedProducts.count==0)
                                        [self showNoPickedProductAlert];
                                    else{
                                        [self startSpinner:@"Saving Pick..."];
                                        [self savePickToFile];

                                        dispatch_async(dispatch_get_main_queue(), ^{
//                                            [[NSUserDefaults standardUserDefaults] setValue:NULL forKey:@"APP_CODE"];
                                            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"SAVED_DOC"];
//                                            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"PICKING"];
                                            [[NSUserDefaults standardUserDefaults]synchronize];

                                            self.pickProducts=Nil;
//                                            self.documentId=nil;
                                            self.selectedProductId=nil;
                                            [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                [self performSegueWithIdentifier:@"printBarcodeFromPickingSegue" sender:self];
                                            }];
                                        });
                                    }
                                }];
    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [finishAlertController addAction:yesAction];
    [finishAlertController addAction:noAction];
    [self presentViewController:finishAlertController animated:YES completion:nil];

}


#pragma mark - button functions

- (IBAction)pickProduct:(id)sender {
    if([self.quantityText hasText]){
        NSMutableDictionary * selectedProduct = [[self.pickProducts objectAtIndex:[self.productTableView indexPathForSelectedRow].row] mutableCopy];

        if(![[selectedProduct valueForKey:@"ShipCaseQty"] boolValue]&&[[self.quantityText text] doubleValue]>=10000){
            [self showQuantityTooHighAlert];
            return;
        }
        else if([[selectedProduct valueForKey:@"ShipCaseQty"] boolValue]&&[[self.quantityText text] doubleValue]>=1000){
            [self showQuantityTooHighAlert];
            return;
        }
        else{
            if([[selectedProduct valueForKey:@"ShipCaseQty"] boolValue]){

                double numOfRequiredCases = [[selectedProduct valueForKey:@"QuantityRequested"] doubleValue]/[[selectedProduct valueForKey:@"CaseQty"] doubleValue];

                if([[self.quantityText text] doubleValue]>numOfRequiredCases){
                    [self showQuantityHigherThanDemandAlert];
                    return;
                }

            }else{
                if([[self.quantityText text] doubleValue]>[[selectedProduct valueForKey:@"QuantityRequested"] doubleValue]){
                    [self showQuantityHigherThanDemandAlert];
                    return;
                }
            }
        }
        [self modifyProductPickQuantity];
    }else{
        [self showErrorAlert:@"Please enter pick quantity"];
    }
}


-(void)modifyProductPickQuantity{

    NSPredicate *selectedProdPred = [NSPredicate predicateWithFormat:@"ProductId == %@", self.selectedProductId];
    NSArray *selectedProductResult = [[self.pickProducts filteredArrayUsingPredicate:selectedProdPred] mutableCopy];

    int index = (int)[self.pickProducts indexOfObject:[selectedProductResult objectAtIndex:0]];
    [self.pickProducts removeObject:[selectedProductResult objectAtIndex:0]];

    NSDictionary * productObject = [[selectedProductResult objectAtIndex:0] mutableCopy];

    if([[productObject valueForKey:@"ShipCaseQty"]boolValue]){
        [productObject setValue:[NSNumber numberWithBool:YES] forKey:@"Picked"];
        [productObject setValue:[NSNumber numberWithDouble:[[self.quantityText text]doubleValue]] forKey:@"PickedNumCases"];
        NSNumber * totaleQuantity = [self multiplyNumbers:[NSNumber numberWithDouble:[[self.quantityText text]doubleValue]] second:[NSNumber numberWithDouble:[[productObject valueForKey:@"CaseQty"] doubleValue]]];
        [productObject setValue:totaleQuantity forKey:@"QuantityPicked"];
    }else{
        [productObject setValue:[NSNumber numberWithBool:YES] forKey:@"Picked"];
        [productObject setValue:[NSNumber numberWithDouble:[[self.quantityText text]doubleValue]] forKey:@"QuantityPicked"];
        [productObject setValue:NULL forKey:@"PickedNumCases"];
    }

    [self.pickProducts insertObject:productObject atIndex:index];

    //save product to temp DB

    @try{
        NSFetchRequest *productRequest = [[NSFetchRequest alloc]init];
        NSPredicate *productPredicateforDB   = [NSPredicate predicateWithFormat:@"documentId == %@  AND productId = %@",self.documentId, self.selectedProductId];
        [productRequest setPredicate:productPredicateforDB];
        [productRequest setEntity:[NSEntityDescription entityForName:@"PickTempProductList" inManagedObjectContext:context]];
        NSError *error;
        NSArray *results= [context executeFetchRequest:productRequest error:&error];

        if(results.count==0){
            NSManagedObject *saveProduct = [NSEntityDescription
                                            insertNewObjectForEntityForName:@"PickTempProductList"
                                            inManagedObjectContext:context];

            [saveProduct setValue:self.documentId forKey:@"documentId"];
            [saveProduct setValue:[productObject valueForKey:@"ProductId"] forKey:@"productId"];
            [saveProduct setValue:[NSNumber numberWithBool:[[productObject valueForKey:@"Picked"]boolValue]] forKey:@"picked"];
            [saveProduct setValue:[NSNumber numberWithDouble:[[productObject valueForKey:@"PickedNumCases"]doubleValue]] forKey:@"pickedNumCases"];
            [saveProduct setValue:[NSNumber numberWithDouble:[[productObject valueForKey:@"QuantityPicked"]doubleValue]] forKey:@"pickedQuantity"];
        }else{
            NSManagedObject * foundPoduct = results.firstObject;
            [foundPoduct setValue:[NSNumber numberWithDouble:[[productObject valueForKey:@"PickedNumCases"]doubleValue]] forKey:@"pickedNumCases"];
            [foundPoduct setValue:[NSNumber numberWithDouble:[[productObject valueForKey:@"QuantityPicked"]doubleValue]] forKey:@"pickedQuantity"];
        }
        [context save:&error];
    }@catch(NSException * ex){
        NSLog(@"Exception occured: %@", ex.description);
        [self showErrorAlert:ex.description];
    }


    //select the next row
    if(self.pickProducts.count-1 > [self.productTableView indexPathForSelectedRow].row){
        //make sure its not the last row

        NSIndexPath *indexPath;

        if(self.productTableView.indexPathForSelectedRow)
            indexPath = [NSIndexPath indexPathForRow:[self.productTableView indexPathForSelectedRow].row +1 inSection:0];
        else indexPath = [NSIndexPath indexPathForRow:0 inSection:0];

        [self.productTableView reloadData];
        [self.productTableView selectRowAtIndexPath:indexPath
                                           animated:YES
                                     scrollPosition:UITableViewScrollPositionNone];
        [self tableView:self.productTableView didSelectRowAtIndexPath:indexPath];
    }else [self.productTableView reloadData];

}

-(void)savePickToFile{

    NSLog(@"Start saving the file to directory..");

    self.endTime = [NSDate date];

    NSMutableArray<WarehousePickingItem> * pickedItems = [[NSMutableArray<WarehousePickingItem> alloc]init];

    NSPredicate * pickPredicate = [NSPredicate predicateWithFormat:@"Picked == YES"];
    NSArray * pickedProducts = [self.pickProducts filteredArrayUsingPredicate:pickPredicate];


    for(NSObject * product in pickedProducts){

        WarehousePickingItem * item = [[WarehousePickingItem alloc]init];

        item.ProductId = [product valueForKey:@"ProductId"];
        item.DepartmentId = [product valueForKey:@"DepartmentId"];

        QuantityDetails * itemQuantity = [[QuantityDetails alloc]init];

        if([[product valueForKey:@"ShipCaseQty"]boolValue]){
            itemQuantity.Quantity = [NSNumber numberWithDouble:[[product valueForKey:@"QuantityPicked"] doubleValue]];
            itemQuantity.NumCases = [NSNumber numberWithDouble:[[product valueForKey:@"PickedNumCases"] doubleValue]];
            itemQuantity.CaseQty = [NSNumber numberWithDouble:[[product valueForKey:@"CaseQty"] doubleValue]];
            itemQuantity.UomId = [product valueForKey:@"UomId"];
        }else{
            itemQuantity.Quantity = [NSNumber numberWithDouble:[[product valueForKey:@"QuantityPicked"] doubleValue]];
            itemQuantity.UomId = [product valueForKey:@"UomId"];
        }

        if([itemQuantity validateQuantity]){
            item.Amount=itemQuantity;
            [pickedItems addObject:item];
        }else{
            //            [self stopSpinner];

            [statusAlert dismissViewControllerAnimated:YES completion:^{
                [self showModelValidationAlert];
            }];
            //            [self showModelValidationAlert];
            return;
        }
    }

    WarehousePicking * pickingObject = [[WarehousePicking alloc]init];

    pickingObject.TransferNumber = [NSString stringWithFormat:@"T%@%@%@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_Location"], [[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_For_Location"], [self.documentId componentsSeparatedByString:@"_"][1]];

    pickingObject.WarehouseLocationId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_Location"];
    pickingObject.ReceivingLocationId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_For_Location"];

    NSString *deptId;

    if([[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"] isKindOfClass:[NSString class]]){
        deptId = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"];
    }else{
        deptId = [[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"]stringValue];
    }

    pickingObject.DepartmentId = deptId;
//    if([[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"] intValue]!=0)
//        pickingObject.DepartmentId = [[[[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Department"] valueForKey:@"DeptId"]stringValue];

    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[NSTimeZone localTimeZone]];
    [cal setLocale:[NSLocale currentLocale]];
    NSDateComponents *toDateComponents = [cal components:( NSCalendarUnitYear   |
                                                          NSCalendarUnitMonth  |
                                                          NSCalendarUnitDay      ) fromDate:[NSDate date]];

    pickingObject.PickForDate= [cal dateFromComponents:toDateComponents];
    pickingObject.SubmissionDateTime = self.endTime;
    pickingObject.SubmittedByUserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
    pickingObject.Items = pickedItems;

    NSLog(@"object creation successful..., save file to directory..");

    //saving to DB

    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];

    [newManagedObject setValue:@"PICKING" forKey:@"documentType"];
    [newManagedObject setValue:@"Finished" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.endTime forKey:@"date"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
    [context save:&error];

    //delete from tempDocList table

    NSFetchRequest *allDocs = [[NSFetchRequest alloc] init];
    [allDocs setEntity:[NSEntityDescription entityForName:@"TempDocStatus" inManagedObjectContext:context]];
    NSPredicate *docPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
    [allDocs setPredicate:docPredicate];
    NSArray *docs = [context executeFetchRequest:allDocs error:&error];
    for (NSManagedObject *doc in docs) {
        [context deleteObject:doc];  }
    NSError *saveError = nil;
    [context save:&saveError];

    //delete from TempProdList table

    NSFetchRequest *allProds = [[NSFetchRequest alloc] init];
    [allProds setEntity:[NSEntityDescription entityForName:@"PickTempProductList" inManagedObjectContext:context]];
    NSPredicate *prodPredicate   = [NSPredicate predicateWithFormat:@"documentId == %@",self.documentId];
    [allProds setPredicate:prodPredicate];
    NSArray *prods = [context executeFetchRequest:allProds error:&error];
    for (NSManagedObject *prod in prods) {
        [context deleteObject:prod];  }
    [context save:&saveError];

    if(error){
        [statusAlert dismissViewControllerAnimated:YES completion:^{
            [self showErrorInitialingAlert];
        }];
        return;
    }else{
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
//#warning change this to make sure picking works
        NSString *stringFromDate = [formatter stringFromDate:self.endTime];
        [self writeStringToFile:[pickingObject toJSONString] fileName:stringFromDate];
        NSLog(@"File Saved!!!");
    }
}


- (IBAction)submitPick:(id)sender {
    [self showFinishPickAlert];
}


#pragma mark - segue funcitons

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"printBarcodeFromPickingSegue"]){
        CNFPrintQRCodeViewController * qrCodeViewContr = segue.destinationViewController;
        NSString * transfer = [NSString stringWithFormat:@"T%@%@%@", [[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_Location"], [[[NSUserDefaults standardUserDefaults] valueForKey:@"PICKING"] valueForKey:@"Pick_For_Location"], [self.documentId componentsSeparatedByString:@"_"][1]];
        qrCodeViewContr.transferNumber = transfer;
    }
}


#pragma mark - iphone file system handler

- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];

    if(error){
        [self stopSpinner];
        [self showErrorAlert:@"Error saving file to the directory, Please try again.."];
        return;
    }

    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }

    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}


-(NSNumber*)multiplyNumbers :(NSNumber*)first second:(NSNumber*)second{
    double multiply = [first doubleValue]*[second doubleValue];
    return [NSNumber numberWithDouble:multiply];
}


@end
