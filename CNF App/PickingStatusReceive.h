//
//  PickingStatusReceive.h
//  CNF App Test
//
//  Created by Anik on 2017-05-30.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface PickingStatusReceive : JSONModel

@property (nonatomic, strong) NSString * DocumentNumber;
@property (nonatomic, strong) NSString * WarehouseLocationId;
@property (nonatomic, strong) NSString * ReceivingLocationId;
@property (nonatomic, strong) NSDate * PickForDate;
@property (nonatomic, strong) NSString * DepartmentId;
@property (nonatomic, strong) NSString * TransferNumber;
@property (nonatomic, strong) NSString * DocuemntStatus;
@property (nonatomic, strong) NSString * DocumentStatusDescription;
@property (nonatomic, strong) NSDate * DocumentProcessDate;
@property (nonatomic, strong) NSString * User;


@end
