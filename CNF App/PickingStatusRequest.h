//
//  PickingStatusSubmit.h
//  CNF App Test
//
//  Created by Anik on 2017-05-30.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface PickingStatusRequest : JSONModel

@property (nonatomic, strong) NSString * warehouseLocation;

@end
