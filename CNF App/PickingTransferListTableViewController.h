//
//  TransferListTableViewController.h
//  CNF App Test
//
//  Created by Anik on 2017-05-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"

@interface PickingTransferListTableViewController : UITableViewController<UIAlertViewDelegate>

@end
