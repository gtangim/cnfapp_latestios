//
//  TransferListTableViewController.m
//  CNF App Test
//
//  Created by Anik on 2017-05-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "PickingTransferListTableViewController.h"
#import "PickingTransferReceiptPrintViewController.h"
#import "CNFAppEnums.h"
#import "Reachability.h"
#import "PickingStatusReceive.h"
#import "PickingStatusRequest.h"

@interface PickingTransferListTableViewController ()
@property (nonatomic, strong) NSMutableArray * transferArray;
@property (nonatomic, strong) NSMutableArray * departmentArray;
@property (nonatomic) int index;
@end

@implementation PickingTransferListTableViewController{
    UIAlertController * statusAlert;
}

@synthesize transferArray=_transferArray, index=_index, departmentArray=_departmentArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationItem.leftBarButtonItem.enabled=NO;

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    [refreshControl addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    [self startSpinner:@"Testing Connection..."];
    [self getDepartmentList];
    [self doWebServiceCall];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)refreshTableData{
    //do web service call here...
    
    [self doWebServiceCall];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}


-(void)getDepartmentList{
    
    NSPredicate *locationPred = [NSPredicate predicateWithFormat:@"K contains[cd] %@", @"DepartmentList"];
    NSObject *locationOffset = [[[[NSUserDefaults standardUserDefaults] valueForKey:@"DATA_LIST"] filteredArrayUsingPredicate:locationPred] objectAtIndex:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filepath = [NSString stringWithFormat: @"%@/%@", applicationDocumentsDir, @"db.json"];
    NSFileHandle *file;
    NSData *databuffer;
    file = [NSFileHandle fileHandleForReadingAtPath:filepath];
    
    [file seekToFileOffset: [[[NSUserDefaults standardUserDefaults] valueForKey:@"START_OFFSET"] intValue]+[[locationOffset valueForKey:@"O"] intValue]];
    
    databuffer = [file readDataOfLength: [[locationOffset valueForKey:@"L"] intValue]];
    [file closeFile];
    
    NSString* jsonStr = [[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
    
    NSError * serializerError;
    
    NSArray * locations = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&serializerError];
    
    if(serializerError){
        NSLog(@"Serializer error, data file not downloaded properly..");
        [self showErrorAlert:@"Serializer error, data file not downloaded properly.."];
    }else{
        self.departmentArray =[locations mutableCopy];
    }
}


-(void)doWebServiceCall{
    if([self connectionTester]!=0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startGetTransferListFromWeb];
        });
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusAlert dismissViewControllerAnimated:YES completion:^{
            [self showErrorAlert:@"No Connection"];
            }];
        });
    }
}


#pragma mark - connection tester

-(int)connectionTester{
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability* reachability = [Reachability reachabilityWithHostName: appDelegate.serverUrl];
    
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
    if(netStatus==1){
        NSLog(@"connection OK...");
    }else{
        NSLog(@"No connection for validation...");
    }
    
    return netStatus;
}


-(void)startGetTransferListFromWeb{
    
    NSLog(@"start getting Picking Status from web service......");
    
    PickingStatusRequest * requestObject = [[PickingStatusRequest alloc] init];
    requestObject.warehouseLocation=[[NSUserDefaults standardUserDefaults]valueForKey:@"OFFICE"];
     
    NSString *post = [requestObject toJSONString];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *len = [NSString stringWithFormat:@"%i", (int)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/cnfmobilewebservice/CNFMobileService.asmx/getPickingStatus", appDelegate.serverUrl]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:len forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                      
                                          if (error) {
                                              NSLog(@"Server Error:%@",error.description);
                                              [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                  [self showErrorAlert:[NSString stringWithFormat:@"Server Error %@", error.description]];
                                              }];
                                          }
                                          if (data) {
                                              NSError * parseError;
                                              NSDictionary *returnedValue = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
                                              
                                              if(parseError){
                                                  NSLog(@"Error serializing web service call..");
                                                  [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                      [self showErrorAlert:[NSString stringWithFormat:@"Parse Error web return: %@", parseError.description]];
                                                  }];

                                              }else{
                                                  if([returnedValue valueForKey:@"Message"]==Nil){
                                                      
                                                      NSArray * returnArray = [[NSJSONSerialization JSONObjectWithData:[[returnedValue valueForKey:@"d"] dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&parseError] mutableCopy];
                                                      
                                                      if(parseError){
                                                          [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                              [self showErrorAlert:[NSString stringWithFormat:@"Parse Error web return: %@", parseError.description]];
                                                          }];
                                                      }else{
                                                          if(returnArray.count>0){
                                                              
                                                              self.transferArray = [[NSMutableArray alloc]init];
                                                              
                                                              for(NSObject * statusObject in returnArray){
                                                                  
                                                                  NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
                                                                  [dic setValue:[statusObject valueForKey:@"DocuemntStatus"] forKey:@"DocuemntStatus"];
                                                                  [dic setValue:[statusObject valueForKey:@"DocumentNumber"] forKey:@"DocumentNumber"];
                                                                  [dic setValue:[self getDateFromJSON:[statusObject valueForKey:@"DocumentProcessDate"]] forKey:@"DocumentProcessDate"];
                                                                  [dic setValue:[statusObject valueForKey:@"DocumentStatusDescription"] forKey:@"DocumentStatusDescription"];
                                                                  [dic setValue:[self getDateFromJSON:[statusObject valueForKey:@"PickForDate"]] forKey:@"PickForDate"];
                                                                  [dic setValue:[statusObject valueForKey:@"TransferNumber"] forKey:@"TransferNumber"];
                                                                  [dic setValue:[statusObject valueForKey:@"User"] forKey:@"User"];
                                                                  [dic setValue:[statusObject valueForKey:@"WarehouseLocationId"] forKey:@"WarehouseLocationId"];
                                                                  [dic setValue:[statusObject valueForKey:@"ReceivingLocationId"] forKey:@"ReceivingLocationId"];
                                                                  
                                                                  if([statusObject valueForKey:@"DepartmentId"]!=[NSNull null]){
                                                                      NSPredicate * objectPred = [NSPredicate predicateWithFormat:@"SELF.DeptId contains %@", [statusObject valueForKey:@"DepartmentId"]];
                                                                      [dic setValue:[[self.departmentArray filteredArrayUsingPredicate:objectPred] valueForKey:@"Name"] forKey:@"DepartmentId"];
                                                                  }else
                                                                      [dic setValue:@"All Department" forKey:@"DepartmentId"];
                                                                  
                                                                  [self.transferArray addObject:dic];
                                                              }
                                                              
                                                              NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"DocumentProcessDate" ascending:NO];
                                                              NSArray *sortDescriptors = @[seqSort];
                                                              
                                                              self.transferArray = [[NSMutableArray alloc]initWithArray:[self.transferArray sortedArrayUsingDescriptors:sortDescriptors]];
                                                              
                                                              [self.tableView reloadData];
                                                              [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                              }];
                                                              
                                                          }else{
                                                              [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                                  [self showErrorAlert:[NSString stringWithFormat:@"No Transfer Information for location %@ to print, Please try again later", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]]];
                                                              }];
                                                          }
                                                      }
                                                  }else{
                                                      NSLog(@"error msg: %@", [returnedValue valueForKey:@"Message"]);
                                                      [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                          [self showErrorAlert:[returnedValue valueForKey:@"Message"]];
                                                      }];
                                                  }
                                              }
                                          }else{
                                              [statusAlert dismissViewControllerAnimated:YES completion:^{
                                                  [self showErrorAlert:@"No Data returned by the web service.."];
                                              }];
                                          }
                                      }];
    [dataTask resume];
    
    
    
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        if (error) {
//            NSLog(@"Server Error:%@",error.description);
//            [statusAlert dismissViewControllerAnimated:YES completion:^{
//                [self showErrorAlert:[NSString stringWithFormat:@"Server Error %@", error.description]];
//            }];
//        }
//        if (data) {
//            NSError * parseError;
//            NSDictionary *returnedValue = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
//            
//            if(parseError){
//                NSLog(@"Error serializing web service call..");
//                [statusAlert dismissViewControllerAnimated:YES completion:^{
//                    [self showErrorAlert:[NSString stringWithFormat:@"Parse Error web return: %@", parseError.description]];
//                }];
//                
//                
//            }else{
//                if([returnedValue valueForKey:@"Message"]==Nil){
//                    
//                    NSArray * returnArray = [[NSJSONSerialization JSONObjectWithData:[[returnedValue valueForKey:@"d"] dataUsingEncoding:NSUTF8StringEncoding]options:0 error:&parseError] mutableCopy];
//                    
//                    if(parseError){
//                        [statusAlert dismissViewControllerAnimated:YES completion:^{
//                            [self showErrorAlert:[NSString stringWithFormat:@"Parse Error web return: %@", parseError.description]];
//                        }];
//                    }else{
//                        if(returnArray.count>0){
//                            
//                            self.transferArray = [[NSMutableArray alloc]init];
//                            
//                            for(NSObject * statusObject in returnArray){
//                                
//                                NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
//                                [dic setValue:[statusObject valueForKey:@"DocuemntStatus"] forKey:@"DocuemntStatus"];
//                                [dic setValue:[statusObject valueForKey:@"DocumentNumber"] forKey:@"DocumentNumber"];
//                                [dic setValue:[self getDateFromJSON:[statusObject valueForKey:@"DocumentProcessDate"]] forKey:@"DocumentProcessDate"];
//                                [dic setValue:[statusObject valueForKey:@"DocumentStatusDescription"] forKey:@"DocumentStatusDescription"];
//                                [dic setValue:[self getDateFromJSON:[statusObject valueForKey:@"PickForDate"]] forKey:@"PickForDate"];
//                                [dic setValue:[statusObject valueForKey:@"TransferNumber"] forKey:@"TransferNumber"];
//                                [dic setValue:[statusObject valueForKey:@"User"] forKey:@"User"];
//                                [dic setValue:[statusObject valueForKey:@"WarehouseLocationId"] forKey:@"WarehouseLocationId"];
//                                [dic setValue:[statusObject valueForKey:@"ReceivingLocationId"] forKey:@"ReceivingLocationId"];
//                                
//                                if([statusObject valueForKey:@"DepartmentId"]!=[NSNull null]){
//                                    NSPredicate * objectPred = [NSPredicate predicateWithFormat:@"SELF.DeptId contains %@", [statusObject valueForKey:@"DepartmentId"]];
//                                    [dic setValue:[[self.departmentArray filteredArrayUsingPredicate:objectPred] valueForKey:@"Name"] forKey:@"DepartmentId"];
//                                }else
//                                    [dic setValue:@"All Department" forKey:@"DepartmentId"];
//                                
//                                [self.transferArray addObject:dic];
//                            }
//                            
//                            NSSortDescriptor * seqSort = [NSSortDescriptor sortDescriptorWithKey:@"DocumentProcessDate" ascending:NO];
//                            NSArray *sortDescriptors = @[seqSort];
//                            
//                            self.transferArray = [[NSMutableArray alloc]initWithArray:[self.transferArray sortedArrayUsingDescriptors:sortDescriptors]];
//                            
//                            [self.tableView reloadData];
//                            [statusAlert dismissViewControllerAnimated:YES completion:^{
//                                
//                            }];
//                            
//                        }else{
//                            [statusAlert dismissViewControllerAnimated:YES completion:^{
//                                [self showErrorAlert:[NSString stringWithFormat:@"No Transfer Information for location %@ to print, Please try again later", [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"]]];
//                            }];
//                            
//                        }
//                    }
//                }else{
//                    NSLog(@"error msg: %@", [returnedValue valueForKey:@"Message"]);
//                    [statusAlert dismissViewControllerAnimated:YES completion:^{
//                         [self showErrorAlert:[returnedValue valueForKey:@"Message"]];
//                    }];
//                    
//                }
//            }
//        }else{
//            [statusAlert dismissViewControllerAnimated:YES completion:^{
//               [self showErrorAlert:@"No Data returned by the web service.."];
//            }];
//            
//        }
//    }];
}


#pragma mark - spinner functions

- (void)startSpinner:(NSString *)message {
    if (!statusAlert){
        statusAlert = [UIAlertController alertControllerWithTitle: nil
                                                          message: message
                                                   preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *customVC     = [[UIViewController alloc] init];
        
        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [customVC.view addSubview:spinner];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterX
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        
        [customVC.view addConstraint:[NSLayoutConstraint
                                      constraintWithItem: spinner
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:customVC.view
                                      attribute:NSLayoutAttributeCenterY
                                      multiplier:1.0f
                                      constant:0.0f]];
        
        [statusAlert setValue:customVC forKey:@"contentViewController"];
        
        [self presentViewController: statusAlert
                           animated: true
                         completion: nil];
    }
}

- (void)stopSpinner {
    if (statusAlert) {
        [statusAlert dismissViewControllerAnimated:YES completion:nil];
        statusAlert = nil;
    }
}



-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

#pragma mark - Table view data source

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    if(section==0)
        return @"Pending Documents";
    else if(section==1)
        return @"Completed Documents";
    else
        return @"Failed Document";
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSPredicate * pendingPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@ OR DocuemntStatus==%@", @"Submitted",@"PreValidated"];
    NSPredicate * faiedPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@", @"Failed"];
    NSPredicate * processedPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@", @"Processed"];
    
    if(section==0)
        return [[self.transferArray filteredArrayUsingPredicate:pendingPredicate] count];
    else if(section==1)
        return [[self.transferArray filteredArrayUsingPredicate:processedPredicate] count];
    else
        return [[self.transferArray filteredArrayUsingPredicate:faiedPredicate] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pickingTransferCell" forIndexPath:indexPath];
   
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm:ss MM-dd-yyyy"];
    
    NSPredicate * pendingPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@ OR DocuemntStatus==%@", @"Submitted",@"PreValidated"];
    NSPredicate * faiedPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@", @"Failed"];
    NSPredicate * processedPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@", @"Processed"];
    
    if(indexPath.section==0){
        
        NSArray * pendingArray = [self.transferArray filteredArrayUsingPredicate:pendingPredicate];
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [[pendingArray objectAtIndex:indexPath.row] valueForKey:@"DocumentNumber"]];
        cell.detailTextLabel.text =[NSString stringWithFormat:@"Done by user %@, on: %@",[[pendingArray objectAtIndex:indexPath.row] valueForKey:@"User"], [formatter stringFromDate:[[pendingArray objectAtIndex:indexPath.row] valueForKey:@"DocumentProcessDate"]]];
        cell.detailTextLabel.textColor = [UIColor yellowColor];
    }else if(indexPath.section==1){
        
        NSArray * processedArray = [self.transferArray filteredArrayUsingPredicate:processedPredicate];
        cell.textLabel.text = [NSString stringWithFormat:@"Transfer Number: %@", [[processedArray objectAtIndex:indexPath.row] valueForKey:@"TransferNumber"]];
        cell.detailTextLabel.text =[NSString stringWithFormat:@"Done by user: %@, on: %@", [[processedArray objectAtIndex:indexPath.row] valueForKey:@"User"], [formatter stringFromDate:[[processedArray objectAtIndex:indexPath.row] valueForKey:@"DocumentProcessDate"]]];
        cell.detailTextLabel.textColor = [UIColor greenColor];
    }else{
        NSArray * failedArray = [self.transferArray filteredArrayUsingPredicate:faiedPredicate];
        cell.textLabel.text = [NSString stringWithFormat:@"Failed Picking"];
        cell.detailTextLabel.text =[NSString stringWithFormat:@"Done by user: %@ on: %@", [[failedArray objectAtIndex:indexPath.row] valueForKey:@"User"],[formatter stringFromDate:[[failedArray objectAtIndex:indexPath.row] valueForKey:@"DocumentProcessDate"]]];
        cell.detailTextLabel.textColor = [UIColor redColor];
    }
    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1){
        [self showPrintTransferAlert:(int)indexPath.row];
    }else if(indexPath.section==2){
        [self showFailedDescriptionAlert:(int)indexPath.row];
    }
}

-(void)showPrintTransferAlert:(int)index{

    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm:ss MM-dd-yyyy"];
    
    NSPredicate * processedPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@", @"Processed"];
    NSArray * processed = [self.transferArray filteredArrayUsingPredicate:processedPredicate];
    
    if([[processed objectAtIndex:index] valueForKey:@"TransferNumber"]!=[NSNull null]){
//        UIAlertView * printAlert = [[UIAlertView alloc] initWithTitle:[[processed objectAtIndex:index] valueForKey:@"TransferNumber"]
//                                                              message:[NSString stringWithFormat:@"Created by user %@, on %@ for location: %@, picked department: %@",[[processed objectAtIndex:index] valueForKey:@"User"], [formatter stringFromDate:[[processed objectAtIndex:index] valueForKey:@"DocumentProcessDate"]], [[processed objectAtIndex:index] valueForKey:@"ReceivingLocationId"],[[processed objectAtIndex:index] valueForKey:@"DepartmentId"] ]
//                                                             delegate:self
//                                                    cancelButtonTitle:@"Cancel"
//                                                    otherButtonTitles:@"Print", nil];
//        printAlert.tag=printTransferNumberAlerts;
//        self.index=index;
//        [printAlert show];
    }else{
//        UIAlertView * printAlert = [[UIAlertView alloc] initWithTitle:@""
//                                                              message:[NSString stringWithFormat:@"Created by user %@, on %@ for location: %@, picked department: %@",[[processed objectAtIndex:index] valueForKey:@"User"], [formatter stringFromDate:[[processed objectAtIndex:index] valueForKey:@"DocumentProcessDate"]], [[processed objectAtIndex:index] valueForKey:@"ReceivingLocationId"],[[processed objectAtIndex:index] valueForKey:@"DepartmentId"] ]
//                                                             delegate:self
//                                                    cancelButtonTitle:@"Cancel"
//                                                    otherButtonTitles: nil];
//        printAlert.tag=printTransferNumberAlerts;
//        self.index=index;
//        [printAlert show];
    }
    
    
}

-(void)showFailedDescriptionAlert:(int)index{
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm:ss MM-dd-yyyy"];
    
//    NSPredicate * failedPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@", @"Failed"];
//    NSArray * failed = [self.transferArray filteredArrayUsingPredicate:failedPredicate];

//    UIAlertView * printAlert = [[UIAlertView alloc] initWithTitle:@""
//                                                          message:[NSString stringWithFormat:@"Created by user %@, on %@ failed due to reason: %@",[[failed objectAtIndex:index] valueForKey:@"User"], [formatter stringFromDate:[[failed objectAtIndex:index] valueForKey:@"DocumentProcessDate"]], [[failed objectAtIndex:index] valueForKey:@"DocumentStatusDescription"] ]
//                                                         delegate:self
//                                                cancelButtonTitle:Nil
//                                                otherButtonTitles:@"Cancel", nil];
//
//    [printAlert show];
}


//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(alertView.tag==printTransferNumberAlerts){
//        if(buttonIndex==1){
//            [self performSegueWithIdentifier:@"printTransferNoSegue" sender:self];
//        }
//    }
//}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqual:@"printTransferNoSegue"]){
        PickingTransferReceiptPrintViewController *detailView=[[PickingTransferReceiptPrintViewController alloc]initWithNibName:@"PickingTransferReceiptPrintViewController" bundle:nil];
        
        detailView = segue.destinationViewController;
        
        NSPredicate * processedPredicate = [NSPredicate predicateWithFormat:@"DocuemntStatus == %@", @"Processed"];
        NSArray * processed = [self.transferArray filteredArrayUsingPredicate:processedPredicate];
        
        detailView.transferNumber = [[processed objectAtIndex:self.index] valueForKey:@"TransferNumber"];
    }
}

#pragma mark - nsdate conversion functions

- (NSDate*) getDateFromJSON:(NSString *)dateString{
    // Expect date in this format "/Date(1268123281843)/"
    int startPos = (int)[dateString rangeOfString:@"("].location+1;
    int endPos = (int)[dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    //    NSLog(@"%llu",milliseconds);
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}


@end
