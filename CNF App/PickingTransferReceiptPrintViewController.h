//
//  TransferReceiptPrintViewController.h
//  CNF App Test
//
//  Created by Anik on 2017-05-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickingTransferReceiptPrintViewController : UIViewController<UIPrintInteractionControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *transferNoLabel;

@property (strong, nonatomic) NSString * transferNumber;

- (IBAction)printTransfer:(id)sender;

@end
