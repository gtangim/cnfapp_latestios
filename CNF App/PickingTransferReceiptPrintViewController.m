//
//  TransferReceiptPrintViewController.m
//  CNF App Test
//
//  Created by Anik on 2017-05-19.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "PickingTransferReceiptPrintViewController.h"

@interface PickingTransferReceiptPrintViewController ()

@end

@implementation PickingTransferReceiptPrintViewController

@synthesize transferNoLabel=_transferNoLabel, transferNumber=_transferNumber;

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.transferNoLabel.text=self.transferNumber;
    
    self.transferNoLabel.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
    [self.view addSubview:self.transferNoLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (IBAction)printTransfer:(id)sender {
    
    [self createPDFfromUIView:self.view saveToDocumentsWithFileName:@"transfer.pdf"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"transfer.pdf"];
    NSData *myData = [NSData dataWithContentsOfFile: path];
    
    if(![self deletePDFFile:@"transfer.pdf"]){
//        [self showErrorAlert:@"Problem with printing, Please try again"];
    }else{
        UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        if(pic && [UIPrintInteractionController canPrintData: myData] ) {
            
            pic.delegate = self;
            
            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = [path lastPathComponent];
            printInfo.duplex = UIPrintInfoDuplexLongEdge;
            pic.printInfo = printInfo;
//            pic.showsPageRange = YES;
            pic.printingItem = myData;
            
            void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
                if(!completed && error){
                    NSLog(@"Print Error: %@", error);
//                    [self showErrorAlert:[NSString stringWithFormat:@"Print Error: %@, Please try again", error]];
                }else if(completed){
//                    [self showInfoAlert:@"Print Completed, Thank you!!"];
                }else{
                    
                }
            };
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    }
}


-(void)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename
{
    // Creates a mutable data object for updating with binary data, like a byte array
    NSMutableData *pdfData = [NSMutableData data];
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    UIGraphicsBeginPDFPage();
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    [aView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    // Retrieves the document directories from the iOS device
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    NSLog(@"documentDirectoryFileName: %@",documentDirectoryFilename);
}

- (BOOL)deletePDFFile:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSString *filePath = [dataPath stringByAppendingPathComponent:fileName];
    NSError *error;
    return [fileManager removeItemAtPath:filePath error:&error];
    
}


//-(void)showErrorAlert:(NSString *)message{
//
//    UIAlertView * errorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Request Error",@"")
//                                                          message:message
//                                                         delegate:self
//                                                cancelButtonTitle:@"OK"
//                                                otherButtonTitles:nil];
//    [errorAlert show];
//}
//
//
//-(void)showInfoAlert:(NSString *)body{
//    UIAlertView * infoAlert = [[UIAlertView alloc] initWithTitle:nil
//                                                         message:body
//                                                        delegate:self
//                                               cancelButtonTitle:@"OK"
//                                               otherButtonTitles:nil];
//    [infoAlert show];
//}
@end
