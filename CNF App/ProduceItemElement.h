//
//  ProduceItemElement.h
//  CNF App
//
//  Created by Anik on 2017-10-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
@interface ProduceItemElement : JSONModel

@property (strong, nonatomic) NSArray<NSString *> * ProduceProductsList;

@end
