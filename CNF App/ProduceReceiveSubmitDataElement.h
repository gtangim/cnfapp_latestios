//
//  ProduceReceiveSubmitDataElement.h
//  CNF App
//
//  Created by Anik on 2018-06-05.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "ProduceReceivingModel.h"

@interface ProduceReceiveSubmitDataElement : JSONModel

@property (strong, nonatomic) NSArray<ProduceReceivingModel>* rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;

@end
