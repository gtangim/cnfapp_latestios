//
//  ProduceReceivingModel.h
//  CNF App
//
//  Created by Anik on 2018-06-05.
//  Copyright © 2018 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol ProduceReceivingModel @end

@interface ProduceReceivingModel : JSONModel

@property (nonatomic, retain) NSString * UserId;
@property (nonatomic, retain) NSString * LocationId;
@property (nonatomic, retain) NSArray<NSString *> * ProductIdList;

@end
