//
//  ProduceTracking.h
//  CNF App
//
//  Created by Anik on 2017-10-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "ProduceItemElement.h"

@protocol ProduceTracking @end

@interface ProduceTracking : JSONModel

@property (nonatomic, retain) NSString * UserId;
@property (nonatomic, retain) NSString * LocationId;
@property (strong, nonatomic) ProduceItemElement * ProduceItems;

@end
