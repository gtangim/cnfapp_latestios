//
//  ProduceTrackingSubmitRequest.h
//  CNF App
//
//  Created by Anik on 2017-10-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "ProduceTracking.h"

@interface ProduceTrackingSubmitRequest : JSONModel

@property (strong, nonatomic) NSArray<ProduceTracking>* rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;

@end
