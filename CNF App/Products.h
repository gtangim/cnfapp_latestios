//
//  Products.h
//  CNF App
//
//  Created by Anik on 2015-03-25.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "Vendors.h"
#import "Supplier.h"
#import "StoreSpecificInfo.h"
#import "DateTime.h"

@interface Products : JSONModel

@property (nonatomic, retain) NSString * ProductId;
@property (nonatomic, retain) NSString * ProductName;
@property (nonatomic, retain) NSString * Upc;

//new addition on 1st May 2017
@property (nonatomic, retain) NSString * DepartmentId;
@property (nonatomic, retain) NSString * SubDepartmentId;
@property (nonatomic, retain) NSString * CategoryId;

@property (nonatomic, retain) NSString * IdList;
@property (nonatomic, retain) NSNumber<Optional> * Size;
@property (nonatomic, retain) NSString<Optional> * Uom;
@property (nonatomic, retain) NSString<Optional> * UomQty;
@property (nonatomic, retain) NSString<Optional> * Brand;
@property (nonatomic, retain) NSArray<Vendors>* Vendors;
@property (nonatomic, retain) Supplier * Supplier;
@property (nonatomic, retain) NSNumber * Delisted;
@property (nonatomic, retain) NSString<Optional> * DelistedReason;
@property (nonatomic, retain) NSNumber * Pulled;
@property (nonatomic, retain) NSString<Optional> * PulledReason;
@property (nonatomic, retain) DateTime<Optional> * PulledDate;
@property (nonatomic, retain) NSNumber * VendorOutOfStock;
@property (nonatomic, retain) DateTime<Optional> * VendorOutOfStockToDate;
@property (nonatomic, retain) NSString<Optional> * VendorOutOfStockReason;

@property (nonatomic, retain) NSArray<StoreSpecificInfo>* InfoByStore;
@property (nonatomic, retain) NSNumber * WarehouseOutOfStock;
@property (nonatomic, retain) NSNumber * CaseQuantity;

//new addition

@property (nonatomic, retain) NSNumber * StandardCost;
@property (nonatomic, retain) NSNumber * BottleDeposit;
@property (nonatomic, retain) NSNumber * EnvironmentalFree;

@property (nonatomic, retain) NSNumber * IsTaxable;

@property (nonatomic, retain) NSNumber * TaxRatePercent;


@end
