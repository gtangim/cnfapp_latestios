//
//  QuantityDetails.h
//  CNF App Test
//
//  Created by Anik on 2017-05-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface QuantityDetails : JSONModel

@property (nonatomic, retain) NSNumber * Quantity;
@property (nonatomic, retain) NSNumber<Optional> * NumCases;
@property (nonatomic, retain) NSNumber<Optional> * CaseQty;
@property (nonatomic, retain) NSString * UomId;

-(BOOL)validateQuantity;

@end
