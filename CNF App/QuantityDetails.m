//
//  QuantityDetails.m
//  CNF App Test
//
//  Created by Anik on 2017-05-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "QuantityDetails.h"

@implementation QuantityDetails

@synthesize NumCases=_NumCases, Quantity=_Quantity, CaseQty=_CaseQty;

-(BOOL)validateQuantity{
    if((self.NumCases!=nil) && (self.CaseQty!=nil)){
        
        if(self.NumCases!=nil){
            if([self.NumCases doubleValue]* [self.CaseQty doubleValue]!=[self.Quantity doubleValue])
                return FALSE;
            else
                return TRUE;
        }else{
            return TRUE;
        }

    }else
        return TRUE;
}

@end
