//
//  ReportElement.h
//  CNF App
//
//  Created by Anik on 2015-04-14.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "DateTime.h"

@protocol ReportElement @end

@interface ReportElement : JSONModel

@property (nonatomic, retain) NSString * ProductId;
@property (nonatomic, retain) NSString * ProductName;
@property (nonatomic, retain) NSNumber<Optional> * Size;
@property (nonatomic, retain) NSString<Optional> * UomQty;
@property (nonatomic, retain) NSString<Optional> * ProductBrand;
@property (nonatomic, retain) NSString<Optional> * Vendor;
@property (nonatomic, retain) NSString<Optional> * Supplier;
@property (nonatomic, retain) NSNumber * CrsQOH;
@property (nonatomic, retain) NSString * Location;
@property (nonatomic, retain) NSNumber * Delisted;
@property (nonatomic, retain) NSString<Optional> * DelistedReason;
@property (nonatomic, retain) NSNumber * Pulled;
@property (nonatomic, retain) NSString<Optional> * PulledReason;
//@property (nonatomic, retain) DateTime<Optional> * PulledDate;

@property (nonatomic, retain) NSDate<Optional> * PulledDate;

@property (nonatomic, retain) NSString<Optional> * VendorId;
@property (nonatomic, retain) NSNumber * VendorOutOfStock;
//@property (nonatomic, retain) DateTime<Optional> * VendorOOSDate;

@property (nonatomic, retain) NSDate<Optional> * VendorOOSDate;

@property (nonatomic, retain) NSNumber * VendorShortShipped;
@property (nonatomic, retain) NSNumber * VendorShortageCount;
@property (nonatomic, retain) NSString<Optional> * VendorOOSReason;
@property (nonatomic, retain) NSNumber * OnOrder;
@property (nonatomic, retain) NSNumber * WarehouseOutOfStock;
@property (nonatomic, retain) NSNumber * WarehouseShortShipped;
@property (nonatomic, retain) NSNumber * ShelfCapacity;
@property (nonatomic, retain) NSNumber<Optional> * NotOrdered;
@property (nonatomic, retain) NSNumber<Optional> * DelistedWithShelfTag;
@property (nonatomic, retain) NSNumber<Optional> * PulledWithShelfTag;
@property (nonatomic, retain) NSNumber<Optional> * VendorOOSWtShelfTag;
@property (nonatomic, retain) NSNumber<Optional> * OnOrderWithShelfTag;
@property (nonatomic, retain) NSNumber<Optional> * WareHouseOOSWtShelfTag;
@property (nonatomic, retain) NSNumber<Optional> * InStore;
@property (nonatomic, retain) NSNumber<Optional> * InReceiving;
@property (nonatomic, retain) NSNumber<Optional> * CapacityZeroWithShelfTag;

@end
