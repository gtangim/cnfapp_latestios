//
//  ShelfTagBatchData.h
//  CNF App Test
//
//  Created by Md Tauhiduzzaman on 2017-08-15.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "ShelfTagBatchElement.h"

@protocol ShelfTagBatchData @end

@interface ShelfTagBatchData : JSONModel

@property (strong, nonatomic) NSString *UserId;
@property (strong, nonatomic) NSString *LocationId;
@property (strong, nonatomic) NSDate *StartTime;
@property (strong, nonatomic) ShelfTagBatchElement *ShelfTagBatch;

@end
