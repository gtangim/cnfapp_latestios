//
//  ShelfTagBatchElement.h
//  CNF App Test
//
//  Created by Md Tauhiduzzaman on 2017-08-16.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol ShelfTagBatchElement @end

@interface ShelfTagBatchElement : JSONModel

@property (nonatomic, retain) NSString<Optional> *BatchId;
@property (nonatomic, retain) NSString *BatchName;
@property (nonatomic, retain) NSMutableArray *BatchProducts;

@end
