//
//  ShelfTagBatchNameTableViewController.h
//  CNF App Test
//
//  Created by Md Tauhiduzzaman on 2017-08-07.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShelfTagBatchElement.h"

@interface ShelfTagBatchNameTableViewController : UITableViewController<UITextFieldDelegate>
- (IBAction)goToProducts:(id)sender;
@end
