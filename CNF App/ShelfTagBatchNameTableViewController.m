//
//  ShelfTagBatchNameTableViewController.m
//  CNF App Test
//
//  Created by Md Tauhiduzzaman on 2017-08-07.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "ShelfTagBatchNameTableViewController.h"
#import "ShelfTagProductAddViewController.h"

@interface ShelfTagBatchNameTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *BatchNameTextField;

@property NSString *StoreId;

@end

@implementation ShelfTagBatchNameTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.BatchNameTextField.delegate = self;
    self.StoreId = [[NSUserDefaults standardUserDefaults]valueForKey:@"OFFICE"];
    if(!self.StoreId.length)
       [self showErrorAlert:@"User not logged in"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"GoToAddProductShelfTagSegue"]){
        ShelfTagBatchElement *newbatch = [[ShelfTagBatchElement alloc]init];
        newbatch.BatchName = self.BatchNameTextField.text;
        ShelfTagProductAddViewController *prodController = (ShelfTagProductAddViewController *)segue.destinationViewController;
        prodController.SelectedBatch = newbatch;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

#pragma mark - alert notification functions

-(void)showErrorAlert:(NSString *)message{
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}


- (IBAction)goToProducts:(id)sender {
    if([self.BatchNameTextField hasText]){
        [self performSegueWithIdentifier:@"GoToAddProductShelfTagSegue" sender:self];
    }else{
        [self showErrorAlert:@"Please enter a valid Batch Name.."];
    }
//    [self performSegueWithIdentifier:@"GoToAddProductShelfTagSegue" sender:self];
}
@end
