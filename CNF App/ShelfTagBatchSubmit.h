//
//  ShelfTagBatchSubmit.h
//  CNF App Test
//
//  Created by Md Tauhiduzzaman on 2017-08-16.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "ShelfTagBatchData.h"

@interface ShelfTagBatchSubmit : JSONModel

@property (strong, nonatomic) NSArray<ShelfTagBatchData> * rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;


@end
