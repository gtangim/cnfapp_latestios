//
//  ShelfTagProductAddViewController.h
//  CNF App Test
//
//  Created by Md Tauhiduzzaman on 2017-08-12.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"
#import "DTDevices.h"
#import "ZBarSDK.h"
#import "StoreSpecificInfo.h"
#import "Products.h"
#import "BarcodeProcessor.h"
#import "ShelfTagBatchElement.h"

@interface ShelfTagProductAddViewController : UIViewController < DTDeviceDelegate, ZBarReaderDelegate, UITextFieldDelegate,UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, NSFetchedResultsControllerDelegate, ZBarReaderDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property ShelfTagBatchElement *SelectedBatch;
@property NSMutableArray *SelectedProducts;

@property (weak, nonatomic) IBOutlet UILabel *locaionLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;


@property (weak, nonatomic) IBOutlet UITextField *UpcTextField;
@property (weak, nonatomic) IBOutlet UITableView *ProductListTable;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

- (IBAction)scanAction:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)submitAction:(id)sender;


@end
