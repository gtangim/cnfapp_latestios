//
//  ShelfTagProductAddViewController.m
//  CNF App Test
//
//  Created by Md Tauhiduzzaman on 2017-08-12.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "ShelfTagProductAddViewController.h"
#import "StoreSpecificInfo.h"
#import "Products.h"
#import "BarcodeProcessor.h"
#import "CNFGetDocumentId.h"
#import "ShelfTagBatchData.h"

@interface ShelfTagProductAddViewController ()
@property (nonatomic, strong) NSString *SelectedStore;
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (nonatomic, strong) DTDevices * scanner;
@property (nonatomic, strong) Products * scannedProduct;
@property (nonatomic) BOOL activateCancelButton;
@property (nonatomic) BOOL foundBarcode;
@property (nonatomic) BOOL scannerAvailable;
@property (nonatomic) BOOL shouldScanProduct;

@property (nonatomic, strong) NSString *documentId;
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;

@end

@implementation ShelfTagProductAddViewController

NSManagedObjectContext * context;
DTDevices *scanner;

@synthesize scanButton=_scanButton;

- (void)viewDidLoad {
    [super viewDidLoad];

    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];

    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"IN_APP"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    self.userNameLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
    self.locaionLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];

    self.UpcTextField.delegate = self;
    self.ProductListTable.delegate = self;
    self.ProductListTable.dataSource = self;
    self.scanner=[DTDevices sharedDevice];
    [self.scanner setDelegate:self];
    [self.scanner connect];

    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkScannerAvailabilityRP) userInfo:nil repeats:NO];

    self.SelectedProducts = [[NSMutableArray alloc]init];
    
    self.SelectedStore = [[[NSUserDefaults standardUserDefaults] valueForKey:@"ShelfTagObject"] valueForKey:@"LocationId"];

    self.scannerAvailable = TRUE;
    
    // Start document
    
    CNFAppDelegate *appDelegate= (CNFAppDelegate *)[[UIApplication sharedApplication]delegate];
    context =[appDelegate managedObjectContext];
    
    self.startTime = [NSDate date];
    self.documentId = [[[CNFGetDocumentId alloc]init] getDocumentId];
    
    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];
    
    [newManagedObject setValue:@"SHELF_TAG_BATCH_CREATE" forKey:@"documentType"];
    [newManagedObject setValue:@"Created" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.startTime forKey:@"date"];
    [context save:&error];

}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    //    [self.scanner disconnect];
    [self.scanner removeDelegate:self];
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.scanner addDelegate:self];
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    self.UpcTextField.text=@"";

    //check if there is a multiple UPC product selected.

    if([[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]){
        [self processBarcode:[[NSUserDefaults standardUserDefaults] valueForKey:@"MULTIPLE_UPC_ID"]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];

    [textField resignFirstResponder];

    if([self.UpcTextField hasText]){
        if([[self.UpcTextField text]length]<3)
            [self showErrorAlert:@"Please enter a 3 digit code"];
        else

            [self processBarcode:self.UpcTextField.text];
    }else{
        [self showErrorAlert:@"Please Enter Valid product ID or UPC"];
    }
    return NO;
}


#pragma mark - Table data source

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shelfTagProductCell" forIndexPath:indexPath];
    Products *productAtRow = [self.SelectedProducts objectAtIndex:indexPath.row];
    cell.textLabel.text = productAtRow.ProductName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@[%@]", productAtRow.ProductId, productAtRow.Upc ];
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.SelectedProducts.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self.SelectedProducts removeObjectAtIndex:indexPath.row];
        [self.ProductListTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    
    return @[deleteAction];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.SelectedProducts removeObjectAtIndex:indexPath.row];
        [self.ProductListTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        
    }
}


#pragma mark - barcode read function

-(void)barcodeData:(NSString *)barcode type:(int)type {
    int sound[]={2000,100};
    [self.scanner playSound:100 beepData:sound length:sizeof(sound) error:nil];
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self processBarcode:barcode];
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =[info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    [reader dismissViewControllerAnimated:YES completion:nil];
//    [reader dismissModalViewControllerAnimated: YES];
    NSString * barcode=symbol.data;
    if([[symbol.data substringToIndex:1]isEqualToString:@"0"]){
        barcode = [symbol.data substringFromIndex:1];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self processBarcode:barcode];
}

-(void)processBarcode:(NSString *)barcode{

    [self.UpcTextField endEditing:YES];

    self.UpcTextField.text=@"";
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"MULTIPLE_UPC_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    NSMutableDictionary * resultDic = [[[BarcodeProcessor alloc] init] processBarcodeFor4DigitPLU:barcode];


    if([resultDic valueForKey:@"errorMsg"]!=NULL){
        NSLog(@"Result returned error..");
        [self showErrorAlert:[resultDic valueForKey:@"errorMsg"]];
    }else{
        NSArray * products = [resultDic valueForKey:@"productArray"];
        if(products.count==0){
            //no product found

            NSLog(@"product not found..");
            [self showErrorAlert:@"Product Not Found..."];

        }else if(products.count>1){
            //multiple products from other vendors

            NSMutableArray * listedProductArray = [[NSMutableArray alloc]init];

            for(NSObject * product in products){
                NSMutableDictionary * listedProduct = [[NSMutableDictionary alloc]init];
                [listedProduct setValue:[product valueForKey:@"ProductName"] forKey:@"ProductName"];
                [listedProduct setValue:[product valueForKey:@"ProductId"] forKey:@"ProductId"];

                [listedProductArray addObject:listedProduct];
            }

            [[NSUserDefaults standardUserDefaults] setObject:listedProductArray forKey:@"MULTIPLE_UPC_PRODUCTS"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            [self performSegueWithIdentifier:@"multipleUpcFromShelfTagPrintSegue" sender:self];

        }else if(products.count==1){
            Products * product = [[Products alloc]init];
            product = [products objectAtIndex:0];

            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ProductId == %@", product.ProductId];
            NSArray *filteredArray = [self.SelectedProducts filteredArrayUsingPredicate:predicate];
            if([filteredArray count]>0) [self showErrorAlert:@"Product already selected"];
            else{
                self.scannedProduct = product;
                [self.SelectedProducts insertObject:self.scannedProduct atIndex:0];
                [self.ProductListTable reloadData];
            }
        }
    }
}



#pragma mark - Scanner Code

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            NSLog(@"Scanner disconnected...");
            break;
        case CONN_CONNECTING:
            NSLog(@"Scanner connecting...");
            break;
        case CONN_CONNECTED:
            NSLog(@"Scanner connected...");
            break;
    }
}

-(void)checkScannerAvailabilityRP{
    if([self.scanner connstate]==1){
        [self.scanButton setTitle:@"" forState:UIControlStateNormal];
        [self.scanButton setBackgroundColor:nil];
        [self.scanButton setImage:[UIImage imageNamed:@"iPhoneCamera.png"] forState:UIControlStateNormal];
        self.scannerAvailable=FALSE;
        //        [self showInfoAlert:@"Scanner Not Available" body:@"Please use Camera for Scanning Barcodes"];
    }else{
        self.scannerAvailable=TRUE;
    }
}

-(void)stopScanner
{
    if(!self.foundBarcode){
        [self.scanner barcodeStopScan:nil];
        self.foundBarcode=NO;
    }
}


#pragma mark - finalize


-(void)saveDocument{
    self.SelectedBatch.BatchProducts = [[NSMutableArray alloc]init];
    for(Products *product in self.SelectedProducts){
        [self.SelectedBatch.BatchProducts addObject:product.ProductId];
    }
    
    NSError *error;
    NSManagedObject *newManagedObject = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"DocStatusInfo"
                                         inManagedObjectContext:context];
    
    [newManagedObject setValue:@"SHELF_TAG_BATCH_CREATE" forKey:@"documentType"];
    [newManagedObject setValue:@"Finished" forKey:@"status"];
    [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"userName"];
    [newManagedObject setValue:self.documentId forKey:@"documentId"];
    [newManagedObject setValue:self.endTime forKey:@"date"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"docUploaded"];
    [context save:&error];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *stringFromDate = [formatter stringFromDate:self.endTime];
    
    ShelfTagBatchData * auditData = [[ShelfTagBatchData alloc]init];
    auditData.UserId= [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];
    auditData.LocationId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OFFICE"];
    auditData.StartTime = self.startTime;
    auditData.ShelfTagBatch = self.SelectedBatch;
    
    [self writeStringToFile:[auditData toJSONString] fileName:stringFromDate];
    NSLog(@"File Saved!!!");

}

#pragma mark - iphone file system handler

- (void)writeStringToFile:(NSString*)jsonData fileName:(NSString *)fileName{
    NSError * error;
    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* fileAtPath = [dataPath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    [[jsonData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

#pragma mark - Show alert code

-(void)showErrorAlert:(NSString *)message{
    self.shouldScanProduct=FALSE;
    UIAlertController *errorAlertController = [UIAlertController
                                               alertControllerWithTitle:NSLocalizedString(@"Request Error",@"")
                                               message:[NSString stringWithFormat:@"%@", message]
                                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.shouldScanProduct=TRUE;
                               }];

    [errorAlertController addAction:okaction];
    [self presentViewController:errorAlertController animated:YES completion:nil];
}

-(void)showInfoAlert:(NSString *)title body:(NSString *)body{
    self.shouldScanProduct=FALSE;
    UIAlertController *infoAlertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(title,@"")
                                              message:[NSString stringWithFormat:@"%@", body]
                                              preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okaction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   self.shouldScanProduct=TRUE;
                               }];

    [infoAlertController addAction:okaction];
    [self presentViewController:infoAlertController animated:YES completion:nil];
}

#pragma mark - button actions


- (IBAction)scanAction:(id)sender {
    if(self.scannerAvailable){
        self.UpcTextField.text = @"";
        [self.scanner barcodeStartScan:nil];
        self.foundBarcode = NO;

        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanner) userInfo:nil repeats:NO];
    }else{
        ZBarReaderViewController *reader = [ZBarReaderViewController new];
        reader.readerDelegate = self;
        [reader.scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
        reader.readerView.zoom = 1.0;
        [self presentViewController:reader animated:YES completion:Nil];
    }
}

- (IBAction)backAction:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:@"Quit batch creation and go back?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Yes"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popToRootViewControllerAnimated:YES];
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"No"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];

    [alert addAction:ok];
    [alert addAction:cancel];

    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)submitAction:(id)sender {
    if(self.SelectedProducts.count == 0){
        [self showErrorAlert:@"No product selected for the batch!"];
    }
    else{
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Confirmation!"
                                      message:@"Are you sure you want to submit this batch?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 self.endTime = [NSDate date];
                                 [self saveDocument];
                                 [self.navigationController popToRootViewControllerAnimated:YES];

                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];

                                 }];

        [alert addAction:ok];
        [alert addAction:cancel];

        [self presentViewController:alert animated:YES completion:nil];
    }
}
@end
