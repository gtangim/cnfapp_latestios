//
//  StoreSpecificInfo.h
//  CNF App
//
//  Created by Anik on 2015-03-03.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "Locations.h"

@protocol StoreSpecificInfo @end

@interface StoreSpecificInfo : JSONModel

@property (nonatomic, retain) Locations * Location;


//change these to decimals

@property (nonatomic,retain) NSNumber* ListPrice;
@property (nonatomic,retain) NSNumber* ActivePrice;

@property (nonatomic, retain) NSNumber * Qoh;
@property (nonatomic, retain) NSNumber * VendorShortShipped;
@property (nonatomic, retain) NSNumber * VendorShortageCount;
@property (nonatomic, retain) NSNumber * WarehouseShortShipped;
@property (nonatomic, retain) NSNumber * OnOrder;
@property (nonatomic, retain) NSNumber * ShelfCapacity;
@property (nonatomic, retain) NSNumber * Noq;
@property (nonatomic, retain) NSNumber * InTransit;

//new additions

@property (nonatomic, retain) NSNumber * WarehouseSeqNumber;
@property (nonatomic, retain) NSString<Optional> * TriggerType;
@property (nonatomic, retain) NSNumber * TriggerInterval;
@property (nonatomic, retain) NSNumber * TriggerDayMask;
@property (nonatomic, retain) NSString<Optional> * TriggerDescription;
@property (nonatomic, retain) NSNumber * OrderFullCases;
@property (nonatomic, retain) NSNumber * VelocityPreviousDay;
@property (nonatomic, retain) NSNumber * VelocitySevenDays;
@property (nonatomic, retain) NSNumber * VelocityThirtyDays;
@property (nonatomic, retain) NSNumber * VelocityNinetyDays;


@end
