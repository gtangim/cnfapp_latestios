//
//  SubmitDataElement.h
//  CNF App
//
//  Created by Anik on 2015-05-12.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "OutOfStockAuditData.h"

@interface SubmitDataElement : JSONModel

@property (strong, nonatomic) NSArray<OutOfStockAuditData>* rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;

@end
