//
//  Supplier.h
//  CNF App
//
//  Created by Anik on 2015-03-03.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "Vendors.h"

@interface Supplier : JSONModel

@property (nonatomic, retain) NSString<Optional> * WarehouseId;
@property (nonatomic, retain) Vendors<Optional> * DefaultVendor;


@end
