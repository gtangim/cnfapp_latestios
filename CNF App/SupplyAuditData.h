//
//  SupplyAuditData.h
//  CNF App
//
//  Created by Anik on 2015-12-30.
//  Copyright © 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "SupplyAuditElement.h"

@protocol SupplyAuditData @end

@interface SupplyAuditData : JSONModel

@property (strong, nonatomic) NSString * UserId;
@property (strong, nonatomic) NSString * LocationId;
@property (strong, nonatomic) NSDate * StartTime;
@property (strong, nonatomic) NSDate * EndTime;

@property (strong, nonatomic) NSArray<SupplyAuditElement>* AuditList;

@end
