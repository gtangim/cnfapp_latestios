//
//  SupplyAuditElement.h
//  CNF App
//
//  Created by Anik on 2015-12-30.
//  Copyright © 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol SupplyAuditElement @end

@interface SupplyAuditElement : JSONModel

@property (nonatomic, retain) NSString * ProductId;
@property (nonatomic, retain) NSNumber * AuditQuantity;

@end
