//
//  SupplySubmitDataElement.h
//  CNF App
//
//  Created by Anik on 2015-12-30.
//  Copyright © 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "SupplyAuditData.h"

@interface SupplySubmitDataElement : JSONModel

@property (strong, nonatomic) NSArray<SupplyAuditData>* rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;

@end
