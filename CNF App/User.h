//
//  User.h
//  CNF App
//
//  Created by Anik on 2015-04-06.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface User : JSONModel

@property (nonatomic, retain) NSString * UserName;
@property (nonatomic, retain) NSString * Password;
@property (nonatomic, retain) NSString * DeviceId;

@end