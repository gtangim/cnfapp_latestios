//
//  UserElement.h
//  CNF App
//
//  Created by Anik on 2015-03-25.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "User.h"

@interface UserElement : JSONModel

@property (nonatomic, retain) User * user;

@end
