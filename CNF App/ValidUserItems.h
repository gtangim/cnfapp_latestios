//
//  ValidUserItems.h
//  CNF App
//
//  Created by Anik on 2015-03-31.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "AuthenticationToken.h"


@interface ValidUserItems : JSONModel

@property (nonatomic, retain) NSString<Optional> * Office;
@property (nonatomic, retain) AuthenticationToken * userToken;
@property (nonatomic, retain) NSArray * userGroups;
@property (nonatomic, retain) NSString * DeviceShortId;

@end
