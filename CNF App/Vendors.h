//
//  Vendors.h
//  CNF App
//
//  Created by Anik on 2015-03-03.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol Vendors @end

@interface Vendors : JSONModel

@property (nonatomic, retain) NSString * VendorId;
@property (nonatomic, retain) NSString * VendorName;
@property (nonatomic, retain) NSNumber * IsActive;

@end
