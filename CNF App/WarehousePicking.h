//
//  WarehousePicking.h
//  CNF App Test
//
//  Created by Anik on 2017-05-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "WarehousePickingItem.h"

@protocol WarehousePicking @end

@interface WarehousePicking : JSONModel

@property (nonatomic, retain) NSString * TransferNumber;
@property (nonatomic, retain) NSString * WarehouseLocationId;
@property (nonatomic, retain) NSString * ReceivingLocationId;
@property (nonatomic, retain) NSString<Optional> * DepartmentId;
@property (nonatomic, retain) NSDate * PickForDate;
@property (nonatomic, retain) NSDate * SubmissionDateTime;
@property (nonatomic, retain) NSString * SubmittedByUserId;

@property (strong, nonatomic) NSArray<WarehousePickingItem> * Items;

@end
