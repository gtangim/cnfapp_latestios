//
//  WarehousePickingItem.h
//  CNF App Test
//
//  Created by Anik on 2017-05-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "QuantityDetails.h"

@protocol WarehousePickingItem @end

@interface WarehousePickingItem : JSONModel

@property (nonatomic, retain) NSString * ProductId;
@property (nonatomic, retain) NSString * DepartmentId;
@property (nonatomic, retain) QuantityDetails * Amount;

@end
