//
//  WarehousePickingSubmitRequest.h
//  CNF App Test
//
//  Created by Anik on 2017-05-25.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "WarehousePicking.h"

@interface WarehousePickingSubmitRequest : JSONModel

@property (strong, nonatomic) NSArray<WarehousePicking>* rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;

@end
