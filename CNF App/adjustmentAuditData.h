//
//  adjustmentAuditData.h
//  CNF App
//
//  Created by Anik on 2017-01-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "adjustmentAuditElement.h"

@protocol adjustmentAuditData @end

@interface adjustmentAuditData : JSONModel

@property (strong, nonatomic) NSString * UserId;
@property (strong, nonatomic) NSString * LocationId;
@property (strong, nonatomic) NSDate * StartTime;
@property (strong, nonatomic) NSDate * EndTime;

@property (strong, nonatomic) NSArray<adjustmentAuditElement>* AuditList;

@end
