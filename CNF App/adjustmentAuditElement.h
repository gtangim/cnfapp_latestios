//
//  adjustmentAuditElement.h
//  CNF App
//
//  Created by Anik on 2017-01-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol adjustmentAuditElement @end

@interface adjustmentAuditElement : JSONModel

@property (nonatomic, retain) NSString * ProductId;
@property (nonatomic, retain) NSNumber * AuditQuantity;
@property (nonatomic, retain) NSString * AdjustmentTypeId;
@property (nonatomic, retain) NSString<Optional> * ConsumingLocationId;

@end
