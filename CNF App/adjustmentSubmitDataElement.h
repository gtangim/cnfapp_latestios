//
//  adjustmentSubmitDataElement.h
//  CNF App
//
//  Created by Anik on 2017-01-18.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "BaseAdjustmentRequest.h"

@interface adjustmentSubmitDataElement : JSONModel

@property (strong, nonatomic) NSArray<BaseAdjustmentRequest> * rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;


@end
