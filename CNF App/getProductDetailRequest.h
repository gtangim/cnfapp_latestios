//
//  getProductDetailRequest.h
//  CNF App Test
//
//  Created by Anik on 2017-06-06.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface getProductDetailRequest : JSONModel
@property (nonatomic, retain) NSString * productId;
@end
