//
//  main.m
//  CNF App
//
//  Created by Anik on 2015-03-03.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CNFAppDelegate class]));
    }
}
