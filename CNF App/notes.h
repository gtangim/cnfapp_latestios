//
//  notes.h
//  CNF App
//
//  Created by Anik on 2016-04-26.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol notes @end

@interface notes : JSONModel

@property (nonatomic, retain) NSString * note;

@end
