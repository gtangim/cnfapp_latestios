//
//  receiptReceiveAuditElement.h
//  CNF App
//
//  Created by Anik on 2016-03-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "receiptReceiveAssociatedProducts.h"
#import "notes.h"

@interface receiptReceiveAuditElement : JSONModel

@property (nonatomic, retain) NSString<Optional> * ReceiptId;
@property (nonatomic, retain) NSString<Optional> * ReceiptNumber;
@property (nonatomic, retain) NSString * VendorId;
@property (nonatomic, retain) NSNumber<Optional> * VosId;
@property (nonatomic, retain) NSString<Optional> * VosNumber;

@property (nonatomic, retain) NSNumber<Optional> * Shipping;
@property (nonatomic, retain) NSNumber<Optional> * Duty;
@property (nonatomic, retain) NSNumber<Optional> * Tax;
@property (nonatomic, retain) NSNumber<Optional> * Discount;
@property (nonatomic, retain) NSNumber<Optional> * Deposit;
@property (nonatomic, retain) NSNumber<Optional> * Fees;
@property (nonatomic, retain) NSNumber<Optional> * TotalCharges;
@property (nonatomic, retain) NSNumber<Optional> * GrandTotal;
@property (nonatomic, retain) NSNumber<Optional> * CalculatedGrandTotal;

@property (strong, nonatomic) NSArray<receiptReceiveAssociatedProducts>* ProductsList;

@property (strong, nonatomic) NSNumber * AttachmentCount;
@property (strong, nonatomic) NSNumber * SendToCM;
@property (strong, nonatomic) NSArray<Optional>* Note;

@end
