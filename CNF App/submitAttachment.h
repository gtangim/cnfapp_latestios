//
//  submitAttachment.h
//  CNF App
//
//  Created by Anik on 2016-04-14.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface submitAttachment : JSONModel

@property (nonatomic, retain) NSString * fileContent;
@property (nonatomic, retain) NSString * docId;
@property (nonatomic, retain) NSString * extension;
@property (nonatomic, retain) NSString * attachmentCounter;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * deviceId;
@property (nonatomic, retain) NSString * token;

@end
