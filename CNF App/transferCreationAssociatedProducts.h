//
//  transferCreationAssociatedProducts.h
//  CNF App
//
//  Created by Anik on 2016-03-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol transferCreationAssociatedProducts @end

@interface transferCreationAssociatedProducts : JSONModel

@property (nonatomic, retain) NSString * ProductId;
@property (nonatomic, retain) NSNumber * Quantity;

@end
