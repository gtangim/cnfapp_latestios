//
//  transferCreationAuditElement.h
//  CNF App
//
//  Created by Anik on 2016-03-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "transferCreationAssociatedProducts.h"

@interface transferCreationAuditElement : JSONModel

@property (nonatomic, retain) NSString * TransferNumber;
@property (nonatomic, retain) NSString * locationId;
@property (nonatomic, retain) NSString<Optional> * Note;

@property (strong, nonatomic) NSArray<Optional,transferCreationAssociatedProducts>  * ProductsList;

@end
