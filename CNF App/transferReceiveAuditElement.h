//
//  transferReceiveAuditElement.h
//  CNF App
//
//  Created by Anik on 2016-03-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "transferReceiveAssociatedProducts.h"

@interface transferReceiveAuditElement : JSONModel

@property (nonatomic, retain) NSString<Optional> * TransferNumber;
@property (nonatomic, retain) NSString<Optional> * TransferId;
@property (nonatomic, retain) NSString<Optional> * locationId;
@property (nonatomic, retain) NSString<Optional> * Note;

@property (strong, nonatomic) NSArray<Optional,transferReceiveAssociatedProducts>  * ProductsList;

@end
