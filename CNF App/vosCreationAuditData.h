//
//  vosCreationAuditData.h
//  CNF App
//
//  Created by Anik on 2016-03-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "vosCreationAuditElement.h"

@protocol vosCreationAuditData @end

@interface vosCreationAuditData : JSONModel

@property (strong, nonatomic) NSString * UserId;
@property (strong, nonatomic) NSString * LocationId;
@property (strong, nonatomic) NSString * ReceivingType;

@property (strong, nonatomic) vosCreationAuditElement * AuditItem;

@end
