//
//  vosGetRequest.h
//  CNF App Test
//
//  Created by Anik on 2017-06-06.
//  Copyright © 2017 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@interface vosGetRequest : JSONModel
@property (nonatomic, retain) NSString * locationId;
@end
