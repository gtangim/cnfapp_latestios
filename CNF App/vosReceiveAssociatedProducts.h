//
//  vosReceiveAssociatedProducts.h
//  CNF App
//
//  Created by Anik on 2016-03-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"

@protocol vosReceiveAssociatedProducts @end

@interface vosReceiveAssociatedProducts : JSONModel

@property (nonatomic, retain) NSString * ProductId;
@property (nonatomic, retain) NSNumber<Optional> * QuantityText;
@property (nonatomic, retain) NSNumber<Optional> * NumCases;
@end
