//
//  vosReceiveAuditData.h
//  CNF App
//
//  Created by Anik on 2016-03-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "vosReceiveAuditElement.h"

@protocol vosReceiveAuditData @end

@interface vosReceiveAuditData : JSONModel

@property (strong, nonatomic) NSString * UserId;
@property (strong, nonatomic) NSString * LocationId;
@property (strong, nonatomic) NSString * ReceivingType;

@property (strong, nonatomic) vosReceiveAuditElement * AuditItem;


@end
