//
//  vosReceiveSubmitDataElement.h
//  CNF App
//
//  Created by Anik on 2016-03-28.
//  Copyright © 2016 CNFIT. All rights reserved.
//

#import "JSONModel.h"
#import "vosReceiveAuditData.h"

@interface vosReceiveSubmitDataElement : JSONModel

@property (strong, nonatomic) NSArray<vosReceiveAuditData>* rcvdItems;
@property (strong, nonatomic) NSString * docType;
@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * deviceId;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSString * docId;

@end
