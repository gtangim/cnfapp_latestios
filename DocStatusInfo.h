//
//  DocStatusInfo.h
//  CNF App
//
//  Created by Anik on 2015-06-11.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DocStatusInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * documentId;
@property (nonatomic, retain) NSString * documentType;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSNumber * version;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * des;

@end
