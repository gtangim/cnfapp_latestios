//
//  DocStatusInfo.m
//  CNF App
//
//  Created by Anik on 2015-06-11.
//  Copyright (c) 2015 CNFIT. All rights reserved.
//

#import "DocStatusInfo.h"


@implementation DocStatusInfo

@dynamic documentId;
@dynamic documentType;
@dynamic status;
@dynamic userName;
@dynamic version;
@dynamic date;
@dynamic des;

@end
